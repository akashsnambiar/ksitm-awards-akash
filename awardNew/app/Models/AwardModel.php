<?php

namespace App\Models;

use CodeIgniter\Model;

class AwardModel extends Model
{
    protected $table            = 'awards';
    protected $primaryKey       = 'id';
    protected $allowedFields    = ['id', 'userID', 'keralashri', 'keralaprabha', 'keralajhothi', 'year'];
}
