<?php

namespace App\Models;

use CodeIgniter\Model;

class AwardareasubModel extends Model
{
    protected $table            = 'awardareasub';
    protected $primaryKey = 'awardAreaSubID';
    protected $allowedFields    = ['awardAreaSubID', 'awardAreaID', 'awardAreaSubTitle', 'status'];
}
