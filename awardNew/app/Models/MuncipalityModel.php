<?php

namespace App\Models;

use CodeIgniter\Model;

class MuncipalityModel extends Model
{
    protected $table            = 'muncipality';
    protected $allowedFields    = ['id', 'disID', 'muncipality'];
}
