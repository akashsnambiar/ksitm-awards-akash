<?php

namespace App\Models;

use CodeIgniter\Model;

class NominationPeriodModel extends Model
{
    protected $table            = 'nomination_period';
    protected $primaryKey = 'id';
    protected $allowedFields    = ['id', 'user_name', 'start_date', 'end_date', 'year','news_feed_mal','news_feed_eng', 'created_time', 'updated_time', 'ip', 'status'
    ];
}
