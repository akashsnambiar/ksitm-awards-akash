<?php

namespace App\Models;

use CodeIgniter\Model;

class BannerModel extends Model
{
    protected $table            = 'banner';
    protected $allowedFields    = ['id','banner_title_en','banner_title_mal','category_id','category_image','year','created_time','updated_time','status'];

}
