<?php

namespace App\Models;

use CodeIgniter\Model;

class RegisterModel extends Model
{
    protected $table            = 'registration';
    protected $allowedFields    = ['id', 'regType', 'nominaterType', 'organisationType', 'localbody',
                                    'localbodyDistrict', 'blockpanchayath', 'gramapanchayath', 'muncipality',
                                    'organisationDept', 'fName', 'mName', 'lName', 'organisationName',
                                    'concernedPerson', 'designation', 'otherContactDetail', 'mobileCode',
                                    'mobileNumber', 'email', 'state', 'district', 'inputDoc', 'inputDocNum',
                                    'docFile', 'username', 'password', 'refCode', 'ip', 'date', 'time', 'status'];

    
}
