<?php

namespace App\Models;

use CodeIgniter\Model;

class EsignModel extends Model
{
    protected $table            = 'esign';
    protected $primaryKey       = 'id';
    protected $allowedFields    = ['id', 'nominationID', 'name', 'country', 'state', 'personal', 'postal',
                                    'pseudonym', 'title', 'dnQualifier', 'hash', 'issuerConuntry', 'issuerState', 'issuerlocal',
                                    'issuerorg', 'issuerOU', 'issuerCN', 'version', 'serialNumber', 'serialNumberHex', 
                                    'validFrom', 'validTo', 'validFrom_time_t', 'validTo_time_t', 'signatureTypeSN',
                                    'signatureTypeLN', 'signatureTypeNID', 'basicConstraints', 'subjectKeyIdentifier', 'authorityKeyI'];

   
}
