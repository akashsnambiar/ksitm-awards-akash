<?php

namespace App\Models;

use CodeIgniter\Model;

class OrganisationsubModel extends Model
{
    protected $table            = 'organisation_sub';
    protected $primaryKey = 'orgSubID';
    protected $allowedFields    = ['orgSubID', 'orgID', 'orgSub', 'status'];

   
}
