<?php

namespace App\Models;

use CodeIgniter\Model;

class GalleryCategoryModel extends Model
{
    protected $table            = 'gallery_category';
    protected $allowedFields    = [ 'id',
                                    'title',
                                    'title_e',
                                    'year',
                                    'status'];

}
