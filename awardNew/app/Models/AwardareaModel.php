<?php

namespace App\Models;

use CodeIgniter\Model;

class AwardareaModel extends Model
{
    protected $table            = 'awardarea';
    protected $primaryKey = 'awardID';
    protected $allowedFields    = ['awardID', 'title','title_e', 'status'];
}
