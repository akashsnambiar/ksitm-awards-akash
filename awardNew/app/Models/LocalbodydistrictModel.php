<?php

namespace App\Models;

use CodeIgniter\Model;

class LocalbodydistrictModel extends Model
{
    protected $table            = 'localbodydistrict';
    protected $allowedFields    = ['id','localID','district'];

    
}
