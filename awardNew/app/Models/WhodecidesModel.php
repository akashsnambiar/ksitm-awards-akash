<?php

namespace App\Models;

use CodeIgniter\Model;

class WhodecidesModel extends Model
{
    protected $table            = 'whodecides';
    protected $allowedFields    = ['id','whodecides','whodecides_e','document','status'];

}
