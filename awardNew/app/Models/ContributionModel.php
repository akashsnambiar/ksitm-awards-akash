<?php

namespace App\Models;

use CodeIgniter\Model;

class ContributionModel extends Model
{
    protected $table            = 'contribution';
    protected $allowedFields    = ['id','title','title_e'];
}
