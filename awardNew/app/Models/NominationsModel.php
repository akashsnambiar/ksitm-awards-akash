<?php 
namespace App\Models;  
use CodeIgniter\Model;
  
class NominationsModel extends Model{
    protected $table = 'nominations';
    protected $primaryKey = 'nominationID';
    protected $allowedFields = [
        'nominationID',
        'nominationRegID',
        'userID',
        'name',
        'dob',
        'age',
        'sex',
        'state',
        'district',
        'taluk',
        'pob',
        'address',
        'email',
        'country',
        'job',
        'photo',
        'workArea',
        'workAreaSub',
        'award',
        'pyn',
        'pAward',
        'pYear',
        'note',
        'donation',
        'achivement',
        'document',
        'wDonation',
        'sDonation',
        'signature',
        'date',
        'dateTime',
        'year',
        'status',
        'review',
        'countID'
    ];
}