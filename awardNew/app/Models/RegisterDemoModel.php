<?php

namespace App\Models;

use CodeIgniter\Model;

class RegisterDemoModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'registrationdemo';
    protected $allowedFields    = ['id', 'regType', 'nominaterType', 'organisationType', 'localbody',
    'localbodyDistrict', 'blockpanchayath', 'gramapanchayath', 'muncipality',
    'organisationDept', 'fName', 'mName', 'lName', 'organisationName',
    'concernedPerson', 'designation', 'otherContactDetail', 'mobileCode',
    'mobileNumber', 'email', 'state', 'district', 'inputDoc', 'inputDocNum',
    'docFile', 'username', 'password', 'refCode','pp', 'ip', 'date', 'time', 'status'];

}
