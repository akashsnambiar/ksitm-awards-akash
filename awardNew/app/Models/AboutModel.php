<?php

namespace App\Models;

use CodeIgniter\Model;

class AboutModel extends Model
{
    protected $table            = 'about_us';
    protected $allowedFields    = ['id','title','text','title_e','text_e'];

   
}
