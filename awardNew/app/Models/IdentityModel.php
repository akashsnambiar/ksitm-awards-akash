<?php

namespace App\Models;

use CodeIgniter\Model;

class IdentityModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'identity_document';
    protected $allowedFields    = ['identityID', 'identityDocument', 'status'];

    
}
