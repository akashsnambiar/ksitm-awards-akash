<?php

namespace App\Models;

use CodeIgniter\Model;

class StateModel extends Model
{
    protected $table            = 'state';
    protected $primaryKey = 'stateID';
    protected $allowedFields    = ['stateID','state','status'];
}
