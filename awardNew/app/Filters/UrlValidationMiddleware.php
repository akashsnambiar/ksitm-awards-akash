<?php 
namespace App\Filters;

use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;

class UrlValidationMiddleware implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
       // Get the requested URL
       $url = $request->uri->getPath();

       // Check if the URL contains any malformed UTF-8 characters
       if (!$this->isValidUTF8($url)) {
           // Malformed UTF-8 character found
           // Log the error or take appropriate action
           log_message('error', 'Malformed UTF-8 character in URL: ' . $url);

           // Optionally, you can return a response to the client
          // return service('response')->setStatusCode(404);
          return redirect()->to(base_url('index.html'));
           
       }
       

        return $request;
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Optionally, perform any post-processing tasks here
        return $response;
    }
    private function isValidUTF8($url)
    {
        // Check if the URL is valid UTF-8
        if (!preg_match('//u', $url)) {
            return false;
        }

        // Check for suspicious encoded sequences
        if (preg_match('/%[cC][0O]%[aA][eE]/', $url)) {
            return false;
        }

        // Additional checks if needed...

        return true;
    }
}
