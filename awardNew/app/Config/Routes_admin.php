<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override( function(){
    return view("index.html");
});
$routes->setAutoRoute(true);
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->match(['get', 'post'], 'nomination', 'Nomination::index');
$routes->match(['get', 'post'], 'nomination2', 'Nomination::step2');
$routes->match(['get', 'post'], 'nomination3', 'Nomination::step3');
$routes->match(['get', 'post'], 'nomination4', 'Nomination::step4');
$routes->match(['get', 'post'], 'nomination5', 'Nomination::step5');

$routes->group('admin', function ($routes){
    $routes->add('/', 'Admin\Login::index');
    $routes->match(['get', 'post'], 'login', 'Admin\Login::loginAuth');
    $routes->match(['get', 'post'], 'logout', 'Admin\Login::logout'); 
    $routes->match(['get', 'post'], 'dashboard', 'Admin\Dashboard::index',['filter' => 'authAdmin']);

    $routes->match(['get', 'post'], 'settings', 'Admin\SettingsController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'nominationperiod', 'Admin\NominationPeriodController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'nominationstatus/(:num)/(:any)', 'Admin\NominationPeriodController::nominationstatus/$1/$2',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'nomperioddelete/(:num)', 'Admin\NominationPeriodController::delete/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'nomPeriodedit/(:any)', 'Admin\NominationPeriodController::nomPeriodedit/$1',['filter' => 'authAdmin']);

    $routes->match(['get', 'post'], 'banner', 'Admin\BannerController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'add_banner', 'Admin\BannerController::addbanner',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'banedit/(:any)', 'Admin\BannerController::banneredit/$1',['filter' => 'authAdmin']);
    $routes->get('bannerstatus/(:any)/(:any)', 'Admin\BannerController::status/$1/$2',['filter' => 'authAdmin']);

    $routes->match(['get', 'post'], 'winners', 'Admin\WinnerController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'add_winners', 'Admin\WinnerController::addwinner',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'winedit/(:any)', 'Admin\WinnerController::winneredit/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'bannercontent', 'Admin\WinnerController::bannercontent',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'windelete/(:any)', 'Admin\WinnerController::windelete/$1',['filter' => 'authAdmin']);

    $routes->match(['get', 'post'], 'homecontent', 'Admin\HomeContentController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'aboutus', 'Admin\AboutUsController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'site/keralashri', 'Admin\SiteManagementController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'site/keralaJyothi', 'Admin\SiteManagementController::keralajyothi',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'site/keralaPrabha', 'Admin\SiteManagementController::keralaprabha',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'galleryCategory', 'Admin\GalleryController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'gallery/add', 'Admin\GalleryController::add',['filter' => 'authAdmin']);
    $routes->get('gallerystatus/(:num)/(:any)', 'Admin\GalleryController::status/$1/$2',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'galleryedit/(:any)', 'Admin\GalleryController::edit/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'gallerydelete/(:num)', 'Admin\GalleryController::delete/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'galleryList', 'Admin\GalleryController::gallery',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'gallery/addimage', 'Admin\GalleryController::addimage',['filter' => 'authAdmin']);
    $routes->get('imagestatus/(:num)/(:any)', 'Admin\GalleryController::imagestatus/$1/$2',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'galleryimgedit/(:any)', 'Admin\GalleryController::galleryimgedit/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'imagedelete/(:num)', 'Admin\GalleryController::imagedelete/$1',['filter' => 'authAdmin']);

    $routes->get('notification', 'Admin\NotificationsController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'notification/add', 'Admin\NotificationsController::notifiadd',['filter' => 'authAdmin']);
    $routes->get('notifistatus/(:num)/(:any)', 'Admin\NotificationsController::notifistatus/$1/$2',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'notifiedit/(:any)', 'Admin\NotificationsController::notifiedit/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'notifidelete/(:num)', 'Admin\NotificationsController::notifidelete/$1',['filter' => 'authAdmin']);

    $routes->get('whocanapply', 'Admin\WhoCanApplyController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'whocanapply/add', 'Admin\WhoCanApplyController::whocanapplyadd',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'whoapply/add', 'Admin\WhoCanApplyController::whoapplyadd',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'whocanedit/(:any)', 'Admin\WhoCanApplyController::whocanedit/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'whocandelete/(:num)', 'Admin\WhoCanApplyController::whocandelete/$1',['filter' => 'authAdmin']);

    $routes->get('contactus', 'Admin\ContactUSController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'contactus/add', 'Admin\ContactUSController::contactusadd',['filter' => 'authAdmin']);

    $routes->get('whodecides', 'Admin\WhoDecidesController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'whodecides/add', 'Admin\WhoDecidesController::whodecidesadd',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'whocandecide/add', 'Admin\WhoDecidesController::whocandecidesadd',['filter' => 'authAdmin']);
    $routes->get('whodecidesstatus/(:num)/(:any)', 'Admin\WhoDecidesController::whodecidesstatus/$1/$2',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'whodecide/edit/(:any)', 'Admin\WhoDecidesController::whoedit/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'whodecide/delete/(:num)', 'Admin\WhoDecidesController::whodelete/$1',['filter' => 'authAdmin']);

    $routes->get('media', 'Admin\MediaController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'media/add', 'Admin\MediaController::mediaadd',['filter' => 'authAdmin']);
    $routes->get('mediastatus/(:num)/(:any)', 'Admin\MediaController::mediastatus/$1/$2',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'mediaedit/(:any)', 'Admin\MediaController::mediaedit/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'mediadelete/(:num)', 'Admin\MediaController::mediadelete/$1',['filter' => 'authAdmin']);


    $routes->get('contribution', 'Admin\ContributionController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'contribution/add', 'Admin\ContributionController::contributionhomeadd',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'contributionadd', 'Admin\ContributionController::contributionadd',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'contributionedit/(:any)', 'Admin\ContributionController::contributionedit/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'contributiondelete/(:num)', 'Admin\ContributionController::contributiondelete/$1',['filter' => 'authAdmin']);

    $routes->get('individual', 'Admin\IndividualController::index',['filter' => 'authAdmin']);
    $routes->get('status/(:num)/(:any)', 'Admin\IndividualController::status/$1/$2',['filter' => 'authAdmin']);
    $routes->get('individual/view/(:any)', 'Admin\IndividualController::view/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'individual/year', 'Admin\IndividualController::yearsubmit',['filter' => 'authAdmin']);


    $routes->get('organisation', 'Admin\OrganisationController::index',['filter' => 'authAdmin']);
    $routes->get('organisationstatus/(:num)/(:any)', 'Admin\OrganisationController::status/$1/$2',['filter' => 'authAdmin']);
    $routes->get('organisation/view/(:any)', 'Admin\OrganisationController::view/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'organisation/year', 'Admin\OrganisationController::yearsubmit',['filter' => 'authAdmin']);

    $routes->get('keralaj/(:any)', 'Admin\KeralajyothiController::index/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'keralajyothistatus/(:num)/(:any)', 'Admin\KeralajyothiController::keralajyothistatus/$1/$2',['filter' => 'authAdmin']);
    $routes->get('keralajyothi/review/(:any)', 'Admin\KeralajyothiController::review/$1',['filter' => 'authAdmin']);
    $routes->get('keralajyothi/view/(:any)', 'Admin\KeralajyothiController::view/$1',['filter' => 'authAdmin']);
    $routes->post('reviewsave', 'Admin\KeralajyothiController::reviewsave',['filter' => 'authAdmin']);
    $routes->post('reviewSubmit', 'Admin\KeralajyothiController::reviewSubmit',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'keralajyothi/generatepdf/(:any)', 'Admin\KeralajyothiController::generatepdf/$1',['filter' => 'authAdmin']);
    $routes->get('keralajyothi/print/(:num)', 'Admin\KeralajyothiController::print/$1',['filter' => 'authAdmin']);
    $routes->get('keralajyothi/accepted/(:any)', 'Admin\KeralajyothiController::accepted/$1',['filter' => 'authAdmin']);
    $routes->get('reviewView/(:any)', 'Admin\KeralajyothiController::reviewView/$1',['filter' => 'authAdmin']);
    $routes->get('keralajyothi/rejected/(:any)', 'Admin\KeralajyothiController::rejected/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'keralajyothi/year/(:any)', 'Admin\KeralajyothiController::yearsubmit/$1',['filter' => 'authAdmin']);

    $routes->get('keralaprabhacat/(:any)', 'Admin\KeralaPrabhaController::index/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'keralaprabhastatus/(:num)/(:any)', 'Admin\KeralaPrabhaController::keralaprabhastatus/$1/$2',['filter' => 'authAdmin']);
    $routes->get('keralaprabha/review/(:any)', 'Admin\KeralaPrabhaController::review/$1',['filter' => 'authAdmin']);
    $routes->get('keralaprabha/view/(:any)', 'Admin\KeralaPrabhaController::view/$1',['filter' => 'authAdmin']);
    $routes->post('keralaprabha/reviewsave', 'Admin\KeralaPrabhaController::reviewsave',['filter' => 'authAdmin']);
    $routes->post('keralaprabha/reviewSubmit', 'Admin\KeralaPrabhaController::reviewSubmit',['filter' => 'authAdmin']);
    $routes->get('keralaprabha/print/(:num)', 'Admin\KeralaPrabhaController::print/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'keralaprabha/generatepdf/(:any)', 'Admin\KeralaPrabhaController::generatepdf/$1',['filter' => 'authAdmin']);
    $routes->get('keralaprabha/accepted/(:any)', 'Admin\KeralaPrabhaController::accepted/$1',['filter' => 'authAdmin']);
    $routes->get('reviewView/(:any)', 'Admin\KeralaPrabhaController::reviewView/$1',['filter' => 'authAdmin']);
    $routes->get('keralaprabha/rejected/(:any)', 'Admin\KeralaPrabhaController::rejected/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'keralaprabha/year/(:any)', 'Admin\KeralaPrabhaController::yearsubmit/$1',['filter' => 'authAdmin']);


    $routes->get('keralashri/(:any)', 'Admin\KeralaSreeController::index/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'keralasreestatus/(:num)/(:any)', 'Admin\KeralaSreeController::keralasreestatus/$1/$2',['filter' => 'authAdmin']);
    $routes->get('keralasree/review/(:any)', 'Admin\KeralaSreeController::review/$1',['filter' => 'authAdmin']);
    $routes->get('keralasree/view/(:any)', 'Admin\KeralaSreeController::view/$1',['filter' => 'authAdmin']);
    $routes->post('keralasree/reviewsave', 'Admin\KeralaSreeController::reviewsave',['filter' => 'authAdmin']);
    $routes->post('keralasree/reviewSubmit', 'Admin\KeralaSreeController::reviewsave',['filter' => 'authAdmin']);
    $routes->get('keralasree/print/(:num)', 'Admin\KeralaSreeController::print/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'keralasree/generatepdf/(:any)', 'Admin\KeralaSreeController::generatepdf/$1',['filter' => 'authAdmin']);
    $routes->get('keralasree/accepted/(:any)', 'Admin\KeralaSreeController::accepted/$1',['filter' => 'authAdmin']);
    $routes->get('reviewView/(:any)', 'Admin\KeralaSreeController::reviewView/$1',['filter' => 'authAdmin']);
    $routes->get('keralasree/rejected/(:any)', 'Admin\KeralaSreeController::rejected/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'keralasree/year/(:any)', 'Admin\KeralaSreeController::yearsubmit/$1',['filter' => 'authAdmin']);


});

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}