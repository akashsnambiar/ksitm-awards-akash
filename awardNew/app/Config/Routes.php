<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override( function(){
    return view("index.html");
});
$routes->setAutoRoute(true);
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->match(['get', 'post'], 'nomination', 'Nomination::index');
$routes->match(['get', 'post'], 'nomination2', 'Nomination::step2');
$routes->match(['get', 'post'], 'nomination3', 'Nomination::step3');
$routes->match(['get', 'post'], 'nomination4', 'Nomination::step4');
$routes->match(['get', 'post'], 'nomination5', 'Nomination::step5');

$routes->group('higherEnd', function ($routes){
    $routes->add('/', 'HigherEnd\Login::index');
    $routes->match(['get', 'post'], 'login', 'HigherEnd\Login::loginAuth');
    $routes->match(['get', 'post'], 'logout', 'HigherEnd\Login::logout'); 
    $routes->match(['get', 'post'], 'dashboard', 'HigherEnd\Dashboard::index',['filter' => 'authAdmin']);

    $routes->match(['get', 'post'], 'settings', 'HigherEnd\SettingsController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'nominationperiod', 'HigherEnd\NominationPeriodController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'nominationstatus/(:num)/(:alphanum)', 'HigherEnd\NominationPeriodController::nominationstatus/$1/$2',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'nomperioddelete/(:num)', 'HigherEnd\NominationPeriodController::delete/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'nomPeriodedit/(:alphanum)', 'HigherEnd\NominationPeriodController::nomPeriodedit/$1',['filter' => 'authAdmin']);

    $routes->match(['get', 'post'], 'banner', 'HigherEnd\BannerController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'add_banner', 'HigherEnd\BannerController::addbanner',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'banedit/(:alphanum)', 'HigherEnd\BannerController::banneredit/$1',['filter' => 'authAdmin']);
    $routes->get('bannerstatus/(:alphanum)/(:alphanum)', 'HigherEnd\BannerController::status/$1/$2',['filter' => 'authAdmin']);

    $routes->match(['get', 'post'], 'winners', 'HigherEnd\WinnerController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'add_winners', 'HigherEnd\WinnerController::addwinner',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'winedit/(:alphanum)', 'HigherEnd\WinnerController::winneredit/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'bannercontent', 'HigherEnd\WinnerController::bannercontent',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'windelete/(:alphanum)', 'HigherEnd\WinnerController::windelete/$1',['filter' => 'authAdmin']);

    $routes->match(['get', 'post'], 'homecontent', 'HigherEnd\HomeContentController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'aboutus', 'HigherEnd\AboutUsController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'site/keralashri', 'HigherEnd\SiteManagementController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'site/keralaJyothi', 'HigherEnd\SiteManagementController::keralajyothi',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'site/keralaPrabha', 'HigherEnd\SiteManagementController::keralaprabha',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'galleryCategory', 'HigherEnd\GalleryController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'gallery/add', 'HigherEnd\GalleryController::add',['filter' => 'authAdmin']);
    $routes->get('gallerystatus/(:num)/(:alphanum)', 'HigherEnd\GalleryController::status/$1/$2',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'galleryedit/(:alphanum)', 'HigherEnd\GalleryController::edit/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'gallerydelete/(:alphanum)', 'HigherEnd\GalleryController::delete/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'galleryList', 'HigherEnd\GalleryController::gallery',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'gallery/addimage', 'HigherEnd\GalleryController::addimage',['filter' => 'authAdmin']);
    $routes->get('imagestatus/(:num)/(:alphanum)', 'HigherEnd\GalleryController::imagestatus/$1/$2',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'galleryimgedit/(:alphanum)', 'HigherEnd\GalleryController::galleryimgedit/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'imagedelete/(:alphanum)', 'HigherEnd\GalleryController::imagedelete/$1',['filter' => 'authAdmin']);

    $routes->get('notification', 'HigherEnd\NotificationsController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'notification/add', 'HigherEnd\NotificationsController::notifiadd',['filter' => 'authAdmin']);
    $routes->get('notifistatus/(:num)/(:alphanum)', 'HigherEnd\NotificationsController::notifistatus/$1/$2',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'notifiedit/(:alphanum)', 'HigherEnd\NotificationsController::notifiedit/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'notifidelete/(:num)', 'HigherEnd\NotificationsController::notifidelete/$1',['filter' => 'authAdmin']);

    $routes->get('whocanapply', 'HigherEnd\WhoCanApplyController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'whocanapply/add', 'HigherEnd\WhoCanApplyController::whocanapplyadd',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'whoapply/add', 'HigherEnd\WhoCanApplyController::whoapplyadd',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'whocanedit/(:alphanum)', 'HigherEnd\WhoCanApplyController::whocanedit/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'whocandelete/(:num)', 'HigherEnd\WhoCanApplyController::whocandelete/$1',['filter' => 'authAdmin']);

    $routes->get('contactus', 'HigherEnd\ContactUSController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'contactus/add', 'HigherEnd\ContactUSController::contactusadd',['filter' => 'authAdmin']);

    $routes->get('whodecides', 'HigherEnd\WhoDecidesController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'whodecides/add', 'HigherEnd\WhoDecidesController::whodecidesadd',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'whocandecide/add', 'HigherEnd\WhoDecidesController::whocandecidesadd',['filter' => 'authAdmin']);
    $routes->get('whodecidesstatus/(:num)/(:alphanum)', 'HigherEnd\WhoDecidesController::whodecidesstatus/$1/$2',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'whodecide/edit/(:alphanum)', 'HigherEnd\WhoDecidesController::whoedit/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'whodecide/delete/(:num)', 'HigherEnd\WhoDecidesController::whodelete/$1',['filter' => 'authAdmin']);

    $routes->get('media', 'HigherEnd\MediaController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'media/add', 'HigherEnd\MediaController::mediaadd',['filter' => 'authAdmin']);
    $routes->get('mediastatus/(:num)/(:alphanum)', 'HigherEnd\MediaController::mediastatus/$1/$2',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'mediaedit/(:alphanum)', 'HigherEnd\MediaController::mediaedit/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'mediadelete/(:num)', 'HigherEnd\MediaController::mediadelete/$1',['filter' => 'authAdmin']);


    $routes->get('contribution', 'HigherEnd\ContributionController::index',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'contribution/add', 'HigherEnd\ContributionController::contributionhomeadd',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'contributionadd', 'HigherEnd\ContributionController::contributionadd',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'contributionedit/(:alphanum)', 'HigherEnd\ContributionController::contributionedit/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'contributiondelete/(:num)', 'HigherEnd\ContributionController::contributiondelete/$1',['filter' => 'authAdmin']);

    $routes->get('individual', 'HigherEnd\IndividualController::index',['filter' => 'authAdmin']);
    $routes->get('status/(:num)/(:alphanum)', 'HigherEnd\IndividualController::status/$1/$2',['filter' => 'authAdmin']);
    $routes->get('individual/view/(:alphanum)', 'HigherEnd\IndividualController::view/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'individual/year', 'HigherEnd\IndividualController::yearsubmit',['filter' => 'authAdmin']);


    $routes->get('organisation', 'HigherEnd\OrganisationController::index',['filter' => 'authAdmin']);
    $routes->get('organisationstatus/(:num)/(:alphanum)', 'HigherEnd\OrganisationController::status/$1/$2',['filter' => 'authAdmin']);
    $routes->get('organisation/view/(:alphanum)', 'HigherEnd\OrganisationController::view/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'organisation/year', 'HigherEnd\OrganisationController::yearsubmit',['filter' => 'authAdmin']);

    $routes->get('keralaj/(:alphanum)', 'HigherEnd\KeralajyothiController::index/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'keralajyothistatus/(:num)/(:alphanum)', 'HigherEnd\KeralajyothiController::keralajyothistatus/$1/$2',['filter' => 'authAdmin']);
    $routes->get('keralajyothi/review/(:alphanum)', 'HigherEnd\KeralajyothiController::review/$1',['filter' => 'authAdmin']);
    $routes->get('keralajyothi/view/(:alphanum)', 'HigherEnd\KeralajyothiController::view/$1',['filter' => 'authAdmin']);
    $routes->post('reviewsave', 'HigherEnd\KeralajyothiController::reviewsave',['filter' => 'authAdmin']);
    $routes->post('reviewSubmit', 'HigherEnd\KeralajyothiController::reviewSubmit',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'keralajyothi/generatepdf/(:alphanum)', 'HigherEnd\KeralajyothiController::generatepdf/$1',['filter' => 'authAdmin']);
    $routes->get('keralajyothi/print/(:num)', 'HigherEnd\KeralajyothiController::print/$1',['filter' => 'authAdmin']);
    $routes->get('keralajyothi/accepted/(:alphanum)', 'HigherEnd\KeralajyothiController::accepted/$1',['filter' => 'authAdmin']);
    $routes->get('reviewView/(:alphanum)', 'HigherEnd\KeralajyothiController::reviewView/$1',['filter' => 'authAdmin']);
    $routes->get('keralajyothi/rejected/(:alphanum)', 'HigherEnd\KeralajyothiController::rejected/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'keralajyothi/year/(:alphanum)', 'HigherEnd\KeralajyothiController::yearsubmit/$1',['filter' => 'authAdmin']);

    $routes->get('keralaprabhacat/(:alphanum)', 'HigherEnd\KeralaPrabhaController::index/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'keralaprabhastatus/(:num)/(:alphanum)', 'HigherEnd\KeralaPrabhaController::keralaprabhastatus/$1/$2',['filter' => 'authAdmin']);
    $routes->get('keralaprabha/review/(:alphanum)', 'HigherEnd\KeralaPrabhaController::review/$1',['filter' => 'authAdmin']);
    $routes->get('keralaprabha/view/(:alphanum)', 'HigherEnd\KeralaPrabhaController::view/$1',['filter' => 'authAdmin']);
    $routes->post('keralaprabha/reviewsave', 'HigherEnd\KeralaPrabhaController::reviewsave',['filter' => 'authAdmin']);
    $routes->post('keralaprabha/reviewSubmit', 'HigherEnd\KeralaPrabhaController::reviewSubmit',['filter' => 'authAdmin']);
    $routes->get('keralaprabha/print/(:num)', 'HigherEnd\KeralaPrabhaController::print/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'keralaprabha/generatepdf/(:alphanum)', 'HigherEnd\KeralaPrabhaController::generatepdf/$1',['filter' => 'authAdmin']);
    $routes->get('keralaprabha/accepted/(:alphanum)', 'HigherEnd\KeralaPrabhaController::accepted/$1',['filter' => 'authAdmin']);
    $routes->get('reviewView/(:alphanum)', 'HigherEnd\KeralaPrabhaController::reviewView/$1',['filter' => 'authAdmin']);
    $routes->get('keralaprabha/rejected/(:alphanum)', 'HigherEnd\KeralaPrabhaController::rejected/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'keralaprabha/year/(:alphanum)', 'HigherEnd\KeralaPrabhaController::yearsubmit/$1',['filter' => 'authAdmin']);


    $routes->get('keralashri/(:alphanum)', 'HigherEnd\KeralaSreeController::index/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'keralasreestatus/(:num)/(:alphanum)', 'HigherEnd\KeralaSreeController::keralasreestatus/$1/$2',['filter' => 'authAdmin']);
    $routes->get('keralasree/review/(:alphanum)', 'HigherEnd\KeralaSreeController::review/$1',['filter' => 'authAdmin']);
    $routes->get('keralasree/view/(:alphanum)', 'HigherEnd\KeralaSreeController::view/$1',['filter' => 'authAdmin']);
    $routes->post('keralasree/reviewsave', 'HigherEnd\KeralaSreeController::reviewsave',['filter' => 'authAdmin']);
    $routes->post('keralasree/reviewSubmit', 'HigherEnd\KeralaSreeController::reviewsave',['filter' => 'authAdmin']);
    $routes->get('keralasree/print/(:num)', 'HigherEnd\KeralaSreeController::print/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'keralasree/generatepdf/(:alphanum)', 'HigherEnd\KeralaSreeController::generatepdf/$1',['filter' => 'authAdmin']);
    $routes->get('keralasree/accepted/(:alphanum)', 'HigherEnd\KeralaSreeController::accepted/$1',['filter' => 'authAdmin']);
    $routes->get('reviewView/(:alphanum)', 'HigherEnd\KeralaSreeController::reviewView/$1',['filter' => 'authAdmin']);
    $routes->get('keralasree/rejected/(:alphanum)', 'HigherEnd\KeralaSreeController::rejected/$1',['filter' => 'authAdmin']);
    $routes->match(['get', 'post'], 'keralasree/year/(:alphanum)', 'HigherEnd\KeralaSreeController::yearsubmit/$1',['filter' => 'authAdmin']);


});

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}