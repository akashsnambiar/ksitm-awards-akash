<?php echo view('includes/header');?>
<?php echo view('includes/inner_page');?>
    <div class="clearfix"></div>
    <!-- About us section -->
    <section class="about-us">
        <div class="container">
            <div class="row">
                <div class="col-md-12 inner-top">
                <?php if(isset($keralajyothi)) {?>
                    <h3><?php echo $keralajyothi['title']; ?></h3>
                    <?php echo $keralajyothi['text']; ?>
                    <?php }?>
                    
                </div>
            </div>
        </div>
    </section>
<?php echo view('includes/help');?>	
<!-- footer -->
<?php echo view('includes/award');?>
<?php echo view('includes/footer');?>
<!-- /footer -->
</body>
</html>
