<!--------------- Footer --------------->
<footer>
    <div class="footer-bottom text-center">
        <p>&copy; <?php echo (isset($copyright_year) ? $copyright_year : '');  ?> Government of Kerala. Developed & Maintained by <a href="https://itmission.kerala.gov.in/" target="_blank" class="footer-label">Kerala State IT Mission (KSITM)</a>.</p>

    </div>
</footer>
<!--------------- /Footer --------------->

</body>

</html>