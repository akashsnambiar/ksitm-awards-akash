<body data-bs-spy="scroll" data-bs-target=".navbar" data-bs-offset="70">
    <!-- TOP NAV -->
    <div class="top-nav" id="home">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-auto">
                    <span class="logo-name">കേരള പുരസ്‌കാരങ്ങള്‍</span>

                </div>
                <div class="col-auto social-icons">

                    <p> <i class='bx bxs-envelope'></i> info@example.com</p>
                    <p> <i class='bx bxs-phone-call'></i> 0471-2518531</p>
                </div>
            </div>
        </div>
    </div>

    <!-- BOTTOM NAV -->
    <nav id="myheader" class="navbar navbar-expand-lg navbar-light bg-white sticky-top">
        <div class="container d-flex">
            <a class="navbar-brand" href="#"><img src="<?php echo act_url() ?>public/kp/img/logo.png" alt="logo" class="logo-img" /></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <!-- <span class="navbar-toggler-icon"></span> -->
                <span class="mdi mdi-menu"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item">
                        <a <?php if ($pagetitle == 'Home') { ?>class="nav-link active" <?php } else { ?>class="nav-link" <?php } ?> href="<?php echo base_url(); ?>"><i class="fa fa-home home_cls" aria-hidden="true"></i></a>
                    </li>
                    <li class="nav-item">
                        <a <?php if ($pagetitle == 'പുരസ്കാരങ്ങളെക്കുറിച്ച്') { ?>class="nav-link active" <?php } else { ?>class="nav-link" <?php } ?> href="<?php echo base_url(); ?>about">പുരസ്കാരങ്ങളെക്കുറിച്ച്</a>
                    </li>
                    <li class="nav-item">
                        <a <?php if ($pagetitle == 'വിജ്ഞാപനം') { ?>class="nav-link active" <?php } else { ?>class="nav-link" <?php } ?> href="<?php echo base_url(); ?>notification">വിജ്ഞാപനം</a>
                    </li>
                    <li class="nav-item">
                        <a <?php if ($pagetitle == 'ദൃശ്യമാധ്യമങ്ങൾ') { ?>class="nav-link active" <?php } else { ?>class="nav-link" <?php } ?> href="<?php echo base_url(); ?>media">ദൃശ്യമാധ്യമങ്ങൾ</a>
                    </li>
                    <li class="nav-item">
                        <a <?php if ($pagetitle == 'winners') { ?>class="nav-link active" <?php } else { ?>class="nav-link" <?php } ?> href="<?php echo base_url(); ?>Winner">വിന്നേഴ്‌സ്‌ </a>
                    </li>
                    <li class="nav-item">
                        <a <?php if ($pagetitle == 'ഗ്യാലറി') { ?>class="nav-link active" <?php } else { ?>class="nav-link" <?php } ?> href="<?php echo base_url(); ?>gallery">പുരസ്‌കാരദാനചടങ്ങുകൾ </a>
                    </li>
                    <!-- <li class="nav-item dropdown">
                        <a <?php if ($pagetitle == 'ഗ്യാലറി') { ?>class="nav-link dropdown-toggle active" <?php } else { ?> class="nav-link dropdown-toggle" <?php } ?> href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            ഗ്യാലറി
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <?php
                           // if (isset($gal_menu)) {
                               // foreach ($gal_menu as $rows) { ?>
                                    <li><a class="dropdown-item" href="<?php // echo base_url(); ?>gallery/sub/<?php // echo base64_encode($rows['id']); ?>"><?php // echo $rows['title']; ?></a></li>
                            <?php // }
                           // } ?>
                        </ul>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link" target="_blank" href="<?php echo base_url(); ?>login">നാമനിർദ്ദേശം </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="btn btn-brand ms-lg-3 nav-link text-white px-4" href="<?php echo base_url(); ?>en" role="button">
                            English
                        </a>

                    </li>
                </ul>

            </div>
        </div>
    </nav>

    <script>
        document.addEventListener("DOMContentLoaded", function() {
            const navLinks = document.querySelectorAll('.navbar .navbar-nav .nav-link.active');
            const sections = document.querySelectorAll('.navbar .navbar-nav .nav-link.active');

            function isElementInViewport(el) {
                const rect = el.getBoundingClientRect();
                return (
                    rect.top >= 0 &&
                    rect.left >= 0 &&
                    rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                    rect.right <= (window.innerWidth || document.documentElement.clientWidth)
                );
            }

            function setNavLinkActive() {
                sections.forEach((section, index) => {
                    const link = navLinks[index];
                    if (isElementInViewport(section)) {
                        link.classList.add("active");
                    } else {
                        link.classList.remove("active");
                    }
                });
            }

            setNavLinkActive();

            window.addEventListener("scroll", setNavLinkActive);
        });
    </script>