<section class="pa-any__query">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d-flex justify-content-center mb-5">

				<div class="col-md-4">
					<div class="image-container">
						<img src="<?php echo act_url(); ?>public/images/keralajyothi.png" alt="കേരള ജ്യോതി">
						<span>കേരള ജ്യോതി</span>
						<div class="overlay"><span>കേരള ജ്യോതി</span></div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="image-container">
					<img src="<?php echo act_url(); ?>public/images/keralaprabha.png"  alt="കേരള പ്രഭ">
						<span>കേരള പ്രഭ</span>
						<div class="overlay"><span>കേരള പ്രഭ</span></div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="image-container">
					<img src="<?php echo act_url(); ?>public/images/keralashri.png"  alt="കേരള ശ്രീ">
						<span>കേരള ശ്രീ </span>
						<div class="overlay"><span>കേരള ശ്രീ </span></div>
					</div>
				</div>
			</div>
		</div>
</section>