<!Doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo (isset($Titletag) ? '' . $Titletag : '') . (isset($pagetitle) ? ' :: ' . $pagetitle : '') . (isset($site_name) ? ' :: ' . $site_name : ''); ?></title>
    <link rel="shortcut icon" href="<?php echo act_url(); ?>public/images/favioc.png">
    <meta name="Description" content="<?php echo (isset($metadescription) ? $metadescription : ''); ?>" />
    <meta name="KeyWords" content="<?php echo (isset($metakeywords) ? $metakeywords : ''); ?>" />
 
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo act_url(); ?>public/kp/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo act_url(); ?>public/kp/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo act_url(); ?>public/kp/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo act_url(); ?>public/kp/css/boxicons.min.css">
    <link rel="stylesheet" href="<?php echo act_url(); ?>public/kp/css/style.css">

    <link rel="stylesheet" href="<?php echo act_url(); ?>public/kp/css/fontawesome-free-6.4.2-web/css/all.min.css"  />
    <link rel="stylesheet" href="<?php echo act_url(); ?>public/kp/css/MaterialDesign-Webfont-master/css/materialdesignicons.min.css"  />

   
    <script src="<?php echo act_url(); ?>public/kp/js/jquery-3.7.0.min.js"></script>
    <script src="<?php echo act_url(); ?>public/kp/js/owl.carousel.min.js"></script>
    <script src="<?php echo act_url(); ?>public/kp/js/app.js"></script>
    <script src="<?php echo act_url() ?>public/js/js/bootstrap.min.js"></script>
    <script src="<?php echo act_url(); ?>public/kp/js/dataToggle.js"></script>

    <!-- <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+Malayalam&display=swap" rel="stylesheet"> -->


</head>