<div class="row">
	<div class="col-md-12 div_p" >
	<table id="customers">
		<tr>
			<th>നാമനിർദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തിയുടെ വിവരങ്ങൾ </th>
		</tr>
	</table><br/>
	<?php if (isset($nomDetails)){?>
	<table id="customers">
			<tr>
			<td>പേര് :  <?php echo $nomDetails['name'];?></td>
			<td><?php $upload_path = FCPATH.'public/images/nomination/';
				if(file_exists($upload_path.$nomDetails['photo']) && $nomDetails['photo']!='') 
				{?><img src="<?php echo act_url();?>public/images/nomination/<?php echo $nomDetails['photo'];?>" width="150" height="150"/><?php } 
				else {?>
				<img src="<?php echo act_url();?>public/images/Image_Available.jpg"  width="75" height="75"/>
				<?php }?>
		</td>
			</tr>
		<tr>
			
			<td>ജനനതീയതി  : <?php if($nomDetails['age']!=0){?><?php echo date("d-m-Y", strtotime($nomDetails['dob']));?><?php } ?></td>
			<td>വയസ് : <?php if($nomDetails['age']!=0){ echo $nomDetails['age'];}else {echo '-';}?></td>
			</tr>
			<tr>
			<td>ലിംഗം : <?php if($nomDetails['sex']==1){?>സ്ത്രീ<?php }else if($nomDetails['sex']==2){ ?>പുരുഷൻ<?php }else if($nomDetails['sex']==3){ ?>ട്രാൻസ്ജെൻഡർ<?php } ?></td>
			<td>സംസ്ഥാനം : <?php echo $nomDetails['state'];?></td>
			</tr>
			<tr>
			<td>ജില്ല : <?php echo $nomDetails['district'];?></td>
			<td>താലൂക്ക് : <?php echo $nomDetails['taluk'];?></td>
			</tr>
			<tr>
			<td>ജനിച്ചസ്ഥലം : <?php echo $nomDetails['pob'];?></td>
			<td>മേൽവിലാസം : <?php echo $nomDetails['address'];?></td>
			</tr>
			<tr>
			<td>ഇ-മെയിൽ അഡ്രസ് : <?php echo $nomDetails['email'];?></td>
			<td>രാജ്യം : <?php echo $nomDetails['country'];?></td>
			</tr>
			<tr>
			<td>ജോലി / തൊഴിൽ : <?php echo $nomDetails['job'];?></td>
			<td></td>
			</tr>
		
		</table><br/>	
		<table id="customers">
		<tr>
			<th>നാമനിർദ്ദേശത്തിൻ്റെ വിശദാംശങ്ങൾ </th>
		</tr>
	</table><br/>	
	<table id="customers">
			
		<tr>
			<td>പ്രവർത്തനമേഖല  : <?php echo $nomDetails['title'];?></td>
			<td>ഉപ മേഖല : <?php echo $nomDetails['awardAreaSubTitle'];?> </td>
			</tr>
			<tr>
			<td>ഏത് കേരള പുരസ്കാരത്തിനാണ് ശിപാർശ : <?php if($nomDetails['award']==1){?>കേരള ജ്യോതി<?php }else if($nomDetails['award']==2){ ?>കേരള പ്രഭ<?php }else if($nomDetails['award']==3){ ?> കേരള ശ്രീ <?php } ?></td>
			<td>പത്മ പുരസ്‌കാരം ലഭിച്ചിട്ടുണ്ടോ: <?php if($nomDetails['pyn']==1){?>ഉണ്ട്<?php }else{ ?>ഇല്ല<?php } ?></td>
			</tr>
			<tr>
			<td>ഏത് പുരസ്‌കാരമാണ് ലഭിച്ചത് : <?php echo $nomDetails['pAward']; ?></td>
			<td>ഏത് വർഷമാണ് ലഭിച്ചത് : <?php if($nomDetails['pYear']!=0) {echo $nomDetails['pYear'];}else { echo "-";}?></td>
			</tr>
			</table>
			<table id="customers">
			<tr>
			<td>നാമനിദ്ദേശം ചെയ്യുന്ന വ്യക്തിയെക്കുറിച്ചുള്ള ചെറുവിവരണം : <?php echo $nomDetails['note'];?></td>
			</tr>
			<tr>
			<td>നാമനിദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തി സമൂഹത്തിന് നൽകിയിട്ടുള്ള കാര്യമായ സംഭാവനകൾ : <?php echo $nomDetails['donation'];?></td>
			</tr>
			<tr>
			<td>നാമനിദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തിയുടെ നേട്ടങ്ങൾ <?php echo $nomDetails['achivement'];?></td>
			
			</tr>
		<tr>
			<td>ഡോക്യുമെൻ്റ് &nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;<?php if($nomDetails['document']!=''){?><a href="<?php echo act_url();?>public/images/nomination/documents/<?php echo $nomDetails['document'];?>" target="_blank">
				<img src="<?php echo act_url();?>public/images/pdficon.png"  width="50" height="50"/></a><?php } ?></td>
			
			</tr>
		</table><br/>
		<table id="customers">
		<tr>
			<th>നാമനിർദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തിയുടെ പ്രവർത്തനങ്ങൾ ചെലുത്തിയ സ്വാധീനം </th>
		</tr>
	</table><br/>		
		<table id="customers">
			<tr>
			<td>പ്രവർത്തനമേഖലയിലെ സംഭാവന : <?php echo $nomDetails['wDonation'];?></td>
			</tr>
			<tr>
			<td>സമൂഹത്തിനുള്ള സംഭാവന : <?php echo $nomDetails['sDonation'];?></td>
			</tr>
			</table>
			<br/>
		
		<table id="customers">
			<tr><td>
			<?php if(($nomDetails['award']==1 && $userAward['keralajhothi']==1))
				{ ?>
					<span class="wrngmsg">
					കേരള അവാർഡിന്റെ "കേരളജ്യോതി" വിഭാഗത്തിലേക്ക് നാമനിർദ്ദേശം ഇതിനകം ചേർത്തിട്ടുണ്ട്. "നാമനിർദ്ദേശം എഡിറ്റ് ചെയ്യുക" തിരഞ്ഞെടുത്ത് വിഭാഗം പരിഷ്ക്കരിക്കുക.</span><?php
				}elseif(($nomDetails['award']==2 && $userAward['keralaprabha']==1))
				{ ?>
					<span class="wrngmsg">
					കേരള അവാർഡിന്റെ "കേരളപ്രഭ" വിഭാഗത്തിലേക്ക് നാമനിർദ്ദേശം ഇതിനകം ചേർത്തിട്ടുണ്ട്. "നാമനിർദ്ദേശം എഡിറ്റ് ചെയ്യുക" തിരഞ്ഞെടുത്ത് വിഭാഗം പരിഷ്ക്കരിക്കുക.</span>
				<?php
				}elseif(($nomDetails['award']==3 && $userAward['keralashri']==1))
				{?>
					<span class="wrngmsg">
					കേരള അവാർഡിന്റെ "കേരളശ്രീ" വിഭാഗത്തിലേക്ക് നാമനിർദ്ദേശം ഇതിനകം ചേർത്തിട്ടുണ്ട്. "നാമനിർദ്ദേശം എഡിറ്റ് ചെയ്യുക" തിരഞ്ഞെടുത്ത് വിഭാഗം പരിഷ്ക്കരിക്കുക.</span></span>
				<?php
				}
				else 
				{
				?>
			<span class="decl_msg"> <input type="checkbox" class="ba b--black-20 pa2 mb2 db ag" id="agree" name="agree" onclick="checkAgree()"> <!--Certified that the above details are correct and true to the best of my knowledge and belief. I have read them carefully and verified. I also understand that, if any information is found false or incorrect or ineligibility is determined at any stage of the selection process, my nomination will be summarily rejected.-->പുരസ്കാരത്തിനായി നാമനിർദ്ദേശം ചെയ്യപ്പെട്ട  വ്യക്തി, നാമനിർദ്ദേശത്തിനായി വ്യക്തിപരമായ ശിപാർശ  നൽകിയിട്ടില്ല. ടിയാളുമായി എനിക്ക് യാതൊരു ബന്ധവുമില്ല.</span><br/><br/><br/>
			
			<?php }?>
			<a  class="btn login-btn mb-4 editnom" href="<?php echo base_url();?>nomination/step1Edit">Edit Nomination</a>
				</td></tr>
		
		</table><br/>															   
<?php }?>

		
	</div>
</div>
