<style>

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 100%;
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}
</style>

	<div id="myModal" class="modal fade" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px;">
                <h5 class="modal-title">Please enter the one time password <br> to verify your account</h5>
				
            </div>
            <div class="modal-body">
			 <p id="otpDisplay"></p>
				 <div id="otpError"></div>
               <div id="otp" class="inputs d-flex flex-row justify-content-center mt-2">
			<input type="text" class="m-2 text-center form-control rounded" name="otp1" id="OTP1" maxlength="1" required>
			<input type="text" class="m-2 text-center form-control rounded" name="otp2" id="OTP2" maxlength="1" required>
			<input type="text" class="m-2 text-center form-control rounded" name="otp3" id="OTP3" maxlength="1" required>
			<input type="text" class="m-2 text-center form-control rounded" name="otp4" id="OTP4" maxlength="1" required>
			<input type="text" class="m-2 text-center form-control rounded" name="otp5" id="OTP5" maxlength="1" required>
			<input type="text" class="m-2 text-center form-control rounded" name="otp6" id="OTP6" maxlength="1" required>
			<div id="otpHidden"></div>
			 </div>
            <div class="mt-4"> <button type="button" class="btn btn-primary btn-lg" id="verify-otp">verify otp</button><button style="margin-left:20px;" type="button" class="btn btn-danger btn-lg" id="resend-otp">Resend otp</button>
		
            </div>
        </div>
    </div>
</div>
</div>

	<script>
document.addEventListener("DOMContentLoaded", function(event) {

function OTPInput() {
const inputs = document.querySelectorAll('#otp > *[id]');
for (let i = 0; i < inputs.length; i++) { inputs[i].addEventListener('keydown', function(event) { if (event.key==="Backspace" ) { inputs[i].value='' ; if (i !==0) inputs[i - 1].focus(); } else { if (i===inputs.length - 1 && inputs[i].value !=='' ) { return true; } else if (event.keyCode> 47 && event.keyCode < 58) { inputs[i].value=event.key; if (i !==inputs.length - 1) inputs[i + 1].focus(); event.preventDefault(); } else if (event.keyCode> 64 && event.keyCode < 91) { inputs[i].value=String.fromCharCode(event.keyCode); if (i !==inputs.length - 1) inputs[i + 1].focus(); event.preventDefault(); } } }); } } OTPInput(); });</script>
	<script>
		$("#resend-otp").click(function () {
		var mob = $('#otpMob').val();
		 var dataString = {
              mobile_number: $("#otpMob").val(),
           };
		var last3 = mob.slice(-3);
			 $.ajax({
					type: 'POST',
					url: '<?php echo base_url();?>nomination/resendOtp',
					data: dataString,
					success: function(data) {
					$('#otpHidden').html(data);
					$('#otpDisplay').html('<span>A code has been sent to</span> <small>*******'+ last3 +'</small>');
                }
		});
		});
		</script>
		<script>
		$("#verify-otp").click(function () {

			// window.location.replace("index.php");
			var otp1 = document.getElementById("OTP1").value;
            var otp2 = document.getElementById("OTP2").value;
			var otp3 = document.getElementById("OTP3").value;
			var otp4 = document.getElementById("OTP4").value;
			var otp5 = document.getElementById("OTP5").value;
			var otp6 = document.getElementById("OTP6").value;
			var otpVal = document.getElementById("otpVal").value;
			
			var result = otp1.concat(otp2);
			var result1 = result.concat(otp3);
			var result2 = result1.concat(otp4);
			var result3 = result2.concat(otp5);
			var result5 = result3.concat(otp6);
			 
			
			
			// alert(otp2);
			if (result5 == otpVal) {
				window.location.href = "<?php echo base_url();?>nomination/save4";
			}
			else {
				$('#otpError').html("<div class='alert alert-danger' role='alert'> OTP not matches</div>");
			}
		});
	</script>