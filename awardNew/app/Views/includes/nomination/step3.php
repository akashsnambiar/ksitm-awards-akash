<form id="demoForm" method="POST" action="<?php echo base_url();?>nomination/save3" enctype="multipart/form-data" class="frmcls">
<?= csrf_field() ?>	
<h2 class="nomh2">നാമനിർദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തിയുടെ പ്രവർത്തനങ്ങൾ ചെലുത്തിയ സ്വാധീനം</h2><br/><br/>
		<div class="err_text" id="err"></div>
		<?php if (isset($validation)): ?>
		<div class="alert alert-warning">
			<?= $validation->listErrors() ?>
		</div>
		<?php endif; ?>
			<div class="row">
				<div class="col-md-12">
						<div class="form-group fl">
				<label for="inputCity" class="inputlbl">പ്രവർത്തനമേഖലയിലെ  സംഭാവന <span class="mandatory">*</span></label>
							<textarea class="form-control textAr" id="wDonation" name="wDonation"></textarea>
						</div>
				</div>
				<div class="col-md-12">
						<div class="form-group fl">
						<label for="inputCity" class="inputlbl">സമൂഹത്തിനുള്ള സംഭാവന  <span class="mandatory">*</span></label>
							<textarea class="form-control textAr" id="sDonation" name="sDonation"></textarea>
						</div>
				</div>
			
				<div class="col-md-12 cf mb2">
					<div class="fl w-100">
						<div class="fl w-25 pa2 bnm"></div>
						<div class="fl w-100">
					<button type="submit" class="btn login-btn mb-4 btn_nom" name="Submit" id="Submit" value="Send">next-step</button>
						</div>
					</div>
				</div>
		</div>
</form>