<script src="<?php echo act_url();?>public/js/login/popper.min.js"></script>
<script src="<?php echo act_url();?>public/js/login/bootstrap.min.js"></script>
<script src="<?php echo act_url();?>public/js/login/es6-shim.min.js"></script>
<script src="<?php echo act_url();?>public/js/validation/FormValidation.min.js"></script>
<script src="<?php echo act_url();?>public/js/validation/plugins/Tachyons.min.js"></script>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        const demoForm = document.getElementById('demoForm');
        const fv = FormValidation.formValidation(demoForm, {
            fields: {
                wDonation: {
                    validators: {
                        notEmpty: {
                            message: 'The Field is required.',
                        },
                    },
                },
                sDonation: {
                    validators: {
                        notEmpty: {
                            message: 'The Field is required.',
                        },
                    },
                },
                
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                tachyons: new FormValidation.plugins.Tachyons(),
                submitButton: new FormValidation.plugins.SubmitButton(),
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh',
                    onPlaced: function (e) {
                        e.iconElement.addEventListener('click', function () {
                            fv.resetField(e.field);
                        });
                    },
                }),
            },
        }).on('core.form.valid', function () {
                var form = $("#demoForm");
                var url = form.attr('action');
                dataString = $("#demoForm").serialize();
                $.ajax({
                type: 'POST',
                url: url,
                data: dataString,
                dataType:"JSON",
                success: function(data) {
                if (data.success==1)
                {
                    $('#err').removeClass('alert');
                    $('#err').removeClass('alert-warning');
                    $("#err").html('');
                    window.location.href = "<?php echo base_url();?>nomination4";
                }
                else{
                  
                    $("#err").html('');
                    $('#err').addClass('alert alert-warning');
                    $("#err").html(data.error);
                     $('html, body').animate({
                        scrollTop: 300
                    }, 400);
                }
                 },
                error: function(data) {
                    // Some error in ajax call
                    alert("some Error");
                }
                    });
        });
    });
</script> 

<script>
    $(document).ready(function() {
        function disableBack() {
            window.history.forward()
        }
        window.onload = disableBack();
        window.onpageshow = function(e) {
            if (e.persisted)
                disableBack();
        }
    });
</script>        