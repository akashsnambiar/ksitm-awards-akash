					<style>
#customers {
font-family: Arial, Helvetica, sans-serif;
border-collapse: collapse;
width: 100%;
font-size:12px;
}

#customers td{
border: 1px solid #ddd;
padding: 12px 30px;
text-align: left;
} 
#customers th {
border: 1px solid #ddd;
padding: 8px;
text-align:center;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers td:hover {background-color: #ddd;}

#customers th {
padding-top: 12px;
padding-bottom: 12px;
background-color: #04AA6D;
color: white;
}
</style>
					
						
<div class="row">
<div class="col-md-12" style="padding:0px;">


<table id="customers">
	<tr>
		<th>നാമനിർദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തിയുടെ വിവരങ്ങൾ </th>
	</tr>
</table><br/>
<?php if (isset($nomDetails)){?>
<table id="customers">
		<tr>
		<td><b>പേര് : </b> <?php echo $nomDetails['name'];?></td>
		<td><?php if($nomDetails['photo']!=''){?><img src="<?php echo act_url();?>public/images/nomination/<?php echo $nomDetails['photo']?>" width="150" height="150"/><?php } ?></td>
		</tr>
	<tr>
		<td><b>ജനനതീയതി  : </b><?php if($nomDetails['age']!=0){?><?php  echo date("d M Y", strtotime($nomDetails['dob'])); ?><?php } ?></td>
		<td><b>വയസ് : </b><?php echo $nomDetails['age'];?></td>
		</tr>
		<tr>
		<td><b>ലിംഗം : </b><?php if($nomDetails['sex']==1){?>സ്ത്രീ<?php }else{ ?>പുരുഷൻ<?php } ?></td>
		<td><b>സംസ്ഥാനം : </b><?php echo $nomDetails['state'];?></td>
		</tr>
		<tr>
		<td><b>ജില്ല : </b><?php echo $nomDetails['district'];?></td>
		<td><b>താലൂക്ക് : </b><?php echo $nomDetails['taluk'];?></td>
		</tr>
		<tr>
		<td><b>ജനിച്ചസ്ഥലം : </b><?php echo $nomDetails['pob'];?></td>
		<td><b>മേൽവിലാസം : </b><?php echo $nomDetails['address'];?></td>
		</tr>
		<tr>
		<td><b>ഇ-മെയിൽ അഡ്രസ് : </b><?php echo $nomDetails['email'];?></td>
		<td><b>രാജ്യം : </b><?php echo $nomDetails['country'];?></td>
		</tr>
		<tr>
		<td><b>ജോലി / തൊഴിൽ : </b><?php echo $nomDetails['job'];?></td>
		</tr>
	</table><br/>	
	<table id="customers">
	<tr>
		<th>നാമനിർദ്ദേശത്തിൻ്റെ വിശദാംശങ്ങൾ </th>
	</tr>
</table><br/>	
<table id="customers">
	<tr>
		<td><b>പ്രവർത്തനമേഖല  : </b><?php echo $nomDetails['title'];?></td>
		<td><b>ഉപ മേഖല : </b><?php echo $nomDetails['awardAreaSubTitle'];?> </td>
		</tr>
		<tr>
		<td><b>ഏത് കേരള പുരസ്കാരത്തിനാണ് ശിപാർശ : </b><?php if($nomDetails['award']==1){?>കേരള ജ്യോതി<?php }else if($nomDetails['award']==2){ ?>കേരള പ്രഭ<?php }else if($nomDetails['award']==3){ ?> കേരള ശ്രീ <?php } ?></td>
		<td><b>പത്മ പുരസ്‌കാരം ലഭിച്ചിട്ടുണ്ടോ: </b><?php if($nomDetails['pyn']==1){?>ഉണ്ട്<?php }else{ ?>ഇല്ല<?php } ?></td>
		</tr>
		<tr>
		<td><b>ഏത് പുരസ്‌കാരമാണ് ലഭിച്ചത് : </b><?php echo $nomDetails['pAward']; ?></td>
		<td><b>ഏത് വർഷമാണ് ലഭിച്ചത് : </b><?php echo $nomDetails['pYear'];?></td>
		</tr>
		</table>
		<table id="customers">
		<tr>
		<td><b>നാമനിദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തിയെക്കുറിച്ചുള്ള ചെറുവിവരണം : </b><?php echo $nomDetails['note'];?></td>
		</tr>
		<tr>
		<td><b>നാമനിദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തി സമൂഹത്തിന് നൽകിയിട്ടുള്ള കാര്യമായ സംഭാവനകൾ : </b><?php echo $nomDetails['donation'];?></td>
		</tr>
		<tr>
		<td><b>നാമനിദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തിയുടെ നേട്ടങ്ങൾ </b><?php echo $nomDetails['achivement'];?></td>
		</tr>
		<tr>
		<td><b>ഡോക്യുമെൻ്റ് &nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;</b><?php if($nomDetails['document']!=''){?><a href="<?php echo act_url();?>public/images/nomination/documents/<?php echo $nomDetails['document'];?>" target="_blank"><img src="<?php echo act_url();?>public/images/pdficon.png"  width="50" height="50"/></a><?php } ?></td>
		</tr>
	</table><br/>
	<table id="customers">
	<tr>
		<th>നാമനിർദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തിയുടെ പ്രവർത്തനങ്ങൾ ചെലുത്തിയ സ്വാധീനം </th>
	</tr>
</table><br/>		
	<table id="customers">
		<tr>
		<td><b>പ്രവർത്തനമേഖലയിലെ സംഭാവന : </b><?php echo $nomDetails['wDonation'];?></td>
		</tr>
		<tr>
		<td><b>സമൂഹത്തിനുള്ള സംഭാവന </b><?php echo $nomDetails['sDonation'];?></td>
		</tr>
		</table>
		<br/>
	
	<table id="esign">
		<img style="float:left;" src="<?php echo act_url();?>public/images/esign.jpg"  width="75" height="75"/><span style="float:left;">&nbsp;&nbsp;Digitally Signed By : <?php echo $aadharName;?></span>
		</table>
	<?php } else {?>
		<table><tr>
		<td><?php echo "No Records";?></td></tr></table>
		<?php }?>
	<br/>	
</div>
</div>
							
<script>
window.setTimeout(function() {
		window.location.href = '<?php echo base_url();?>nomination/save4';
		}, 7000);
</script> 