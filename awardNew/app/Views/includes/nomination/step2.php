<form id="demoForm" method="POST" action="<?php echo base_url(); ?>nomination/save2" enctype="multipart/form-data" class="frmcls">
	<?= csrf_field() ?>
	<h2 class="nomh2">നാമനിർദ്ദേശത്തിൻ്റെ വിശദാംശങ്ങൾ </h2><br /><br />
	<div  class="err_text" id="err"></div>
	<?php if (isset($validation)): ?>
		<div class="alert alert-warning">
			<?= $validation->listErrors() ?>
		</div>
	<?php endif; ?>
	<div class="row">
		<div class="col-md-12 nomdiv">
			<div class="col-md-6 frmlbl">
				<div class="form-group fl">
					<label for="inputCity" class="inputlbl">പ്രവർത്തനമേഖല <span class="mandatory">*</span></label>
					<select id="workArea" name="workArea" class="form-control">
						<option value="">--Select--</option>
						<?php foreach ($workArea as $row) { ?>
							<?php if ($row['awardID'] != 0) { ?>
								<option value="<?php echo $row['awardID']; ?>">
									<?php echo $row['title']; ?>
								</option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>

			</div>
			<div class="col-md-6">
				<div class="form-group fl">
					<label for="inputCity" class="inputlbl">ഉപ മേഖല<span class="mandatory">*</span></label>
					<select id="workAreaSub" name="workAreaSub" class="form-control">
						<option value="">--Select--</option>
						<?php foreach ($workAreaSub as $row) { ?>
							<option value="<?php echo $row['awardAreaSubID']; ?>"
								data-chained="<?php echo $row['awardAreaID']; ?>">
								<?php echo $row['awardAreaSubTitle']; ?>
							</option>
						<?php } ?>
					</select>
				</div>

			</div>
			<div class="col-md-6">
				<div class="form-group fl">
					<label for="inputCity"  class="inputlbl">ഏത് കേരള പുരസ്കാരത്തിനാണ് ശിപാർശ<span
							class="mandatory">*</span></label>
					<select id="award" name="award" class="form-control">
						<option value="">--Select--</option>
						<?php foreach ($userAward as $row) {
							?>
							<?php if ($row['keralajhothi'] == 0) { ?>
								<option value="1">കേരള ജ്യോതി</option>
							<?php } ?>
							<?php if ($row['keralaprabha'] == 0) { ?>
								<option value="2">കേരള പ്രഭ</option>
							<?php } ?>
							<?php if ($row['keralashri'] == 0) { ?>
								<option value="3">കേരള ശ്രീ</option>
							<?php }
						} ?>
					</select>
				</div>

			</div>
			<div class="col-md-6">
				<div class="form-group fl">
					<label for="inputCity"  class="inputlbl">പത്മ പുരസ്‌കാരം ലഭിച്ചിട്ടുണ്ടോ<span
							class="mandatory">*</span></label>
					<select id="pyn" name="pyn" class="form-control">
						<option value="">--Select--</option>
						<option value="1">ഉണ്ട്</option>
						<option value="2">ഇല്ല</option>
					</select>
				</div>

			</div>
			<div class="col-md-6">
				<div class="form-group fl">
					<label for="inputCity" class="inputlbl">ഏത് പുരസ്‌കാരമാണ് ലഭിച്ചത് <span
							class="mandatory"></span></label>
					<select id="pAward" name="pAward" class="form-control">
						<option value="">--Select--</option>
						<option value="പദ്മ വിഭൂഷൺ">പദ്മ വിഭൂഷൺ</option>
						<option value="പദ്മ ഭൂഷൺ">പദ്മ ഭൂഷൺ</option>
						<option value="പദ്മ ശ്രീ">പദ്മ ശ്രീ</option>
					</select>
				</div>

			</div>
			<div class="col-md-6">
				<div class="form-group fl">
					<label for="inputCity"  class="inputlbl">ഏത് വർഷമാണ് ലഭിച്ചത് <span class="mandatory"></span></label>
					<input type="number" class="form-control ba b--black-20 pa2 mb2 db" id="pYear" name="pYear"	placeholder="ഏത്  വർഷമാണ് ലഭിച്ചത്">
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group fl">
				<label for="inputCity" class="inputlbl">നാമനിദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തിയെക്കുറിച്ചുള്ള ചെറുവിവരണം
					<span class="mandatory">*</span></label>
				<textarea class="form-control textAr" id="note" name="note"></textarea>
			</div>

		</div>
		<div class="col-md-12">
			<div class="form-group fl">
				<label for="inputCity"  class="inputlbl">നാമനിദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തി സമൂഹത്തിന് നൽകിയിട്ടുള്ള
					കാര്യമായ സംഭാവനകൾ<span class="mandatory">*</span></label>
				<textarea class="form-control textAr" id="donation" name="donation"></textarea>
			</div>

		</div>
		<div class="col-md-12">
			<div class="form-group fl">
				<label for="inputCity"  class="inputlbl">നാമനിദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തിയുടെ നേട്ടങ്ങൾ <span
						class="mandatory">*</span></label>
				<textarea class="form-control textAr" id="achivement" name="achivement"></textarea>
			</div>

		</div>
		<div class="col-md-6">
			<div class="form-group fl">
				<label for="inputZip"  class="inputlbl">ഡോക്യുമെൻ് <span class="mandatory"></span></label>
				<input type="file" class="form-control ba b--black-20 pa2 mb2 db file_lbl" id="doc" name="doc">
					<p class="nomination_plabel">Only PDF files are allowed, with a maximum size of 15 MB.</p>
				<div id="uploaded_image_doc"></div>
			</div>
		</div>
		<div class="col-md-6 cf mb2">
			<div class="fl w-100">
				<div class="fl w-25 pa2 bnm"></div>
				<div class="fl w-100">
					<button  type="submit" class="btn login-btn mb-4 btn_nom" name="Submit" id="Submit"
						value="Send">next-step</button>
				</div>
			</div>
		</div>
	</div>
</form>