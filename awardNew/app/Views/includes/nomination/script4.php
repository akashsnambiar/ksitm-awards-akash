<script src="<?php echo act_url();?>public/js/login/popper.min.js"></script>
<script src="<?php echo act_url();?>public/js/login/bootstrap.min.js"></script>
<script src="<?php echo act_url();?>public/js/login/es6-shim.min.js"></script>
<script src="<?php echo act_url();?>public/js/validation/FormValidation.min.js"></script>
<script src="<?php echo act_url();?>public/js/validation/plugins/Tachyons.min.js"></script>
<script>
$("#SubmitOtp").click(function () {
var mob = $('#mobile_number').val();
    var dataString = {
        mobile_number: $("#mobile_number").val(),
    };
var last3 = mob.slice(-3);

        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>nomination/otpSend',
            data: dataString,
            success: function(data) {
            $('#otpHidden').html(data);
            $('#otpDisplay').html('<span>A code has been sent to</span> <small>*******'+ last3 +'</small>');
            $('#myModal').modal({backdrop: 'static', show: true, keyboard: false});
        }
});
});

$(document).ready(function() {
function disableBack() {
    window.history.forward()
}
window.onload = disableBack();
window.onpageshow = function(e) {
    if (e.persisted)
        disableBack();
}
});
//check confirmation 
$('#agree').change(function () {
location.href = "<?php echo act_url();?>esign/index.php";
});
</script>     