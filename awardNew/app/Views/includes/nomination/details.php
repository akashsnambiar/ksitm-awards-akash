<div class="row">
	<div class="col-md-12" style="padding:0px;">
		<table id="customers">
			<tr>
				<th>നാമനിർദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തിയുടെ വിവരങ്ങൾ </th>
			</tr>
		</table><br />
		<?php if (isset($userDetails)) { ?>
			<table id="customers">
				<tr>
					<td>
						<h3 class="h3_lbl">Reg. No. :
							<?php echo $userDetails['nominationRegID']; ?>
						</h3>
					</td>
					<td>
						<?php
						$upload_path = FCPATH . 'public/images/nomination/';
						if (file_exists($upload_path . $userDetails['photo']) && $userDetails['photo'] != '') { ?><img src="<?php echo act_url(); ?>public/images/nomination/<?php echo $userDetails['photo']; ?>" width="150"
								height="150" />
						<?php } else { ?>
							<img src="<?php echo act_url(); ?>public/images/Image_Available.jpg" width="75" height="75" />
						<?php } ?>
					</td>
				</tr>
				<tr>
					<td> പേര് : 
						<?php echo $userDetails['name']; ?>
					</td>
					<td>ജനനതീയതി : 
						<?php echo date("d-m-Y", strtotime($userDetails['dob'])); ?>
					</td>

				</tr>
				<tr>
					<td>വയസ് : 
						<?php if ($userDetails['age'] != 0) {
							echo $userDetails['age'];
						} else {
							echo '-';
						} ?>
					</td>
					<td>ലിംഗം : 
						<?php if ($userDetails['sex'] == 1) { ?>സ്ത്രീ
						<?php } else if ($userDetails['sex'] == 2) { ?>പുരുഷൻ
						<?php } else if ($userDetails['sex'] == 3) { ?>ട്രാൻസ്ജെൻഡർ
						<?php } ?>
					</td>

				</tr>
				<tr>
					<td>സംസ്ഥാനം : 
						<?php echo $userDetails['state']; ?>
					</td>
					<td>ജില്ല : 
						<?php echo $userDetails['district']; ?>
					</td>

				</tr>
				<tr>
					<td>താലൂക്ക് : 
						<?php echo $userDetails['taluk']; ?>
					</td>
					<td>ജനിച്ചസ്ഥലം : 
						<?php echo $userDetails['pob']; ?>
					</td>

				</tr>
				<tr>
					<td>മേൽവിലാസം : 
						<?php echo $userDetails['address']; ?>
					</td>
					<td>ഇ-മെയിൽ അഡ്രസ് : 
						<?php echo $userDetails['email']; ?>
					</td>

				</tr>
				<tr>
					<td>രാജ്യം : 
						<?php echo $userDetails['country']; ?>
					</td>
					<td>ജോലി / തൊഴിൽ : 
						<?php echo $userDetails['job']; ?>
					</td>

				</tr>

			</table><br />
			<table id="customers">
				<tr>
					<th>നാമനിർദ്ദേശത്തിൻ്റെ വിശദാംശങ്ങൾ </th>
				</tr>
			</table><br />
			<table id="customers">
				<tr>
					<td>പ്രവർത്തനമേഖല : 
						<?php echo $userDetails['title']; ?>
					</td>
					<td>ഉപ മേഖല : 
						<?php echo $userDetails['awardAreaSubTitle']; ?>
					</td>
				</tr>
				<tr>
					<td>ഏത് കേരള പുരസ്കാരത്തിനാണ് ശിപാർശ : 
						<?php if ($userDetails['award'] == 1) { ?>കേരള ജ്യോതി
						<?php } else if ($userDetails['award'] == 2) { ?>കേരള പ്രഭ
						<?php } else if ($userDetails['award'] == 3) { ?> കേരള ശ്രീ
						<?php } ?>
					</td>
					<td>പത്മ പുരസ്‌കാരം ലഭിച്ചിട്ടുണ്ടോ: 
						<?php if ($userDetails['pyn'] == 1) { ?>ഉണ്ട്
						<?php } else { ?>ഇല്ല
						<?php } ?>
					</td>
				</tr>
				<tr>
					<td>ഏത് പുരസ്‌കാരമാണ് ലഭിച്ചത് : 
						<?php echo $userDetails['pAward']; ?>
					</td>
					<td>ഏത് വർഷമാണ് ലഭിച്ചത് : 
						<?php if ($userDetails['pYear'] != 0) {
							echo $userDetails['pYear'];
						} else {
							echo '-';
						} ?>
					</td>
				</tr>
			</table>
			<table id="customers">
				<tr>
					<td>നാമനിദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തിയെക്കുറിച്ചുള്ള ചെറുവിവരണം : 
						<?php echo $userDetails['note']; ?>
					</td>
				</tr>
				<tr>
					<td>നാമനിദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തി സമൂഹത്തിന് നൽകിയിട്ടുള്ള കാര്യമായ സംഭാവനകൾ : 
						<?php echo $userDetails['donation']; ?>
					</td>
				</tr>
				<tr>
					<td>നാമനിദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തിയുടെ നേട്ടങ്ങൾ :
						<?php echo $userDetails['achivement']; ?>
					</td>

				</tr>
				<tr>
					<td>ഡോക്യുമെൻ്റ് &nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;
						<?php $doc_path = FCPATH . 'public/images/nomination/documents/';
						if (file_exists($doc_path . $userDetails['document']) && $userDetails['document'] != '') { ?>
							<a href="<?php echo act_url(); ?>public/images/nomination/documents/<?php echo $userDetails['document']; ?>"
								target="_blank"><img src="<?php echo act_url(); ?>public/images/pdficon.png" width="50"
									height="50" /></a>
						<?php } else {

						} ?>
					</td>

				</tr>
			</table><br />
			<table id="customers">
				<tr>
					<th>നാമനിർദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തിയുടെ പ്രവർത്തനങ്ങൾ ചെലുത്തിയ സ്വാധീനം </th>
				</tr>
			</table><br />
			<table id="customers">
				<tr>
					<td>പ്രവർത്തനമേഖലയിലെ സംഭാവന : 
						<?php echo $userDetails['wDonation']; ?>
					</td>
				</tr>
				<tr>
					<td>സമൂഹത്തിനുള്ള സംഭാവന :
						<?php echo $userDetails['sDonation']; ?>
					</td>
				</tr>
			</table>
			<?php if (isset($esign)) { ?>
				<table id="customers">
					<tr>
						<td>നാമനിർദ്ദേശം ചെയ്യുന്ന ആളിൻ്റെ ഒപ്പ് :</td>
						<td><img src="<?php echo act_url(); ?>public/images/esign.jpg" width="50"
								height="50" />&nbsp;&nbsp;Digitally Signed By :
							<?php echo $esign['name']; ?>
						</td>
					</tr>
				</table>
			<?php } ?>
		<?php } else {
			echo "No Record";
		} ?><br />
	</div>
</div>