<style>

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 100%;
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

</style>

<div id="myModal" class="modal fade" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px;">
                <h5 class="modal-title">Please enter the one time password <br> to verify your account</h5>
            </div>
            <div class="modal-body">
			 <p id="otpDisplay"></p>
			<div id="otpError"></div>
			<div id="otp" class="inputs d-flex flex-row justify-content-center mt-2">
			<input type="text" class="m-2 text-center form-control rounded" name="otp1" id="OTP1" maxlength="1" required>
			<input type="text" class="m-2 text-center form-control rounded" name="otp2" id="OTP2" maxlength="1" required>
			<input type="text" class="m-2 text-center form-control rounded" name="otp3" id="OTP3" maxlength="1" required>
			<input type="text" class="m-2 text-center form-control rounded" name="otp4" id="OTP4" maxlength="1" required>
			<input type="text" class="m-2 text-center form-control rounded" name="otp5" id="OTP5" maxlength="1" required>
			<input type="text" class="m-2 text-center form-control rounded" name="otp6" id="OTP6" maxlength="1" required>
			 </div>
            <div class="mt-4"> <button type="button" class="btn btn-primary btn-lg" id="verify-otp">verify otp</button><button style="margin-left:20px;" type="button" class="btn btn-danger btn-lg" id="resend-otp">Resend otp</button></div>
        </div>
    </div>
</div>
</div>
<div id="myModal1" class="modal fade" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px;">
                <h5 class="modal-title">Please enter the one time password <br> to verify your account</h5>
            </div>
            <div class="modal-body">
			 <p id="otpDisplay1"></p>
			 <div id="otpError1"></div>
             <div id="otp" class="inputs d-flex flex-row justify-content-center mt-2">
			<input type="text" class="m-2 text-center form-control rounded" name="otp7" id="OTP7" maxlength="1" required>
			<input type="text" class="m-2 text-center form-control rounded" name="otp8" id="OTP8" maxlength="1" required>
			<input type="text" class="m-2 text-center form-control rounded" name="otp9" id="OTP9" maxlength="1" required>
			<input type="text" class="m-2 text-center form-control rounded" name="otp10" id="OTP10" maxlength="1" required>
			<input type="text" class="m-2 text-center form-control rounded" name="otp11" id="OTP11" maxlength="1" required>
			<input type="text" class="m-2 text-center form-control rounded" name="otp12" id="OTP12" maxlength="1" required>
			 </div>
            <div class="mt-4"> <button type="button" class="btn btn-primary btn-lg" id="verify-otp1">verify otp</button><button style="margin-left:20px;" type="button" class="btn btn-danger btn-lg" id="resend-otp1">Resend otp</button>
		    </div>
        </div>
    </div>
</div>
</div>
<script>
document.addEventListener("DOMContentLoaded", function(event) {

function OTPInput() {
const inputs = document.querySelectorAll('#otp > *[id]');

for (let i = 0; i < inputs.length; i++) { inputs[i].addEventListener('keydown', function(event) { if (event.key==="Backspace" ) { inputs[i].value='' ; if (i !==0) inputs[i - 1].focus(); } else { if (i===inputs.length - 1 && inputs[i].value !=='' ) { return true; } else if (event.keyCode> 47 && event.keyCode < 58) { inputs[i].value=event.key; if (i !==inputs.length - 1) inputs[i + 1].focus(); event.preventDefault(); } else if (event.keyCode> 64 && event.keyCode < 91) { inputs[i].value=String.fromCharCode(event.keyCode); if (i !==inputs.length - 1) inputs[i + 1].focus(); event.preventDefault(); } } }); } } OTPInput(); });</script>
<div id="otpHidden"></div>
<script>
	$("#resend-otp").click(function () {
	var mob = otpMob;
	var dataString='';
	
	var csrf_test_name = $('input[name="csrf_test_name"]').val(); 
	var last3 = mob.slice(-3);
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url();?>register/resendOtp',
				data: dataString,
                dataType: 'JSON',
                headers: {
                    'X-CSRF-TOKEN': csrf_test_name
                },
				success: function(data) {
                    $('input[name="csrf_test_name"]').val(data.token);
					$('#otpHidden').html(data.res);
					$('#otpDisplay').html('<span>A code has been sent to</span> <small>*******'+ last3 +'</small>');
				}		
	});
	});
</script>
        
<script>
$("#verify-otp").click(function () {
	// window.location.replace("index.php");
	var otp1 = document.getElementById("OTP1").value;
	var otp2 = document.getElementById("OTP2").value;
	var otp3 = document.getElementById("OTP3").value;
	var otp4 = document.getElementById("OTP4").value;
	var otp5 = document.getElementById("OTP5").value;
	var otp6 = document.getElementById("OTP6").value;

	var otpVals = otpVal;
	var result = otp1.concat(otp2);
	var result1 = result.concat(otp3);
	var result2 = result1.concat(otp4);
	var result3 = result2.concat(otp5);
	var result5 = result3.concat(otp6);

	var csrf_test_name = $('input[name="csrf_test_name"]').val(); 
	
	if (result5 == otpVals) {
		
		//$('input[name="csrf_test_name"]').val(data.token);
		window.location.href = "<?php echo base_url();?>register/saveOrginal";
	}
	else {
		
		$('#otpError').html("<div class='alert alert-danger' role='alert'> OTP not matches</div>");
	}
});
</script>
	
<div id="otpHidden1"></div>
<script>
	$("#resend-otp1").click(function () {
	var mob = otpMob1;
	var dataString = '';
	var csrf_test_name = $('input[name="csrf_test_name"]').val(); 
	var last3 = mob.slice(-3);
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url();?>register/resendOtp1',
				data: dataString,
                dataType: 'JSON',
                headers: {
                    'X-CSRF-TOKEN': csrf_test_name
                },
				success: function(data) {	
				$('input[name="csrf_test_name"]').val(data.token);
				$('#otpHidden1').html(data.res);
				$('#otpDisplay1').html('<span>A code has been sent to</span> <small>*******'+ last3 +'</small>');
			}
	});
	});
	</script>
	
	<script>
	$("#verify-otp1").click(function () {
		var otp7 = document.getElementById("OTP7").value;
		var otp8 = document.getElementById("OTP8").value;
		var otp9 = document.getElementById("OTP9").value;
		var otp10 = document.getElementById("OTP10").value;
		var otp11 = document.getElementById("OTP11").value;
		var otp12 = document.getElementById("OTP12").value;

		var otpVal1s = otpVal1;
		var result6 = otp7.concat(otp8);
		var result7 = result6.concat(otp9);
		var result8 = result7.concat(otp10);
		var result9 = result8.concat(otp11);
		var result10 = result9.concat(otp12);		

		if (result10 == otpVal1s) {
			window.location.href = "<?php echo base_url();?>register/saveOrgOrginal";
		}
		else {
			
			$('#otpError1').html("<div class='alert alert-danger' role='alert'> OTP not matches</div>");
		}
	});
</script>