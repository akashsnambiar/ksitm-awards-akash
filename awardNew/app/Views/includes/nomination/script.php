<script src="<?php echo act_url();?>public/js/login/popper.min.js"></script>
<script src="<?php echo act_url();?>public/js/login/bootstrap.min.js"></script>
<script src="<?php echo act_url();?>public/js/login/es6-shim.min.js"></script>
<script src="<?php echo act_url();?>public/js/validation/FormValidation.min.js"></script>
<script src="<?php echo act_url();?>public/js/validation/plugins/Tachyons.min.js"></script>

<script>
$(document).ready(function () 
 { 
  $("#watch-me").click(function()
  {
    $("#show-me:hidden").show('slow');
   $("#show-me-two").hide();
   });
   $("#watch-me").click(function()
  {
    if($('watch-me').prop('checked')===false)
   {
    $('#show-me').hide();
   }
  });
  
  $("#see-me").click(function()
  {
    $("#show-me-two:hidden").show('slow');
   $("#show-me").hide();
   });
   $("#see-me").click(function()
  {
    if($('see-me-two').prop('checked')===false)
   {
    $('#show-me-two').hide();
   }
  });
  
 });
</script>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        const demoForm = document.getElementById('demoForm');
        const fv = FormValidation.formValidation(demoForm, {
            fields: {
                nominater_type: {
                    validators: {
                        notEmpty: {
                            message: 'The Nominater Type is required',
                        },
                    },
                },
                first_name: {
                    validators: {
                        notEmpty: {
                            message: 'The Name is required',
                        },
                    },
                },
                mobile_code: {
                    validators: {
                        notEmpty: {
                            message: 'The Code is required',
                        },
                    },
                },
                mobile_number: {
                    validators: {
                        integer: {
                                message: 'The value is not an integer.',
                        },
                        stringLength: {
                            min: 10,
                            max: 10,
                            message: 'Invalid Mobile Number.',
                        },
                        notEmpty: {
                            message: 'The Mobile Number is required.',
                        },
                    },
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'The email address is required',
                        },
                        emailAddress: {
                            message: 'The input is not a valid email address',
                        },
                    },
                },
                    inputDoc: {
                    validators: {
                        notEmpty: {
                            message: 'Select Identity Document to Upload is required',
                        },
                    },
                },
                    inputDocNum: {
                    validators: {
                        notEmpty: {
                            message: 'Identity Document Number is required',
                        },
                    },
                },
                                    
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                tachyons: new FormValidation.plugins.Tachyons(),
                submitButton: new FormValidation.plugins.SubmitButton(),
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh',
                    onPlaced: function (e) {
                        e.iconElement.addEventListener('click', function () {
                            fv.resetField(e.field);
                        });
                    },
                }),
            },
        }).on('core.form.valid', function () { 
            
            var form = $("#demoForm");
            var url = form.attr('action');
            var mob = $('#mobile_number').val();
            var last3 = mob.slice(-3);

            file_data = $('#file').prop('files')[0]; //get the file
            form_data = new FormData(); //form_data is a FormData() object 
            form_data.append('file', file_data); //append the file in form_data object
            form_data.append('nominater_type',$('#nominater_type').val()); // e.g. other form data such as first_name, last_name will be stored as serialized
            form_data.append('first_name',$('#first_name').val());
            form_data.append('middle_name',$('#middle_name').val());
            form_data.append('last_name',$('#last_name').val());
            form_data.append('mobile_code',$('#mobile_code').val());
            form_data.append('mobile_number',$('#mobile_number').val());
            form_data.append('email',$('#email').val());
            form_data.append('inputDoc',$('#inputDoc').val());
            form_data.append('inputDocNum',$('#inputDocNum').val());
            form_data.append('regType',$('#regType').val());
            form_data.append('g-recaptcha-response',$('#g-recaptcha-response').val());
            //alert(form_data);

                $.ajax({
                method: 'POST',
                url: url,
                dataType: 'JSON',
                contentType: false,
                processData: false,
                data: form_data,
                success: function(data) {
                if (data.success==1)
                {
                $('#err').removeClass('alert');
                $('#err').removeClass('alert-warning');
                $("#err").html('');
                $('#otpHidden').html(data.res);
                $('#otpDisplay').html('<span>A code has been sent to</span> <small>*******'+ last3 +'</small>');
                $('#myModal').modal('show');
                $('.modal').css("z-index","9999");
                }
                else{
                    $('#err').addClass('alert alert-warning');
                    $("#err").html(data.res); 
                }
        },
        
        error: function(data) {
            // Some error in ajax call
            alert("some Error");
        }
            });
        });
    });
</script>
   
<script>
    document.addEventListener('DOMContentLoaded', function () {
        const demoForm = document.getElementById('demoForm1');
        const fv = FormValidation.formValidation(demoForm, {
            fields: {
                organisation_type: {
                    validators: {
                        notEmpty: {
                            message: 'Type of Organisation is required',
                        },
                    },
                },
                organisationName: {
                    validators: {
                        notEmpty: {
                            message: 'Name of Organisation is required',
                        },
                    },
                },
                    concernedPerson: {
                    validators: {
                        notEmpty: {
                            message: 'Name of the Concerned Person is required',
                        },
                    },
                }, 
                    designation: {
                    validators: {
                        notEmpty: {
                            message: 'Designation is required',
                        },
                    },
                },
                mobile_code: {
                    validators: {
                        notEmpty: {
                            message: 'The Code is required',
                        },
                    },
                },
                mobile_numbers: {
                    validators: {
                        integer: {
                                message: 'The value is not an integer.',
                        },
                        stringLength: {
                            min: 10,
                            max: 10,
                            message: 'Invalid Mobile Number.',
                        },
                        notEmpty: {
                            message: 'The Mobile Number is required.',
                        },
                    },
                },
                emails: {
                    validators: {
                        notEmpty: {
                            message: 'The email address is required',
                        },
                        emailAddress: {
                            message: 'The input is not a valid email address',
                        },
                    },
                },
                    state: {
                    validators: {
                        notEmpty: {
                            message: 'State / UT is required',
                        },
                    },
                },
                district: {
                    validators: {
                        notEmpty:{
                            message: 'District is required',
                        },
                    },
                },
                    inputDoc: {
                    validators: {
                        notEmpty: {
                            message: 'Select Identity Document to Upload is required',
                        },
                    },
                },
                    inputDocNum: {
                    validators: {
                        notEmpty: {
                            message: 'Identity Document Number is required',
                        },
                    },
                },
               
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                tachyons: new FormValidation.plugins.Tachyons(),
                submitButton: new FormValidation.plugins.SubmitButton(),
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh',
                    onPlaced: function (e) {
                        e.iconElement.addEventListener('click', function () {
                            fv.resetField(e.field);
                        });
                    },
                }),
            },
        }).on('core.form.valid', function () {

            var form = $("#demoForm1");
            var url = form.attr('action');
            var mob = $('#mobile_numbers').val();
            var last3 = mob.slice(-3);

            //var datastring =$('#demoForm1').serialize();
            file_data = $('#files').prop('files')[0]; //get the file
            form_data = new FormData(); //form_data is a FormData() object 
            form_data.append('files', file_data); //append the file in form_data object
            
            form_data.append('organisation_type',$('#organisation_type').val()); // e.g. other form data such as first_name, last_name will be stored as serialized
            form_data.append('organisation_dept',$('#organisation_dept').val());
            form_data.append('localbody_type',$('#localbody_type').val());
            form_data.append('localbodydis',$('#localbodydis').val());
            form_data.append('localbody_panchayath',$('#localbody_panchayath').val());
            form_data.append('localbody_localbodyBlkPan',$('#localbody_localbodyBlkPan').val());
            form_data.append('localbody_muncipality',$('#localbody_muncipality').val());
            form_data.append('organisationName',$('#organisationName').val());
            form_data.append('concernedPerson',$('#concernedPerson').val());
            form_data.append('designation',$('#designation').val());
            form_data.append('mobile_code',$('#mobile_code').val());
            form_data.append('mobile_numbers',$('#mobile_numbers').val());
            form_data.append('emails',$('#emails').val());
            form_data.append('otherContactDetail',$('#otherContactDetail').val());
            form_data.append('state',$('#state').val());
            form_data.append('district',$('#district').val());
            form_data.append('inputDoc',$('#inputDoc2').val());
            form_data.append('inputDocNum',$('#inputDocNum2').val());
            form_data.append('regType',$('#regType2').val());
            form_data.append('g-recaptcha-response',$('#g-recaptcha-response-1').val());

            $.ajax({
                method: 'POST',
                url: url,
                dataType: 'JSON',
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                success: function(data) {
                if (data.success==1)
                {
                    $('#err').removeClass('alert');
                    $('#err').removeClass('alert-warning');
                    $("#err").html('');
                    $('#otpHidden1').html(data.res);
                    $('#otpDisplay1').html('<span>A code has been sent to</span> <small>*******'+ last3 +'</small>');
                    $('#myModal1').modal('show');
                    $('.modal').css("z-index","9999");   
                } 
                else{
                    $('#err_org').addClass('alert alert-warning');
                    $("#err_org").html(data.res);
                }  
        },
        error: function(data) {
            
            // Some error in ajax call
            alert("some Error");
        }
            });
        });
    });
</script>
	
<script src="<?php echo act_url();?>public/js/chained/jquery.chained.js?v=1.0.0" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" charset="utf-8">
$(function() {

/* For jquery.chained.js */
$("#organisation_dept").chained("#organisation_type");
/* Show button after each pulldown has a value. */
    $("#localbodydis").chained("#localbody_type");
    $("#localbody_panchayath").chained("#localbodydis");
    $("#localbody_localbodyBlkPan").chained("#localbodydis");
    $("#localbody_muncipality").chained("#localbodydis");
$("#organisation_type").bind("change", function(event) {
    if ($("option:selected", $("#organisation_type")).val()=="1") {
        document.getElementById("ministry").style.display = "block";
    } else {
        document.getElementById("ministry").style.display = "none";
    }
    if ($("option:selected", $("#organisation_type")).val()=="6") {
        document.getElementById("localbody").style.display = "block";
    } else {
        document.getElementById("localbody").style.display = "none";
    }
});
$("#localbody_type").bind("change", function(event) {
    if ($("option:selected", $("#localbody_type")).val()=="3") {
        document.getElementById("panchayath").style.display = "block";
    } else {
        document.getElementById("panchayath").style.display = "none";
    }
    if ($("option:selected", $("#localbody_type")).val()=="2") {
        document.getElementById("BlkPan").style.display = "block";
    } else {
        document.getElementById("BlkPan").style.display = "none";
    }
    if ($("option:selected", $("#localbody_type")).val()=="5") {
        document.getElementById("muncipality").style.display = "block";
    } else {
        document.getElementById("muncipality").style.display = "none";
    }
    });
});
</script>
          
<script type="text/javascript" charset="utf-8">
$(function() {

/* Show button after each pulldown has a value. */
$("#state").bind("change", function(event) {
    if ($("option:selected", $("#state")).val()!="16") {
        document.getElementById("distClose").style.display = "none";
    } else {
        document.getElementById("distClose").style.display = "block";
    }
});
});
</script>
		  
<script>
$(document).ready(function(){
    $(document).on('change', '#file', function(){
        var name = document.getElementById("file").files[0].name;
        var form_data = new FormData();
        var ext = name.split('.').pop().toLowerCase();
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("file").files[0]);
        var f = document.getElementById("file").files[0];
        var fsize = f.size||f.fileSize;
        form_data.append("file", document.getElementById('file').files[0]);
    
        $.ajax({
            url:"<?php echo base_url();?>register/upload_file",
            method:"POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend:function(){
                $('#uploaded_image').html("<label class='text-success'>Image Uploading...</label>");
            },   
            success:function(data)
            {
                $('#uploaded_image').html(data);
            }
            });

        });
    });
</script>
<script>
$(document).ready(function(){
    $(document).on('change', '#files', function(){
        var name = document.getElementById("files").files[0].name;
        var form_data = new FormData();
        var ext = name.split('.').pop().toLowerCase();
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("files").files[0]);
        var f = document.getElementById("files").files[0];
        var fsize = f.size||f.fileSize;

        form_data.append("files", document.getElementById('files').files[0]);
        $.ajax({
        url:"<?php echo base_url();?>register/upload_files",
        method:"POST",
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend:function(){
            $('#uploaded_images').html("<label class='text-success'>Image Uploading...</label>");
        },   
        success:function(data)
        {
            $('#uploaded_images').html(data);
        }
    });
 
 });
});
</script>
<script>
function checkEmail() {
    $("#loaderIcon").show();
    jQuery.ajax({
        url: "<?php echo base_url();?>register/checkEmail",
        data:'email='+$("#email").val(),
        type: "POST",
        success:function(data){
        $("#user-availability-status").html(data);
        $("#loaderIcon").hide();

    },
    error:function (){}
    });
}
</script>
<script>
function checkEmailID() {
    $("#loaderIcons").show();

    jQuery.ajax({
        url: "<?php echo base_url();?>register/checkEmails",
        data:'email='+$("#emails").val(),
        type: "POST",
        success:function(data){
        $("#user-availability-statuss").html(data);
        $("#loaderIcons").hide();

    },
    error:function (){}
    });
}
</script>
<script>
function checkMobile() {
    $("#loaderMobileIcons").show();
    jQuery.ajax({
    url: "<?php echo base_url();?>register/checkMobile",
    data:'mobile='+$("#mobile_number").val(),
    type: "POST",
    success:function(data){
    $("#user-mobile-statuss").html(data);
    $("#loaderMobileIcons").hide();

    },
    error:function (){}
    });
}
</script>
<script>
function checkMobiles() {
    $("#loaderMobileIconss").show();
    jQuery.ajax({
    url: "<?php echo base_url();?>register/checkMobiles",
    data:'mobile='+$("#mobile_numbers").val(),
    type: "POST",
    success:function(data){
    $("#user-mobile-status").html(data);
    $("#loaderMobileIconss").hide();

    },
    error:function (){}
    });
}
</script>