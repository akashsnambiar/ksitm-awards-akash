<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="<?php echo act_url(); ?>public/images/favioc.png">
	<title>
		<?php echo (isset($Titletag) ? '' . $Titletag : '') . (isset($pagetitle) ? ' :: ' . $pagetitle : '') . (isset($site_name) ? ' :: ' . $site_name : ''); ?>
	</title>
	<meta name="Description" content="<?php echo (isset($metadescription) ? $metadescription : ''); ?>" />
	<meta name="KeyWords" content="<?php echo (isset($metakeywords) ? $metakeywords : ''); ?>" />
	<meta name="author" content="">
	<link rel="stylesheet" href="<?php echo act_url(); ?>public/css/login/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
</head>

<body class="nav-md">
	<style>
		#nom_receipt {
			font-family: Arial, Helvetica, sans-serif;
			width: 95%;
			font-size: 13px;
			margin: 5px;
		}

		#nom_receipt td {
			padding: 12px 30px;
			text-align: left;

		}

		.cust_lbl {
			width: 40%;
		}

		.prin_head {
			font-size: 20px;
		}

		#nom_receipt th {
			padding: 8px;
			text-align: center;
		}

		#nom_receipt th {
			padding-top: 12px;
			padding-bottom: 12px;
		}
	</style>
	<link rel="stylesheet" href="<?php echo act_url(); ?>public/css/css/nomination.css" />

	<div class="container-fluid" style="margin-top:50px;">
		<?php if (isset($ref['nominationRegID'])) {?>
		<table id="nom_receipt" style="border:1px solid black;">
			<tr>
				<td></td>
				<td><button style="float:right;" class="btn btn-default" onClick="window.print();">
						<i class="fa fa-print"></i> Print</button><br></td>
			</tr>
			<tr>
				<th colspan="2">
					<h4 class="prin_head"><strong>താങ്കളുടെ നാമനിർദേശം പൂർത്തിയായിരിക്കുന്നു</h4>
					<b>നാമനിർദ്ദേശത്തിൻ്റെ വിശദാംശങ്ങൾ</b>
				</th>
			</tr>


			<tr>
				<td class="cust_lbl"><b>Ref No :
						<?php echo $ref['nominationRegID']; ?><b></td>
				<td></td>
			</tr>
			<tr>
			<tr>
				<td class="cust_lbl"><b>പേര് : </b></td>
				<td>
					<?php echo $ref['name']; ?>
				</td>
			</tr>
			<tr>
				<td class="cust_lbl"><b>മേൽവിലാസം : </b></td>
				<td>
					<?php echo $ref['address']; ?>
				</td>
			</tr>
			<tr>
				<td class="cust_lbl"><b>പ്രവർത്തനമേഖല : </b></td>
				<td>
					<?php echo $ref['title']; ?>
				</td>
			</tr>
			<tr>
				<td class="cust_lbl"><b>ഏത് കേരള പുരസ്കാരത്തിനാണ് ശിപാർശ : </b></td>
				<td>
					<?php if ($ref['award'] == 1) { ?>കേരള ജ്യോതി
					<?php } else if ($ref['award'] == 2) { ?>കേരള പ്രഭ
					<?php } else if ($ref['award'] == 3) { ?> കേരള ശ്രീ
					<?php } ?>
				</td>
			</tr>
			<tr>
				<td class="cust_lbl"><b>നാമനിർദ്ദേശം ചെയ്യുന്ന ആളിൻ്റെ പേര് : </b></td>
				<td>
					<?php if ($ref['organisationName'] != '') {
						echo $ref['organisationName'];
					} else {
						echo $ref['fName'];
						echo $ref['lName'];
					}
					?>
				</td>
			</tr>
			<tr>
				<td class="cust_lbl"><b>ഡിജിറ്റലായി ഒപ്പിട്ടത്: </b></td>
				<td> <img src="<?php echo act_url(); ?>public/images/esign.jpg" width="50" height="50" />&nbsp;&nbsp;
					<?php echo $ref['esignname']; ?>
				</td>

				</td>
			</tr>

		</table>
<?php }?>
	</div>

</body>

</html>