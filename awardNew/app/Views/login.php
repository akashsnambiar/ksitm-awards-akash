<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo (isset($Titletag) ? ''.$Titletag :'').(isset($pagetitle) ? ' :: '.$pagetitle :'').(isset($site_name) ? ' :: ' .$site_name :'');?></title>
    <link rel="shortcut icon" href="<?php echo act_url();?>public/images/favioc.png"> 
    <link rel="stylesheet" href="<?php echo act_url(); ?>public/kp/css/bootstrap.min.css">
     <script src="<?php echo act_url() ?>public/js/js/bootstrap.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <link rel="stylesheet" href="<?php echo act_url(); ?>public/kp/css/login.css">
</head>
<body>
    <!-- Section: Design Block -->
    <section class="overflow-hidden mb-5">
        <div class="container  px-md-5 text-center text-lg-start my-5">
            <div class="row gx-lg-5 align-items-center mb-5">
                <div class="roller-img col-lg-6 mb-5 mb-lg-0 loginlogo" >
                    <ul>
                        <li class="img1"><img src="<?php echo act_url();?>public/images/keralajyothi.png" /></li>
                        <li></li>
                        <li class="img2"><img src="<?php echo act_url();?>public/images/keralaprabha.png" /></li>
                        <li></li>
                        <li></li>
                        <li class="img3"><img src="<?php echo act_url();?>public/images/keralashri.png" /></li>

                    </ul>
                </div>

                <div class="col-lg-5 mb-5 mb-lg-0 position-relative">
                    <div id="radius-shape-1" class="position-absolute rounded-circle shadow-5-strong"></div>
                    <div id="radius-shape-2" class="position-absolute shadow-5-strong"></div>

                    <div class="card bg-glass lognbox">
                        <div class="card-body px-md-5">
                        <div class="d-flex justify-content-center">
                            <img src="<?php echo act_url();?>public/images/login/logo.png" alt="login" class="login-card-img pb-3 imglogo">
                        </div>
                            <form id="demoForm" name="demoForm" method="POST" action="<?php echo base_url(); ?>login/loginCheck">
                                <?= csrf_field() ?>
                                <?php if (session()->getFlashdata('msg')) : ?>
                                    <div class="alert alert-warning">
                                        <?= session()->getFlashdata('msg') ?>
                                    </div>
                                <?php endif; ?>

                                <?php if (isset($validation)) : ?>
                                    <div class="alert alert-warning">
                                        <?= $validation->listErrors(); ?>
                                    </div>
                                <?php endif; ?>
                               
                                <!-- Email input -->
                                <div class="form-outline mb-4">
                                    <input autocomplete="off" type="text" name="username" id="username" class="form-control" placeholder="User Name">
                                </div>
                                <!-- Password input -->
                                <div class="form-outline mb-4">
                                    <input autocomplete="off" type="password" name="password" id="password" class="form-control" placeholder="***********">
                                </div>
                                <div class="g-recaptcha" data-sitekey="<?php echo $siteKey; ?>"></div> <br />

                                <!-- Submit button -->
                                <div class="d-grid gap-2">
                                    <button type="submit" class="btn btn-primary login-btn mb-4" name="Submit" id="Submit" value="Send">Login</button>
                                </div><?php if (isset( $nomperiod) && $nomperiod==1) {?>
                               
                                <p class="login-card-footer-text">Don't have an account? <a href="<?php echo base_url(); ?>register" class="register">Register here</a></p>
                                <?php }?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Section: Design Block -->
<!--------------- Footer --------------->
<footer class="fixed-bottom">
    <div class="footer-bottom text-center pt-4">
        <p>&copy; <?php echo (isset($copyright_year) ? $copyright_year : '');  ?> Government of Kerala. Developed & Maintained by <a href="https://itmission.kerala.gov.in/" target="_blank" class="footer-label">Kerala State IT Mission (KSITM)</a>.</p>

    </div>
</footer>
<!--------------- /Footer --------------->
</body>

</html>