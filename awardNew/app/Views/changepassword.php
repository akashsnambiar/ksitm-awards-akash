<?php echo view('includes/nomination/header');?>
  <main class="d-flex align-items-center min-vh-100 py-3 py-md-0">
    <div class="container">
      <div class="card login-card">
        <div class="row no-gutters">
          <div class="col-md-12">
            <div class="card-body">
              <div class="brand-wrapper log-class-menu">
              <img onclick="redirect('<?php echo base_url();?>')" src="<?php echo act_url();?>public/images/login/logo.png" alt="logo" class="logo">
                <a style="margin-left:10px;" class="login-menu" href="<?php echo base_url();?>login/logout">Logout</a>
                <a  class="login-menu" style="margin-left:10px; background-color:#679767;" href="<?php echo base_url();?>Account/changePassword">Change Password</a>
                <a class="login-menu" href="<?php echo base_url();?>nomination/myAccount"> Welcome, 
                  <?php if (isset($_SESSION['displayName']))
                  {
                    echo $_SESSION['displayName'];
                  }?></a>
                  </div> <hr/>
                <h2 class="login-card-title title-head">Change Password</h2>
                <?php if(session()->getFlashdata('msg')):?>
                            <div class="alert alert-warning">
                              <?= session()->getFlashdata('msg') ?>
                            </div>
                        <?php endif;?>
                <?php if(isset($validation)):?>
                <div class="alert alert-warning">
                   <?= $validation->listErrors() ?>
                </div>
                <?php endif;?>
                <form autocomplete="off" id="demoForm" method="POST" action="<?php echo base_url();?>Account/save"  style="max-width:100%;">
                <?= csrf_field() ?>	
                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="form-group fl">
                        <label for="exampleFormControlSelect1">Type Old password<span class="mandatory">*</span></label>
                        </div>
                      </div>
                        <div class="col-md-6">
                        <div class="form-group fl">
                          <input type="password" class="form-control ba b--black-20 pa2 mb2 db" id="oldpassword" name="oldpassword" placeholder="Enter Password" style="margin-bottom:4px;" value="<?php echo (isset($oldpassword)) ? $oldpassword :set_value('oldpassword'); ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="form-group fl">
                        <label for="exampleFormControlSelect1">Type New password<span class="mandatory">*</span></label>
                        </div>
                      </div>
                        <div class="col-md-6">
                        <div class="form-group fl">
                          <input type="password" class="form-control ba b--black-20 pa2 mb2 db" id="newpassword" name="newpassword" placeholder="Enter New Password" style="margin-bottom:4px;"  value="<?php echo (isset($newpassword)) ? $newpassword :set_value('newpassword'); ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="form-group fl">
                        <label for="exampleFormControlSelect1">Confirm New password<span class="mandatory">*</span></label>
                        </div>
                      </div>
                        <div class="col-md-6">
                        <div class="form-group fl">
                          <input type="password" class="form-control ba b--black-20 pa2 mb2 db" id="confirmpassword" name="confirmpassword" placeholder="Retype New Password" style="margin-bottom:4px;"  value="<?php echo (isset($confirmpassword)) ? $confirmpassword :set_value('confirmpassword'); ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group fl">
                        <button style="float:right;padding: 0.5rem 1.5rem; margin-top:31px;" type="submit" class="btn btn-primary" name="Submit" id="Submit" value="Send">Submit</button>
                      </div>
                      </div>
                        <div class="col-md-6">
                         </div></div>
                      </div>
                    </div>
                  </div>
                </form>
         <?php echo view('includes/nomination/footer');?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
</body>
</html>

