<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>കേരള പുരസ്‌കാരങ്ങള്‍</title>
 

  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo act_url();  ?>public/vendors/feather/feather.css">
  <link rel="stylesheet" href="<?php echo act_url();  ?>public/vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo act_url();  ?>public/vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" href="<?php echo act_url();  ?>public/vendors/typicons/typicons.css">
  <link rel="stylesheet" href="<?php echo act_url();  ?>public/vendors/simple-line-icons/css/simple-line-icons.css">
  <link rel="stylesheet" href="<?php echo act_url();  ?>public/vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  
  <link rel="stylesheet" href="<?php echo act_url();  ?>public/css/vertical-layout-light/style.css">

  <!-- Plugin css for this page -->
  
  <!-- End plugin css for this page -->
 
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo act_url();  ?>public/images/favioc.png" />
  <script src='https://www.google.com/recaptcha/api.js'></script>

  <link href="<?php echo act_url() ?>public/css/bootstrap.min.css" rel="stylesheet" >
  <script src="<?php echo act_url() ?>public/js/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo act_url() ?>public/js/js/popper.min.js"></script>
  <script src="<?php echo act_url() ?>public/js/js/bootstrap.min.js"></script>

  <script src="<?php echo act_url() ?>public/kp/js/script.js"></script>


  <link rel="stylesheet" href="<?php echo act_url(); ?>public/kp/css/MaterialDesign-Webfont-master/css/materialdesignicons.min.css"  />
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/7.1.96/css/materialdesignicons.min.css" integrity="sha512-NaaXI5f4rdmlThv3ZAVS44U9yNWJaUYWzPhvlg5SC7nMRvQYV9suauRK3gVbxh7qjE33ApTPD+hkOW78VSHyeg==" crossorigin="anonymous" referrerpolicy="no-referrer" /> -->

  <!-- <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script> -->

  <script src="<?php echo act_url(); ?>public/js/ckeditor5-build-classic/ckeditor.js"></script>
  <link rel="stylesheet" href="<?php echo act_url(); ?>public/kp/css/fontawesome-free-6.4.2-web/css/all.min.css" />
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ==" crossorigin="anonymous" referrerpolicy="no-referrer" /> -->

  <script type="text/javascript" language="javascript" src="<?php echo act_url(); ?>public/js/js/jquery-3.7.0.min.js"></script>

  <link rel="stylesheet" href="<?php echo act_url(); ?>public/js/datatables/css/bootstrap.css"  />
  <link rel="stylesheet" href="<?php echo act_url(); ?>public/js/datatables/css/bootstrap.min.css"  />

  
</head>
