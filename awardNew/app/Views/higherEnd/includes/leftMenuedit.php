<style>
  .mdi-circle {
    margin-right: 6px;
    font-size: 0.5em;
  }

  ul {
    list-style-type: none;
  }

  .active {
    color: blue;

  }

  .sticky {
    position: relative;
    position: -webkit-sticky;
    position: sticky;
    top: 0;
  }

  .dropdown-item.active {
    background-color: green;
    position: absolute;
    top: 0;
    width: 100%;
    color: #000;

  }

  .sidebar .nav .nav-item .dropdown-item.show {
    display: block;
  }
</style>
<div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="index.html">
              <i class="mdi mdi-grid-large menu-icon"></i>
              <span class="menu-title">Home</span>
            </a>
          </li>
          <li class="nav-item has-submenu">
            <a class="nav-link" data-bs-toggle="collapse" href="#form-elements" aria-expanded="false" aria-controls="form-elements">
              <i class="menu-icon mdi mdi-card-text-outline"></i>
              <span class="menu-title">Site Management</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="form-elements">
              <ul class="submenu nav flex-column sub-menu">
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>admin/homecontent">Home Content</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>admin/aboutus">About Us</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>admin/site/keralasree">Kerala Shri</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>admin/site/keralajyothi">Kerala Jyothi</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>admin/site/keralaprabha">Kerala Prabha</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>admin/notification">Notifications</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>admin/media">Media</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>admin/gallerycategory">Gallery Category</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>admin/gallery">Gallery</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>admin/contactus">Contact Us</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>admin/whocanapply">Who Can Apply</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>admin/contribution">Awards for Outstanding<br/> Contribution</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>admin/whodecides">Who Decides</a></li>
              </ul>
            </div>
            
          </li>
          <li class="nav-item has-submenu">
            <a class="nav-link" data-bs-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
              <i class="menu-icon mdi mdi-account-circle-outline"></i>
              <span class="menu-title">Registration</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="auth">
              <ul class="submenu nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>admin/individual"> Individual </a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>admin/organisation"> Organisation </a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item has-submenu">
            <a class="nav-link" data-bs-toggle="collapse" href="#new" aria-expanded="false" aria-controls="new">
              <i class="menu-icon mdi mdi-layers-outline"></i>
              <span class="menu-title">New Nominations</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="new">
              <ul class="submenu nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>admin/keralajyothi/1">Kerala Jyothi</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>admin/keralaprabha/1">kerala Prabha</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>admin/keralasree/1">kerala shri</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item has-submenu">
            <a class="nav-link" data-bs-toggle="collapse" href="#accepted" aria-expanded="false" aria-controls="accepted">
              <i class="menu-icon mdi mdi-layers-outline"></i>
              <span class="menu-title">Accepted Nominations</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="accepted">
              <ul class="submenu nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>admin/keralajyothi/accepted/2">Kerala Jyothi</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>admin/keralaprabha/accepted/2">kerala Prabha</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>admin/keralasree/accepted/2">kerala shri</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item has-submenu">
            <a class="nav-link" data-bs-toggle="collapse" href="#rejected" aria-expanded="false" aria-controls="rejected">
              <i class="menu-icon mdi mdi-layers-outline"></i>
              <span class="menu-title">Rejected Nominations</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="rejected">
              <ul class="submenu nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>admin/keralajyothi/rejected/3">Kerala Jyothi</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>admin/keralaprabha/rejected/3">kerala Prabha</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>admin/keralasree/rejected/3">kerala shri</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </nav>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
  <script>
    document.addEventListener("DOMContentLoaded", function() {
      document.querySelectorAll('.sidebar .nav-link').forEach(function(element) {

        element.addEventListener('click', function(e) {

          let nextEl = element.nextElementSibling;
          let parentEl = element.parentElement;

          if (nextEl) {
            e.preventDefault();
            let mycollapse = new bootstrap.Collapse(nextEl);

            if (nextEl.classList.contains('show')) {
              mycollapse.hide();
            } else {
              mycollapse.show();
              // find other submenus with class=show
              var opened_submenu = parentEl.parentElement.querySelector('.submenu.show');
              // if it exists, then close all of them
              if (opened_submenu) {
                new bootstrap.Collapse(opened_submenu);
              }
            }
          }
        }); // addEventListener
      }) // forEach
    });
    // DOMContentLoaded  end
  </script>
      <script src="<?php echo act_url(); ?>public/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="<?php echo act_url(); ?>public/vendors/chart.js/Chart.min.js"></script>
  <script src="<?php echo act_url(); ?>public/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
  <script src="<?php echo act_url(); ?>public/vendors/progressbar.js/progressbar.min.js"></script>

  <script src="<?php echo act_url(); ?>public/js/adminjs/off-canvas.js"></script>
	<script src="<?php echo act_url(); ?>public/js/adminjs/hoverable-collapse.js"></script>
	<script src="<?php echo act_url(); ?>public/js/adminjs/template.js"></script>
	<script src="<?php echo act_url(); ?>public/js/adminjs/settings.js"></script>
	<script src="<?php echo act_url(); ?>public/js/adminjs/todolist.js"></script>
  <script src="<?php echo act_url(); ?>public/js/adminjs/dashboard.js"></script>