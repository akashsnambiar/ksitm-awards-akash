<style>
  .mdi-circle {
    margin-right: 6px;
    font-size: 0.5em;
  }

  ul {
    list-style-type: none;
  }

  .active {
    color: blue;
    background-color: #e6e6e6;
  }
</style>
<!-- <div class="container-fluid page-body-wrapper"> -->
<div class="page-body-wrapper">
  <!-- partial:partials/_sidebar.html -->
  <nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item ">
        <a class="nav-link" href="<?php echo base_url(); ?>higherEnd/dashboard">
          <i class="menu-icon mdi mdi-home"></i>
          <span class="menu-title">Home</span>
        </a>
      </li>
      <?php if ($_SESSION['AdminRole'] == 'admin') { ?>

        <li class="nav-item ">
          <a class="nav-link" href="<?php echo base_url(); ?>higherEnd/nominationperiod">
            <i class="menu-icon mdi mdi-calendar-range"></i>
            <span class="menu-title">Nomination Period</span>
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="<?php echo base_url(); ?>higherEnd/banner">
            <i class="menu-icon mdi mdi-image-filter-none"></i>
            <span class="menu-title">Banner</span>
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="<?php echo base_url(); ?>higherEnd/winners">
            <i class="menu-icon mdi mdi-trophy-variant"></i>
            <span class="menu-title">Winners</span>
          </a>
        </li>
        <li class="nav-item has-submenu">
          <a class="nav-link" href="#">
            <i class="menu-icon mdi mdi-layers-outline"></i>
            <span class="menu-title">Site Management</span>
            <i class="menu-arrow "></i> </a>
          <ul class="submenu collapse sub-menu">
            <li class="dropdown-item"><a class="nav-link" id="nav-link" href="<?php echo base_url(); ?>higherEnd/homecontent"><span class="mdi mdi-circle"></span>Home Content</a></li>
            <li class="dropdown-item"><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/aboutus"><span class="mdi mdi-circle"></span>About Us</a></li>
            <li class="dropdown-item"><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/site/keralashri"><span class="mdi mdi-circle"></span>Kerala Shri</a></li>
            <li class="dropdown-item"><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/site/keralaJyothi"><span class="mdi mdi-circle"></span>Kerala Jyothi</a></li>
            <li class="dropdown-item"><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/site/keralaPrabha"><span class="mdi mdi-circle"></span>Kerala Prabha</a></li>
            <li class="dropdown-item"><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/notification"><span class="mdi mdi-circle"></span>Notifications</a></li>
            <li class="dropdown-item"><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/media"><span class="mdi mdi-circle"></span>Media</a></li>
            <li class="dropdown-item"><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/galleryCategory"><span class="mdi mdi-circle"></span>Award Category</a></li>
            <li class="dropdown-item"><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/galleryList"><span class="mdi mdi-circle"></span>Award Ceremony</a></li>
            <li class="dropdown-item"><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/contactus"><span class="mdi mdi-circle"></span>Contact Us</a></li>
            <li class="dropdown-item"><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/whocanapply"><span class="mdi mdi-circle"></span>Who Can Apply</a></li>
            <li class="dropdown-item"><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/contribution"><span class="mdi mdi-circle"></span>Awards for Outstanding<br /> Contribution</a></li>
            <li class="dropdown-item"><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/whodecides"><span class="mdi mdi-circle"></span>Who Decides</a></li>

          </ul>
        </li>
        <li class="nav-item has-submenu">
          <a class="nav-link" href="#">
            <i class="menu-icon mdi mdi-account-circle-outline"></i>
            <span class="menu-title">Registration</span>
            <i class="menu-arrow "></i> </a>
          <ul class="submenu collapse sub-menu">
            <li><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/individual"><span class="mdi mdi-circle"></span>Individual</a></li>
            <li><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/organisation"><span class="mdi mdi-circle"></span> Organisation</a></li>
          </ul>
        </li>
        <li class="nav-item has-submenu">
          <a class="nav-link" href="#">
            <i class="menu-icon mdi mdi-account-multiple-plus"></i>
            <span class="menu-title">New Nominations</span>
            <i class="menu-arrow "></i> </a>
          <ul class="submenu collapse sub-menu">
            <li><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/keralaj/kjn"><span class="mdi mdi-circle"></span>Kerala Jyothi</a></li>
            <li><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/keralaprabhacat/kpn"><span class="mdi mdi-circle"></span>kerala Prabha</a></li>
            <li><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/keralashri/ksn"><span class="mdi mdi-circle"></span>kerala shri</a> </li>
          </ul>
        </li>
        <li class="nav-item has-submenu">
          <a class="nav-link" href="#">
            <i class="menu-icon mdi mdi-checkbox-marked"></i>
            <span class="menu-title">Accepted Nominations</span>
            <i class="menu-arrow "></i> </a>
          <ul class="submenu collapse sub-menu">
            <li><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/keralajyothi/accepted/kja"><span class="mdi mdi-circle"></span>Kerala Jyothi</a></li>
            <li><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/keralaprabha/accepted/kpa"><span class="mdi mdi-circle"></span>kerala Prabha</a></li>
            <li><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/keralasree/accepted/ksa"><span class="mdi mdi-circle"></span>kerala shri</a> </li>
          </ul>
        </li>
        <li class="nav-item has-submenu">
          <a class="nav-link" href="#">
            <i class="menu-icon mdi mdi-close-circle-outline"></i>
            <span class="menu-title">Rejected Nominations</span>
            <i class="menu-arrow "></i> </a>
          <ul class="submenu collapse sub-menu">
            <li><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/keralajyothi/rejected/kjr"><span class="mdi mdi-circle"></span>Kerala Jyothi</a></li>
            <li><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/keralaprabha/rejected/kpr"><span class="mdi mdi-circle"></span>kerala Prabha</a></li>
            <li><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/keralasree/rejected/ksr"><span class="mdi mdi-circle"></span>kerala shri</a> </li>
          </ul>
        </li>
      <?php } else { ?>
        <li class="nav-item has-submenu">
          <a class="nav-link" href="#">
            <i class="menu-icon mdi mdi-checkbox-marked"></i>
            <span class="menu-title">Accepted Nominations</span>
            <i class="menu-arrow "></i> </a>
          <ul class="submenu collapse sub-menu">
            <li><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/keralajyothi/accepted/kja"><span class="mdi mdi-circle"></span>Kerala Jyothi</a></li>
            <li><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/keralaprabha/accepted/kpa"><span class="mdi mdi-circle"></span>kerala Prabha</a></li>
            <li><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/keralasree/accepted/ksa"><span class="mdi mdi-circle"></span>kerala shri</a> </li>
          </ul>
        </li>
        <li class="nav-item has-submenu">
          <a class="nav-link" href="#">
            <i class="menu-icon mdi mdi-close-circle-outline"></i>
            <span class="menu-title">Rejected Nominations</span>
            <i class="menu-arrow "></i> </a>
          <ul class="submenu collapse sub-menu">
            <li><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/keralajyothi/rejected/kjr"><span class="mdi mdi-circle"></span>Kerala Jyothi</a></li>
            <li><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/keralaprabha/rejected/kpr"><span class="mdi mdi-circle"></span>kerala Prabha</a></li>
            <li><a class="nav-link" href="<?php echo base_url(); ?>higherEnd/keralasree/rejected/ksr"><span class="mdi mdi-circle"></span>kerala shri</a> </li>
          </ul>
        </li>
      <?php } ?>
    </ul>

  </nav>
  <script>
    document.addEventListener("DOMContentLoaded", function() {
      document.querySelectorAll('.sidebar .nav-link').forEach(function(element) {

        element.addEventListener('click', function(e) {

          let nextEl = element.nextElementSibling;
          let parentEl = element.parentElement;

          if (nextEl) {
            e.preventDefault();
            let mycollapse = new bootstrap.Collapse(nextEl);

            if (nextEl.classList.contains('show')) {
              mycollapse.hide();
            } else {
              mycollapse.show();
              $(this).addClass('active');
              // find other submenus with class=show
              var opened_submenu = parentEl.parentElement.querySelector('.submenu.show');
              // if it exists, then close all of them
              if (opened_submenu) {
                new bootstrap.Collapse(opened_submenu);
                $(this).addClass('active');
              }
            }
          }
        }); // addEventListener
      }) // forEach
    });
    // DOMContentLoaded  end
  </script>

  <script src="<?php echo act_url(); ?>public/vendors/js/vendor.bundle.base.js"></script>
  <script src="<?php echo act_url(); ?>public/vendors/chart.js/Chart.min.js"></script>
  <script src="<?php echo act_url(); ?>public/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
  <script src="<?php echo act_url(); ?>public/vendors/progressbar.js/progressbar.min.js"></script>

  <script src="<?php echo act_url(); ?>public/js/adminjs/off-canvas.js"></script>
  <script src="<?php echo act_url(); ?>public/js/adminjs/hoverable-collapse.js"></script>
  <script src="<?php echo act_url(); ?>public/js/adminjs/template.js"></script>
  <script src="<?php echo act_url(); ?>public/js/adminjs/settings.js"></script>
  <script src="<?php echo act_url(); ?>public/js/adminjs/todolist.js"></script>
  <script src="<?php echo act_url(); ?>public/js/adminjs/dashboard.js"></script>