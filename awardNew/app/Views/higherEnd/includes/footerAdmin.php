 <!-- content-wrapper ends -->
 <!-- partial:partials/_footer.html -->
 <footer class="footer" style="margin-left: 0; margin-bottom:0">
   <div class="d-flex justify-content-end ">
     <span class="text-muted text-center text-sm-left d-block d-sm-inline-block"><a href="http://www.itmission.kerala.gov.in/" target="_blank"><img src="<?php echo act_url(); ?>public/images/itmission.png" width="30" /></a> </span>
     <span class="float-none float-sm-right d-block mt-1 text-center">Copyright © <?php echo $copyright_year; ?>. All rights reserved.</span>
   </div>
 </footer>
 <!-- partial -->
 </div>
 <!-- main-panel ends -->
 </div>
 <!-- page-body-wrapper ends -->
 </div>
 <!-- container-scroller -->