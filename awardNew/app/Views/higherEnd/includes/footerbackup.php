 <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block"><a href="http://www.itmission.kerala.gov.in/" target="_blank"><img src="<?php echo act_url(); ?>public/images/itmission.png" width="40"/></a> </span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Copyright © <?= $copyrightYear;?>. All rights reserved.</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="<?php echo act_url(); ?>public/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="<?php echo act_url(); ?>public/vendors/chart.js/Chart.min.js"></script>
  <script src="<?php echo act_url(); ?>public/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
  <script src="<?php echo act_url(); ?>public/vendors/progressbar.js/progressbar.min.js"></script>

  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <!-- <script src="<?php // echo act_url(); ?>public/js/off-canvas.js"></script>
  <script src="<?php // echo act_url(); ?>public/js/hoverable-collapse.js"></script>
  <script src="<?php //echo act_url(); ?>public/js/template.js"></script>
  <script src="<?php //echo act_url(); ?>public/js/settings.js"></script>
  <script src="<?php //echo act_url(); ?>public/js/todolist.js"></script> -->
  <!-- endinject -->
  <!-- Custom js for this page-->
  <!-- <script src="<?php //echo act_url(); ?>public/js/jquery.cookie.js" type="text/javascript"></script>
  <script src="<?php //echo act_url(); ?>public/js/dashboard.js"></script>
  <script src="<?php //echo act_url(); ?>public/js/Chart.roundedBarCharts.js"></script> -->
  <!-- End custom js for this page-->
  <script>
  (function($) {
  'use strict';
  $(function() {
      if ($("#doughnutNewChart").length) {
      var doughnutChartCanvas = $("#doughnutNewChart").get(0).getContext("2d");
      var doughnutPieData = {
        datasets: [{
          data: [<?= $nominationNewKeralajyothi;?>, <?= $nominationNewKeralaprabha;?>, <?= $nominationNewKeralashri;?>],
          backgroundColor: [
            "#FDD0C7",
            "#52CDFF",
            "#81DADA"
          ],
          borderColor: [
            "#FDD0C7",
            "#52CDFF",
            "#81DADA"
          ],
        }],
  
        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
          'കേരള ജ്യോതി',
          'കേരള പ്രഭ',
          'കേരള ശ്രീ',
        ]
      };
      var doughnutPieOptions = {
        cutoutPercentage: 50,
        animationEasing: "easeOutBounce",
        animateRotate: true,
        animateScale: false,
        responsive: true,
        maintainAspectRatio: true,
        showScale: true,
        legend: false,
        legendCallback: function (chart) {
          var text = [];
          text.push('<div class="chartjs-legend"><ul class="justify-content-center">');
          for (var i = 0; i < chart.data.datasets[0].data.length; i++) {
            text.push('<li><span style="background-color:' + chart.data.datasets[0].backgroundColor[i] + '">');
            text.push('</span>');
            if (chart.data.labels[i]) {
              text.push(chart.data.labels[i]);
            }
            text.push('</li>');
          }
          text.push('</div></ul>');
          return text.join("");
        },
        
        layout: {
          padding: {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
          }
        },
        tooltips: {
          callbacks: {
            title: function(tooltipItem, data) {
              return data['labels'][tooltipItem[0]['index']];
            },
            label: function(tooltipItem, data) {
              return data['datasets'][0]['data'][tooltipItem['index']];
            }
          },
            
          backgroundColor: '#fff',
          titleFontSize: 14,
          titleFontColor: '#0B0F32',
          bodyFontColor: '#737F8B',
          bodyFontSize: 11,
          displayColors: false
        }
      };
      var doughnutChart = new Chart(doughnutChartCanvas, {
        type: 'doughnut',
        data: doughnutPieData,
        options: doughnutPieOptions
      });
      document.getElementById('doughnutnew-chart-legend').innerHTML = doughnutChart.generateLegend();
    }
    if ($("#doughnutAccChart").length) {
      var doughnutChartCanvas = $("#doughnutAccChart").get(0).getContext("2d");
      var doughnutPieData = {
        datasets: [{
          data: [<?= $nominationAcceptedKeralajyothi;?>, <?= $nominationAcceptedKeralaprabha;?>, <?= $nominationAcceptedKeralashri;?>],
          backgroundColor: [
            "#FDD0C7",
            "#52CDFF",
            "#81DADA"
          ],
          borderColor: [
            "#FDD0C7",
            "#52CDFF",
            "#81DADA"
          ],
        }],
  
        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
          'കേരള ജ്യോതി',
          'കേരള പ്രഭ',
          'കേരള ശ്രീ',
        ]
      };
      var doughnutPieOptions = {
        cutoutPercentage: 50,
        animationEasing: "easeOutBounce",
        animateRotate: true,
        animateScale: false,
        responsive: true,
        maintainAspectRatio: true,
        showScale: true,
        legend: false,
        legendCallback: function (chart) {
          var text = [];
          text.push('<div class="chartjs-legend"><ul class="justify-content-center">');
          for (var i = 0; i < chart.data.datasets[0].data.length; i++) {
            text.push('<li><span style="background-color:' + chart.data.datasets[0].backgroundColor[i] + '">');
            text.push('</span>');
            if (chart.data.labels[i]) {
              text.push(chart.data.labels[i]);
            }
            text.push('</li>');
          }
          text.push('</div></ul>');
          return text.join("");
        },
        
        layout: {
          padding: {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
          }
        },
        tooltips: {
          callbacks: {
            title: function(tooltipItem, data) {
              return data['labels'][tooltipItem[0]['index']];
            },
            label: function(tooltipItem, data) {
              return data['datasets'][0]['data'][tooltipItem['index']];
            }
          },
            
          backgroundColor: '#fff',
          titleFontSize: 14,
          titleFontColor: '#0B0F32',
          bodyFontColor: '#737F8B',
          bodyFontSize: 11,
          displayColors: false
        }
      };
      var doughnutChart = new Chart(doughnutChartCanvas, {
        type: 'doughnut',
        data: doughnutPieData,
        options: doughnutPieOptions
      });
      document.getElementById('doughnutacc-chart-legend').innerHTML = doughnutChart.generateLegend();
    }
    if ($("#doughnutRejChart").length) {
      var doughnutChartCanvas = $("#doughnutRejChart").get(0).getContext("2d");
      var doughnutPieData = {
        datasets: [{
          data: [<?= $nominationRejectedKeralajyothi;?>, <?= $nominationRejectedKeralaprabha;?>, <?= $nominationRejectedKeralashri;?>],
          backgroundColor: [
            "#FDD0C7",
            "#52CDFF",
            "#81DADA"
          ],
          borderColor: [
            "#FDD0C7",
            "#52CDFF",
            "#81DADA"
          ],
        }],
  
        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
          'കേരള ജ്യോതി',
          'കേരള പ്രഭ',
          'കേരള ശ്രീ',
        ]
      };
      var doughnutPieOptions = {
        cutoutPercentage: 50,
        animationEasing: "easeOutBounce",
        animateRotate: true,
        animateScale: false,
        responsive: true,
        maintainAspectRatio: true,
        showScale: true,
        legend: false,
        legendCallback: function (chart) {
          var text = [];
          text.push('<div class="chartjs-legend"><ul class="justify-content-center">');
          for (var i = 0; i < chart.data.datasets[0].data.length; i++) {
            text.push('<li><span style="background-color:' + chart.data.datasets[0].backgroundColor[i] + '">');
            text.push('</span>');
            if (chart.data.labels[i]) {
              text.push(chart.data.labels[i]);
            }
            text.push('</li>');
          }
          text.push('</div></ul>');
          return text.join("");
        },
        
        layout: {
          padding: {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
          }
        },
        tooltips: {
          callbacks: {
            title: function(tooltipItem, data) {
              return data['labels'][tooltipItem[0]['index']];
            },
            label: function(tooltipItem, data) {
              return data['datasets'][0]['data'][tooltipItem['index']];
            }
          },
            
          backgroundColor: '#fff',
          titleFontSize: 14,
          titleFontColor: '#0B0F32',
          bodyFontColor: '#737F8B',
          bodyFontSize: 11,
          displayColors: false
        }
      };
      var doughnutChart = new Chart(doughnutChartCanvas, {
        type: 'doughnut',
        data: doughnutPieData,
        options: doughnutPieOptions
      });
      document.getElementById('doughnutrej-chart-legend').innerHTML = doughnutChart.generateLegend();
    }
  });
})(jQuery);
  </script>
</body>

</html>

