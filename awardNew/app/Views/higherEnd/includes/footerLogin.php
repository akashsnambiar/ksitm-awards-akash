<!-- page-body-wrapper ends -->
<footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block"><a href="http://www.itmission.kerala.gov.in/" target="_blank"><img src="<?php echo act_url(); ?>public/images/itmission.png" width="40" /></a> </span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Copyright © <?php echo $copyright_year;?>. All rights reserved.</span>
          </div>
        </footer>
  </div>
  <!-- container-scroller -->
<!-- plugins:js -->
<script src="<?php echo act_url(); ?>public/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="<?php echo act_url(); ?>public/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <!-- <script src="http://localhost/myProject/awardNew/public/js/off-canvas.js"></script>
  <script src="http://localhost/myProject/awardNew/public/js/hoverable-collapse.js"></script>
  <script src="http://localhost/myProject/awardNew/public/js/template.js"></script>
  <script src="http://localhost/myProject/awardNew/public/js/settings.js"></script>
  <script src="http://localhost/myProject/awardNew/public/js/todolist.js"></script> -->
  <!-- endinject -->
</body>

</html>
