<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>
<script src='https://www.google.com/recaptcha/api.js'></script>

<body>
    <div class="container">
        <div class="col-lg-12 col-md-12">

            <!-- page content -->

            <div class="right_col" role="main">
                <div class="mt-5">
                    <!-- <div class="page-title mt-3">
                        <div class="title_left">
                            <h4>Control Panel for <?php echo $site_name; ?></h4><br />
                        </div>
                    </div> -->
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <!-- Error -->
                                <?php if (isset($validation)) : ?>
                                    <div class="alert alert-warning">
                                        <?= $validation->listErrors() ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (isset($error)) : ?>
                                    <div class="alert alert-warning">
                                        <?= $error ?>
                                    </div>
                                <?php endif; ?>
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Banner</h5>
                                    </div>
                                    <?php if(isset($banner['id'])){ ?>
                                    <form class="form-horizontal p-5" name="formList" id="formList" method="POST" action="<?php echo base_url(); ?>higherEnd/banedit/<?php echo encode_url($banner['id']); ?>" enctype="multipart/form-data">
                                    <?= csrf_field() ?>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Banner Title[Malayalam]</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="title" type="text" name="title" value="<?php echo (isset($banner['banner_title_mal'])) ? $banner['banner_title_mal'] : set_value('title'); ?>" placeholder="Title [Malayalam]" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;"></p>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Banner Title[English]</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="title" type="text" name="title_e" value="<?php echo (isset($banner['banner_title_en'])) ? $banner['banner_title_en'] : set_value('title_e'); ?>" placeholder="Title [Malayalam]" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;"></p>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Year</label>
                                            <div class="col-sm-7">
                                                <select name="banyear" value="<?php echo (isset($banner['year'])) ? $banner['year'] : set_value('banyear'); ?>" class="form-select" aria-placeholder="Select">
                                                <option value="<?php echo $banner['year'];  ?>" hidden><?php echo (isset($banner['year'])) ? $banner['year'] : set_value('banyear'); ?></option>
                                                    <option value="2022">2022</option>
                                                    <option value="2023">2023</option>
                                                    <option value="2024">2024</option>
                                                    <option value="2025">2025</option>
                                                    <option value="2026">2026</option>
                                                    <option value="2027">2027</option>
                                                    <option value="2028">2028</option>
                                                    <option value="2029">2029</option>
                                                    <option value="2030">2030</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Banner Category</label>
                                            <div class="col-sm-7">
                                                <select name="title_cat" class="form-select" value="<?php echo (isset($banner['nomination_category'])) ? $banner['nomination_category'] : set_value('title_cat'); ?>" >
                                                <option value="<?php echo $banner['nid'];  ?>" hidden><?php echo (isset($banner['nomination_category'])) ? $banner['nomination_category'] : set_value('title_cat'); ?></option>
                                                    <?php
                                                    if (isset($nomcat)) {
                                                        foreach ($nomcat as $row) {  ?>
                                                            <option value="<?php echo $row['id'];  ?>"><?php echo $row['nomination_category'];  ?></option>
                                                    <?php }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Award Image</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="photo" type="file" name="file" value="<?php echo (isset($banner['category_image'])) ? $banner['category_image'] : set_value('file'); ?>" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;">We request you not to keep your image title field empty, Upload image leass than 2MB</p>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-4">
                                                <div class="g-recaptcha" data-sitekey="<?php echo $siteKey; ?>"></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <button style="margin-top:30px;padding: 10px 30px;" type="submit" class="btn btn-primary" name="Submit" value="Send">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                    <?php  }else{ ?>
                                        <div class="text-center" >
                                        <h4 class="text-center">No Records</h4>
                                        </div>
                                   <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <!-- /page content -->
    </div>
    </div>
    <!-- footer content -->
    <?php echo view('higherEnd/includes/footerAdmin'); ?>
    <!-- /footer content -->
</body>

</html>