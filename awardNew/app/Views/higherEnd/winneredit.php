<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>
<script src='https://www.google.com/recaptcha/api.js'></script>

<body>
    <div class="container">
        <div class="col-lg-12 col-md-12">

            <!-- page content -->

            <div class="right_col" role="main">
                <div class="mt-5">
                    <!-- <div class="page-title mt-3">
                        <div class="title_left">
                            <h4>Control Panel for <?php echo $site_name; ?></h4><br />
                        </div>
                    </div> -->
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <!-- Error -->
                                <?php if (isset($validation)) : ?>
                                    <div class="alert alert-warning">
                                        <?= $validation->listErrors() ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (isset($error)) : ?>
                                    <div class="alert alert-warning">
                                        <?= $error ?>
                                    </div>
                                <?php endif; ?>
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Winners &nbsp;› <small>Edit</small></h5>
                                    </div>
                                    <?php if (isset($winner['id'])) { ?>
                                        <form class="form-horizontal p-5" name="formList" id="formList" method="POST" action="<?php echo base_url(); ?>higherEnd/winedit/<?php echo encode_url($winner['id']); ?>" enctype="multipart/form-data">
                                            <?= csrf_field() ?>
                                            <div class="row form-group">
                                                <label class="col-sm-3 control-label">Banner Title[Malayalam]</label>
                                                <div class="col-sm-7">
                                                    <select id="banner_title_mal" name="banner_title_mal" value="<?php echo (isset($winner['banner_title_mal'])) ? $winner['banner_title_mal'] : set_value('banner_title_mal'); ?>" class="form-select" id="title-cat-mal">
                                                        <option value="<?php echo $winner['banner_title_id'];  ?>" hidden><?php echo (isset($winner['banner_title_mal'])) ? $winner['banner_title_mal'] : set_value('banner_title_mal'); ?></option>
                                                        <?php
                                                        if (isset($banner)) {
                                                            foreach ($banner as $row) {  ?>
                                                                <option value="<?php echo $row['bid'];  ?>"><?php echo $row['banner_title_mal'];  ?></option>
                                                        <?php }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-3 control-label">Banner Title[English]</label>
                                                <div class="col-sm-7">
                                                    <input type="text" id="eng" value="<?php echo (isset($winner['banner_title_en'])) ? $winner['banner_title_en'] : set_value('title_e'); ?>" class="form-control" readonly />
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-3 control-label">Year</label>
                                                <div class="col-sm-7">
                                                    <input type="text" id="year" name="win_year" class="form-control" value="<?php echo (isset($winner['year'])) ? $winner['year'] : set_value('win_year'); ?>" readonly />
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-3 control-label">Banner Category</label>
                                                <div class="col-sm-7">
                                                    <input type="text" id="cat" class="form-control" value="<?php echo (isset($winner['nomination_category'])) ? $winner['nomination_category'] : set_value('title_cat'); ?>" readonly />
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-3 control-label">Winner Name</label>
                                                <div class="col-sm-7">
                                                    <input type="text" name="name" value="<?php echo (isset($winner['winner_name'])) ? $winner['winner_name'] : set_value('name'); ?>" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-3 control-label">Winner Name(Malayalam)</label>
                                                <div class="col-sm-7">
                                                    <input type="text" name="name_mal" value="<?php echo (isset($winner['winner_name_mal'])) ? $winner['winner_name_mal'] : set_value('name_mal'); ?>" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-3 control-label">Area of Expertise</label>
                                                <div class="col-sm-7">
                                                    <select name="awardarea" value="<?php echo (isset($winner['title'])) ? $winner['title'] : set_value('awardarea'); ?>" class="form-select" id="title-cat-mal">
                                                        <option value="<?php echo (isset($winner['awardarea_id'])) ?>"><?php echo (isset($winner['title'])) ? $winner['title'] : set_value('awardarea'); ?></option>
                                                        <?php
                                                        if (isset($award)) {
                                                            foreach ($award as $row) {  ?>
                                                                <option value="<?php echo $row['awardID'];  ?>"><?php echo $row['title'];  ?></option>
                                                        <?php }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-3 control-label">Breif Description of area of Expertise(English)</label>
                                                <div class="col-sm-7">
                                                    <input type="text" name="description" value="<?php echo (isset($winner['description'])) ? $winner['description'] : set_value('description'); ?>" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-3 control-label">Breif Description of area of Expertise(Malayalam)</label>
                                                <div class="col-sm-7">
                                                    <input type="text" name="description_mal" value="<?php echo (isset($winner['description_mal'])) ? $winner['description_mal'] : set_value('description_mal'); ?>" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-3 control-label">Award Image</label>
                                                <div class="col-sm-7">
                                                    <input class="form-control" id="photo" type="file" name="file" value="<?php echo (isset($winner['category_image'])) ? $winner['category_image'] : set_value('file'); ?>" />
                                                    <p style="color:#CC3333; font-size:10px; margin-top:10px;">We request you not to keep your image title field empty, Upload image leass than 2MB</p>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-sm-3"></div>
                                                <!-- <div class="col-sm-4">
                                                <div class="g-recaptcha" data-sitekey="<?php // echo $siteKey; 
                                                                                        ?>"></div>
                                            </div> -->
                                                <div class="col-sm-4">
                                                    <button style="margin-top:30px;padding: 10px 30px;" type="submit" class="btn btn-primary" name="Submit" value="Send">Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                    <?php  } else { ?>
                                        <div class="text-center">
                                            <h4 class="text-center">No Records</h4>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <!-- /page content -->
    </div>
    </div>
    <!-- footer content -->
    <?php echo view('higherEnd/includes/footerAdmin'); ?>
    <!-- /footer content -->
    <script>
        $('#banner_title_mal').on("change", function() {
            var titleid = $('#banner_title_mal').val();
            //alert(titleid);
            jQuery.ajax({
                type: 'POST',
                url: '<?php echo base_url(); ?>higherEnd/bannercontent',
                data: 'titleid=' + $("#banner_title_mal").val(),
                success: function(response) {
                    //response = JSON.parse(response);
                    console.log("aaa", response.banner.banner_title_en);
                    $("#eng").val(response.banner.banner_title_en);
                    $("#year").val(response.banner.year);
                    $("#cat").val(response.banner.nomination_category);

                }
            });
        });
    </script>
</body>

</html>