<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>
<script src='https://www.google.com/recaptcha/api.js'></script>

<body>
    <div class="container">
        <div class="col-lg-12 col-md-12">

            <!-- page content -->

            <div class="right_col" role="main">
                <div class="mt-5">
                    <!-- <div class="page-title mt-3">
                        <div class="title_left">
                            <h4>Control Panel for <?php echo $site_name; ?></h4><br />
                        </div>
                    </div> -->
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                 <!-- Error -->
                                 <?php if (isset($validation)) : ?>
                                        <div class="alert alert-warning">
                                            <?= $validation->listErrors() ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (isset($error)) : ?>
                                        <div class="alert alert-warning">
                                            <?= $error ?>
                                        </div>
                                    <?php endif; ?>
                                <div class="card">
                                    <div class="card-header"><h5>Site Management &nbsp; › <small>Add Gallery Images</small></h5></div>
                                    <form class="form-horizontal p-5" name="formList" id="formList" method="POST" action="<?php echo base_url(); ?>higherEnd/gallery/addimage" enctype="multipart/form-data">
                                    <?= csrf_field() ?>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Select Option</label>
                                            <div class="col-sm-7">
                                                <select name="title_cat" name="title_cat" class="form-control">
                                                    <?php foreach ($imgcat as $row) { ?>
                                                        <option style="color: black;" value="<?php echo $row['id']; ?>"><?php echo $row['title']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Winner Name [Malayalam]</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="title" type="text" name="title" value="" placeholder="Winner Name [Malayalam]" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;"></p>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Winner Name [English]</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="title_e" type="text" name="title_e" value="" placeholder="Winner Name [English]" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;"></p>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Award Category [Malayalam]</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="field" type="text" name="field" value="" placeholder="Award Category [Malayalam]" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;"></p>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Award Category [English]</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="field_e" type="text" name="field_e" value="" placeholder="Award Category [English]" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;"></p>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Image</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="photo" type="file" name="file" value="" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;">We request you not to keep your image title field empty, Upload image leass than 2MB</p>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-3"></div>
                                            <!-- <div class="col-sm-4">
                                                <div class="g-recaptcha" data-sitekey="<?php // echo $siteKey; ?>"></div>
                                            </div> -->
                                            <div class="col-sm-4">
                                                <button style="margin-top:30px;padding: 10px 30px;" type="submit" class="btn btn-primary" name="Submit" value="Send">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
    </div>
    <!-- footer content -->
    <?php echo view('higherEnd/includes/footerAdmin'); ?>
    <!-- /footer content -->
</body>

</html>