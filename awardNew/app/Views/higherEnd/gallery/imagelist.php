<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>
<style>
  .text-wrap {
        white-space: normal;
        font-size: 0.9rem;
    }

    .wrap-text {
        white-space: normal !important;
        line-height: 1.5 !important;
    }
</style>
<body>
  <div class="container">
    <div class="col-md-12 left_col">

      <div class="right_col" role="main">
        <div class="mt-3">
          <!-- <div class="page-title mt-3">
            <div class="title_left">
              <h4>Control Panel for <?php echo $site_name; ?></h4><br />
            </div>

          </div> -->
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <div class="x_content">
                    <form name="frmnews_list" method="POST" action="">
                    <?= csrf_field() ?>
                      <p class="text-muted font-13 m-b-30 text-end">&nbsp;
                        <a href="<?php echo base_url(); ?>higherEnd/gallery/addimage" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp; Add Award Ceremony</a>
                      </p>
                      <div class="card">
                        <div class="card-header">
                          <h5>Site Management &nbsp; ›<small>List Gallery</small></h5>
                        </div>
                        <div class="table-responsive  p-3 mt-3">
                          <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                              <tr>
                                <th>SlNo:</th>
                                <th>Title</th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>#Edit</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                              $count_no = 1;
                              foreach ($imgcat as $row) {
                              ?>
                                <tr>
                                  <td><?php echo $count_no; ?></td>
                                  <td><?php echo $row['title'] ?> [<?php echo $row['title_e'] ?>]</td>
                                  <td><img src="<?php echo act_url(); ?>public/images/gallery/<?php echo $row['gal_img']; ?>" style=" width:80px; height:60px;" /></td>
                                  <td>
                                    <?php if ($row['status'] == 1) {
                                    ?>
                                      <a href="<?php echo base_url(); ?>higherEnd/imagestatus/<?= $row['id'] ?>/0/" class="btn btn-success btn-xs"> Success </a>
                                    <?php } else {
                                    ?>
                                      <a href="<?php echo base_url(); ?>higherEnd/imagestatus/<?= $row['id'] ?>/1/" class="btn btn-danger btn-xs"> Failure </a>
                                    <?php } ?>
                                  </td>
                                  <td>
                                    <a href="<?php echo base_url(); ?>higherEnd/galleryimgedit/<?php echo encode_url($row['id']) ?>" class="btn btn-info btn-xs mb-1"><i class="fa fa-pencil"></i> Edit</a>
                                    <a href="<?php echo base_url(); ?>higherEnd/imagedelete/<?php echo encode_url($row['id']) ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                  </td>
                                </tr>
                              <?php $count_no++;
                              } ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /page content -->
    </div>
  </div>
  </div>
  <!-- footer content -->
  <?php echo view('higherEnd/includes/footerAdmin'); ?>
  <!-- /footer content -->

  <!-- Datatables -->
  <script src="<?php echo act_url(); ?>public/js/datatable.js"></script>
  <script src="<?php echo act_url(); ?>public/js/datatable.min.js"></script>
  <script src="<?php echo act_url(); ?>public/js/datatableBootstrap.js"></script>


  <!-- Datatables -->

  <script>
        $(document).ready(function() {
            $('#datatable').DataTable({
                dom: 'Bfrtip',
                // buttons: [
                //     'copy', 'csv', 'excel', 'pdf', 'print'
                // ]
                columnDefs: [{
                        targets: '_all',
                        className: 'wrap-text'
                    } // Apply wrap-text class to all columns
                ],
            });
        });
    </script>
</body>

</html>