<script src='https://www.google.com/recaptcha/api.js'></script>
<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>

<body>
  <div class="container">
    <!-- page content -->
    <div class="right_col" role="main">
      <div class="">
        <!-- <div class="page-title">
          <div class="title_left">
            <h4>Control Panel for <?php echo $site_name;  ?></h4><br />
          </div>
        </div> -->
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel mt-3">

              <div class="card">
                <div class="card-header">
                  <h5>Site Management &nbsp; › <small>Gallery</small></h5>
                </div>
                <?php if (isset($gallery['id'])) { ?>
                  <form name="frmproduct_add_text" method="POST" action="<?php echo base_url(); ?>higherEnd/galleryedit/<?php echo encode_url($gallery['id']) ?>" class="form-horizontal p-5">
                    <?= csrf_field() ?>
                    <!-- Error -->
                    <?php if (isset($validation)) : ?>
                      <div class="alert alert-warning">
                        <?php $validation->listErrors() ?>
                      </div>
                    <?php endif; ?>

                    <div class="form-group d-flex">
                      <label class="col-sm-3 control-label">Gallery Category Title [Malayalam]</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" name="home_title" placeholder="Gallery Category Title Malayalam" value="<?php echo (isset($gallery['title'])) ? $gallery['title'] : set_value('home_title'); ?>" />
                      </div>
                    </div>
                    <div class="form-group d-flex">
                      <label class="col-sm-3 control-label">Gallery Category Title [English]</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" name="home_title_e" placeholder="Gallery Category Title English" value="<?php echo (isset($gallery['title_e'])) ? $gallery['title_e'] : set_value('home_title_e'); ?>" />
                      </div>
                    </div>
                    <div class="form-group d-flex">
                      <label class="col-sm-3 control-label">Year</label>
                      <div class="col-sm-7">
                        <select name="year" class="form-control" value="<?php echo (isset($gallery['year'])) ? $gallery['year'] : set_value('year'); ?>">
                          <option value="<?php echo $gallery['year'];  ?>" hidden><?php echo (isset($gallery['year'])) ? $gallery['year'] : set_value('year'); ?></option>
                          <option value="2022">2022</option>
                          <option value="2023">2023</option>
                          <option value="2024">2024</option>
                          <option value="2025">2025</option>
                          <option value="2026">2026</option>
                          <option value="2027">2027</option>
                          <option value="2028">2028</option>
                          <option value="2029">2029</option>
                          <option value="2030">2030</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <!-- <div class="col-sm-12">
                      <div class="g-recaptcha" data-sitekey="<?php // echo $siteKey; 
                                                              ?>"></div>
                    </div> -->
                    </div>
                    <div class="form-group">
                      <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary" name="Submit" value="Sign up">Submit</button>
                      </div>
                    </div>
                  </form>
                <?php  } else { ?>
                  <div class="text-center">
                    <h4 class="text-center">No Records</h4>
                  </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>


  </div>
  <!-- /page content -->


  </div>

  <!-- footer content -->
  <?php echo view('higherEnd/includes/footerAdmin'); ?>
  <!-- /footer content -->


</body>

</html>