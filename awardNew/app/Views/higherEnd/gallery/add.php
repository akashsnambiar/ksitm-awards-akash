<script src='https://www.google.com/recaptcha/api.js'></script>
<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>

<body>
  <div class="container">
    <!-- page content -->
    <div class="right_col" role="main">
      <div class="mt-5">
        <!-- <div class="page-title mt-3">
            <div class="title_left">
              <h3>Control Panel for <?php echo $site_name;  ?></h3><br />
            </div>
          </div> -->
        <div class="clearfix"></div>
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
              <!-- Error -->
              <!-- <?php // if (isset($validation)) : 
                    ?>
                      <div class="alert alert-warning">
                        <?php // $validation->listErrors() 
                        ?>
                      </div>
                    <?php // endif; 
                    ?> -->
              <?php
              // print_r($homecontent);exit;
              // foreach ($gallerycategory as $row) {  
              ?>


              <div class="card">
                <div class="card-header">
                  <h5>Site Management &nbsp; › <small>Add Gallery Category</small></h5>
                </div>
                <form name="frmproduct_add_text" method="POST" action="<?php echo base_url(); ?>higherEnd/gallery/add" class="form-horizontal  p-5">
                <?= csrf_field() ?>

                  <div class="form-group d-flex">
                    <label class="col-sm-3 control-label">Gallery Category Title [Malayalam]</label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control" name="home_title" placeholder="Gallery Category Title Malayalam" value="" />
                    </div>
                  </div>
                  <div class="form-group d-flex">
                    <label class="col-sm-3 control-label">Gallery Category Title [English]</label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control" name="home_title_e" placeholder="Gallery Category Title English" value="" />
                    </div>
                  </div>
                  <div class="form-group d-flex">
                    <label class="col-sm-3 control-label">Year</label>
                    <div class="col-sm-7">
                      <select name="year" class="form-control">
                        <option>Select year</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                        <option value="2025">2025</option>
                        <option value="2026">2026</option>
                        <option value="2027">2027</option>
                        <option value="2028">2028</option>
                        <option value="2029">2029</option>
                        <option value="2030">2030</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <!-- <div class="col-sm-12">
                      <div class="g-recaptcha" data-sitekey="<?php // echo $siteKey; ?>"></div>
                    </div> -->
                  </div>
                  <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                      <button type="submit" class="btn btn-primary" name="Submit" value="Sign up">Submit</button>
                    </div>
                  </div>
                  <?php // } 
                  ?>
                </form>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>


  </div>
  </div>
  <!-- /page content -->
  <!-- footer content -->
  <?php echo view('higherEnd/includes/footerAdmin'); ?>
  <!-- /footer content -->


</body>

</html>