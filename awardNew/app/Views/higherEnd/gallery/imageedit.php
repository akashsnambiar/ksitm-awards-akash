<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>
<script src='https://www.google.com/recaptcha/api.js'></script>

<body>
    <div class="container">

        <!-- page content -->

        <div class="right_col" role="main">
            <div class="">
                <div class="page-title mt-3">
                    <!-- <div class="title_left">
                            <h4>Control Panel for <?php // echo $site_name; 
                                                    ?></h4><br />
                        </div> -->
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h5>Site Management &nbsp; ›<small>Add Gallery Images</small></h5>

                                <div class="clearfix"></div>
                            </div>
                            <!-- Error -->
                            <?php if (isset($validation)) : ?>
                                <div class="alert alert-warning">
                                    <?= $validation->listErrors() ?>
                                </div>
                            <?php endif; ?>
                            <?php if (isset($error)) : ?>
                                <div class="alert alert-warning">
                                    <?= $error ?>
                                </div>
                            <?php endif; ?>

                            <div class="card x_content">
                                <?php if (isset($gallery['id'])) { ?>
                                    <form class="form-horizontal p-5" name="formList" id="formList" method="POST" action="<?php echo base_url(); ?>higherEnd/galleryimgedit/<?php echo encode_url($gallery['id']); ?>" enctype="multipart/form-data">
                                        <?= csrf_field() ?>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Select Option</label>
                                            <div class="col-sm-7">
                                                <select name="title_cat" name="title_cat" class="form-control">
                                                    <?php foreach ($imgcat as $row) { ?>
                                                        <option style="color: black;" value="<?php echo $row['gid']; ?>" <?php if ($gallery['cat_id'] == $row['gid']) { ?> selected="selected" <?php } ?>><?php echo $row['gtitle']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Winner Name [Malayalam]</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="title" type="text" name="title" value="<?php echo (isset($gallery['title'])) ? $gallery['title'] : set_value('title'); ?>" placeholder="Winner Name [Malayalam]" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;"></p>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Winner Name [English]</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="title_e" type="text" name="title_e" value="<?php echo (isset($gallery['title_e'])) ? $gallery['title_e'] : set_value('title_e'); ?>" placeholder="Winner Name [English]" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;"></p>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Award Category [Malayalam]</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="field" type="text" name="field" value="<?php echo (isset($gallery['field'])) ? $gallery['field'] : set_value('field'); ?>" placeholder="Award Category [Malayalam]" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;"></p>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Award Category [English]</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="field_e" type="text" name="field_e" value="<?php echo (isset($gallery['field_e'])) ? $gallery['field_e'] : set_value('field_e'); ?>" placeholder="Award Category [English]" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;"></p>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Image</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="photo" type="file" name="file" value="<?php echo (isset($gallery['gal_img'])) ? $gallery['gal_img'] : set_value('file'); ?>" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;">We request you not to keep your image title field empty, Upload image leass than 2MB</p>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-3 control-label"></label>
                                                <div class="col-sm-7">
                                                    <img src="<?php echo act_url(); ?>public/images/gallery/<?php echo $gallery['gal_img']; ?>" style=" width:80px; height:80px;" />

                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-sm-3"></div>
                                                <!-- <div class="col-sm-4">
                                                <div class="g-recaptcha" data-sitekey="<?php // echo $siteKey; 
                                                                                        ?>"></div>
                                            </div> -->
                                                <div class="col-sm-4">
                                                    <button style="margin-top:30px;padding: 10px 30px;" type="submit" class="btn btn-primary" name="Submit" value="Send">Submit</button>
                                                </div>
                                            </div>
                                    </form>
                                <?php  } else { ?>
                                    <div class="text-center">
                                        <h4 class="text-center">No Records</h4>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>




        <!-- /page content -->

    </div>
    <!-- footer content -->
    
    <!-- /footer content -->
    </div>
    <?php echo view('higherEnd/includes/footerAdmin'); ?>

    <!-- Bootstrap -->
    <script src="<?php echo act_url(); ?>adminscripts/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo act_url(); ?>adminscripts/build/js/custom.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo act_url(); ?>adminscripts/production/js/moment/moment.min.js"></script>
    <script src="<?php echo act_url(); ?>adminscripts/production/js/datepicker/daterangepicker.js"></script>



    <script>
        $(document).ready(function() {
            $('#newsdate').daterangepicker({
                singleDatePicker: true,
                format: 'YYYY-MM-DD',
                calender_style: "picker_4"
            }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
        });
    </script>

</body>

</html>