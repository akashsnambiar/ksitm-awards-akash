<!-- <script src='https://www.google.com/recaptcha/api.js'></script> -->
<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>

<body>
  <div class="container-wrapper">
    <div class="container">

      <div class="right_col" role="main">
        <div class="">
          <div class="page-title mt-3">
            <!-- <div class="title_left">
              <h4>Control Panel for <?php echo $site_name;  ?></h3><br />
            </div> -->
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <!-- Error -->
                <?php if (isset($validation)) : ?>
                  <div class="alert alert-warning">
                    <?= $validation->listErrors() ?>
                  </div>
                <?php endif; ?>
                <div class="card">
                  <div class="card-header">
                    <h5>Site Management &nbsp; › <small>Kerala Jyothi</small></h5>
                  </div>
                  <form name="frmproduct_add_text" method="POST" action="<?php echo base_url(); ?>higherEnd/site/keralaJyothi" class="form-horizontal p-5">
                  <?= csrf_field() ?>
                    <?php
                    // print_r($homecontent);exit;
                    foreach ($aboutcontent as $row) {  ?>


                      <div class="form-group row">
                        <label class="col-sm-3 control-label">Kerala Jyothi page [Malayalam]</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" name="home_title" placeholder="Kerala Jyothi page Malayalam" value="<?php echo (isset($row['title'])) ? $row['title'] : set_value('home_title'); ?>" />
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-3 control-label">Kerala Jyothi page [English]</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" name="home_title_e" placeholder="Kerala Jyothi page English" value="<?php echo (isset($row['title_e'])) ? $row['title_e'] : set_value('home_title_e'); ?>" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Kerala Jyothi Text [Malayalam]</label>
                        <div class="col-sm-12">
                          <textarea name="text" id="editor" class="editor"><?php echo (isset($row['text'])) ? $row['text'] : set_value('text'); ?></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Kerala Jyothi Text [English]</label>
                        <div class="col-sm-12">
                          <textarea name="text_e" id="ckeditor" class="ckeditor"><?php echo (isset($row['text_e'])) ? $row['text_e'] : set_value('text_e'); ?></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <!-- <div class="col-sm-12">
                          <div class="g-recaptcha" data-sitekey="<?php // echo $siteKey; ?>"></div>
                        </div> -->
                      </div>
                      <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                          <button type="submit" class="btn btn-primary" name="Submit" value="Sign up">Submit</button>
                        </div>
                      </div>
                    <?php } ?>
                  </form>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>


    </div>
  </div>
  </div>
  <!-- footer content -->
  <?php echo view('higherEnd/includes/footerAdmin'); ?>
  <!-- /footer content -->


  <!-- Text editor script -->
  <script>
    ClassicEditor
      .create(document.querySelector('#editor'))
      .catch(error => {
        console.error(error);
      });
    ClassicEditor
      .create(document.querySelector('#ckeditor'))
      .catch(error => {
        console.error(error);
      });
  </script>

  <!-- Text editor script -->

</body>

</html>