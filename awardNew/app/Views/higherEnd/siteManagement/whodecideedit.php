<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>
<script src='https://www.google.com/recaptcha/api.js'></script>

<body>
    <div class="container">
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="mt-3">
           
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <!-- Error -->
                            <?php if (isset($validation)) : ?>
                                    <div class="alert alert-warning">
                                        <?= $validation->listErrors() ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (isset($error)) : ?>
                                    <div class="alert alert-warning">
                                        <?= $error ?>
                                    </div>
                                <?php endif; ?>
                            <div class="card">
                                <div class="card-header"><h5>Site Management &nbsp; › <small>Add Document</small></h5></div>
                                <?php if (isset($whodecides['id'])) { ?>
                                <form class="form-horizontal p-3" name="formList" id="formList" method="POST" action="<?php echo base_url(); ?>admin/whodecide/edit/<?php echo encode_url($whodecides['id']); ?>" enctype="multipart/form-data">
                                <?= csrf_field() ?>
                                    <div class="row form-group">
                                        <label class="col-sm-3 control-label">Document Title [Malayalam]</label>
                                        <div class="col-sm-7">
                                            <input class="form-control" id="title" type="text" name="title" value="<?php echo (isset($whodecides['whodecides'])) ? $whodecides['whodecides'] : set_value('title'); ?>" placeholder="Title [Malayalam]" />
                                            <p style="color:#CC3333; font-size:10px; margin-top:10px;">We request you not to keep your Notification Title field empty</p>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-sm-3 control-label">Document Title [English]</label>
                                        <div class="col-sm-7">
                                            <input class="form-control" id="title_e" type="text" name="title_e" value="<?php echo (isset($whodecides['whodecides_e'])) ? $whodecides['whodecides_e'] : set_value('title_e'); ?>" placeholder="Title [English]" />
                                            <p style="color:#CC3333; font-size:10px; margin-top:10px;">We request you not to keep your Notification Title field empty</p>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-sm-3 control-label">Document Upload</label>
                                        <div class="col-sm-7">
                                            <div class="d-flex">
                                                <input class="form-control" id="photo" type="file" name="file" value="<?php echo (isset($whodecides['document'])) ? $whodecides['document'] : set_value('file'); ?>" />
                                                <a href="<?php echo base_url(); ?>notifications/<?php echo $whodecides['document'] ?>" target="_blank"><b>View</b></a>
                                            </div>
                                            <p style="color:#CC3333; font-size:10px; margin-top:10px;">Upload only PDF File</p>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3"></div>
                                        <!-- <div class="col-sm-4">
                                            <div class="g-recaptcha" data-sitekey="<?php // echo $siteKey; ?>"></div>
                                        </div> -->
                                        <div class="col-sm-4">
                                            <button style="margin-top:30px;padding: 10px 30px;" type="submit" class="btn btn-primary" name="Submit" value="Send">Submit</button>
                                        </div>
                                    </div>
                                </form>
                                <?php  } else { ?>
                                    <div class="text-center">
                                        <h4 class="text-center">No Records</h4>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>



    </div>
    </div>

    <!-- /page content -->

    <!-- footer content -->
    <?php echo view('higherEnd/includes/footerAdmin'); ?>
    <!-- /footer content -->



</body>

</html>