<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>
<script src='https://www.google.com/recaptcha/api.js'></script>

<body>
    <div class="container">
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="mt-5">
                <!-- <div class="page-title">
                    <div class="title_left">
                        <h4>Control Panel for <?php echo $site_name; ?></h4><br />
                    </div>
                </div> -->
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <!-- Error -->
                            <?php if (isset($validation)) : ?>
                                <div class="alert alert-warning">
                                    <?= $validation->listErrors() ?>
                                </div>
                            <?php endif; ?>
                            <?php if (isset($error)) : ?>
                                <div class="alert alert-warning">
                                    <?= $error ?>
                                </div>
                            <?php endif; ?>
                            <div class="card">

                                <div class="card-header">
                                    <h5>Site Management &nbsp; ›<small>Edit Who Decides</small></h5>
                                </div>
                                <form class="form-horizontal p-5 pb-1" name="formList" id="formList" method="POST" action="<?php echo base_url(); ?>higherEnd/whodecides/add" enctype="multipart/form-data">
                                <?= csrf_field() ?>
                                    <div class="row form-group">
                                        <label class="col-sm-12 control-label">Who Decides Title [Malayalam]</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" id="title" type="text" name="title" value="<?php echo (isset($homecnt['title4'])) ? $homecnt['title4'] : set_value('title'); ?>" placeholder="Who Can Apply Title [Malayalam]" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Who Decides Content [Malayalam]</label>
                                        <div class="col-sm-12">
                                            <textarea name="text" id="editor" class="editor"><?php echo (isset($homecnt['text4'])) ? $homecnt['text4'] : set_value('text'); ?></textarea>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-sm-12 control-label">Who Decides Title [English]</label>
                                        <div class="col-sm-12">
                                            <input class="form-control" id="title_e" type="text" name="title4_e" value="<?php echo (isset($homecnt['title4_e'])) ? $homecnt['title4_e'] : set_value('title4_e'); ?>" placeholder="Who Can Apply Title [English]" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Who Decides Content [English]</label>
                                        <div class="col-sm-12">
                                            <textarea name="text4_e" id="ckeditor" class="editor"><?php echo (isset($homecnt['text4_e'])) ? $homecnt['text4_e'] : set_value('text4_e'); ?></textarea>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3"></div>
                                        <!-- <div class="col-sm-4">
                                            <div class="g-recaptcha" data-sitekey="<?php // echo $siteKey; ?>"></div>
                                        </div> -->
                                        <div class="col-sm-4">
                                            <button style="margin-top:30px;padding: 10px 30px;" type="submit" class="btn btn-primary" name="Submit" value="Send">Submit</button>
                                        </div>
                                    </div>
                                </form>
                                <div class="card" style="margin-left: 1em; margin-right: 1em;">
                                    <p class="text-muted font-13 m-b-30 mt-3">&nbsp;
                                        <a href="<?php echo base_url(); ?>higherEnd/whocandecide/add" class="btn btn-success"><i class="fa fa-bolt"></i> Add Documents</a>
                                    </p>
                                    <div class="table-responsive" style="margin-left: 1em; margin-right: 1em;">
                                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>SlNo:</th>
                                                    <th>Title</th>
                                                    <th>Document</th>
                                                    <th>Status</th>
                                                    <th>#Delete</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $count_no = 1;
                                                foreach ($whodecides as $row) { ?>
                                                    <tr>
                                                        <td><?php echo $count_no; ?></td>
                                                        <td><?php echo $row['whodecides'] ?> [<?php echo $row['whodecides_e'] ?>]</td>
                                                        <td><a href="<?php echo base_url(); ?>notifications/<?php echo $row['document'] ?>" target="_blank"><b>View</b></a></td>
                                                        <td>
                                                            <?php if ($row['status'] == 1) {
                                                            ?>
                                                                <a href="<?php echo base_url(); ?>higherEnd/whodecidesstatus/<?= $row['id'] ?>/0/" class="btn btn-success btn-xs"> Success </a>
                                                            <?php } else {
                                                            ?>
                                                                <a href="<?php echo base_url(); ?>higherEnd/whodecidesstatus/<?= $row['id'] ?>/1/" class="btn btn-danger btn-xs"> Failure </a>
                                                            <?php } ?>
                                                        </td>
                                                        <td>
                                                            <a href="<?php echo base_url(); ?>higherEnd/whodecide/edit/<?php echo encode_url($row['id']) ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                                            <a href="<?php echo base_url(); ?>higherEnd/whodecide/delete/<?php echo $row['id'] ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                                        </td>
                                                    </tr>
                                                <?php $count_no++;
                                                } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>

    <!-- /page content -->

    <!-- footer content -->
    <?php echo view('higherEnd/includes/footerAdmin'); ?>
    <!-- /footer content -->



    <script src="<?php echo act_url(); ?>public/js/datatables/datatable.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/datatable.min.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/datatableBootstrap.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/dataTables.buttons.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#datatable').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                columnDefs: [{
                    render: function(data, type, full, meta) {
                        return "<div class='text-wrap width-200'>" + data + "</div>";
                    },
                    targets: 2
                }]
            });
        });
    </script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#ckeditor'))
            .catch(error => {
                console.error(error);
            });
    </script>

</body>

</html>