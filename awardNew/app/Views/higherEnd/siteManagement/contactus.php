<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>
<script src='https://www.google.com/recaptcha/api.js'></script>

<body>
    <div class="container">
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="mt-5">
                <!-- <div class="page-title mt-3">
                        <div class="title_left">
                            <h4>Control Panel for <?php echo $site_name; ?></h4><br />
                        </div>
                    </div> -->
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <!-- Error -->
                            <?php if (isset($validation)) : ?>
                                    <div class="alert alert-warning">
                                        <?= $validation->listErrors() ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (isset($error)) : ?>
                                    <div class="alert alert-warning">
                                        <?= $error ?>
                                    </div>
                                <?php endif; ?>
                            <div class="card">
                                
                                <div class="card-header">
                                    <h5>Site Management &nbsp; ›<small>Edit Contact Us page</small></h5>
                                </div>
                                <form class="form-horizontal p-5 pb-1" name="formList" id="formList" method="POST" action="<?php echo base_url(); ?>higherEnd/contactus/add" enctype="multipart/form-data">
                                <?= csrf_field() ?>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Contact Us [Malayalam]</label>
                                        <div class="col-sm-12 text-danger">
                                            <textarea name="text" id="editor" class="editor"><?php echo (isset($contactus['text'])) ? $contactus['text'] : set_value('text'); ?></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Contact Us [English]</label>
                                        <div class="col-sm-12 text-danger">
                                            <textarea name="text_e" id="ckeditor" class="editor"><?php echo (isset($contactus['text_e'])) ? $contactus['text_e'] : set_value('text_e'); ?></textarea>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3"></div>
                                        <!-- <div class="col-sm-4">
                                            <div class="g-recaptcha" data-sitekey="<?php // echo $siteKey; ?>"></div>
                                        </div> -->
                                        <div class="col-sm-4">
                                            <button style="margin-top:30px;padding: 10px 30px;" type="submit" class="btn btn-primary" name="Submit" value="Send">Submit</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>



    </div>
    <!-- /page content -->
    </div>
    <!-- footer content -->
    <?php echo view('higherEnd/includes/footerAdmin'); ?>
    <!-- /footer content -->



    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#ckeditor'))
            .catch(error => {
                console.error(error);
            });
    </script>

</body>

</html>