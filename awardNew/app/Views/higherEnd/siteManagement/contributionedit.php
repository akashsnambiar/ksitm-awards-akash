<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>
<script src='https://www.google.com/recaptcha/api.js'></script>

<body>
    <div class="container">
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="mt-3">
                <!-- <div class="page-title mt-3">
                    <div class="title_left">
                        <h4>Control Panel for <?php // echo $site_name; 
                                                ?></h4><br />
                    </div>
                </div> -->
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <!-- Error -->
                            <?php if (isset($validation)) : ?>
                                <div class="alert alert-warning">
                                    <?= $validation->listErrors() ?>
                                </div>
                            <?php endif; ?>
                            <?php if (isset($error)) : ?>
                                <div class="alert alert-warning">
                                    <?= $error ?>
                                </div>
                            <?php endif; ?>
                            <div class="card">
                                <div class="card-header">
                                    <h5>Site Management &nbsp; ›<small>Edit Awards for Outstanding Contribution</small></h5>
                                </div>
                                <?php if (isset($whocanapply['id'])) { ?>
                                    <form class="form-horizontal p-5" name="formList" id="formList" method="POST" action="<?php echo base_url(); ?>higherEnd/contributionedit/<?php echo encode_url($whocanapply['id']) ?>">
                                        <?= csrf_field() ?>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Fields of Outstanding Contribution [Malayalam]</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="title" type="text" name="title" value="<?php echo (isset($whocanapply['title'])) ? $whocanapply['title'] : set_value('title'); ?>" placeholder="Malayalam" />
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Fields of Outstanding Contribution [English]</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="title_e" type="text" name="title_e" value="<?php echo (isset($whocanapply['title_e'])) ? $whocanapply['title_e'] : set_value('title_e'); ?>" placeholder="English" />
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-3"></div>
                                            <!-- <div class="col-sm-4">
                                            <div class="g-recaptcha" data-sitekey="<?php // echo $siteKey; 
                                                                                    ?>"></div>
                                        </div> -->
                                            <div class="col-sm-4">
                                                <button style="margin-top:30px;padding: 10px 30px;" type="submit" class="btn btn-primary" name="Submit" value="Send">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                <?php  } else { ?>
                                    <div class="text-center">
                                        <h4 class="text-center">No Records</h4>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <!-- /page content -->

    <!-- footer content -->
    <?php echo view('higherEnd/includes/footerAdmin'); ?>
    <!-- /footer content -->



</body>

</html>