<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>
<script src='https://www.google.com/recaptcha/api.js'></script>

<body>
    <div class="nav-md">
        <div class="container">
            <div class="right_col" role="main">
                <div class="mt-3 mb-3">
                    <!-- <div class="page-title mt-1">
                        <div class="title_left">
                            <h4>Control Panel for <?php echo $siteName;  ?></h4><br />
                        </div>
                    </div> -->
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="card">
                                    <div class="card-header"><h5>Site Management &nbsp; ›<small>Edit About Us Page</small></h5></div>
                                    <form name="frmproduct_add_text" method="POST" action="aboutus" class="form-horizontal p-5">
                                    <?= csrf_field() ?>
                                        <!-- Error -->
                                        <?php if (isset($validation)) : ?>
                                            <div class="alert alert-warning">
                                                <?= $validation->listErrors() ?>
                                            </div>
                                        <?php endif; ?>
                                        <?php
                                        // print_r($homecontent);exit;
                                        foreach ($aboutcontent as $row) {  ?>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">About page [Malayalam]</label>
                                                <div class="col-sm-9">
                                                    <textarea name="about_title" class="form-control" ><?php echo (isset($row['title'])) ? $row['title'] : set_value('about_title'); ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">About page [English]</label>
                                                <div class="col-sm-9">
                                                    <textarea name="about_title_e" class="form-control" ><?php echo (isset($row['title_e'])) ? $row['title_e'] : set_value('home_title_e'); ?></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">About page Text [Malayalam]</label>
                                                <div class="col-sm-12">
                                                    <textarea name="text" id="editor" class="editor"><?php echo (isset($row['text'])) ? $row['text'] : set_value('text'); ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">About page Text [English]</label>
                                                <div class="col-sm-12">
                                                    <textarea name="text_e" id="ckeditor" class="ckeditor"><?php echo (isset($row['text_e'])) ? $row['text_e'] : set_value('text_e'); ?></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <!-- <div class="col-sm-4">
                                                    <div class="g-recaptcha" data-sitekey="<?php // echo $siteKey; ?>"></div>
                                                </div> -->
                                                <div class="col-sm-4">
                                                    <button style="margin-top:30px;padding: 10px 30px;" type="submit" class="btn btn-primary" name="Submit" value="Sign up">Submit</button>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </form>
                                </div>



                            </div>
                        </div>
                    </div>

                </div>
            </div>



        </div>
        <!-- /page content -->
        <!-- footer content -->
        <?php echo view('higherEnd/includes/footerAdmin'); ?>
        <!-- /footer content -->

    </div>
    </div>
    <!-- Text editor script -->
  <script>
    ClassicEditor
      .create(document.querySelector('#editor'))
      .catch(error => {
        console.error(error);
      });
    ClassicEditor
      .create(document.querySelector('#ckeditor'))
      .catch(error => {
        console.error(error);
      });
  </script>
  
  <!-- Text editor script -->
</body>

</html>