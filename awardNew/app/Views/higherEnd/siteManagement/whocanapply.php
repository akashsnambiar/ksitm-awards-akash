<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>
<script src='https://www.google.com/recaptcha/api.js'></script>

<body>
    <div class="container">
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="mt-3">
                <!-- <div class="page-title">
                    <div class="title_left">
                        <h4>Control Panel for <?php echo $site_name; ?></h4><br />
                    </div>
                </div> -->
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <!-- Error -->
                            <?php if (isset($validation)) : ?>
                                <div class="alert alert-warning">
                                    <?= $validation->listErrors() ?>
                                </div>
                            <?php endif; ?>
                            <?php if (isset($error)) : ?>
                                <div class="alert alert-warning">
                                    <?= $error ?>
                                </div>
                            <?php endif; ?>
                            <!-- Error -->
                            <div class="card">
                                <div class="card-header">
                                    <h5>Site Management &nbsp; ›<small>Edit Who Can Apply Content</small></h5>
                                </div>
                                <form class="form-horizontal p-5 pb-1" name="formList" id="formList" method="POST" action="<?php echo base_url(); ?>higherEnd/whoapply/add" enctype="multipart/form-data">
                                <?= csrf_field() ?>
                                    <div class="row form-group">
                                        <label class="col-sm-3 control-label">Who Can Apply Title [Malayalam]</label>
                                        <div class="col-sm-7">
                                            <input class="form-control" id="title" type="text" name="title" value="<?php echo (isset($homecnt['title'])) ? $homecnt['title'] : set_value('title'); ?>" placeholder="Who Can Apply Title [Malayalam]" />
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-sm-3 control-label">Who Can Apply Title [English]</label>
                                        <div class="col-sm-7">
                                            <input class="form-control" id="title_e" type="text" name="title_e" value="<?php echo (isset($homecnt['title_e'])) ? $homecnt['title_e'] : set_value('title_e'); ?>" placeholder="Who Can Apply Title [English]" />
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3"></div>
                                        <!-- <div class="col-sm-4">
                                            <div class="g-recaptcha" data-sitekey="<?php // echo $siteKey; ?>"></div>
                                        </div> -->
                                        <div class="col-sm-4">
                                            <button style="margin-top:30px;padding: 10px 30px;" type="submit" class="btn btn-primary" name="Submit" value="Send">Submit</button>
                                        </div>
                                    </div>
                                </form>

                                <form class="form-horizontal p-5" name="formList" id="formList" method="POST" action="<?php echo base_url(); ?>higherEnd/whocanapply/add">
                                <?= csrf_field() ?>
                                    <div class="row form-group">
                                        <label class="col-sm-3 control-label">Who can nominate (Malayalam)</label>
                                        <div class="col-sm-7">
                                            <input class="form-control" id="title" type="text" name="title" value="" placeholder="Malayalam" />
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-sm-3 control-label">Who can nominate (English)</label>
                                        <div class="col-sm-7">
                                            <input class="form-control" id="title_e" type="text" name="title_e" value="" placeholder="English" />
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3"></div>
                                        <!-- <div class="col-sm-4">
                                            <div class="g-recaptcha" data-sitekey="<?php // echo $siteKey; ?>"></div>
                                        </div> -->
                                        <div class="col-sm-4">
                                            <button style="margin-top:30px;padding: 10px 30px;" type="submit" class="btn btn-primary" name="Submit" value="Send">Submit</button>
                                        </div>
                                    </div>
                                </form>

                                <div class="table-responsive mb-3" style="margin-left: 1em;">
                                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" width="100%">
                                        <thead>
                                            <tr>
                                                <th>SlNo:</th>
                                                <th>Title</th>
                                                <th>#Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $count_no = 1;
                                            foreach ($whocanapply as $row) { ?>
                                                <tr>
                                                    <td><?php echo $count_no; ?></td>
                                                    <td><?php echo $row['title'] ?> [<?php echo $row['title_e'] ?>]</td>
                                                    <td>
                                                        <a href="<?php echo base_url(); ?>higherEnd/whocanedit/<?php echo encode_url($row['id']) ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                                        <a href="<?php echo base_url(); ?>higherEnd/whocandelete/<?php echo $row['id'] ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                                    </td>
                                                </tr>
                                            <?php $count_no++;
                                            } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>



    </div>
    <!-- /page content -->
    </div>

    <!-- footer content -->
    <?php echo view('higherEnd/includes/footerAdmin'); ?>
    <!-- /footer content -->


   <!-- Datatables -->
    <script src="<?php echo act_url(); ?>public/js/datatables/datatable.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/datatable.min.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/datatableBootstrap.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/dataTables.buttons.min.js"></script>
    <!-- Datatables -->

   

    <script>
        $(document).ready(function() {
            $('#datatable').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                columnDefs: [{
                    render: function(data, type, full, meta) {
                        return "<div class='text-wrap width-200'>" + data + "</div>";
                    },
                    targets: 2
                }]
            });
        });
    </script>
</body>

</html>