<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>

<body>
    <div class="container">
        <div class="col-md-12 left_col">
            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="page-title mt-3">
                        <!-- <div class="title_left">
                                <h4>Control Panel for <?php echo $site_name; ?></h4><br />
                            </div> -->
                    </div>
                    <div class="clearfix"></div>
                    <!-- <a href="<?php echo base_url(); ?>higherEnd/keralasree/year/<?php echo $print; ?>"> <span class="mdi mdi-arrow-left-bold" style="font-size: 20px;">Back</span></a> -->

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h5>Nominations &nbsp; ›<small>Kerala Sree - Details</small></h5>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <p class="text-muted font-13 m-b-30">&nbsp;

                                    </p>
                                    <?php
                                    if (isset($view['nominationID'])) { ?>
                                        <a href="<?php echo base_url(); ?>higherEnd/keralasree/print/<?= $view['nominationID']; ?>" target="_blank" class="btn btn-primary"><i class="fa fa-download"></i> Generate PDF</a>
                                    <?php } else {
                                    }
                                    ?>
                                    <br /><br />
                                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                                        <tbody>
                                            <tr>
                                                <td>ഫോട്ടോ</td>
                                                <td><img src="<?php echo act_url(); ?>public/images/nomination/<?php echo $view['photo']; ?>" width="150" height="150" /></td>
                                            </tr>
                                            <tr>
                                                <td>പേര് </td>
                                                <td><?php echo $view['name']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>ജനനതീയതി</td>
                                                <td><?php echo $view['dob']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>വയസ്</td>
                                                <td><?php echo $view['age']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>ലിംഗം</td>
                                                <td><?php if ($view['sex'] == 1) { ?> Female <?php } else if ($view['sex'] == 2) { ?> Male <?php } else if ($view['sex'] == 3) { ?>Transgender <?php } ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>രാജ്യം </td>
                                                <td><?php echo $view['country']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>സംസ്ഥാനം </td>
                                                <td><?php echo $view['state']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>ജില്ല </td>
                                                <td><?php echo $view['district']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>താലൂക്ക് </td>
                                                <td><?php echo $view['taluk']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>ജനിച്ചസ്ഥലം </td>
                                                <td><?php echo $view['pob']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>മേൽവിലാസം</td>
                                                <td><?php echo $view['address']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>ഇ-മെയിൽ അഡ്രസ് </td>
                                                <td><?php echo $view['email']; ?></td>
                                            </tr>

                                            <tr>
                                                <td>ജോലി / തൊഴിൽ </td>
                                                <td><?php echo $view['job']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>പ്രവർത്തനമേഖല </td>
                                                <td><?php echo $view['title']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>ഉപ മേഖല </td>
                                                <td><?php echo $view['awardAreaSubTitle']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>ഏത് കേരള പുരസ്കാരത്തിനാണ് ശിപാർശ</td>
                                                <td><?php if ($view['award'] == 1) { ?> കേരള ജ്യോതി <?php } else if ($view['award'] == 2) { ?> കേരള പ്രഭ <?php } else if ($view['award'] == 3) { ?>കേരള ശ്രീ <?php } ?></td>
                                            </tr>
                                            <tr>
                                                <td>പത്മ പുരസ്‌കാരം ലഭിച്ചിട്ടുണ്ടോ </td>
                                                <td><?php if ($view['pyn'] == 1) { ?> ഉണ്ട് <?php } else { ?> ഇല്ല <?php } ?></td>
                                            </tr>
                                            <tr>
                                                <td>ഏത് പുരസ്‌കാരമാണ് ലഭിച്ചത്</td>
                                                <td><?php echo $view['pAward']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>ഏത് വർഷമാണ് ലഭിച്ചത്</td>
                                                <td><?php echo $view['pYear']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>നാമനിദ്ദേശം ചെയ്യുന്ന വ്യക്തിയെക്കുറിച്ചുള്ള ചെറുവിവരണം </td>
                                                <td><?php echo $view['note']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>നാമനിദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തി സമൂഹത്തിന് നൽകിയിട്ടുള്ള കാര്യമായ സംഭാവനകൾ</td>
                                                <td><?php echo $view['donation']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>നാമനിദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തിയുടെ നേട്ടങ്ങൾ </td>
                                                <td><?php echo $view['achivement']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>പ്രവർത്തനമേഖലയിലെ സംഭാവന</td>
                                                <td><?php echo $view['wDonation']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>സമൂഹത്തിനുള്ള സംഭാവന </td>
                                                <td><?php echo $view['sDonation']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>ഡോക്യുമെൻ്റ്</td>
                                                <td>
                                                    <?php
                                                    if ($view['document']) { ?>
                                                        <a href="<?php echo act_url(); ?>public/images/nomination/documents/<?php echo $view['document']; ?>" target="_blank"><img src="<?php echo act_url(); ?>public/images/pdficon.png" width="50" height="50" /></a>
                                                    <?php } else { ?>
                                                        No document
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>നാമനിർദ്ദേശം ചെയ്യുന്ന ആളിൻ്റെ ഒപ്പ് </td>
                                                <td><img style="float:left;" src="<?php echo act_url(); ?>public/images/esign.jpg" width="50" height="50" /><span style="float:left;">&nbsp;&nbsp;Digitally Signed By : <?php echo $view['ename']; ?></span></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
    </div>
    <!-- footer content -->
    <?php echo view('higherEnd/includes/footerAdmin'); ?>
    <!-- /footer content -->



</body>

</html>