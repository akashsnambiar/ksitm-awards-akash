<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>

<body>
    <div class="nav-md">
        <div class="container" style="width: 68em;">
            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <!-- <div class="page-title">
                        <div class="title_left">
                            <h4>Control Panel for <?php // echo $site_name;  ?></h4><br />
                        </div>

                    </div> -->
                    <div class="clearfix"></div>
                    <a href="<?php echo base_url(); ?>higherEnd/keralasree/year/<?php echo $print; ?>"> <span class="mdi mdi-arrow-left-bold" style="font-size: 20px;">Back</span></a>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <!-- blockquote -->
                                        <?php //print_r($review); exit;
                                        $count_no = 1;
                                        if ($reviewcount != 0) {
                                            if (isset($review)) {
                                                foreach ($review as $row) {
                                        ?>

                                                <?php if ($count_no % 2 != 0) { ?>
                                                    <div class=""><label>Review :</label>

                                                        <div class="card">
                                                            <div class="p-5">
                                                                <?php echo $row['review']; ?>
                                                            </div>
                                                            <div class="container-fluid">
                                                                <cite title="Source Title" style="margin-left: 26em;">--<?php echo $row['first_name'] ?> <?php echo $row['last_name']; ?> <span style=" float:right;"><?php echo $row['time']; ?></span></cite>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <blockquote>

                                                        <footer>
                                                        </footer>
                                                    </blockquote>
                                                <?php } else { ?>
                                                    <blockquote class="blockquote-reverse">
                                                        <?php echo $row->review; ?>
                                                        <footer> <cite title="Source Title"><span style=" float:left;"><?php echo $row['time']; ?></span><?php echo $row['first_name']; ?> <?php echo $row['last_name']; ?></cite>
                                                        </footer>
                                                    </blockquote>
                                                <?php } ?>
                                        <?php $count_no++;
                                                }
                                            } else {
                                            }
                                        } ?>
                                    </div>
                                    <h4>റിവ്യൂ നൽകുക </h4>
                                    <hr />

                                    <div class="x_content">

                                        <form name="frmproduct_add_text" method="POST" action="<?php echo base_url(); ?>higherEnd/keralasree/reviewSubmit" class="form-horizontal">
                                        <?= csrf_field() ?>
                                            <!-- Error -->
                                            <?php // if (isset($alert) ? $alert : '') { 
                                            ?>
                                            <!-- <div class="alert alert-success">
                                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                <?php // echo (isset($alert) ? $alert : ''); 
                                                ?>
                                            </div> -->
                                            <?php // } 
                                            ?>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <textarea name="text" id="ckeditor" class="ckeditor"><?php // echo (isset($text)) ? $text : set_value('text');  
                                                                                                            ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-9 col-sm-offset-3">
                                                    <input type="hidden" name="nomid" value="<?php echo $nomid; ?>">
                                                    <button type="submit" class="btn btn-primary" name="Submit" value="Sign up">Submit</button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>

                </div>
            </div>



        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php // echo view('higherEnd/includes/footerAdmin'); ?>
        <!-- /footer content -->
    </div>
    </div>
    <script src="<?php echo act_url(); ?>public/js/ckeditor5-build-classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#ckeditor'))
            .catch(error => {
                console.error(error);
            });
    </script>

</body>

</html>