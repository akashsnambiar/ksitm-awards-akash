<?php echo view('higherEnd/includes/header'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Kerala State IT Mission </title>
 </head>
<style>
#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
  font-size:12px;
}

#customers td{
  padding: 12px 30px;
  text-align: left;
} 
#customers th {
  padding: 8px;
  text-align:center;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers td:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
 color:#000000;
 font-size:16px;
 text-decoration:underline;
}
</style>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        

        <!-- top navigation -->
        
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main" style="margin-left:0px;">
          <div class="">
            


            <div class="row">
              <div class="col-md-12">
                <div class="x_panel" style="border:none;">
                  
                  <div class="x_content">

                    <section class="content invoice">
                      <!-- title row -->
                      <div class="row">
                        <div class="col-xs-12 invoice-header">
						<br/>
                          <h1>
                                          <i class="fa fa-trophy"></i> <?php // echo $viewHead;?></small>
                                          <!--<small class="pull-right">Date: 16/08/2016</small>-->
										  <button style="float:right;" class="btn btn-default" onClick="window.print();"><i class="fa fa-print"></i> Print</button>
                                      </h1><br/>
									  
                        </div>
                        <!-- /.col -->
                      </div>
                     <br/><br/>
                      <!-- Table row -->
                      <div class="row">
						
			                        <div class="col-md-12" style="padding:0px;">
									
									
									<table id="customers">
									   <tr>
									      <th>നാമനിർദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തിയുടെ വിവരങ്ങൾ </th>
								       </tr>
									</table><br/>
									<table id="customers">
										  <tr>
											<td width="50%"><h3 style=" color:#006600; margin:0px;">Reg. No. : <?php echo $view['nominationRegID'];?></h3></td>
											<td><img src="<?php echo act_url();?>public/images/nomination/<?php echo $view['photo'];?>" width="150" height="150"/></td>
										  </tr>
										<tr>
										    <td> <b>പേര് : </b> <?php echo $view['name'];?></td>
											<td><b>ജനനതീയതി  : </b><?php echo $view['dob'];?></td>
											
										  </tr>
										  <tr>
										    <td><b>വയസ് : </b><?php echo $view['age'];?></td>
											<td><b>ലിംഗം : </b><?php if($view['sex']==1){?> Female <?php }else if($view['sex']==2){ ?> Male <?php }else if($view['sex']==3){ ?>Transgender <?php } ?></td>
											
										  </tr>
										  <tr>
										    <td><b>രാജ്യം : </b><?php echo $view['country'];?></td>
											<td><b>സംസ്ഥാനം : </b><?php echo $view['state'];?></td>
										  </tr>
										  <tr>
										    
											<td><b>ജില്ല : </b><?php echo $view['district'];?></td>
											<td><b>താലൂക്ക് : </b><?php echo $view['taluk'];?></td>
										  </tr>
										  <tr>
											<td><b>ജനിച്ചസ്ഥലം : </b><?php echo $view['pob'];?></td>
											<td><b>മേൽവിലാസം : </b><?php echo $view['address'];?></td>
										  </tr>
										  <tr>
										    <td><b>ജോലി / തൊഴിൽ : </b><?php echo $view['job'];?></td>
											<td><b>ഇ-മെയിൽ അഡ്രസ് : </b><?php echo $view['email'];?></td>
											
										  </tr>
										  
										
										</table><br/>	
	                                   <table id="customers">
									   <tr>
									      <th>നാമനിർദ്ദേശത്തിൻ്റെ വിശദാംശങ്ങൾ </th>
								       </tr>
									</table><br/>	
									<table id="customers">
										  
										<tr>
											<td><b>പ്രവർത്തനമേഖല  : </b><?php echo $view['title'];?></td>
											<td><b>ഉപ മേഖല : </b><?php echo $view['awardAreaSubTitle'];?></td>
										  </tr>
										  <tr>
											<td><b>ഏത് കേരള പുരസ്കാരത്തിനാണ് ശിപാർശ : </b><?php if($view['award']==1){?> കേരള ജ്യോതി <?php }else if($view['award']==2){ ?> കേരള പ്രഭ <?php }else if($view['award']==3){ ?>കേരള ശ്രീ    <?php } ?></td>
											<td><b>പത്മ പുരസ്‌കാരം ലഭിച്ചിട്ടുണ്ടോ: </b><?php if($view['pyn']==1){?> ഉണ്ട് <?php }else{ ?> ഇല്ല <?php } ?></td>
										  </tr>
										  <tr>
											<td><b>ഏത് പുരസ്‌കാരമാണ് ലഭിച്ചത് : </b><?php echo $view['pAward']; ?></td>
											<td><b>ഏത് വർഷമാണ് ലഭിച്ചത് : </b><?php echo $view['pYear'];?></td>
										  </tr>
										  </table><br/><br/><br/><br/><br/><br/><br/>
										  <table id="customers">
										  <tr>
											<td><b>നാമനിദ്ദേശം ചെയ്യുന്ന വ്യക്തിയെക്കുറിച്ചുള്ള ചെറുവിവരണം : </b> <br/><br/> <?php echo $view['note'];?></td>
										  </tr>
										  <tr>
											<td><b>നാമനിദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തി സമൂഹത്തിന് നൽകിയിട്ടുള്ള കാര്യമായ സംഭാവനകൾ : </b><br/><br/><?php echo $view['donation'];?></td>
										  </tr>
										  <tr>
											<td><b>നാമനിദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തിയുടെ നേട്ടങ്ങൾ </b><br/><br/><?php echo $view['achivement'];?></td>
											
										  </tr>
										  
										
										</table><br/>
										<table id="customers">
									   <tr>
									      <th>നാമനിർദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തിയുടെ പ്രവർത്തനങ്ങൾ ചെലുത്തിയ സ്വാധീനം </th>
								       </tr>
									</table><br/>		
										<table id="customers">
										  <tr>
											<td><b>പ്രവർത്തനമേഖലയിലെ സംഭാവന : </b><br/><br/><?php echo $view['wDonation'];?></td>
										  </tr>
										  <tr>
											<td><b>സമൂഹത്തിനുള്ള സംഭാവന </b><br/><br/><?php echo $view['sDonation'];?></td>
										  </tr>
										  </table>
										  <table id="customers">
										  <tr>
											<td><b>തിയതി </b><br/><br/><?php echo $view['date'];?></td>
											<td style="text-align:right;"><b>നാമനിർദ്ദേശം ചെയ്യുന്ന ആളിൻ്റെ ഒപ്പ് </b><br/><br/><span style="float:right;">&nbsp;&nbsp;Digitally Signed By : <?php echo $view['ename'];?></span><img style="float:right;" src="<?php echo act_url();?>public/images/esign.jpg"  width="50" height="50"/></td>
										  </tr>
										
										</table><br/>
									</div>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo act_url();?>public/adminscripts/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo act_url();?>public/adminscripts/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo act_url();?>public/adminscripts/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo act_url();?>public/adminscripts/vendors/nprogress/nprogress.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo act_url();?>public/adminscripts/build/js/custom.min.js"></script>
  </body>
</html>