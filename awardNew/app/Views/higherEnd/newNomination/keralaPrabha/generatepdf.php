<?php echo view('higherEnd/includes/header'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Kerala State IT Mission </title>

    <!-- Bootstrap -->
    <link href="<?php echo act_url(); ?>public/adminscripts/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo act_url(); ?>public/adminscripts/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="<?php echo act_url(); ?>public/adminscripts/build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">


            <!-- top navigation -->

            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main" style="margin-left:0px;">
                <div class="">



                    <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel" style="border:none;">

                                <div class="x_content">

                                    <section class="content invoice">
                                        <!-- title row -->
                                        <div class="row">
                                            <div class="col-xs-12 invoice-header">
                                                <br />
                                                <h1>
                                                    <i class="fa fa-trophy"></i> കേരള പ്രഭ <small><?php // echo $reson;
                                                                                                    ?></small>
                                                    <!--<small class="pull-right">Date: 16/08/2016</small>-->
                                                    <button style="float:right;" class="btn btn-default" onClick="window.print();"><i class="fa fa-print"></i> Print</button>
                                                </h1>

                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <br /><br />

                                        <!-- Table row -->
                                        <div class="row">
                                            <div class="col-xs-12 table">
                                                <table class="table table-striped">
                                                    <tbody>
                                                        <tr>
                                                            <td><b>നമ്പർ </b></td>
                                                            <td><b>രജിസ്റ്റർ നമ്പർ</b></td>
                                                            <td><b>നാമനിർദ്ദേശം ചെയ്തയാൾ</b></td>
                                                            <td><b>നാമനിർദ്ദേശം ചെയ്യപ്പെട്ടയാൾ</b></td>
                                                            <td><b>പ്രവർത്തനമേഖല</b></td>
                                                            <td><b>തീയതി</b></td>
                                                            <td><b>ഫോട്ടോ</b></td>
                                                        </tr>

                                                        <?php
                                                        if(isset($keralaprabha)){
                                                        $count_no = 1;
                                                        foreach ($keralaprabha as $row) { ?>
                                                            <tr>
                                                                <td><?php echo $count_no; ?></td>
                                                                <td><?php echo $row['nominationRegID']; ?></td>
                                                                <?php if ($row['regType'] == 1) { ?>
                                                                    <td><?php echo $row['fName']; ?> <?php echo $row['mName']; ?> <?php echo $row['lName']; ?><br />( <?php echo $row['nominator']; ?> )<br/><br/>
                                                                    <img style="width: 1.3em; height: 1.2em;" src="<?php echo act_url();?>public/images/esign.jpg"  /><span>Digitally Signed By : </span><?php echo $row['ename'] ;?></td>
                                                                <?php } else { ?>
                                                                    <td><?php echo $row['organisationName']; ?><br /> ( ഓർഗനൈസേഷൻ )</td>
                                                                <?php } ?>
                                                                <td><?php echo $row['name']; ?></td>
                                                                <td><?php echo $row['title']; ?></td>
                                                                <td><?php echo $row['date']; ?></td>
                                                                <td>
                                                                    <?php if ($row['photo'] != '') { ?>
                                                                        <img src="<?php echo act_url(); ?>public/images/nomination/<?php echo $row['photo']; ?>" alt="<?php echo $row['nominationRegID']; ?>" width="81" height="81">
                                                                </td>
                                                            <?php } else { ?>
                                                                <img src="<?php echo act_url(); ?>public/images/noImage.jpg" alt="<?php echo $row['nominationRegID']; ?>" width="81" height="81">
                                                            <?php } ?>
                                                            </tr>
                                                        <?php $count_no++;
                                                        } ?>
                                                        <?php  } else { ?>
                                    <div class="text-center">
                                        <h4 class="text-center">No Records</h4>
                                    </div>
                                <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <!-- /.row -->




                                        <!-- /.row -->

                                        <!-- this row will not appear when printing -->

                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->

            <!-- footer content -->

            <!-- /footer content -->
        </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo act_url(); ?>public/adminscripts/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo act_url(); ?>public/adminscripts/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo act_url(); ?>public/adminscripts/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo act_url(); ?>public/adminscripts/vendors/nprogress/nprogress.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo act_url(); ?>public/adminscripts/build/js/custom.min.js"></script>
</body>

</html>