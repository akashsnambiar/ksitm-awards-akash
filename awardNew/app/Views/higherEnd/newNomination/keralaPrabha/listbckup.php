<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>
<style>
  .year-dropdown {
    color: #333;
    background-color: #fff;
    border-color: #adadad;
  }

  .text-wrap {
    white-space: normal;
    font-size: 0.9rem;
  }

  .wrap-text {
    white-space: normal !important;
    line-height: 1.5 !important;
  }
</style>

<body>
  <div class="container">
    <div class="col-md-12 left_col">

      <div class="right_col" role="main">
        <div class="mt-3">
          <div class="page-title">
            <form name="frmproduct_add_text" method="POST" action="<?php echo base_url(); ?>higherEnd/keralaprabha/year/<?php echo $print; ?>" onchange="formsubmit()" id="searchform">
              <?= csrf_field() ?>
              <div class="d-flex justify-content-center pb-3">
                <div class="title_left px-3">
                  <label>Select Year wise</label>
                  <select class="btn btn-default dropdown-toggle year-dropdown px-5" id="year" name="year">
                    <option value="">All</option>
                    <?php
                    foreach ($year as $rows) { ?>
                      <option value="<?php echo $rows['year']; ?>" <?php if ($rows['year'] == $year_sel) {
                                                                      echo 'selected';
                                                                    }  ?>><?php echo $rows['year'] ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </form>
          </div>

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="card">
                <div class="card-header">
                  <h5><small>Kerala Prabha > <?php echo $pagename; ?></small></h5>
                </div>
                <form method="POST" action="<?php echo base_url(); ?>higherEnd/keralaprabha/generatepdf/<?php echo $print; ?>" id="pdfform" target="_blank">
                  <?= csrf_field() ?>
                  <div class="ms-3 mt-3">
                    <button type="submit" class="btn btn-primary btn-pdf" onclick="pdfsubmit(event)"><i class="fa fa-download"></i> Generate PDF</button>
                  </div>
                  <input type="hidden" id="printyr" name="year" value="<?php echo $year_sel; ?>" />
                </form>
                <div class="table-responsive p-3">
                  <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>SlNo:</th>
                        <th>Reg. No.</th>
                        <th>Nominator Name</th>
                        <th>Nominee</th>
                        <th>Photo</th>
                        <th>Date</th>
                        <?php if ($_SESSION['AdminRole'] == 'admin') { ?>
                          <th>Status</th>
                        <?php } else { ?>
                          <th>Status</th>
                        <?php } ?>
                        <th>Review</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if (isset($keralaprabha)) {
                        $count_no = 1;
                        foreach ($keralaprabha as $row) {

                      ?>
                          <tr>
                            <td><?php echo $count_no; ?></td>
                            <td><?php echo $row['nominationRegID']; ?></td>
                            <?php if ($row['regType'] == 1) { ?>
                              <td>
                                <?php echo $row['fName']; ?> <?php echo $row['mName']; ?> <?php echo $row['lName']; ?><br />( <?php echo $row['nominator']; ?> )<br /><br />
                                <img style="width: 1.3em; height: 1.2em;" src="<?php echo act_url(); ?>public/images/esign.jpg" /><span>Digitally Signed By : </span><?php echo $row['ename']; ?>
                              </td>
                            <?php } else { ?>
                              <td><?php echo $row['organisationName']; ?><br /> ( ഓർഗനൈസേഷൻ )</td>
                            <?php } ?>

                            <td><?php echo $row['name']; ?></td>
                            <td>
                              <?php if ($row['photo']) { ?>
                                <img src="<?php echo act_url(); ?>public/images/nomination/<?php echo $row['photo']; ?>" style=" width:100px; height:100px;" />
                              <?php } else { ?>
                                <img src="<?php echo act_url(); ?>public/images/Image_Available.jpg" style=" width:100px; height:100px;" />
                              <?php } ?>
                            </td>
                            <td><?php echo $row['date'];  ?><input type="hidden" value="<?php echo $row['year']; ?>" id="yrid"><input type="hidden" value="<?php echo $yr; ?>" id="all"></td>

                            <?php if ($_SESSION['AdminRole'] == 'admin') { ?>
                              <td>
                                <?php if ($row['statuss'] == 0) {
                                ?>
                                  <?php if ($row['review'] == 0) { ?>
                                    <a href="" onclick="openacceptModal('<?= $row['nominationID']; ?>')" class="btn btn-success btn-xs mb-1" data-bs-toggle="modal" data-bs-target="#acceptModal"> Accept </a>
                                    <a href="" onclick="openModal('<?= $row['nominationID']; ?>')" data-toggle="modal" data-target="#search_form" class="btn btn-danger btn-xs" data-bs-toggle="modal" data-bs-target="#exampleModal" data-toggle="tooltip"> Reject </a>

                                  <?php } else { ?>
                                    <a href="" onclick="openacceptModal('<?= $row['nominationID']; ?>')" class="btn btn-success btn-xs mb-1" data-bs-toggle="modal" data-bs-target="#acceptModal"> Accept </a>
                                    <a href="<?php echo base_url(); ?>higherEnd/keralaprabhastatus/<?= $row['nominationID'] ?>/2" class="btn btn-danger btn-xs" data-toggle="tooltip"> Reject </a>
                                  <?php } ?>
                                <?php } else if ($row['statuss'] == 1) { ?>
                                  <a href="<?php echo base_url(); ?>higherEnd/keralaprabhastatus/<?= $row['nominationID'] ?>/0" class="btn btn-success btn-xs mb-1"> Restore </a>
                                <?php } else if ($row['statuss'] == 2) { ?>
                                  <a href="<?php echo base_url(); ?>higherEnd/keralaprabhastatus/<?= $row['nominationID'] ?>/0" class="btn btn-danger btn-xs"> Restore </a>
                                <?php } ?>
                              </td>
                            <?php } else { ?>
                              <td>
                                <?php if ($row['statuss'] == 1) { ?>
                                  <p class="btn btn-success btn-xs mb-1"> Accepted </p>
                                <?php }
                                if ($row['statuss'] == 2) { ?>
                                  <p class="btn btn-danger btn-xs"> Rjected </p>
                                <?php } ?>
                              </td>
                            <?php } ?>

                            <td>
                              <?php if ($reviewStatus == 1) { ?>
                                <a href="<?php echo base_url(); ?>higherEnd/keralaprabha/review/<?= encode_url($row['nominationID']) ?>" class="btn btn-warning btn-xs mb-1"><i class="fa fa-pencil"></i> Review </a>
                              <?php } else { ?>
                                <a href="<?php echo base_url(); ?>higherEnd/reviewView/<?= encode_url($row['nominationID']) ?>" class="btn btn-warning btn-xs mb-1"><i class="fa fa-pencil"></i> Review </a>
                              <?php } ?>
                              <a href="<?php echo base_url(); ?>higherEnd/keralaprabha/view/<?= encode_url($row['nominationID']) ?>" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                            </td>
                          </tr>
                        <?php $count_no++;
                        } ?>
                      <?php  } else { ?>
                        <div class="text-center">
                          <h4 class="text-center">No Records</h4>
                        </div>
                      <?php } ?>
                    </tbody>
                  </table>
                  <form method="POST" id="search_form" action="<?php echo base_url(); ?>higherEnd/keralaprabha/reviewsave">
                    <?= csrf_field() ?>
                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">റിവ്യൂ നൽകുക</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                          </div>
                          <div class="modal-body">
                            <form>
                              <?= csrf_field() ?>
                              <div class="mb-3">
                                <input type="text" name="nomid" id="modalid" value="" hidden /><br />
                                <label for="message-text" class="col-form-label">Review</label>
                                <textarea name="text" class="form-control" rows="2" cols="200" style="height:100px;" id="recipient-name"></textarea>
                              </div>
                            </form>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="button" id="rejectNom" type="submit" name="Submit" class="btn btn-primary" data-bs-target="#confirmModal" data-bs-toggle="modal" data-bs-dismiss="modal">Reject</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal fade" id="confirmModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                          </div>
                          <div class="modal-body">
                            <form>
                              <?= csrf_field() ?>
                              <div class="modal-body">
                                Are you sure you want to reject the nomination
                              </div>
                            </form>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                            <button type="button" id="rejectNom" type="submit" name="Submit" class="btn btn-primary" onclick="form_submit()">Yes</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                  <form method="POST" id="acceptForm" action="">
                    <?= csrf_field() ?>
                    <div class="modal fade" id="acceptModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                          </div>
                          <div class="">
                            <form>
                              <?= csrf_field() ?>
                              <div class="modal-body">
                                Are you sure you want to accept the nomination
                                <input type="text" name="nomid" id="modalNomiid" value="" hidden /><br />
                              </div>
                            </form>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                            <button type="button" id="rejectNom" type="submit" name="Submit" class="btn btn-primary" onclick="accept_submit()">Ok</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  <!-- /page content -->

  <!-- footer content -->
  <?php echo view('higherEnd/includes/footerAdmin'); ?>
  <!-- /footer content -->


  <!-- Datatables -->
  <script src="<?php echo act_url(); ?>public/js/datatables/datatable.js"></script>
  <script src="<?php echo act_url(); ?>public/js/datatables/datatable.min.js"></script>
  <script src="<?php echo act_url(); ?>public/js/datatables/datatableBootstrap.js"></script>
  <!-- <script src="<?php echo act_url(); ?>public/js/datatables/dataTables.buttons.min.js"></script> -->
  <script src="<?php echo act_url(); ?>public/js/datatables/jszip.min.js"></script>
  <script src="<?php echo act_url(); ?>public/js/datatables/pdfmake.min.js"></script>
  <script src="<?php echo act_url(); ?>public/js/datatables/vfs_fonts.js"></script>
  <script src="<?php echo act_url(); ?>public/js/datatables/buttons.html5.min.js"></script>
  <script src="<?php echo act_url(); ?>public/js/datatables/buttons.print.min.js"></script>
  <!-- Datatables -->
  <script>
    $(document).ready(function() {
      $('#datatable').DataTable({
        dom: 'Bfrtip',
        // buttons: [
        //     'copy', 'csv', 'excel', 'pdf', 'print'
        // ]
        columnDefs: [{
            targets: '_all',
            className: 'wrap-text'
          } // Apply wrap-text class to all columns
        ],
      });
    });
  </script>
  <script type="text/javascript">
    function form_submit() {
      document.getElementById("search_form").submit();
    }

    function openModal(id) {
      if (id) {
        $("#modalid").val(id);

      }

    }

    function accept_submit() {
      document.getElementById("acceptForm").submit();
    }

    function openacceptModal(id) {
      if (id) {
        $("#modalNomiid").val(id);
        $('#acceptForm').attr('action', '<?php echo base_url(); ?>higherEnd/keralaprabhastatus/' + id + '/1');
      }

    }
    
    // function pdfsubmit() {
    //   document.getElementById("pdfform").submit();

    // }
    function pdfsubmit(event) {
      event.preventDefault(); // Prevent the default form submission
      var form = document.getElementById("pdfform");
      form.submit();
      // Reload the current page after a delay (e.g., 1 second)
      setTimeout(function() {
        window.location.reload();
        //window.location.href = "<?php echo base_url(); ?>higherEnd/keralaprabhacat/kpn";
      }, 1000); // 1000 milliseconds = 1 second
    }

    // Function to regenerate CSRF token after page reload
    // function regenerateCsrfToken() {
    //   var csrfInput = document.querySelector('input[name="csrf_test_name"]');
    //   fetch("<?php echo base_url(); ?>higherEnd/keralaprabhacat/kpn")
    //     .then(response => response.json())
    //     .then(data => {
    //       csrfInput.value = data.csrfToken;
    //     })
    //     .catch(error => console.error('Error:', error));
    // }

    // // Call the function to regenerate CSRF token after page reload
    // window.onload = regenerateCsrfToken;


    function formsubmit() {
      document.getElementById("searchform").submit();
    }
    document.addEventListener("DOMContentLoaded", function() {
      var aaaa = 1;
      var yearid = $('#yrid').val();
      var all = $('#all').val();
      //$('#printyr').html(yearid);
      var dropdown = document.getElementById("year");
      if (all != "") {
        dropdown.html("All");
      } else {
        for (let i = 0; i < dropdown.options.length; i++) {

          if (dropdown.options[i].value === yearid) {
            dropdown.selectedIndex = i;
            $('#printyr').val(yearid);
            break;
          }
        }
      }
    });
    function pdfsubmit(event) {
      event.preventDefault(); // Prevent the default form submission
      var form = document.getElementById("pdfform");
      form.submit();
      // Reload the current page after a delay (e.g., 1 second)
      setTimeout(function() {
        window.location.reload();
        //window.location.href = "<?php echo base_url(); ?>higherEnd/keralaprabhacat/kpn";
      }, 1000); // 1000 milliseconds = 1 second
    }
  </script>
</body>

</html>