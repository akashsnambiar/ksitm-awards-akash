<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>
<script src='https://www.google.com/recaptcha/api.js'></script>
<style>
    .text-wrap {
        white-space: normal;
        font-size: 0.9rem;
    }

    .wrap-text {
        white-space: normal !important;
        line-height: 1.5 !important;
    }
</style>

<body>
    <div class="container">
        <div class="col-lg-12 col-md-12">

            <!-- page content -->

            <div class="right_col" role="main">
                <div class="mt-5">
                    <!-- <div class="page-title mt-3">
                        <div class="title_left">
                            <h4>Control Panel for <?php echo $site_name; ?></h4><br />
                        </div>
                    </div> -->
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <!-- Error -->
                                <?php if (isset($validation)) : ?>
                                    <div class="alert alert-warning">
                                        <?= $validation->listErrors() ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (isset($error)) : ?>
                                    <div class="alert alert-warning">
                                        <?= $error ?>
                                    </div>
                                <?php endif; ?>
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Winners</h5>
                                    </div>
                                    <form class="form-horizontal p-5" name="formList" id="formList" method="POST" action="<?php echo base_url(); ?>higherEnd/add_winners" enctype="multipart/form-data">
                                    <?= csrf_field() ?>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Banner Title[Malayalam]</label>
                                            <div class="col-sm-7">
                                                <select name="title_cat_mal" class="form-select" id="title-cat-mal">
                                                    <option>--- Select ---</option>
                                                    <?php
                                                    if (isset($banner)) {
                                                        foreach ($banner as $row) {  ?>
                                                            <option value="<?php echo $row['id'];  ?>"><?php echo $row['banner_title_mal'];  ?></option>
                                                    <?php }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Banner Title[English]</label>
                                            <div class="col-sm-7">
                                                <input type="text" id="eng" class="form-control" readonly />
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Year</label>
                                            <div class="col-sm-7">
                                                <input type="text" name="win_year" id="year" class="form-control" readonly />
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Banner Category</label>
                                            <div class="col-sm-7">
                                                <input type="text" id="cat" class="form-control" readonly />
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Winner Name(English)</label>
                                            <div class="col-sm-7">
                                                <input type="text" name="name" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Winner Name(Malayalam)</label>
                                            <div class="col-sm-7">
                                                <input type="text" name="name_mal" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Area of Expertise</label>
                                            <div class="col-sm-7">
                                                <select name="awardarea" class="form-select" id="title-cat-mal">
                                                    <option>--- Select ---</option>
                                                    <?php
                                                    if (isset($award)) {
                                                        foreach ($award as $row) {  ?>
                                                            <option value="<?php echo $row['awardID'];  ?>"><?php echo $row['title'];  ?></option>
                                                    <?php }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Breif Description of area of Expertise(English)</label>
                                            <div class="col-sm-7">
                                                <input type="text" name="description" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Breif Description of area of Expertise(Malayalam)</label>
                                            <div class="col-sm-7">
                                                <input type="text" name="description_mal" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Winner Image</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" name="year" value="<?php date_default_timezone_set('Asia/Kolkata');
                                                                                                            echo date("Y-m-d H:i:s") ?>" hidden />
                                                <input class="form-control" id="photo" type="file" name="file" value="" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;">We request you not to keep your image title field empty, Upload image leass than 2MB</p>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-3"></div>
                                            <!-- <div class="col-sm-4">
                                                <div class="g-recaptcha" data-sitekey="<?php // echo $siteKey; ?>"></div>
                                            </div> -->
                                            <div class="col-sm-4">
                                                <button style="margin-top:30px;padding: 10px 30px;" type="submit" class="btn btn-primary" name="Submit" value="Send">Submit</button>
                                            </div>
                                        </div>
                                    </form>

                                    <div class="table-responsive mb-3 mx-5">
                                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap">
                                            <thead>
                                                <tr>
                                                    <th>SlNo:</th>
                                                    <th>Banner Title</th>
                                                    <th>Banner Category</th>
                                                    <th>Year</th>
                                                    <th>Winner Name</th>
                                                    <th>Area of Expertise</th>
                                                    <th>Description</th>
                                                    <th>Description Malayalam</th>
                                                    <th>Winner Image</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $count_no = 1;
                                                if (isset($winner)) {
                                                    foreach ($winner as $row) { ?>
                                                        <tr>
                                                            <td><?php echo $count_no; ?></td>
                                                            <td><?php echo $row['banner_title_mal'] ?> [<?php echo $row['banner_title_en'] ?>]</td>
                                                            <td><?php if (isset($row['nomination_category'])) {
                                                                    echo $row['nomination_category'];
                                                                } ?></td>
                                                            <td><?php echo $row['year'] ?></td>
                                                            <td><?php echo $row['winner_name'] ?>[<?php echo $row['winner_name_mal'] ?>]</td>
                                                            <td><?php echo $row['title_e'] ?></td>
                                                            <td><?php echo $row['description'] ?></td>
                                                            <td><?php echo $row['description_mal'] ?></td>
                                                            <td><img src="<?php echo act_url(); ?>public/images/banner/<?php echo $row['winner_photo']; ?>" style=" width:80px; height:60px;" /></td>
                                                            <td>
                                                                <a href="<?php echo base_url(); ?>higherEnd/winedit/<?php echo encode_url($row['id']) ?>"><iconify-icon icon="bx:edit" class="text-dark p-1" style="font-size: 1.3rem;"></iconify-icon></a>

                                                                <a href="" onclick="openacceptModal('<?= $row['id']; ?>')" data-bs-toggle="modal" data-bs-target="#acceptModal"><iconify-icon icon="clarity:trash-solid" class="p-1" style="font-size: 1.3rem; color: #c40202c9;"></iconify-icon></a>

                                                            </td>
                                                        </tr>
                                                <?php $count_no++;
                                                    }
                                                } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <form method="POST" id="acceptForm" action="">
                                    <?= csrf_field() ?>
                                        <div class="modal fade" id="acceptModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <div class="">
                                                        <form>
                                                        <?= csrf_field() ?>
                                                            <div class="modal-body">
                                                                Are you sure you want to delete
                                                                <input type="text" name="nomid" id="modalNomiid" value="" hidden /><br />
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                                                        <button type="button" id="rejectNom" type="submit" name="Submit" class="btn btn-primary" onclick="accept_submit()">Yes</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <!-- /page content -->
    </div>
    </div>
    <!-- footer content -->
    <?php echo view('higherEnd/includes/footerAdmin'); ?>
    <!-- /footer content -->
    
    <script type="text/javascript" language="javascript" src="<?php echo act_url(); ?>public/js/js/iconify-icon.min.js"></script>
    <script>
        $('#title-cat-mal').on("change", function() {
            var titleid = $('#title-cat-mal').val();
            //alert(titleid);
            var csrf_test_name = $('input[name="csrf_test_name"]').val();
            jQuery.ajax({
                type: 'POST',
                url: '<?php echo base_url(); ?>higherEnd/bannercontent',
                data: 'titleid=' + $("#title-cat-mal").val(),
                dataType: 'JSON',
                headers: {
                    'X-CSRF-TOKEN': csrf_test_name
                },
                success: function(response) {
                    //response = JSON.parse(response);
                    $('input[name="csrf_test_name"]').val(response.token);
                    console.log("aaa", response);
                    $("#eng").val(response.banner.banner_title_en);
                    $("#year").val(response.banner.year);
                    $("#cat").val(response.banner.nomination_category);

                }
            });
        });

        function accept_submit() {
            document.getElementById("acceptForm").submit();
        }

        function openacceptModal(id) {
            if (id) {
                $("#modalNomiid").val(id);
                $('#acceptForm').attr('action', '<?php echo base_url(); ?>higherEnd/windelete/' + id);
            }

        }
    </script>

    <!-- Datatables -->
    <script src="<?php echo act_url(); ?>public/js/datatables/datatable.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/datatable.min.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/datatableBootstrap.js"></script>
    <!-- <script src="<?php echo act_url(); ?>public/js/datatables/dataTables.buttons.min.js"></script> -->
    <script src="<?php echo act_url(); ?>public/js/datatables/jszip.min.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/pdfmake.min.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/vfs_fonts.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/buttons.html5.min.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/buttons.print.min.js"></script>
    <!-- Datatables -->

    <script>
        $(document).ready(function() {
            $('#datatable').DataTable({
                dom: 'Bfrtip',
                // buttons: [
                //     'copy', 'csv', 'excel', 'pdf', 'print'
                // ]
                columnDefs: [{
                        targets: '_all',
                        className: 'wrap-text'
                    } // Apply wrap-text class to all columns
                ],
            });
        });
    </script>
</body>

</html>