<!-- partial -->
<div class="main-panel">
  <div class="content-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <div class="home-tab">
          <div class="d-sm-flex align-items-center justify-content-between border-bottom">
            <div>
            </div>
          </div> 
          <div class="tab-content tab-content-basic">
            <div class="tab-pane fade show active" style="background-color: #f4f4f4 !important;" id="overview" role="tabpanel" aria-labelledby="overview">
              <div class="row">
                <div class="col-sm-12">
                  <div class="statistics-details d-flex align-items-center justify-content-between">
                    <div>
                      <p class="statistics-title">New Nominations</p>
                      <h3 class="rate-percentage"><?= $nominationNew; ?></h3>
                      <canvas class="my-auto" id="doughnutNewChart" height="150"></canvas>
                      <div id="doughnutnew-chart-legend" class="mt-5 text-center"></div>
                    </div>
                    <div>
                      <p class="statistics-title">Accepted Nominations</p>
                      <h3 class="rate-percentage"><?= $nominationAccepted; ?></h3>
                      <canvas class="my-auto" id="doughnutAccChart" height="150"></canvas>
                      <div id="doughnutacc-chart-legend" class="mt-5 text-center"></div>
                    </div>
                    <div>
                      <p class="statistics-title">Rejected Nominations</p>
                      <h3 class="rate-percentage"><?= $nominationRejected; ?></h3>
                      <canvas class="my-auto" id="doughnutRejChart" height="150"></canvas>
                      <div id="doughnutrej-chart-legend" class="mt-5 text-center"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>