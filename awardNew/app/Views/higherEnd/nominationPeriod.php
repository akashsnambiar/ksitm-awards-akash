<?php

echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>
<script src='https://www.google.com/recaptcha/api.js'></script>
<style>

    .text-wrap {
        white-space: normal;
        font-size: 0.9rem;
    }

    .wrap-text {
        white-space: normal !important;
        line-height: 1.5 !important;
    }
</style>
<body>
    <div class="container body">
        <div class="main_container">
            <div class="right_col" role="main">
                <div class="mt-3 mb-3">
                    <!-- <div class="page-title">
                        <div class="title_left">
                            <h4>Control Panel for <?php echo $site_name; ?></h4><br />
                        </div>

                    </div> -->
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="card">
                                    <div class="card-header"><h5>Nomination Period</h5></div>
                                    <form name="formList" id="formList" method="POST" action="<?php echo base_url(); ?>higherEnd/nominationperiod" class="form-horizontal p-5">
                                    <?= csrf_field() ?>
                                        <div class="form-group d-flex">
                                            <label class="col-sm-3 control-label">Nomination Start Date</label>
                                            <div class="col-sm-7">
                                                <input type="date" class="form-control" name="start_date" required />
                                            </div>
                                        </div>
                                        <div class="form-group d-flex">
                                            <label class="col-sm-3 control-label">Nomination End date</label>
                                            <div class="col-sm-7">
                                                <input type="date" class="form-control" name="end_date" required />
                                                <input type="text" class="form-control" name="year" required value="<?php date_default_timezone_set('Asia/Kolkata'); echo date("Y-m-d H:i:s") ?>" hidden />
                                            </div>
                                        </div>
                                        <div class="form-group d-flex">
                                            <label class="col-sm-3 control-label">Scrolling text(Malayalam)</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" name="news_mal" required />
                                            </div>
                                        </div>
                                        <div class="form-group d-flex">
                                            <label class="col-sm-3 control-label">Scrolling text (English)</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" name="news_eng" required />
                                            </div>
                                        </div>
                                        <div class="form-group d-flex">
                                            <div class="col-sm-7">
                                                <?php if (isset($message)) : ?>
                                                    <div class="alert alert-warning">
                                                        <?= $message ?>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            
                                            <div class="col-sm-4">
                                                <div class="g-recaptcha" data-sitekey="<?php echo $siteKey; ?>"></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <button style="margin-top:30px;padding: 10px 30px;" type="submit" class="btn btn-primary" name="Submit" value="Sign up">Submit</button>
                                            </div>
                                        </div>
                                    </form>

                                    <div class="" style="margin-left: 1em; margin-right: 1em;">

                                        <div class="table-responsive" style="margin-left: 1em; margin-right: 1em;">
                                            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>SlNo:</th>
                                                        <!-- <th>User Created</th> -->
                                                        <th>Start Date</th>
                                                        <th>End Date</th>
                                                        <th>Year</th>
                                                        <th>Scrolling text(Malayalam)</th>
                                                        <th>Scrolling text(English)</th>
                                                        <th>Status</th>
                                                        <th>#Edit</th>
                                                        <!-- <th>#Delete</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $count_no = 1;
                                                    foreach ($nomperiod as $row) { ?>
                                                        <tr>
                                                            <td><?php echo $count_no; ?></td>
                                                            <td><?php echo $row['start_date'] ?></td>
                                                            <td><?php echo $row['end_date'] ?></td>
                                                            <td><?php echo $row['year'] ?></td>
                                                            <td><?php echo $row['news_feed_mal'] ?></td>
                                                            <td><?php echo $row['news_feed_eng'] ?></td>
                                                            <td>
                                                                <?php if ($row['status'] == 1) {
                                                                    
                                                                ?>
                                                                    <a href="<?php echo base_url(); ?>higherEnd/nominationstatus/<?= $row['id'] ?>/0/" class="btn btn-success btn-xs">Open</a>
                                                                <?php } else {
                                                                ?>
                                                                    <a href="<?php echo base_url(); ?>higherEnd/nominationstatus/<?= $row['id'] ?>/1/" class="btn btn-danger btn-xs"> Closed </a>
                                                                <?php } ?>
                                                            </td>
                                                            <td><a href="<?php echo base_url(); ?>higherEnd/nomPeriodedit/<?php echo encode_url($row['id']) ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a></td>
                                                            <!-- <td>
                                                                <a href="<?php // echo base_url(); ?>higherEnd/nomperioddelete/<?php // echo $row['id'] ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                                            </td> -->
                                                        </tr>
                                                    <?php $count_no++;
                                                    } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>



        </div>
        <!-- /page content -->
    </div>
    </div>
    <!-- footer content -->
    <?php echo view('higherEnd/includes/footerAdmin'); ?>
    <!-- /footer content -->
    <!-- Datatables -->
  <script src="<?php echo act_url(); ?>public/js/datatable.js"></script>
  <script src="<?php echo act_url(); ?>public/js/datatable.min.js"></script>
  <script src="<?php echo act_url(); ?>public/js/datatableBootstrap.js"></script>


  <!-- Datatables -->

  <script>
        $(document).ready(function() {
            $('#datatable').DataTable({
                dom: 'Bfrtip',
                // buttons: [
                //     'copy', 'csv', 'excel', 'pdf', 'print'
                // ]
                columnDefs: [{
                        targets: '_all',
                        className: 'wrap-text'
                    } // Apply wrap-text class to all columns
                ],
            });
        });
    </script>
</body>

</html>