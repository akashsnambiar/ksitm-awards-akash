<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>
<script src='https://www.google.com/recaptcha/api.js'></script>

<body>
    <div class="container body">
        <div class="main_container">

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="page-title mt-3">
                        <!-- <div class="title_left">
                            <h4>Control Panel for <?php echo $site_name; ?></h4><br />
                        </div> -->

                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">

                                <div class="card mt-3">
                                    <!-- Error -->
                                    <?php if (isset($validation)) : ?>
                                        <div class="alert alert-warning">
                                            <?= $validation->listErrors() ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (isset($error)) : ?>
                                        <div class="alert alert-warning">
                                            <?= $error ?>
                                        </div>
                                    <?php endif; ?>
                                    <h5 class="card-header">Site Management &nbsp; › <small style="color:blue;">Add Notifications</small></h5>

                                    <form class="form-horizontal p-5" name="formList" id="formList" method="POST" action="<?php echo base_url(); ?>higherEnd/notification/add" enctype="multipart/form-data">
                                    <?= csrf_field() ?>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Notification Title [Malayalam]</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="title" type="text" name="title" value="" placeholder="Title [Malayalam]" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;">We request you not to keep your Notification Title field empty</p>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Notification Title [English]</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="title_e" type="text" name="title_e" value="" placeholder="Title [English]" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;">We request you not to keep your Notification Title field empty</p>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Type of notification</label>
                                            <div class="col-sm-7">
                                                <input type="radio" name="not_type" value="Y" /><label for="Year wise" class="mx-1">Year wise</label>
                                                <input type="radio" name="not_type" value="G" class="mx-1" /><label for="General">General</label>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Year</label>
                                            <div class="col-sm-7">
                                                <input type="text" name="year" value="" class="form-control" id="currentYear" readonly />
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Notification Upload</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="photo" type="file" name="file" value="" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;">Upload only PDF File, Upload pdf less than 10MB</p>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-3"></div>
                                            <!-- <div class="col-sm-4">
                                                <div class="g-recaptcha" data-sitekey="<?php // echo $siteKey; ?>"></div>
                                            </div> -->
                                            <div class="col-sm-4">
                                                <button style="margin-top:30px;padding: 10px 30px;" type="submit" class="btn btn-primary" name="Submit" value="Send">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>



                            </div>
                        </div>
                    </div>

                </div>
            </div>



        </div>
    </div>
    </div>
    </div>
    <!-- /page content -->

    <!-- footer content -->
    <?php echo view('higherEnd/includes/footerAdmin'); ?>
    <!-- /footer content -->

    </div>
    <script>
        $(document).ready(function() {
            var currentYear = new Date().getFullYear();
            $('#currentYear').val(currentYear);
        });
    </script>


</body>

</html>