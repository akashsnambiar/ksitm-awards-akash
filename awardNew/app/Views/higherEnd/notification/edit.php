<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>
<script src='https://www.google.com/recaptcha/api.js'></script>

<body>
    <div class="container nav-md">
        <div class="container">
            <div class="col-lg-12 col-md-12 left_col">
                <div class="mt-5">
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                               
                                <div class="card">
                                    <!-- Error -->
                                    <?php if (isset($validation)) : ?>
                                        <div class="alert alert-warning">
                                            <?= $validation->listErrors() ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (isset($error)) : ?>
                                        <div class="alert alert-warning">
                                            <?= $error ?>
                                        </div>
                                    <?php endif; ?>
                                    <h5 class="card-header">Site Management &nbsp; › <small style="color:blue;">Add Notifications</small></h5>

                                    <?php if(isset($notification['id'])){ ?>
                                        
                                    <form class="form-horizontal p-5" name="formList" id="formList" method="POST" action="<?php echo base_url(); ?>higherEnd/notifiedit/<?php if(isset($notification['id'])){echo encode_url($notification['id']); } ?>" enctype="multipart/form-data">
                                    <?= csrf_field() ?>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Notification Title [Malayalam]</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="title" type="text" name="title" value="<?php echo (isset($notification['not_title'])) ? $notification['not_title'] : set_value('title'); ?>" placeholder="Title [Malayalam]" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;">We request you not to keep your Notification Title field empty</p>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Notification Title [English]</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="title_e" type="text" name="title_e" value="<?php echo (isset($notification['not_title_e'])) ? $notification['not_title_e'] : set_value('title_e'); ?>" placeholder="Title [English]" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;">We request you not to keep your Notification Title field empty</p>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Type of notification</label>
                                            <div class="col-sm-7">
                                                <input type="radio" name="not_type" value="Y" <?php if($notification['not_type'] == 'Y') { echo 'checked'; } ?> /><label for="Year wise" class="mx-1">Year wise</label>
                                                <input type="radio" name="not_type" value="G" <?php if($notification['not_type'] == 'G') { echo 'checked'; } ?> class="mx-1" /><label for="General">General</label>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Year</label>
                                            <div class="col-sm-7">
                                                <input type="text" name="year" value="<?php echo (isset($notification['year'])) ? $notification['year'] : set_value('year'); ?>" class="form-control" id="currentYear" readonly />
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Notification Upload</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="photo" type="file" name="file" value="<?php echo (isset($notification['not_pdf'])) ? $notification['not_pdf'] : set_value('file'); ?>" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;">Upload only PDF File, Upload pdf less than 10MB</p>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-3"></div>
                                            <!-- <div class="col-sm-4">
                                                <div class="g-recaptcha" data-sitekey="<?php // echo $siteKey; ?>"></div>
                                            </div> -->
                                            <div class="col-sm-4">
                                                <button style="margin-top:30px;padding: 10px 30px;" type="submit" class="btn btn-primary" name="Submit" value="Send">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                    <?php  }else{ ?>
                                        <div class="text-center" >
                                        <h4 class="text-center">No Records</h4>
                                        </div>
                                   <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <!-- /footer content -->
    </div>
    </div>
    <?php echo view('higherEnd/includes/footerAdmin'); ?>
   

</body>

</html>