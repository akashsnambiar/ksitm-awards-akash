<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>
<style>
    table.dataTable tbody td {
        word-break: break-word;
        vertical-align: top;
    }

    .text-wrap {
        white-space: normal;
    }

    .width-200 {
        width: 220px;
    }

    .year-dropdown {
        color: #333;
        background-color: #fff;
        border-color: #adadad;
    }
</style>

<body>
    <div class="container">
        <div class="page-title mt-3">
            <form name="frmproduct_add_text" method="POST" action="<?php echo base_url(); ?>higherEnd/organisation/year" id="search_form">
                <?= csrf_field() ?>
                <div class="d-flex justify-content-center pb-3">
                    <div class="title_left px-3">
                        <label>Select Year wise</label>
                        <select class="btn btn-default dropdown-toggle year-dropdown px-5" id="year" name="year" onchange="formsubmit()">
                            <option value="">All</option>
                            <?php foreach ($regyear as $rows) { ?>
                                <option id="year" value="<?php echo $rows['regdate']; ?>" <?php if (isset($year) && $year == $rows['regdate']) echo 'selected="selected"'; ?>><?php echo $rows['regdate'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </form>
        </div>
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <h5 class="mt-1">Registration › <small>Organisation</small></h5>
                </div>
                <div class="table-responsive  p-3 mt-3">
                    <table id="datatable" class="table table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th>SlNo:</th>
                                <th>Organisation Type</th>
                                <th>Organisation</th>
                                <th>Name</th>
                                <th>Registered On</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count_no = 1;
                            foreach ($organisation as $row) {

                            ?>
                                <tr>
                                    <td><?php echo $count_no; ?></td>
                                    <td><?php echo $row['organisation_e']; ?></td>
                                    <td><?php echo $row['organisationName']; ?></td>
                                    <td><?php echo $row['concernedPerson']; ?></td>
                                    <td><?php echo $row['date'];  ?><input type="hidden" value="<?php echo $row['year']; ?>" id="yrid"><input type="hidden" value="<?php echo $yr; ?>" id="all"></td>
                                    <td>
                                        <?php if ($row['status'] == 1) {
                                        ?>
                                            <a href="<?php echo base_url(); ?>higherEnd/organisationstatus/<?php echo $row['id'];  ?>/1" class="btn btn-success btn-xs"> Success </a>
                                        <?php  } else {
                                        ?>
                                            <a href="<?php echo base_url(); ?>higherEnd/organisationstatus/<?php echo $row['id']; ?>/0" class="btn btn-danger btn-xs"> Failure </a>
                                        <?php  } ?>
                                    </td>
                                    <td>
                                        <a href="<?php echo base_url(); ?>higherEnd/organisation/view/<?php echo encode_url($row['id']); ?>" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                                    </td>
                                </tr>
                            <?php $count_no++;
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- footer content -->
    <?php echo view('higherEnd/includes/footerAdmin'); ?>
    <!-- /footer content -->

    <!-- Datatables -->
    <script src="<?php echo act_url(); ?>public/js/datatables/datatable.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/datatable.min.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/datatableBootstrap.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/dataTables.buttons.min.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/jszip.min.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/pdfmake.min.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/vfs_fonts.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/buttons.html5.min.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/buttons.print.min.js"></script>
    <!-- Datatables -->

    <script>
        // function formsubmit() {
        //     document.getElementById("search_form").submit();
        // }
        function formsubmit() {
            var selectedYear = document.getElementById("year").value;
            var encryptedYearPromise = encodeUrl(selectedYear);
            Promise.all([encryptedYearPromise]).then(function(values) {
                var encryptedId = values[0];
                window.location.href = "<?php echo base_url('higherEnd/organisation/year?year='); ?>" + encryptedId;
            });
        }



        $(document).ready(function() {
            $('#datatable').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });

            var yearid = $('#yrid').val();
            console.log('qqqqqqqqqqqqqqqqqq');
            console.log('yearidddd', yearid);
            var all = $('#all').val();
            console.log("yearid", yearid);
            console.log("all", all);
            if (all != "") {
                $('#year').val("");
            } else {
                $('#year').val(yearid);
            }

        });
    </script>
</body>

</html>