<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>

<body>
  <div class="container">
    <div class="col-md-12 left_col">

      <div class="right_col" role="main">
        <div class="">

          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel mt-5">
                <?php if (isset($registercontent)) { ?>
                  <h5>Registration › Organisation Details ›<small> <?php echo  $registercontent['concernedPerson']; ?></small></h5>

                  <div class="table-responsive mt-3">
                    <!-- <a href="<?php // echo base_url(); 
                                  ?>higherEnd/organisation/year"> <span class="mdi mdi-arrow-left-bold" style="font-size: 20px;">Back</span></a> -->

                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <tbody>
                        <?php if ($registercontent['organisationType'] != 0) { ?>
                          <tr>
                            <td>Type of Organisation</td>
                            <td><?php echo $registercontent['organisation']; ?></td>
                          </tr>
                        <?php } ?>
                        <?php if ($registercontent['organisationDept'] != 0) { ?>
                          <tr>
                            <td>Department</td>
                            <td><?php echo $registercontent['orgSub']; ?></td>
                          </tr>
                        <?php } ?>

                        <tr>
                          <td>Local Body</td>
                          <td><?php echo $registercontent['localbody']; ?></td>
                        </tr>


                        <tr>
                          <td>District</td>
                          <td><?php echo $registercontent['district']; ?></td>
                        </tr>


                        <tr>
                          <td>Block Panchayath</td>
                          <td><?php echo $registercontent['blockpanchayath']; ?></td>
                        </tr>


                        <tr>
                          <td>Panchayath</td>
                          <td><?php echo $registercontent['panchayath']; ?></td>
                        </tr>


                        <tr>
                          <td>Muncipality</td>
                          <td><?php echo $registercontent['muncipality']; ?></td>
                        </tr>

                        <tr>
                          <td>Name of Organisation</td>
                          <td><?php echo $registercontent['organisationName']; ?></td>
                        </tr>
                        <tr>
                          <td>Name of the Concerned Person</td>
                          <td><?php echo $registercontent['concernedPerson']; ?></td>
                        </tr>
                        <tr>
                          <td>Designation</td>
                          <td><?php echo $registercontent['designation']; ?></td>
                        </tr>
                        <tr>
                          <td>Mobile No.</td>
                          <td><?php echo $registercontent['mobileCode']; ?> <?php echo $registercontent['mobileNumber']; ?></td>
                        </tr>
                        <tr>
                          <td>Email-ID</td>
                          <td><?php echo $registercontent['email']; ?></td>
                        </tr>
                        <tr>
                          <td>Other Contact Detail</td>
                          <td><?php echo $registercontent['otherContactDetail']; ?></td>
                        </tr>

                        <tr>
                          <td>State / UT</td>
                          <td><?php echo $registercontent['state']; ?></td>
                        </tr>


                        <tr>
                          <td>District</td>
                          <td><?php echo $registercontent['district']; ?></td>
                        </tr>


                        <tr>
                          <td>Identity Document</td>
                          <td><?php echo $registercontent['identityDocument']; ?></td>
                        </tr>

                        <tr>
                          <td>Identity Document Number</td>
                          <td><?php echo $registercontent['inputDocNum']; ?></td>
                        </tr>
                        <tr>
                          <td>Document</td>
                          <?php
                          if ($registercontent['docFile']) { ?>
                            <td><a href="<?php echo act_url(); ?>public/images/registration/organisation/<?php echo $registercontent['docFile']; ?>" download="doc"><img src="<?php echo act_url(); ?>public/images/pdficon.png" width="40" /></a></td>
                          <?php } else { ?>
                            <td>No doc uploaded</td>
                          <?php } ?>
                        </tr>

                      </tbody>
                    </table>

                  </div>
                <?php  } else { ?>
                  <div class="text-center">
                    <h4 class="text-center">No Records</h4>
                  </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>


    </div>
  </div>
  </div>
  <!-- footer content -->
  <?php echo view('higherEnd/includes/footerAdmin'); ?>
  <!-- /footer content -->


</body>

</html>