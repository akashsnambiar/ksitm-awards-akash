<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>

<body>
    <div class="container">
        <div class="col-md-12">
            <!-- page content -->
            <div class="" role="main">
                <div class="">
                    <div class="row mt-3">
                        <?php if (isset($registercontent)) { ?>
                            <h5>Registration › Individual Details ›<small> <?php echo  $registercontent['fName']; ?> <?php echo $registercontent['mName']; ?> <?php echo $registercontent['lName']; ?></small></h5>

                            <div class="clearfix"></div>
                            <div class="table-responsive m-3">
                                <!-- <a href="<?php echo base_url(); ?>higherEnd/individual/year"> <span class="mdi mdi-arrow-left-bold" style="font-size: 20px;">Back</span></a> -->
                                <table class="table table-striped table-bordered">
                                    <tbody>
                                        <tr>
                                            <td>Name</td>
                                            <td><?php echo  $registercontent['fName']; ?> <?php echo $registercontent['mName']; ?> <?php echo $registercontent['lName']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Nominater Type</td>
                                            <td><?php echo $registercontent['nominator']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Mobile Number</td>
                                            <td><?php echo $registercontent['mobileCode']; ?> <?php echo $registercontent['mobileNumber']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td><?php echo $registercontent['email']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Identity Document</td>
                                            <td><?php echo $registercontent['identityDocument']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Identity Document Number </td>
                                            <td><?php echo $registercontent['inputDocNum']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Document</td>
                                            <?php
                                            if ($registercontent['docFile']) { ?>
                                                <td><a href="<?php echo act_url(); ?>public/images/registration/individual/<?php echo $registercontent['docFile']; ?>" download="doc"><img src="<?php echo act_url(); ?>public/images/pdficon.png" width="40" /></a></td>
                                            <?php } else { ?>
                                                <td>No doc uploaded</td>
                                            <?php } ?>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        <?php  } else { ?>
                            <div class="text-center">
                                <h4 class="text-center">No Records</h4>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>


    </div>
    <!-- footer content -->
    <?php echo view('higherEnd/includes/footerAdmin'); ?>
    <!-- /footer content -->
    </div>
    </div>
    <!-- Datatables -->
    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <!-- Bootstrap -->
    <script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <!-- FastClick -->
    <script src="https://cdn.datatables.net/buttons/2.3.6/js/dataTables.buttons.min.js"></script>
    <!-- NProgress -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.3.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.3.6/js/buttons.print.min.js"></script>

    <!-- /Datatables -->
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        });
    </script>
</body>

</html>