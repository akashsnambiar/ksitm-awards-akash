<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>
<script src='https://www.google.com/recaptcha/api.js'></script>

<body>
    <div class="container body">
        <div class="main_container">
            <div class="right_col" role="main">
                <div class="mt-3 mb-4">
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="card">
                                    <div class="card-header"><h5>Nomination Period</h5></div>
                                    <?php if(isset($nomperiod['id'])){ ?>
                                    <form name="formList" id="formList" method="POST" action="<?php echo base_url(); ?>higherEnd/nomPeriodedit/<?php echo encode_url($nomperiod['id']); ?>" class="form-horizontal  p-5">
                                    <?= csrf_field() ?> 
                                        <!-- <?php // if($alert==TRUE){
                                                ?>
                                        <div class="alert alert-danger" role="alert">
                                            <?php // echo validation_errors(); 
                                                ?>
                                            <?php // if($message!=''){
                                                //echo $message;
                                                // }
                                                ?>
                                        </div>
                                        <?php // } 
                                        ?> -->
                                        <div class="form-group d-flex">
                                            <label class="col-sm-3 control-label">Nomination Start Date</label>
                                            <div class="col-sm-7">
                                                <input type="date" class="form-control" name="start_date" placeholder="Start Date" value="<?php echo (isset($nomperiod['start_date'])) ? $nomperiod['start_date'] : set_value('start_date'); ?>" />
                                            </div>
                                        </div>
                                        <div class="form-group d-flex">
                                            <label class="col-sm-3 control-label">Nomination End date</label>
                                            <div class="col-sm-7">
                                                <input type="date" class="form-control" name="end_date" placeholder="End Date" required value="<?php echo (isset($nomperiod['end_date'])) ? $nomperiod['end_date'] : set_value('end_date'); ?>" />
                                                <input type="hidden" name="nomid" value="<?php echo $nomperiod['id']; ?>"  />
                                            </div>
                                        </div>
                                        <div class="form-group d-flex">
                                            <label class="col-sm-3 control-label">Scrolling text(Malayalam)</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" name="news_mal" value="<?php echo (isset($nomperiod['news_feed_mal'])) ? $nomperiod['news_feed_mal'] : set_value('news_mal'); ?>" required />
                                            </div>
                                        </div>
                                        <div class="form-group d-flex">
                                            <label class="col-sm-3 control-label">Scrolling text (English)</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" name="news_eng" value="<?php echo (isset($nomperiod['news_feed_eng'])) ? $nomperiod['news_feed_eng'] : set_value('news_eng'); ?>" required />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-4">
                                                <div class="g-recaptcha" data-sitekey="<?php echo $siteKey; ?>"></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <button style="margin-top:30px;padding: 10px 30px;" type="submit" class="btn btn-primary" name="Submit" value="Sign up">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                    <?php  }else{ ?>
                                        <div class="text-center" >
                                        <h4 class="text-center">No Records</h4>
                                        </div>
                                   <?php } ?>
                                    
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>



        </div>
        <!-- /page content -->
    </div>
    </div>
    <!-- footer content -->
    <?php echo view('higherEnd/includes/footerAdmin'); ?>
    <!-- /footer content -->

</body>

</html>