<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100">
          <div class="col-lg-4 col-md-4 mx-auto">
            <div class="auth-form-light text-left p-5">
              <div class="brand-logo">
                <img src="<?php echo act_url(); ?>public/images/logo.png" class="rounded mx-auto d-block" alt="logo">
              </div>
              <?php if(session()->getFlashdata('msg')):?>
                    <div class="alert alert-warning">
                       <?= session()->getFlashdata('msg') ?>
                    </div>
                <?php endif;?>
                
                <?php if(isset($validation)):?>
                <div class="alert alert-warning">
                   <?= $validation->listErrors() ?>
                </div>
                <?php endif;?>
              <h4>Admin Login</h4>
              <h6 class="fw-light"></h6>
             
              <form action="<?php echo base_url(); ?>higherEnd/login" method="post" class="pt-3">
              <?= csrf_field() ?>
                <div class="form-group">
                  <input type="text" name="username" class="form-control form-control-lg" placeholder="Username" value="">
                </div>
                <div class="form-group">
                  <input type="password"  name="password" class="form-control form-control-lg" autocomplete="off" id="exampleInputPassword1" placeholder="Password">
                </div>
                <div class="form-group">
                <div class="g-recaptcha" data-sitekey="<?php echo $siteKey;?>"></div> <br/>
                </div>
                <div class="mt-3">
                  <button type="submit" value="Send" name="Login" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" >SIGN IN</button>
                </div>
                <!-- <div class="my-2 d-flex justify-content-between align-items-center">
                  <div class="form-check">
                    <label class="form-check-label text-muted">
                      <input type="checkbox" class="form-check-input">
                      Keep me signed in
                    </label>
                  </div>
                  <a href="#" class="auth-link text-black">Forgot password?</a>
                </div>
                <div class="mb-2">
                  <button type="button" class="btn btn-block btn-facebook auth-form-btn">
                    <i class="ti-facebook me-2"></i>Connect using facebook
                  </button>
                </div>
                <div class="text-center mt-4 fw-light">
                  Don't have an account? <a href="register.html" class="text-primary">Create</a>
                </div> -->
              </form>
            </div>
          </div>
        </div>
        
      </div>
      <!-- content-wrapper ends -->
      
    </div>
    
