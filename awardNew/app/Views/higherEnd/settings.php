<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>
<script src='https://www.google.com/recaptcha/api.js'></script>

<body>
  <div class="container body">
    <div class="main_container">
      <div class="right_col" role="main">
        <div class="m-3">
          <!-- <div class="page-title">
            <div class="title_left">
              <h4>Control Panel for <?php echo $site_name; ?></h4><br />
            </div>
          </div> -->
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h5>Settings</h5>
                  <div class="clearfix"></div>
                </div>
                <!-- Error -->
                <?php if (isset($validation)) : ?>
                  <div class="alert alert-warning">
                    <?= $validation->listErrors() ?>
                  </div>
                <?php endif; ?>
                
                <div class="card p-5">
                  <form name="formList" id="formList" method="POST" action="<?php echo base_url(); ?>higherEnd/settings" class="form-horizontal">
                  <?= csrf_field() ?>
                    <div class="form-group d-flex">
                      <label class="col-sm-3 control-label">Admin Email</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" name="admin_email" placeholder="Admin Email" value="<?php echo (isset($admin_email)) ? $admin_email : set_value('admin_email'); ?>" readonly/>
                      </div>
                    </div>
                    <div class="form-group d-flex">
                      <label class="col-sm-3 control-label">Site Name</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" name="site_name" placeholder="Site Name"  value="<?php echo (isset($site_name)) ? $site_name : set_value('site_name'); ?>" />
                      </div>
                    </div>
                    <div class="form-group d-flex">
                      <label class="col-sm-3 control-label">Copy Right Year</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" name="copyright_year" placeholder="Copy Right Year"   value="<?php echo (isset($copyright_year)) ? $copyright_year : set_value('copyright_year'); ?>" />
                      </div>
                    </div>
                    <div class="form-group d-flex">
                      <label class="col-sm-3 control-label">Title Tag</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" name="title_tags" placeholder="Title Tag" value="<?php echo (isset($title_tags)) ? $title_tags : set_value('title_tags'); ?>" />
                      </div>
                    </div>
                    <div class="form-group d-flex">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Meta Tags</label>
                      <div class="col-md-7 col-sm-9 col-xs-12">
                        <textarea name="meta_tags" id="meta_tags" class="resizable_textarea form-control"  style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 75px;"><?php echo (isset($meta_tags)) ? $meta_tags : set_value('meta_tags'); ?></textarea>
                      </div>
                    </div>
                    <div class="form-group d-flex">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Meta Description</label>
                      <div class="col-md-7 col-sm-9 col-xs-12">
                        <textarea name="meta_description" id="meta_description" class="resizable_textarea form-control"  style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 75px;"><?php echo (isset($meta_description)) ? $meta_description : set_value('meta_description'); ?></textarea>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-sm-3"></div>
                      <!-- <div class="col-sm-4">
                        <div class="g-recaptcha" data-sitekey="<?php // echo $siteKey; ?>"></div>
                      </div> -->
                      <div class="col-sm-4">
                        <button style="margin-top:30px;padding: 10px 30px;" type="submit" class="btn btn-primary" name="Submit" value="Sign up">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>



              </div>
            </div>
          </div>

        </div>
      </div>



    </div>
    <!-- /page content -->
  </div>
  </div>
  <!-- footer content -->
  <?php echo view('higherEnd/includes/footerAdmin'); ?>
  <!-- /footer content -->

</body>

</html>