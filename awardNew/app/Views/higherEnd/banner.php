<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>
<script src='https://www.google.com/recaptcha/api.js'></script>

<body>
    <div class="container">
        <div class="col-lg-12 col-md-12">

            <!-- page content -->

            <div class="right_col" role="main">
                <div class="mt-5">
                    <!-- <div class="page-title mt-3">
                        <div class="title_left">
                            <h4>Control Panel for <?php echo $site_name; ?></h4><br />
                        </div>
                    </div> -->
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <!-- Error -->
                                <?php if (isset($validation)) : ?>
                                    <div class="alert alert-warning">
                                        <?= $validation->listErrors() ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (isset($error)) : ?>
                                    <div class="alert alert-warning">
                                        <?= $error ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (isset($message)) : ?>
                                    <div class="alert alert-warning">
                                        <?= $message ?>
                                    </div>
                                <?php endif; ?>
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Banner</h5>
                                    </div>
                                    <form class="form-horizontal p-5" name="formList" id="formList" method="POST" action="<?php echo base_url(); ?>higherEnd/add_banner" enctype="multipart/form-data">
                                    <?= csrf_field() ?>

                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Banner Title[Malayalam]</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="title" type="text" name="title" value="" placeholder="Title [Malayalam]" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;"></p>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Banner Title[English]</label>
                                            <div class="col-sm-7">
                                                <input class="form-control" id="title" type="text" name="title_e" value="" placeholder="Title [Malayalam]" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;"></p>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Year</label>
                                            <div class="col-sm-7">
                                                <select name="banyear" class="form-select">
                                                    <option>--- Select ---</option>
                                                    <option value="2022">2022</option>
                                                    <option value="2023">2023</option>
                                                    <option value="2024">2024</option>
                                                    <option value="2025">2025</option>
                                                    <option value="2026">2026</option>
                                                    <option value="2027">2027</option>
                                                    <option value="2028">2028</option>
                                                    <option value="2029">2029</option>
                                                    <option value="2030">2030</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Banner Category</label>
                                            <div class="col-sm-7">
                                                <select name="title_cat" class="form-select">
                                                    <option>--- Select ---</option>
                                                    <?php
                                                    if (isset($nomcat)) {
                                                        foreach ($nomcat as $row) {  ?>
                                                            <option value="<?php echo $row['id'];  ?>"><?php echo $row['nomination_category'];  ?></option>
                                                    <?php }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-sm-3 control-label">Award Image</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" name="year" value="<?php date_default_timezone_set('Asia/Kolkata');
                                                                                                            echo date("Y-m-d H:i:s") ?>" hidden />
                                                <input class="form-control" id="photo" type="file" name="file" value="" />
                                                <p style="color:#CC3333; font-size:10px; margin-top:10px;">We request you not to keep your image title field empty, Upload image leass than 2MB</p>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-3"></div>
                                            <!-- <div class="col-sm-4">
                                                <div class="g-recaptcha" data-sitekey="<?php // echo $siteKey; ?>"></div>
                                            </div> -->
                                            <div class="col-sm-4">
                                                <button style="margin-top:30px;padding: 10px 30px;" type="submit" class="btn btn-primary" name="Submit" value="Send">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                    
                                    <div class="table-responsive mb-3" style="margin-left: 1em;">
                                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>SlNo:</th>
                                                    <th>Banner Title</th>
                                                    <th>Banner Category</th>
                                                    <th>Year</th>
                                                    <th>Award Image</th>
                                                    <th>#Edit</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $count_no = 1;
                                                foreach ($banner as $row) { ?>
                                                    <tr>
                                                        <td><?php echo $count_no; ?></td>
                                                        <td><?php echo $row['banner_title_mal'] ?> [<?php echo $row['banner_title_en'] ?>]</td>
                                                        <td><?php if (isset($row['nomination_category'])) {
                                                                echo $row['nomination_category'];
                                                            } ?></td>
                                                        <td><?php echo $row['year'] ?></td>
                                                        <td><img src="<?php echo act_url(); ?>public/images/banner/<?php echo $row['category_image']; ?>" style=" width:80px; height:60px;" /></td>
                                                        <td>
                                                            <a href="<?php echo base_url(); ?>higherEnd/banedit/<?php echo encode_url($row['id']) ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                                        </td>
                                                        <td>
                                                            <?php if ($row['status'] == 1) {
                                                            ?>
                                                                <a href="<?php echo base_url(); ?>higherEnd/bannerstatus/<?php echo encode_url($row['id']) ?>/0/" class="btn btn-success btn-xs"> Active </a>
                                                            <?php } else {
                                                            ?>
                                                                <a href="<?php echo base_url(); ?>higherEnd/bannerstatus/<?php echo encode_url($row['id']) ?>/1/" class="btn btn-danger btn-xs"> Inactive </a>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                <?php $count_no++;
                                                } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <!-- /page content -->
    </div>
    </div>
    <!-- footer content -->
    <?php echo view('higherEnd/includes/footerAdmin'); ?>
    <!-- /footer content -->
</body>

</html>