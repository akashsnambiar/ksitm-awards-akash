<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>
<script src='https://www.google.com/recaptcha/api.js'></script>

<body>
    <div class="container">
        <div class="right_col" role="main">
            <div class="mt-3">
                <!-- <div class="page-title mt-3">
                    <div class="title_left">
                        <h4>Control Panel for <?php echo $site_name; ?></h4><br />
                    </div>

                </div> -->
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="card x_content">
                                <!-- Error -->
                                <?php if (isset($validation)) : ?>
                                    <div class="alert alert-warning">
                                        <?= $validation->listErrors() ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (isset($error)) : ?>
                                    <div class="alert alert-warning">
                                        <?= $error ?>
                                    </div>
                                <?php endif; ?>
                                <h5 class="card-header">Site Management &nbsp; › <small style="color:blue;">Add Media</small></h5>

                                <form class="form-horizontal p-5" name="formList" id="formList" method="POST" action="<?php echo base_url(); ?>higherEnd/media/add" enctype="multipart/form-data">
                                <?= csrf_field() ?>
                                    <div class="row form-group">
                                        <label class="col-sm-3 control-label">Media Title [Malayalam]</label>
                                        <div class="col-sm-7">
                                            <input class="form-control" id="title" type="text" name="title" value="" placeholder="Title [Malayalam]" />
                                            <p style="color:#CC3333; font-size:10px; margin-top:10px;">We request you not to keep your Media Title field empty</p>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-sm-3 control-label">Media Title [English]</label>
                                        <div class="col-sm-7">
                                            <input class="form-control" id="title_e" type="text" name="title_e" value="" placeholder="Title [English]" />
                                            <p style="color:#CC3333; font-size:10px; margin-top:10px;">We request you not to keep your Media Title field empty</p>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-sm-3 control-label">Embed Video</label>
                                        <div class="col-sm-7">
                                            <textarea name="embed" rows="2" cols="200" style="height:100px;" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3"></div>
                                        <!-- <div class="col-sm-4">
                                            <div class="g-recaptcha" data-sitekey="<?php // echo $siteKey; ?>"></div>
                                        </div> -->
                                        <div class="col-sm-4">
                                            <button style="margin-top:30px;padding: 10px 30px;" type="submit" class="btn btn-primary" name="Submit" value="Send">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>



                        </div>
                    </div>
                </div>

            </div>
        </div>



    </div>
    <!-- /page content -->
    </div>
    <!-- footer content -->
    <?php echo view('higherEnd/includes/footerAdmin'); ?>
    <!-- /footer content -->
    
    



</body>

</html>