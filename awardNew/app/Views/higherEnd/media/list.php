<?php
echo view('higherEnd/includes/header');
echo view('higherEnd/includes/headerTop');
echo view('higherEnd/includes/leftMenu');
?>

<body>
  <div class="container">
    <div class="col-md-12 left_col">
      <!-- page content -->

      <div class="right_col" role="main">
        <div class="mt-3">
          <!-- <div class="page-title mt-3">
            <div class="title_left">
              <h4>Control Panel for <?php echo $site_name; ?></h4><br />
            </div>

          </div> -->
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
               
                <div class="x_content">
                  <form name="frmnews_list" method="POST" action="">
                  <?= csrf_field() ?>
                    <p class="text-muted font-13 m-b-30 text-end">&nbsp;
                      <a href="<?php echo base_url(); ?>higherEnd/media/add" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add Media</a>
                    </p>
                    <div class="card">
                    <h5 class="card-header">Site Management &nbsp; › <small>Media List</small></h5>
                      <div class="table-responsive  p-3 mt-3">
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>SlNo:</th>
                              <th>Title</th>
                              <th>Status</th>
                              <th>#Edit</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                            $count_no = 1;
                            foreach ($media as $row) {
                            ?>
                              <tr>
                                <td><?php echo $count_no; ?></td>
                                <td><?php echo $row['title'] ?> [<?php echo $row['title_e'] ?>]</td>
                                <td>
                                  <?php if ($row['status'] == 1) {
                                  ?>
                                    <a href="<?php echo base_url(); ?>higherEnd/mediastatus/<?= $row['id'] ?>/0/" class="btn btn-success btn-xs"> Success </a>
                                  <?php } else {
                                  ?>
                                    <a href="<?php echo base_url(); ?>higherEnd/mediastatus/<?= $row['id'] ?>/1/" class="btn btn-danger btn-xs"> Failure </a>
                                  <?php } ?>
                                </td>
                                <td>
                                  <a href="<?php echo base_url(); ?>higherEnd/mediaedit/<?php echo encode_url($row['id']) ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit</a>
                                  <a href="<?php echo base_url(); ?>higherEnd/mediadelete/<?php echo $row['id'] ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                </td>
                              </tr>
                            <?php $count_no++;
                            } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /page content -->
  </div>
  </div>

  <!-- footer content -->
  <?php echo view('higherEnd/includes/footerAdmin'); ?>
  <!-- /footer content -->


  <!-- Datatables -->
  <script src="<?php echo act_url(); ?>public/js/datatables/datatable.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/datatable.min.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/datatableBootstrap.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/dataTables.buttons.min.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/jszip.min.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/pdfmake.min.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/vfs_fonts.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/buttons.html5.min.js"></script>
    <script src="<?php echo act_url(); ?>public/js/datatables/buttons.print.min.js"></script>
    <!-- Datatables -->

  <script>
    $(document).ready(function() {
      $('#datatable').DataTable({
        dom: 'Bfrtip',
        buttons: [
          'copy', 'csv', 'excel', 'pdf', 'print'
        ]
      });
    });
  </script>

</body>

</html>