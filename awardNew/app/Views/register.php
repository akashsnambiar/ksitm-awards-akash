<?php echo view('includes/nomination/header');?>
<style>
.col-md-4, .col-md-6, .col-md-8 {
    
    height: 120px !important;
}
</style>

<script src='https://www.google.com/recaptcha/api.js'></script>
  <main class="d-flex align-items-center min-vh-100 py-3 py-md-0">
    <div class="container">
      <div class="card login-card">
        <div class="row no-gutters">
          
          <div class="col-md-12">
            <div class="card-body">
              <div class="brand-wrapper log-class-menu">
               <img onclick="redirect('<?php echo base_url();?>')" src="<?php echo act_url();?>public/images/login/logo.png" alt="logo" class="logo">
                <a class="login-menu" href="<?php echo base_url();?>login">Login</a>
                 </div> <hr/>
                <h2 class="login-card-title title-head">Registration</h2>
                <?php if(session()->getFlashdata('msg')):?>
                            <div class="alert alert-warning">
                              <?= session()->getFlashdata('msg') ?>
                            </div>
                        <?php endif;?>
                <?php if(isset($validation)):?>
                <div class="alert alert-warning err">
                   <?= $validation->listErrors() ?>
                </div>
                <?php endif;?>
              <div class="row">
			    <div class="col-md-12 rad-space">
                 <input id='watch-me' name='test' type='radio' <?php echo $checkedInd;?> /> <span class="rad-span">Individual</span>
                 <input id='see-me' name='test' type='radio' <?php echo $checkedOrg;?>/> <span class="rad-span">Organisation</span>
				 
				 </div>
			</div>
			   <?php echo view('includes/nomination/individual');?>
			   <?php echo view('includes/nomination/organisation');?>
         <?php echo view('includes/nomination/footer');?>
            </div>
          </div>
        </div>
      </div>
      
    </div>
	<?php echo view('includes/nomination/otpReg');?>
  </main>
  <?php echo view('includes/nomination/script');?>
</body>
</html>

