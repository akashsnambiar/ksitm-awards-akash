<?php echo view('includes/header'); ?>
<?php echo view('includes/menu'); ?>
  
<div class="clearfix"> </div>
<section class="about-us">
    <div class="container mb-5">
        <div class="ntn_cls">
        </div>
        <form id="frmnotification" name="frmnotification" method="POST" >
          <?= csrf_field() ?>
        <div class="row slection-nav">
            <div class="col-md-3 col-sm-3 col-xs-12 text-center">
            <p> 
            <select class="btn btn-default dropdown-toggle notif-dropdown" id="year_lbl">
                <option value="">Select Year</option>
                <?php
                if (isset($notyear)){ 
                foreach ($notyear as $rows) { ?>
                    <option value="<?php echo $rows['year']; ?>" <?php if ($year_sel==$rows['year'])  { echo 'selected'; }?> ><?php echo $rows['year'] ?></option>
                <?php } }?>
            </select>
            </p>
            <p> വാർഷിക വിജ്ഞാപനങ്ങൾ </p>
            <div id="notDiv">
            <?php if (isset($prev_not)) { ?>
                <?php  foreach ($prev_not as $row) { ?>
                    <p class="dashNotification">
                        <a class="notificationLink" id="ctl00_ContentPlaceHolder2_LinkButton1" href="<?php echo base_url(); ?>notification/sub/<?php echo encode_url($row['id']); ?>/<?php echo encode_url($row['year']); ?>"><?php echo $row['not_title']; ?></a>
                    </p>
            <?php }
            } else { ?>
                <p class="dashNotification" id="errormsg">No Notifications</p>
           <?php }?>
           </div>
            <p> പൊതുവായ വിജ്ഞാപനങ്ങൾ </p>
            
            <?php if (isset($notification)) { ?>
                <?php  foreach ($notification as $row) { ?>
                    <p class="dashNotification">
                        <a class="notificationLink" id="ctl00_ContentPlaceHolder2_LinkButton1" href="<?php echo base_url(); ?>notification/sub/<?php echo encode_url($row['id']); ?>/<?php echo encode_url($row['year']); ?>"><?php echo $row['not_title']; ?></a>
                    </p>
            <?php }
            } ?>
            </div>
           <!-- general notifications  -->
            <?php if (isset($not_dis)) { ?>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <div class="pdf-box">
                        <iframe src="<?php echo act_url(); ?>public/notifications/<?php echo $not_dis['not_pdf']; ?>" id="ctl00_ContentPlaceHolder2_frame" class="pdfbox"></iframe>
                    </div>
                </div>
            <?php } ?>
        </div>
            </form>
    </div>
</section>
<script>




    $(".notif-dropdown").change(function() {
       var year = $('#year_lbl').val();
        var csrf_test_name = $('input[name="csrf_test_name"]').val(); 
        url = "<?php echo base_url(); ?>notification/prevsub/" + year;
        $.ajax({
                type: "POST",
                url: url,
                data: { year: year },
               dataType: 'JSON',
                headers: {
                    'X-CSRF-TOKEN': csrf_test_name
                },
                success: function(response) {
                   $('input[name="csrf_test_name"]').val(response.token);
                   //var data = JSON.parse(response.res);
                   if (response.res && response.res.length > 0) {
                    $("#notDiv").html("");
                    $.each(response.res, function(index, element) {
                        var id = element.id;
                        var stryear = element.year;
                        var not_type = element.not_type;
                        var not_title = element.not_title;
                       
                        var encryptedIdPromise = encodeUrl(id);
                        var encryptedYearPromise = encodeUrl(stryear);

                        Promise.all([encryptedIdPromise, encryptedYearPromise]).then(function(values) {
                            var encryptedId = values[0];
                            var encryptedYear = values[1];
                            var subid =encryptedId+'/'+encryptedYear;
                            var txt_lbl='<p class="dashNotification"><a class="notificationLink" id="ctl00_ContentPlaceHolder2_LinkButton1" href="<?php echo base_url(); ?>notification/sub/'+subid+'/">'+not_title+'</a></p>';
                            $("#notDiv").append(txt_lbl);
                        });
                    });
                   }
                   else {
                            $("#errormsg").show();
                            var error = "No notifications";
                            $("#errormsg").html(error);
                        }
                    },
                    
                    error: function(xhr, textStatus, errorThrown) {
                            // Handle CSRF token mismatch error
                            if (xhr.status == 403) {
                                window.location.href = "<?php echo base_url('Error/show403'); ?>"; // Redirect to error page
                            } else {
                                // Handle other errors
                            }
                        }
               
            });


    });
  </script>
<!-- /footer -->
<?php echo view('includes/footer'); ?>