<?php echo view('includes/nomination/header');?>
<link rel="stylesheet" href="<?php echo act_url();?>public/css/css/nomination.css" />
  <main class="d-flex align-items-center min-vh-100 py-3 py-md-0">
    <div class="container">
      <div class="card login-card">
        <div class="row no-gutters">
          <div class="col-md-12">
            <div class="card-body">
            <div class="brand-wrapper log-class-menu">
              <img onclick="redirect('<?php echo base_url();?>')" src="<?php echo act_url();?>public/images/login/logo.png" alt="logo" class="logo">
                <a style="margin-left:10px;" class="login-menu" href="<?php echo base_url();?>login/logout">Logout</a>
                <a class="login-menu" href="<?php echo base_url();?>nomination/myAccount"> Welcome, 
                  <?php if (isset($_SESSION['displayName']))
                  {
                    echo $_SESSION['displayName'];
                  }?></a>   </div> <hr/><br/>
                <p class="norecord">താങ്കളുടെ  നാമനിർദ്ദേശം   പൂർത്തിയായിട്ടില്ല . കുറച്ചു സമയത്തിന് ശേഷം വീണ്ടും നാമനിർദ്ദേശം സമർപ്പിക്കുക .</p><br/><br/>
			         <?php echo view('includes/nomination/footer');?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
</body>
 <?php echo view('includes/nomination/script1');?>
</html>
