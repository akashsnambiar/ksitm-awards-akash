<?php 

use CodeIgniter\HTTP\CLIRequest;
//use CodeIgniter\HTTP\Exceptions\HTTPException;
//use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\URI;
//use CodeIgniter\Router\Exceptions\RouterException;
use Config\App;
use Config\Services;

if (! function_exists('act_url')) {
    /**
     * act URL
     *
     * Returns the "actURL" from your config file
     *
     * @param App|null $altConfig Alternate configuration to use
     */
    function act_url(?App $altConfig = null): string
    {
        $config = $altConfig ?? config('App');
        return $config->actURL;
    }
}
// function encode_url($string, $key = "", $url_safe = TRUE)
// {
//     if ($key === null || $key === "") {
//         $key = "samplekey";
//     }
//     $CI = \Config\Services::encrypter(null, $key);
//     $ret = $CI->encrypt($string);
//     if ($url_safe) {
//         $ret = strtr($ret, array('+' => '.',
//                                  '=' => '_',
//                                  '/' => '~'));
//     }
//     return $ret;
// }
function encode_url($string, $key = "")
{
    if ($key === null || $key === "") {
        //$key = "samplekey";
        $key = "samplekey";
    }

    // Generate an IV (Initialization Vector)
    $ivLength = openssl_cipher_iv_length('aes-256-cbc');
    $iv = openssl_random_pseudo_bytes($ivLength);

    // Encrypt the string using AES-256 in CBC mode
    $encrypted = openssl_encrypt($string, 'aes-256-cbc', $key, OPENSSL_RAW_DATA, $iv);

    // Combine the IV and encrypted data
    $combined = $iv . $encrypted;

    // Convert the combined data to hexadecimal representation
    $encoded = bin2hex($combined);

    return $encoded;
}

//--------------decode-----------------------

// function decode_url($string, $key = "")
// {
//     if ($key === null || $key === "") {
//         $key = "samplekey";
//     }
//     // Ensure $string has even length
//     if (strlen($string) % 2 != 0) {
//         $string = '0' . $string;
//         print_r($string);exit;
//     }
//     // Convert the hexadecimal string back to binary
//     $combined = hex2bin($string);
    
//     // Separate the IV and encrypted data
//     $ivLength = openssl_cipher_iv_length('aes-256-cbc');
//     $iv = substr($combined, 0, $ivLength);
//     $encrypted = substr($combined, $ivLength);

//     if (strlen($iv) !== $ivLength || empty($encrypted)) {
//         // IV or encrypted data is missing or invalid
//         error_log("IV or encrypted data is missing or invalid");
//         return false;
//     }

//     // Decrypt the data using AES-256 in CBC mode
//     $decrypted = openssl_decrypt($encrypted, 'aes-256-cbc', $key, OPENSSL_RAW_DATA, $iv);
// //echo "aaaaaaaaaaa";echo $decrypted;exit;
//     if ($decrypted === false) {
//         // Decryption failed
//         error_log("Decryption failed: " . openssl_error_string());
//         return false;
//     }
    
//     return $decrypted;
// }
function decode_url($encoded_string, $key = "")
{
    if ($key === null || $key === "") {
        $key = "samplekey";
    }
    
    // Filter out non-hexadecimal characters
    $string = preg_replace('/[^a-fA-F0-9]/', '', $encoded_string);
    
    // Ensure $string has even length
    if (strlen($string) % 2 != 0) {
        $string = '0' . $string;
    }
    
    // Check if the filtered string is different from the original string
    if ($string !== $encoded_string) {
        // Redirect to an error page or handle the error as appropriate
        // Example: Redirect to an error page
        header("Location: index.html");
        exit();
    }
    
    // Convert the hexadecimal string back to binary
    $combined = hex2bin($string);
    
    // Separate the IV and encrypted data
    $ivLength = openssl_cipher_iv_length('aes-256-cbc');
    $iv = substr($combined, 0, $ivLength);
    $encrypted = substr($combined, $ivLength);

    if (strlen($iv) !== $ivLength || empty($encrypted)) {
        // IV or encrypted data is missing or invalid
        error_log("IV or encrypted data is missing or invalid");
        return false;
    }

    // Decrypt the data using AES-256 in CBC mode
    $decrypted = openssl_decrypt($encrypted, 'aes-256-cbc', $key, OPENSSL_RAW_DATA, $iv);

    if ($decrypted === false) {
        // Decryption failed
        error_log("Decryption failed: " . openssl_error_string());
        return false;
    }
    
    return $decrypted;
}

