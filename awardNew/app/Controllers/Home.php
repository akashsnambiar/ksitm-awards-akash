<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Libraries\SiteSettings; // Import library
use App\Models\HomeModel;
use App\Models\GalleryCategoryModel;
use App\Models\WhocanapplyModel;
use App\Models\WhodecidesModel;
use App\Models\ContributionModel;
use App\Models\WinnerModel;
use App\Models\BannerModel;
use App\Models\NominationsModel;
use App\Models\RegisterModel;
use App\Models\AboutModel;
use App\Models\NominationPeriodModel;


class Home extends BaseController
{
   public function index()
   {

      $sitesettngs = new SiteSettings(); // create an instance of Library
      $res = $sitesettngs->get_site_settings(); // calling method
      $data   =   array();
      foreach ($res as $row) {
         $data1[$row->config_key] = $row->config_value;
      }
      $data['Titletag'] = $data1['title_tags'];
      $data['metadescription'] = $data1['meta_description'];
      $data['metakeywords'] = $data1['meta_tags'];
      $data['pagetitle'] = 'Home';
      $data['site_name'] = $data1['site_name'];
      $data['copyright_year'] = $data1['copyright_year'];
      //print_r($data);
      // if (isset($_SESSION['SiteVersion'])) {
      //    $session->remove('SiteVersion');
      // }
      $_SESSION['SiteVersion'] = "";

      $homeModel = new HomeModel(); //home model
      $data['home'] = $homeModel->where('id', 1)->First();
      $galCategoryModel = new GalleryCategoryModel(); // gallery category model
      $data['gal_menu'] = $galCategoryModel->where('status', 1)->findAll();

      $whocanapplyModel = new WhocanapplyModel(); // Who can apply model
      $data['whocanapply'] = $whocanapplyModel->findAll();

      $whodecidesModel = new WhodecidesModel(); //Who decides model
      $data['whodecides'] = $whodecidesModel->findAll();

      $contriModel = new ContributionModel(); // Who contribution model
      $data['contri'] = $contriModel->findAll();
      // print_r($data['home']);exit; 

      $aboutmodel = new AboutModel();
      $data['keralajyothi'] = $aboutmodel->where('id', 3)->findAll();
      $data['keralaprabha'] = $aboutmodel->where('id', 4)->findAll();
      $data['keralashri'] = $aboutmodel->where('id', 2)->findAll();

      $bannermodel = new BannerModel();
      $data['banner'] = $bannermodel
         ->select('banner.*')
         ->where('status', 1)->findAll();
      $data['banner'] = $bannermodel
         ->select('banner.*,n.id as nid, n.nomination_category as nomination_category')
         ->join('nomination_category AS n', 'banner.category_id = n.id')
         ->where('year in (select max(year) from banner WHERE banner.status = 1)')
         ->findAll();
      //echo $bannermodel->db->getLastQuery();
      //exit;

      $winnermodel = new WinnerModel();
      $data['winner'] = $winnermodel
         ->select('winner.id as id,winner.banner_title_id as banner_title_id,winner.winner_name as winner_name,winner.winner_photo as winner_photo,winner.status as status,b.id as bid,b.banner_title_mal as banner_title_mal,b.banner_title_en as banner_title_en,b.banner_title_mal as banner_title_mal,b.year as year,b.category_id as category_id,b.status as bstatus,b.category_image as category_image,n.id as nid, n.nomination_category as nomination_category')
         ->join('banner AS b', 'winner.banner_title_id = b.id')
         ->join('nomination_category AS n', 'b.category_id = n.id')
         ->where('year in (select max(year) from banner WHERE banner.status = 1)')
         ->where('winner.status', 1)
         ->findAll();
      //echo $winnermodel->db->getLastQuery();exit;
      // print_r($data['winner']);exit;

      $nominationModel = new NominationsModel();
      $data['nominations'] = $nominationModel
         ->countAllResults();
      $registerModel = new RegisterModel();
      $data['registered'] = $registerModel->where('status', 1)
         ->countAllResults();
      $data['nominationAccepted'] = $nominationModel->where('status', 1)
         ->countAllResults();


      $data['nomperiod'] =  $this->getNominationPeriod();

      echo view('home', $data);
   }
   public function getNominationPeriod()
   {
      $checkDate   = date("Y-m-d");
      $checkYear   = date("Y");
      //$d = date('Y-m-d', strtotime($dt));
      $nominationperiodModel = new NominationPeriodModel();
      $nomperiod =  $nominationperiodModel->where('status', 1)
         ->where('start_date <=', $checkDate)
         ->where('end_date >=', $checkDate)
         ->where('year =', $checkYear)->findAll();
      //echo $nominationperiodModel->db->getLastQuery();
      return $nomperiod;
   }
}
