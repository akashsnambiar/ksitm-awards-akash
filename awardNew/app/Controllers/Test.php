<?php

namespace App\Controllers;

use App\Controllers\BaseController;


use App\Models\LoginModel;
use App\Models\NominationsModel;
use App\Models\AwardModel;
use App\Models\RegisterModel;
use App\Models\ReviewModel;
use App\Models\EsignModel;

class Test extends BaseController
{
    public function index()
    {
        echo view('test');
    }
    public function save()
    {
        $session = session();
        $lgnModel = new LoginModel();
        $username = sanitize_filename($this->request->getVar('username'));
        $password = $this->request->getVar('password');
        $data = $lgnModel->where('username', $username)->first();
        if($data){
            $pass = $data['password'];

          
            $authenticatePassword = password_verify($password, $pass);
            if($authenticatePassword){
                if($data['regType']==1){
                    $username=$data['fName'];
                }else{
                    $username=$data['organisationName'];
                }
                $ses_data = [
                    'UserLog' => $data['id'],
                    'UserName' => $username,
                    'regType' => $data['regType'],
                    'displayName' => $username,
                    'isUserLoggedIn' => TRUE
                ];
                $session->set($ses_data);
            }
            return redirect()->to('test1');
        }
        
    }
   
}
