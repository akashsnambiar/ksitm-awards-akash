<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\NominationsModel;
use App\Models\AwardModel;
use App\Models\RegisterModel;
use App\Models\ReviewModel;
use App\Models\EsignModel;

class Test1 extends BaseController
{
    public function index()
    {
        $session = session();
        if ((isset($_SESSION['isUserLoggedIn']) == 1) && (isset($_SESSION['UserLog']) != '')) {
            echo view('test1');
        }
        
    }
   
}
