<?php



namespace App\Controllers;

use App\Controllers\BaseController;


class Error extends BaseController
{
    public function show403()
    {
        echo view('errors/error_page.html'); // Load your HTML error page
    }
}