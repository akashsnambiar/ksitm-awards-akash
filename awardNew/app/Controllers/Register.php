<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Libraries\SiteSettings; // Import library
use App\Models\RegisterModel;
use App\Models\RegisterDemoModel;
use App\Models\AwardModel;
use App\Models\NominatorModel;
use App\Models\IdentityModel;
use App\Models\OrganisationModel;
use App\Models\OrganisationsubModel;
use App\Models\LocalbodytypeModel;
use App\Models\LocalbodydistrictModel;
use App\Models\PanchayathModel;
use App\Models\BlockPanchayathModel;
use App\Models\MuncipalityModel;
use App\Models\StateModel;
use App\Models\DistrictModel;

use App\Models\AdminModel;
use CodeIgniter\Files\File;

class Register extends BaseController
{
    
public function index()
{
    //
    $session = session();
    $data = $this->getCommon();
    $data['pagetitle'] = 'Registration';

    //form data
    $data['checkedInd']='checked';
    $data['checkedOrg']='';
    $data['displayInd']='';
    $data['displayOrg']="style='display:none;'";

    $nominatormodel = new NominatorModel();//create nominator model
    $data['nominator'] = $nominatormodel->where('status', 1)
                        ->orderBy('NomID',"DESC")->findall(); 

    $identitymodel = new IdentityModel();//create identity model
    $data['identity'] = $identitymodel->where('status', 1)
                        ->orderBy('identityID',"DESC")->findall();  

    $orgmodel = new OrganisationModel();//create organisation model
    $data['organisation'] = $orgmodel->where('status', 1)
                        ->orderBy('orgID',"ASC")->findall();  
                        
    $orgsubmodel = new OrganisationsubModel();//create organisation sub model
    $data['organisationsub'] = $orgsubmodel->where('status', 1)
                        ->orderBy('orgSub',"ASC")->findall();  

    $localbodymodel = new LocalbodytypeModel();//create local bodytype model
    $data['localbody'] = $localbodymodel
                        ->orderBy('localID',"ASC")->findall();  
        
    $localbodyistmodel = new LocalbodydistrictModel();//create local body district model
    $data['localbodyDis'] = $localbodyistmodel
                        ->orderBy('id',"ASC")->findall();  
    
    $localbodyPanmodel = new PanchayathModel();//create local body panchayath model
    $data['localbodyPan'] = $localbodyPanmodel
                        ->orderBy('id',"ASC")->findall(); 
    
    $localbodyBlkPanmodel = new BlockPanchayathModel();//create local body panchayath Block model
    $data['localbodyBlkPan'] = $localbodyBlkPanmodel
                        ->orderBy('id',"ASC")->findall();
    
    $muncipalitymodel = new MuncipalityModel();//create muncipality model
    $data['localbodyMuncipality'] = $muncipalitymodel
                        ->orderBy('id',"ASC")->findall();
    
    $statemodel = new StateModel();//create state model
    $data['state'] = $statemodel
                        ->orderBy('state',"ASC")->findall();
    
    $districtmodel = new DistrictModel();//create district model
    $data['district'] = $districtmodel->where('status', 1)
                        ->orderBy('district',"ASC")->findall();
                                            
        // echo $localbodymodel->db->getLastQuery();
    echo view('register', $data);
}

public function save()
{
    helper(['form']);
    $session = session();
    $encrypter = \Config\Services::encrypter();
        
    $data = $this->getCommon();
    $data['pagetitle'] = 'Registration';
        
    $token = csrf_hash();
    $data['token'] = $token;

    $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
   
   /*  if($captcha_response != '')
    { 
        $keySecret = $data['secretKey'];
        $check = array(
            'secret'		=>	$keySecret,
            'response'		=>	$this->request->getVar('g-recaptcha-response')
        );
        $startProcess = curl_init();
        curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($startProcess, CURLOPT_POST, true);
        curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
        curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
        $receiveData = curl_exec($startProcess);
        $finalResponse = json_decode($receiveData, true);
        
        /* if($finalResponse['success'])//start final response
        { */
            $rules = [
                'nominater_type'      => 'required',
                'first_name'      => 'required|alpha_space',
                'mobile_number'      => 'required',
                'email'      => 'required|valid_email',
                'inputDoc'      => 'required',
                'inputDocNum'      => 'required',];
            
            $messages = [
                "nominater_type" => ["required" => "Select Nominator Type",],
                "first_name" => ["required" => "First Name  is required",],
                "mobile_number" => ["required" => "Mobile No  is required",],
                "email" => ["valid_email" => "Enter valid Email ID ",],
                "inputDoc" => ["required" => "Select Identity Document",],
                "inputDocNum" => ["required" => "Identity Document Number is required",],
            ]; 

            // print_r($rules); print_r($messages);exit;
            if($this->validate($rules, $messages))
            {
                
                $input = $this->validate([
                    'file' => [
                        'uploaded[file]',
                        'mime_in[file,application/pdf]',
                        'max_size[file,20480]', //->10MB
                        'ext_in[file,pdf]',
                    ]
                ]);
                if (!$input) {
                    $data['success'] = 0;
                    $data['res']='Upload Document in Pdf Format.'; 
                    return $this->response->setJSON($data);
                    exit;  

                }
                else{

                $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                $password = substr( str_shuffle( $chars ), 0, 8 );
                $pass = password_hash($password, PASSWORD_DEFAULT);
                $rad = "0123456789";
                $tgl = substr( str_shuffle( $rad ), 0, 3 );
                $kode = sanitize_filename($this->request->getVar('email'));
                $ss = explode('@',$kode);
                $username = "KPL". str_pad($ss[0], 3, 0, STR_PAD_LEFT) . $tgl;
                $regdemomodel = new RegisterDemoModel();//create Register model

                $docFile= "";
                $lastID =0;
                
                if (isset($_SESSION['regDoc']))
                {
                    $regDoc = $_SESSION['regDoc'];
                }
                else{
                        $regDoc =  ""; 
                }
                $regdemomodel->save([
                    'regType' => sanitize_filename($this->request->getVar('regType')),
                    'nominaterType'  => sanitize_filename($this->request->getVar('nominater_type')),
                    'fName'  => sanitize_filename($this->request->getVar('first_name')),
                    'mName'  => sanitize_filename($this->request->getVar('middle_name')),
                    'lName'  => sanitize_filename($this->request->getVar('last_name')),
                    'mobileCode'  => sanitize_filename($this->request->getVar('mobile_code')),
                    'mobileNumber'  => sanitize_filename($this->request->getVar('mobile_number')),
                    'email'  => sanitize_filename($this->request->getVar('email')),
                    'inputDoc'  => sanitize_filename($this->request->getVar('inputDoc')),
                    'inputDocNum'  => sanitize_filename($this->request->getVar('inputDocNum')),
                    'docFile'  => $regDoc,
                    'username'  => $username,
                    'password' => $pass,
                    'refCode' => $password,
                    'ip' => $this->request->getIPAddress(),
                    'date' => date("Y-m-d"),
                ]);
                //echo $regdemomodel->db->getLastQuery();exit;
                $lastID = $regdemomodel->InsertID();
                
                /*----------------------------------------------------------------------*/
                    $mobile = sanitize_filename($this->request->getVar('mobile_number'));
                    $pin = mt_rand(100000, 999999);
                    $details['mobile'] =$mobile;
                    $details['otp'] =$pin;
                
                    date_default_timezone_set('Asia/Kolkata');
                    $otpdate = date('d-m-y h:i:s');
                    $details['otp_date'] =$otpdate;
                    
                    $message1= "KLMGOV- ".$pin." is the One Time Password(OTP). Please enter OTP to proceed";
            
                    $str='username=eofficeksitm&password=eofcalerts&sender=KLMGov&numbers='.$mobile.'&message='.$message1;
                    $ch = curl_init();
                    $url= 'http://api.esms.kerala.gov.in/fastclient/SMSclient.php?'.$str;
                    
                    curl_setopt($ch, CURLOPT_URL, $url) or die("error1");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); //return as a variable
                    $result = curl_exec($ch);
                    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    curl_close($ch);
                    //send mail
                    $toemail=sanitize_filename($this->request->getVar('email'));
                    $result = $this->setMailsettings();
                    $result['to'] = $toemail;

                    $subject ="OTP Details";
                    $message = "KLMGOV- ".$pin." is the One Time Password(OTP). Please enter OTP to proceed"."<br>";
                    $content ="
                        <html>
                            <head>
                            <title></title>
                            </head>
                            <body style='background-color:#EEFFD5'>
                            <table cellpadding='0' cellspacing='0' border='0' width='673'>
                                    <tr>
                                    <td width='673' bgcolor='#EEFFD5'>
                                        <p></p><br>
                                        <p>Dear User,</p><br>
                                        <p style='font-size:15px; margin:0px 10px 0px 15px; text-align:justify;'>$message</p><br>
                                        </td>
                                    </tr>
                                    <tr>
                                    <td width='673' bgcolor='#EEFFD5'>
                                        <p></p>
                                        <p>Regards,</p>
                                        <p>". $result['fromname']."</p><br>
                                        </td>
                                    </tr>
                                
                            </table>
                            </body>
                            </html>";

                    
                   $this->sendMail($subject,$message,$result);
                    //end send mail
                    if (isset($_SESSION['UserLogDemo']))
                    {            
                        $session->remove('UserLogDemo');
                        $session->remove('otpVal');
                        $session->remove('otpMob');
                        $session->remove('otpEmail');
                    }  
                    
                    $ses_data = [
                    'UserLogDemo' =>   $lastID,
                    'otpVal' => $pin,
                    'otpMob' => $mobile,
                    'otpEmail' => $toemail
            ];  
            $session->set($ses_data);
            // print_r($ses_data);exit;
            $data['success'] = 1;
            $data['res']= '<script> 
                    var otpVal ="'.$pin.'";
                    var otpMob ="'.$mobile.'";    
                </script>'; 
                return $this->response->setJSON($data);
                exit;   
                    
            }
            }
            else
            {
                $data1['validation']=$this->validator;
                $data['success']=0;
                $data['res']= $data1['validation']->listErrors();
                return $this->response->setJSON($data);
                exit;    
            }

        /* }
        else{
            $data['success'] = 0;
                $data['res']='Validation Fails.Try Again'; 
                return $this->response->setJSON($data);
                exit;    
        }   //end final response 
    } 
    else
    {

        $data['success'] = 0;
        $data['res']='Invalid Captcha'; 
        return $this->response->setJSON($data);
        exit;    
    } */   //end captch aresponse
    
}//end save function

    
// resend otp for individual
function resendOtp(){
    $session = session();
    $token = csrf_hash();
    $data=array();
    $data['token'] = $token;
    $mobile = "";
    $toemail = "";

    if (isset($_SESSION['UserLogDemo']))
    {
        $mobile = $_SESSION['otpMob'];
        $toemail = $_SESSION['otpEmail'];
    }
     $pin = mt_rand(100000, 999999);
     $details['mobile'] =$mobile;
     $details['otp'] =$pin;
     //$this->session->set_userdata('OTP', $pin);
     date_default_timezone_set('Asia/Kolkata');
     $otpdate = date('d-m-y h:i:s');
     $details['otp_date'] =$otpdate;
     $message1= "KLMGOV- ".$pin."is the One Time Password(OTP). Please enter OTP to proceed";

     $str='username=eofficeksitm&password=eofcalerts&sender=KLMGov&numbers='.$mobile.'&message='.$message1;
         $ch = curl_init();
         $url= 'http://api.esms.kerala.gov.in/fastclient/SMSclient.php?'.$str;
         curl_setopt($ch, CURLOPT_URL, $url) or die("error1");
         curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); //return as a variable
         $result = curl_exec($ch);
         $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
         curl_close($ch);
        //send mail
        $result = $this->setMailsettings();
        $result['to'] = $toemail;

        $subject ="OTP Details";
        $message = "KLMGOV- ".$pin." is the One Time Password(OTP). Please enter OTP to proceed"."<br>";
        $content ="
            <html>
                <head>
                <title></title>
                </head>
                <body style='background-color:#EEFFD5'>
                <table cellpadding='0' cellspacing='0' border='0' width='673'>
                        <tr>
                        <td width='673' bgcolor='#EEFFD5'>
                            <p></p><br>
                            <p>Dear User,</p><br>
                            <p style='font-size:15px; margin:0px 10px 0px 15px; text-align:justify;'>$message</p><br>
                            </td>
                        </tr>
                        <tr>
                        <td width='673' bgcolor='#EEFFD5'>
                            <p></p>
                            <p>Regards,</p>
                            <p>". $result['fromname']."</p><br>
                            </td>
                        </tr>
                    
                </table>
                </body>
                </html>";

        
       $this->sendMail($subject,$message,$result);
        //end send mail
        $data['res'] ='<script> 
                var otpVal ="'.$pin.'";
                var otpMob ="'.$mobile.'";    
            </script>'; 
            return $this->response->setJSON($data);   
            exit;        
 }
//saving individual form data
   public function saveOrginal()
   {
        $session = session();
        $id ='';
        if (isset($_SESSION['UserLogDemo']))
        {
            $id = $_SESSION['UserLogDemo'];
        }
        $regdemomodel = new RegisterDemoModel();//create reg demo model
        $data['userOrg'] = $regdemomodel->where('id', $id)->first(); 
        
        $regmodel = new RegisterModel();//create Register model
        
        $regmodel->save([
            'regType' => $data['userOrg']['regType'],
            'nominaterType'  => $data['userOrg']['nominaterType'],
            'fName'  => $data['userOrg']['fName'],
            'mName'  => $data['userOrg']['mName'],
            'lName'  => $data['userOrg']['lName'],
            'mobileCode'  => $data['userOrg']['mobileCode'],
            'mobileNumber'  => $data['userOrg']['mobileNumber'],
            'email'  => $data['userOrg']['email'],
            'inputDoc'  => $data['userOrg']['inputDoc'],
            'inputDocNum'  => $data['userOrg']['inputDocNum'],
            'docFile'  => $data['userOrg']['docFile'],
            'username'  => $data['userOrg']['username'],
            'password' => $data['userOrg']['password'],
            'refCode' => $data['userOrg']['refCode'],
           'ip' => $this->request->getIPAddress(),
            'date' => date("Y-m-d"),
        ]);

        $lastregID = $regmodel->InsertID();
        if ($lastregID!='')
        {
            $awardmodel = new AwardModel();//create award model
            $awardmodel->save([
                'userID' => $lastregID,
                'year' => date("Y"),
            ]);
    
        
            if($data['userOrg']['regType']==1){
                $username=$data['userOrg']['fName'];
            }else{
                $username=$data['userOrg']['organisationName'];
            }

            $ses_data = [
                'UserLog' => $lastregID,
                'regType' => $data['userOrg']['regType'],
                'displayName' => $username
            ];
            $session->set($ses_data);
         }
		 
		/* ---------------------------Mail------------------------------------------*/
        $mobile = $data['userOrg']['mobileNumber'];
        $uname = $data['userOrg']['fName'];
        $loid = $data['userOrg']['username'];
        $lopass =$data['userOrg']['refCode'];
                 $pin = mt_rand(100000, 999999);
                 $details['mobile'] =$mobile;
                 $details['otp'] =$pin;
                 //$this->session->set_userdata('OTP', $pin);
                 date_default_timezone_set('Asia/Kolkata');
                 $otpdate = date('d-m-y h:i:s');
                 $details['otp_date'] =$otpdate;
                 $message1= "KLMGOV-Dear ".$uname.", Thank You for registering with us.Your Login ID : ".$loid." and Password : ".$lopass;
         
                $str='username=eofficeksitm&password=eofcalerts&sender=KLMGov&numbers='.$mobile.'&message='.$message1;
                $ch = curl_init();
                $url= 'http://api.esms.kerala.gov.in/fastclient/SMSclient.php?'.$str;
                curl_setopt($ch, CURLOPT_URL, $url) or die("error1");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); //return as a variable
                $result = curl_exec($ch);
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);

                //send mail
                $toemail=$data['userOrg']['email'];
                $result = $this->setMailsettings();
                $result['to'] = $toemail;

                $subject ="Login Details";
                $message = "Dear ".$uname.", Thank You for registering with us.Your Login ID : ".$loid." and Password : ".$lopass."<br>";
                $content ="
                    <html>
                        <head>
                        <title></title>
                        </head>
                        <body style='background-color:#EEFFD5'>
                        <table cellpadding='0' cellspacing='0' border='0' width='673'>
                                <tr>
                                <td width='673' bgcolor='#EEFFD5'>
                                    <p></p><br>
                                    <p>Dear User,</p><br>
                                    <p style='font-size:15px; margin:0px 10px 0px 15px; text-align:justify;'>$message</p><br>
                                    </td>
                                </tr>
                                <tr>
                                <td width='673' bgcolor='#EEFFD5'>
                                    <p></p>
                                    <p>Regards,</p>
                                    <p>". $result['fromname']."</p><br>
                                    </td>
                                </tr>
                            
                        </table>
                        </body>
                        </html>";

              $this->sendMail($subject,$message,$result);
             //end send mail 
 /*--------------------------Mail-------------------------------------------*/
  //$this->Register_model->delUser();
  return redirect()->to(base_url().'nomination');


 }
//end registration save data
function checkEmail()
{
    $token = csrf_hash();
    $data=array();
    $data['token'] = $token;
    if (isset($_POST['email']))
    {
    $total =0;
    $email = sanitize_filename($_POST['email']);
    $regmodel = new RegisterModel();//create Register model
    $total = $regmodel->where('email', $email) ->countAllResults();   
    //echo $regmodel->db->getLastQuery();
    if($total>0) {
        $data['res'] = "<span class='status-not-available'> Email-ID Already Exist.</span>  <script>$(document).ready(function(){
        $('#email').val('');
        });
        $('#form-group-email').removeClass('green');
        $('#form-group-email').addClass('red');
    </script>";
    return $this->response->setJSON($data);
    }else{
       
        $data['res'] = "<span class='status-available'></span>";
        return $this->response->setJSON($data);
    }
}
    else{
        echo view("index.html");
    }
	

}
function checkMobile()
{
    $token = csrf_hash();
    $data=array();
    $data['token'] = $token;
    if (isset($_POST['mobile']))
    {
    $mobile_number = sanitize_filename($_POST['mobile']);
    $total =0;
    $regmodel = new RegisterModel();//create Register model
    $total = $regmodel->where('mobileNumber', $mobile_number)->countAllResults();  
     if($total>0) {
        $data['res'] =
            "<span class='status-not-available'> Mobile Number Already Exist.</span>  <script>$(document).ready(function(){
            $('#mobile_number').val('');
            });
            $('#form-group-mobile').removeClass('green');
            $('#form-group-mobile').addClass('red');
            </script>";
            return $this->response->setJSON($data);
        }else{
            $data['res'] ="<span class='status-available'></span>";
            return $this->response->setJSON($data);
           
        }
    }
    else{
        echo view ("index.html");
    }

}
function upload_file()
{
    $token = csrf_hash();
    $data=array();
    $data['token'] = $token;
    $session = session();
    $filedoc=$this->request->getFile('file');
    //print_r( $filedoc);exit;
    if (isset($filedoc))
   {
    $upload_path = FCPATH.'public/images/registration/individual';
    $ext = $filedoc->getClientExtension();
    $newName='Ind-Doc-'.time().'.'.$ext;

    $input = $this->validate([
        'file' => [
            'uploaded[file]',
            'mime_in[file,application/pdf]',
            'max_size[file,20480]',
            'ext_in[file,pdf]',
        ]
    ]);
    if (!$input) {
       
        $data['res'] ="<span class='red'>Upload Pdf Files.</span>";
        return $this->response->setJSON($data);
    } else {
        $res = $filedoc->move($upload_path,$newName);
        if ( $res==1)
            {
                if (isset($_SESSION['regDoc']))
                     {            
                         $session->remove('regDoc');
                     }  
                $_SESSION['regDoc']=$newName; 
                $data['res'] ="<span class='green'>Document Uploaded.</span>"; 
                return $this->response->setJSON($data);
            }else{
                $data['res'] = "<span class='red'>Invalid File.Upload pdf Files.</span>";
                return $this->response->setJSON($data);
            }

        }
    }
    else{
        echo view('index.html');
    }
}
//save organisation data
public function saveOrg()
{
    helper(['form']);
    $session = session();
    $encrypter = \Config\Services::encrypter();

    $data = $this->getCommon();
    $data['pagetitle'] = 'Registration';
     $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
     $filedoc=$this->request->getFile('files');
     
    $token = csrf_hash();
    $data['token'] = $token;
    
    /* if($captcha_response != '')
    { 
        $keySecret = $data['secretKey'];
        $check = array(
            'secret'		=>	$keySecret,
            'response'		=>	$this->request->getVar('g-recaptcha-response')
        );

        $startProcess = curl_init();
        curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($startProcess, CURLOPT_POST, true);
        curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
        curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
        $receiveData = curl_exec($startProcess);
        $finalResponse = json_decode($receiveData, true);
        /* if($finalResponse['success'])
        { */
           
            $rules = [
                'organisation_type'      => 'required',
                'organisationName'      => 'required',
                'concernedPerson'      => 'required|alpha_space',
                'designation'      => 'required|alpha_space',
                'emails'      => 'required|valid_email',
                'state'      => 'required',
                'district'      => 'required',
                'inputDoc'      => 'required',
                'inputDocNum'      => 'required',];
            $messages = [
                    "organisation_type" => ["required" => "Type of Organisation Required",],
                    "organisationName" => ["required" => "Name of Organisation Required",],
                    "concernedPerson" => ["required" => "Name of the Concerned Person Required",],
                    "designation" => ["required" => "Designation Required",],
                    "emails" => ["valid_email" => "Enter valid Email ID",],
                    "state" => ["required" => "State Required",],
                    "district" => ["required" => "District Required",],
                    "inputDoc" => ["required" => "Select Identity Document to Upload Required",],
                    "inputDocNum" => ["required" => "Identity Document Number Required",],
                ];   
              
            if($this->validate($rules, $messages))
            {

                $input = $this->validate([
                    'files' => [
                        'uploaded[files]',
                        'mime_in[files,application/pdf]',
                        'max_size[files,20480]', //->10MB
                        'ext_in[files,pdf]',
                        
                    ]
                ]);
                if (!$input) {
                    $data['success'] = 0;
                    $data['res']='Upload Document in Pdf Format.'; 
                    return $this->response->setJSON($data);
                    exit;  

                }
                else
                {
                $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                $password = substr( str_shuffle( $chars ), 0, 8 );
                $pass = password_hash($password, PASSWORD_DEFAULT);
                $rad = "0123456789";
                $tgl = substr( str_shuffle( $rad ), 0, 3 );
                $kode = sanitize_filename($this->request->getVar('emails'));
                $ss = explode('@',$kode);
                $username = "KPL". str_pad($ss[0], 3, 0, STR_PAD_LEFT) . $tgl;
                $regdemomodel = new RegisterDemoModel();//create Register model

                //$docname =$this->request->getFile('file');
                //$docFile= $docname->getName();
                $docFile= "";
                $lastID =0;
                
                if($this->request->getVar('localbody_localbodyBlkPan')==''){
                    $blockpanchayath = 0;
                }else{
                    $blockpanchayath = $this->request->getVar('localbody_localbodyBlkPan');
                }
                if($this->request->getVar('localbody_panchayath')==''){
                    $gramapanchayath = 0;
                }else{
                    $gramapanchayath = $this->request->getVar('localbody_panchayath');
                }
                if($this->request->getVar('localbody_muncipality')==''){
                    $muncipality = 0;
                }else{
                    $muncipality = $this->request->getVar('localbody_muncipality');
                }
                    if($this->request->getVar('localbodydis')==''){
                    $localbodyDistrict = 0;
                }else{
                    $localbodyDistrict = $this->request->getVar('localbodydis');
                }
                if($this->request->getVar('localbody_type')==''){
                    $localbody = 0;
                }else{
                    $localbody = $this->request->getVar('localbody_type');
                }
                if($this->request->getVar('organisation_dept')==''){
                    $organisation_dept = 0;
                }else{
                    $organisation_dept = $this->request->getVar('organisation_dept');
                }
                
                if (isset($_SESSION['regDoc']))
                {
                $regDoc = $_SESSION['regDoc'];
                }
                else{
                $regDoc =  ""; 
                }

                $regdemomodel->save([
                    'regType' => sanitize_filename($this->request->getVar('regType')),
                    'organisationType'  => sanitize_filename($this->request->getVar('organisation_type')),
                    'localbody'  => $localbody,
                    'localbodyDistrict'  => $localbodyDistrict,
                    'blockpanchayath'  => $blockpanchayath,
                    'gramapanchayath'  => $gramapanchayath,
                    'muncipality'  => $muncipality,
                    'organisationDept'  => $organisation_dept,
                    'organisationName'  => sanitize_filename($this->request->getVar('organisationName')),
                    'concernedPerson'  => sanitize_filename($this->request->getVar('concernedPerson')),
                    'mobileCode'  => sanitize_filename($this->request->getVar('mobile_code')),
                    'mobileNumber'  => sanitize_filename($this->request->getVar('mobile_numbers')),
                    'email'  => sanitize_filename($this->request->getVar('emails')),
                    'designation'  => sanitize_filename($this->request->getVar('designation')),
                    'otherContactDetail'  => sanitize_filename($this->request->getVar('otherContactDetail')),
				    'state' => sanitize_filename($this->request->getVar('state')),
				    'district' => sanitize_filename($this->request->getVar('district')),
				    'inputDoc' => sanitize_filename($this->request->getVar('inputDoc')),
				    'inputDocNum' => sanitize_filename($this->request->getVar('inputDocNum')),
                    'docFile'  => $regDoc,
                    'username'  => $username,
                    'password' => $pass,
                    'refCode' => $password,
                   'ip' => $this->request->getIPAddress(),
                    'date' => date("Y-m-d"),
                ]);
                $lastID = $regdemomodel->InsertID();
                  /*----------------------------------------------------------------------*/
                  $mobile = sanitize_filename($this->request->getVar('mobile_numbers'));
                  $pin = mt_rand(100000, 999999);
                  $details['mobile'] =$mobile;
                  $details['otp'] =$pin;
                 
                  date_default_timezone_set('Asia/Kolkata');
                  $otpdate = date('d-m-y h:i:s');
                  $details['otp_date'] =$otpdate;
                  
                  $message1= "KLMGOV- ".$pin." is the One Time Password(OTP). Please enter OTP to proceed";
          
                  $str='username=eofficeksitm&password=eofcalerts&sender=KLMGov&numbers='.$mobile.'&message='.$message1;
                  $ch = curl_init();
                  $url= 'http://api.esms.kerala.gov.in/fastclient/SMSclient.php?'.$str;
                     
                  curl_setopt($ch, CURLOPT_URL, $url) or die("error1");
                  curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); //return as a variable
                  $result = curl_exec($ch);
                  $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                  curl_close($ch);
                  
                  //send mail
                  $toemail=sanitize_filename($this->request->getVar('emails'));
                  $result = $this->setMailsettings();
                  $result['to'] = $toemail;

                  $subject ="OTP Details";
                  $message = "KLMGOV- ".$pin." is the One Time Password(OTP). Please enter OTP to proceed"."<br>";
                  $content ="
                      <html>
                          <head>
                          <title></title>
                          </head>
                          <body style='background-color:#EEFFD5'>
                          <table cellpadding='0' cellspacing='0' border='0' width='673'>
                                  <tr>
                                  <td width='673' bgcolor='#EEFFD5'>
                                      <p></p><br>
                                      <p>Dear User,</p><br>
                                      <p style='font-size:15px; margin:0px 10px 0px 15px; text-align:justify;'>$message</p><br>
                                      </td>
                                  </tr>
                                  <tr>
                                  <td width='673' bgcolor='#EEFFD5'>
                                      <p></p>
                                      <p>Regards,</p>
                                      <p>". $result['fromname']."</p><br>
                                      </td>
                                  </tr>
                              
                          </table>
                          </body>
                          </html>";

                  
                 $this->sendMail($subject,$message,$result);
                  //end send mail

                  if (isset($_SESSION['UserLogDemo']))
                  {            
                      $session->remove('UserLogDemo');
                      $session->remove('otpVal1');
                      $session->remove('otpMob1');
                      $session->remove('otpEmails');
                  }  
                  
                  $ses_data = [
                     'UserLogDemo' =>   $lastID,
                     'otpVal1' => $pin,
                     'otpMob1' => $mobile,
                     'otpEmails' => $toemail
             ];  
             $session->set($ses_data);

             $data['success'] = 1;
             $data['res']= '<script> 
                  var otpVal1 ="'.$pin.'";
                  var otpMob1 ="'.$mobile.'";    
                 </script>'; 
             return $this->response->setJSON($data);
             exit;      
            }    
            }  
            else
            {
                $data1['validation']=$this->validator;
                $data['success']=0;
                $data['res']= $data1['validation']->listErrors();
                return $this->response->setJSON($data);
                exit;                      
            } 
       /*  }
        else
        {
            $data['success'] = 0;
            $data['res']='Validation Error.Try Agaain'; 
            return $this->response->setJSON($data);
            exit;               
        }//end final response

    } 
    else{
        $data['success'] = 0;
        $data['res']='Invalid Captcha'; 
        return $this->response->setJSON($data);
        exit;           
    } */   //end captcha     
}//end function


function checkMobiles()
{
    $token = csrf_hash();
    $data=array();
    $data['token'] = $token;
    if (isset($_POST['mobile']))
    {
    $mobile_number = sanitize_filename($_POST['mobile']);
    $total =0;
    $regmodel = new RegisterModel();//create Register model
    $total = $regmodel->where('mobileNumber', $mobile_number)
                                        ->countAllResults();  
     if($total>0) {
        $data['res'] =
           "<span class='status-not-available'> Mobile Number Already Exist.</span>  <script>$(document).ready(function(){
            $('#mobile_numbers').val('');
            });
            $('#form-group-mobiles').removeClass('green');
            $('#form-group-mobiles').addClass('red');
            </script>";
        return $this->response->setJSON($data);
        }else{
            $data['res'] = "<span class='status-available'></span>";
            return $this->response->setJSON($data);
        }
    }
    else{
        echo view ("index.html");
    }

}

function checkEmails()
{
   $token = csrf_hash();
   $data=array();
   $data['token'] = $token;
   $total =0;
   if (isset($_POST['email']))
   {
    $email = sanitize_filename($_POST['email']);  
    $regmodel = new RegisterModel();//create Register model
    $total = $regmodel->where('email', $email)->countAllResults();             
        if($total>0) {
            $data['res'] =
            "<span class='status-not-available'> Email-ID Already Exist</span>  <script>$(document).ready(function(){
            $('#email').val('');
            });
            $('#form-group-emails').removeClass('green');
            $('#form-group-emails').addClass('red');
            </script>";
            return $this->response->setJSON($data);
        }else{
            $data['res'] = "<span class='status-available'></span>";
            return $this->response->setJSON($data);
        }
    }
    else{
        echo view("index.html");
    }
}

function upload_files()
{
    
    $session = session();
    $token = csrf_hash();
    $data=array();
    $data['token'] = $token;
   
    $filedoc=$this->request->getFile('files');
    if (isset($filedoc))
   {
    $upload_path = FCPATH.'public/images/registration/organisation';
    $ext = $filedoc->getClientExtension();
    $newName='Ind-Doc-'.time().'.'.$ext;
    
    $input = $this->validate([
        'files' => [
            'uploaded[files]',
            'mime_in[files,application/pdf]',
            'max_size[files,20480]',
            'ext_in[files,pdf]',
        ]
    ]);
    if (!$input) {
        $data['res'] = "<span class='red'>Upload Pdf Files.</span>";
        return $this->response->setJSON($data);
    } else {
        $res = $filedoc->move($upload_path,$newName);
        if ( $res==1)
            {
                if (isset($_SESSION['regDoc']))
                     {            
                         $session->remove('regDoc');
                     }  
                $_SESSION['regDoc']=$newName; 
                $data['res'] = "<span class='green'>Document Uploaded.</span>"; 
                return $this->response->setJSON($data);
            }else{
                $data['res'] ="<span class='red'>Invalid File.Upload pdf Files.</span>";
                return $this->response->setJSON($data);
            }

        }
    }
    else
    {
        echo view("index.html"); 
    }
}

// resend otp for organsation
public function resendOtp1(){
    $session = session();
    $token = csrf_hash();
    $data=array();
    $data['token'] = $token;

    $mobile = "";
    $toemail = "";
    if (isset($_SESSION['UserLogDemo']))
    {
        $mobile = $_SESSION['otpMob1'];
        $toemail = $_SESSION['otpEmails'];
    }
     $pin = mt_rand(100000, 999999);
     $details['mobile'] =$mobile;
     $details['otp'] =$pin;
     //$this->session->set_userdata('OTP', $pin);
     date_default_timezone_set('Asia/Kolkata');
     $otpdate = date('d-m-y h:i:s');
     $details['otp_date'] =$otpdate;
     $message1= "KLMGOV- ".$pin."is the One Time Password(OTP). Please enter OTP to proceed";

     $str='username=eofficeksitm&password=eofcalerts&sender=KLMGov&numbers='.$mobile.'&message='.$message1;
         $ch = curl_init();
         $url= 'http://api.esms.kerala.gov.in/fastclient/SMSclient.php?'.$str;
         curl_setopt($ch, CURLOPT_URL, $url) or die("error1");
         curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); //return as a variable
         $result = curl_exec($ch);
         $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
         curl_close($ch);
         //send mail
        $result = $this->setMailsettings();
        $result['to'] = $toemail;

        $subject ="OTP Details";
        $message = "KLMGOV- ".$pin." is the One Time Password(OTP). Please enter OTP to proceed"."<br>";
        $content ="
            <html>
                <head>
                <title></title>
                </head>
                <body style='background-color:#EEFFD5'>
                <table cellpadding='0' cellspacing='0' border='0' width='673'>
                        <tr>
                        <td width='673' bgcolor='#EEFFD5'>
                            <p></p><br>
                            <p>Dear User,</p><br>
                            <p style='font-size:15px; margin:0px 10px 0px 15px; text-align:justify;'>$message</p><br>
                            </td>
                        </tr>
                        <tr>
                        <td width='673' bgcolor='#EEFFD5'>
                            <p></p>
                            <p>Regards,</p>
                            <p>". $result['fromname']."</p><br>
                            </td>
                        </tr>
                    
                </table>
                </body>
                </html>";
       $this->sendMail($subject,$message,$result);
        //end send mail
        $data['res']= '<script> 
                var otpVal1 ="'.$pin.'";
                var otpMob1 ="'.$mobile.'";    
               </script>';
            return $this->response->setJSON($data);   
            exit;        
 }

//save organisation registration
public function saveOrgOrginal()
{
    $session = session();
    $id ='';
    if (isset($_SESSION['UserLogDemo']))
    {
        $id = $_SESSION['UserLogDemo'];
    }
    $regdemomodel = new RegisterDemoModel();//create reg demo model
    $data['userOrg'] = $regdemomodel->where('id', $id)->first();
    $regmodel = new RegisterModel();//create Register model 
    if($data['userOrg']['organisationType'] =='1'){
        $regmodel->save([
            'regType' => $data['userOrg']['regType'],
            'organisationType' => $data['userOrg']['organisationType'],
            'organisationDept' => $data['userOrg']['organisationDept'],
            'organisationName' => $data['userOrg']['organisationName'],
            'localbody' => $data['userOrg']['localbody'],
            'localbodyDistrict' => $data['userOrg']['localbodyDistrict'],
            'blockpanchayath' => $data['userOrg']['blockpanchayath'],
            'gramapanchayath' => $data['userOrg']['gramapanchayath'],
            'muncipality' => $data['userOrg']['muncipality'],
            'concernedPerson' => $data['userOrg']['concernedPerson'],
            'mobileCode' => $data['userOrg']['mobileCode'],
            'mobileNumber' => $data['userOrg']['mobileNumber'],
            'email' => $data['userOrg']['email'],
            'designation' => $data['userOrg']['designation'],
            'otherContactDetail' => $data['userOrg']['otherContactDetail'],
            'state' => $data['userOrg']['state'],
            'district' => $data['userOrg']['district'],
            'inputDoc' => $data['userOrg']['inputDoc'],
            'inputDocNum' => $data['userOrg']['inputDocNum'],
            'docFile' => $data['userOrg']['docFile'],
            'username' => $data['userOrg']['username'],
            'password' => $data['userOrg']['password'],
            'refCode' => $data['userOrg']['refCode'],
            'ip' => $this->request->getIPAddress(),
            'date' => date("Y-m-d"),
        ]);
        }else{
        $regmodel->save([
            'regType' => $data['userOrg']['regType'], 
            'organisationType' => $data['userOrg']['organisationType'],
            'organisationName' => $data['userOrg']['organisationName'],
            'localbody' => $data['userOrg']['localbody'],
            'localbodyDistrict' => $data['userOrg']['localbodyDistrict'],
            'blockpanchayath' => $data['userOrg']['blockpanchayath'],
            'gramapanchayath' => $data['userOrg']['gramapanchayath'],
            'muncipality' => $data['userOrg']['muncipality'],
            'concernedPerson' => $data['userOrg']['concernedPerson'],
            'mobileCode' => $data['userOrg']['mobileCode'],
            'mobileNumber' => $data['userOrg']['mobileNumber'],
            'email' => $data['userOrg']['email'],
            'designation' => $data['userOrg']['designation'],
            'otherContactDetail' => $data['userOrg']['otherContactDetail'],
             'state' => $data['userOrg']['state'],
            'district' => $data['userOrg']['district'],
            'inputDoc' => $data['userOrg']['inputDoc'],
            'inputDocNum' => $data['userOrg']['inputDocNum'],
            'docFile' => $data['userOrg']['docFile'],
            'username' => $data['userOrg']['username'],
            'password' => $data['userOrg']['password'],
            'refCode' => $data['userOrg']['refCode'],
            'ip' => $this->request->getIPAddress(),
            'date' => date("Y-m-d"),
        ]);
        
        }
        $lastregID = $regmodel->InsertID();
        if ($lastregID!='')
        {
            $awardmodel = new AwardModel();//create award model
            $awardmodel->save([
                'userID' => $lastregID,
                'year' => date("Y"),
            ]);
            if($data['userOrg']['regType']==1){
                $username=$data['userOrg']['fName'];
            }else{
                $username=$data['userOrg']['organisationName'];
            }

            $ses_data = [
                'UserLog' => $lastregID,
                'regType' => $data['userOrg']['regType'],
                'displayName' => $username
            ];
            $session->set($ses_data);
         }
         /* ---------------------------SMS------------------------------------------*/
        $mobile = $data['userOrg']['mobileNumber'];
        $uname = $data['userOrg']['fName'];
        $loid = $data['userOrg']['username'];
        $lopass =$data['userOrg']['refCode'];
        $pin = mt_rand(100000, 999999);
        $details['mobile'] =$mobile;
        $details['otp'] =$pin;
        //$this->session->set_userdata('OTP', $pin);
        date_default_timezone_set('Asia/Kolkata');
        $otpdate = date('d-m-y h:i:s');
        $details['otp_date'] =$otpdate;
        $message1= "KLMGOV-Dear ".$uname.", Thank You for registering with us.Your Login ID : ".$loid." and Password : ".$lopass;

        $str='username=eofficeksitm&password=eofcalerts&sender=KLMGov&numbers='.$mobile.'&message='.$message1;
        $ch = curl_init();
        $url= 'http://api.esms.kerala.gov.in/fastclient/SMSclient.php?'.$str;
        curl_setopt($ch, CURLOPT_URL, $url) or die("error1");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); //return as a variable
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

          //send mail
          $toemail=$data['userOrg']['email'];
          $result = $this->setMailsettings();
          $result['to'] = $toemail;

          $subject ="Login Details";
          $message = "Dear ".$uname.", Thank You for registering with us.Your Login ID : ".$loid." and Password : ".$lopass."<br>";
          $content ="
              <html>
                  <head>
                  <title></title>
                  </head>
                  <body style='background-color:#EEFFD5'>
                  <table cellpadding='0' cellspacing='0' border='0' width='673'>
                          <tr>
                          <td width='673' bgcolor='#EEFFD5'>
                              <p></p><br>
                              <p>Dear User,</p><br>
                              <p style='font-size:15px; margin:0px 10px 0px 15px; text-align:justify;'>$message</p><br>
                              </td>
                          </tr>
                          <tr>
                          <td width='673' bgcolor='#EEFFD5'>
                              <p></p>
                              <p>Regards,</p>
                              <p>". $result['fromname']."</p><br>
                              </td>
                          </tr>
                      
                  </table>
                  </body>
                  </html>";

        $this->sendMail($subject,$message,$result);
       //end send mail 
        /*--------------------------Mail-------------------------------------------*/
        return redirect()->to(base_url().'nomination');

}
//start site settings
public function getCommon()
{
    $sitesettngs = new SiteSettings(); // create an instance of Library
    $res =$sitesettngs->get_site_settings(); // calling method
    $data	=	array();
    foreach($res as $row){
        $data1[$row->config_key] = $row->config_value;
    }
    $data['Titletag'] = $data1['title_tags'];
    $data['metadescription'] = $data1['meta_description'];
    $data['metakeywords'] = $data1['meta_tags'];
    $data['site_name'] = $data1['site_name'];
    $data['siteKey'] = $data1['siteKey'];
    $data['secretKey'] = $data1['secretKey'];
    $data['copyright_year'] = $data1['copyright_year'];
    return $data;  
}
public function setMailsettings()
{
   
    $adminmodel = new AdminModel();   
    $res = $adminmodel->where('status', 1)->first();
    $result['frommail'] = $res['email'];
    $result['fromname'] = $res['first_name'];
    return $result;
}

public function sendMail($subject,$content,$result)
{
$email = \Config\Services::email(); 
/*
$email->setFrom($result['frommail'], $result['fromname']);
$email->setTo( $result['to']);
$email->setSubject($subject);
$email->setMessage($content);
$email->send(); */
}
}//end class

