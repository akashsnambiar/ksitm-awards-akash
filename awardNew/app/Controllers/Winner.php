<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Libraries\SiteSettings; // Import library
use App\Models\WinnerModel;
use App\Models\NominationCategoryModel;
use App\Models\AwardareaModel;
use CodeIgniter\Database\RawSql;

class Winner extends BaseController
{

   public function getCommon()
   {
      $sitesettngs = new SiteSettings();
      $res = $sitesettngs->get_site_settings();
      $data   =   array();
      foreach ($res as $row) {
         $data1[$row->config_key] = $row->config_value;
      }
      $data['Titletag'] = $data1['title_tags'];
      $data['metadescription'] = $data1['meta_description'];
      $data['metakeywords'] = $data1['meta_tags'];
      $data['site_name'] = $data1['site_name'];
      $data['copyright_year'] = $data1['copyright_year'];
      return $data;
   }
  
   public function index()
   {
      $data = $this->getCommon();
      $data['pagetitle'] = 'winners';

      $nomcatmodel = new NominationCategoryModel();
      $data['nomcat'] = $nomcatmodel->findAll();

      $awardmodel = new AwardareaModel();
      $data['awardarea'] = $awardmodel->findAll();

      $winnermodel = new WinnerModel();
      $data['winneryear'] = $winnermodel
         ->select('distinct (year)')
         ->join('banner', 'winner.banner_title_id = banner.id')
         ->findAll();
      //  echo $winnermodel->db->getLastQuery();exit;
      $data['winner'] = $winnermodel
         ->select('winner.id as id,winner.banner_title_id as banner_title_id,winner.winner_name as winner_name,winner_name_mal as winner_name_mal,winner.winner_photo as winner_photo,winner.status as status,b.id as bid,b.banner_title_mal as banner_title_mal,b.banner_title_en as banner_title_en,b.banner_title_mal as banner_title_mal,b.year as year,b.category_id as category_id,b.status as bstatus,b.category_image as category_image,n.id as nid, n.nomination_category as nomination_category, a.title, a.title_e')
         ->join('banner AS b', 'winner.banner_title_id = b.id')
         ->join('nomination_category AS n', 'b.category_id = n.id')
         ->join('awardarea AS a', 'a.awardID = winner.awardarea_id')
         ->where('winner.status', 1)
         ->orderBy('b.year', 'desc')
         ->orderBy('b.category_id', 'asc')
         ->findAll();
      $data['winner_kj'] = $winnermodel
         ->select('winner.id as id,winner.banner_title_id as banner_title_id,winner.winner_name as winner_name,winner.winner_photo as winner_photo,winner.status as status,b.id as bid,b.banner_title_mal as banner_title_mal,b.banner_title_en as banner_title_en,b.banner_title_mal as banner_title_mal,b.year as year,b.category_id as category_id,b.status as bstatus,b.category_image as category_image,n.id as nid, n.nomination_category as nomination_category')
         ->join('banner AS b', 'winner.banner_title_id = b.id')
         ->join('nomination_category AS n', 'b.category_id = n.id')
         ->where('winner.status', 1)
         ->where('category_id', 1)
         ->countAllResults();
      $data['winner_kp'] = $winnermodel
         ->select('winner.id as id,winner.banner_title_id as banner_title_id,winner.winner_name as winner_name,winner.winner_photo as winner_photo,winner.status as status,b.id as bid,b.banner_title_mal as banner_title_mal,b.banner_title_en as banner_title_en,b.banner_title_mal as banner_title_mal,b.year as year,b.category_id as category_id,b.status as bstatus,b.category_image as category_image,n.id as nid, n.nomination_category as nomination_category')
         ->join('banner AS b', 'winner.banner_title_id = b.id')
         ->join('nomination_category AS n', 'b.category_id = n.id')
         ->where('winner.status', 1)
         ->where('category_id', 2)
         ->countAllResults();
      $data['winner_ks'] = $winnermodel
         ->select('winner.id as id,winner.banner_title_id as banner_title_id,winner.winner_name as winner_name,winner.winner_photo as winner_photo,winner.status as status,b.id as bid,b.banner_title_mal as banner_title_mal,b.banner_title_en as banner_title_en,b.banner_title_mal as banner_title_mal,b.year as year,b.category_id as category_id,b.status as bstatus,b.category_image as category_image,n.id as nid, n.nomination_category as nomination_category')
         ->join('banner AS b', 'winner.banner_title_id = b.id')
         ->join('nomination_category AS n', 'b.category_id = n.id')
         ->where('winner.status', 1)
         ->where('category_id', 3)
         ->countAllResults();
      //   echo $winnermodel->db->getLastQuery();exit;
      echo view('winners', $data);
   }
   public function winnerSearch()
   {
      $data = $this->getCommon();
      $data['pagetitle'] = 'winners';
     
      $award = $this->request->getVar('award');
      $year = $this->request->getVar('year');
      $field = $this->request->getVar('field');
     
      $nomcatmodel = new NominationCategoryModel();
      $data['nomcat'] = $nomcatmodel->findAll();

      $winnermodel = new WinnerModel();

      $builder = $winnermodel->select('winner.id as id,b.category_id as category_id,winner.winner_name as winner_name,winner.banner_title_id as banner_title_id,winner_name_mal as winner_name_mal,winner.winner_photo as winner_photo,winner.awardarea_id as awardarea_id,winner.status as status,b.id as bid,b.banner_title_mal as banner_title_mal,b.banner_title_en as banner_title_en,b.banner_title_mal as banner_title_mal,b.year as year,b.status as bstatus,b.category_image as category_image,n.id as nid, n.nomination_category as nomination_category,a.title, a.title_e')
         ->join('banner AS b', 'winner.banner_title_id = b.id')
         ->join('nomination_category AS n', 'b.category_id = n.id')
         ->join('awardarea AS a', 'a.awardID = winner.awardarea_id')
         ->where('winner.status', 1);
      // ->findAll();
      if (($year) != "") {
         $builder->where('year', $year);
      }
      if (($award) != "") {
         $builder->where('category_id ', $award);
      }
      if (($field) != "") {
         $builder->where('awardarea_id', $field);
      }
      $builder->orderBy('b.year', 'desc')
         ->orderBy('b.category_id', 'asc');
      $result = $builder->findAll();
      //echo $winnermodel->db->getLastQuery();exit;
      //$build = "";

      // if (($award) != "") {
      //    $build = 'nomination_category.id = w.cat_id AND w.cat_id = ' . $award;
      // }
      // if (($year) != "") {
      //    $build = 'nomination_category.id = w.cat_id AND w.win_year = ' . $year;
      // }
      // if (($field) != "") {
      //    $build = 'nomination_category.id = w.cat_id AND w.awardarea_id = ' . $field;
      // }

      // $buildercount = $nomcatmodel->select('nomination_category.id, nomination_category.nomination_category, COUNT(w.cat_id) as count')
      //    ->join('winner w', new RawSql($build), 'left')
      //    ->groupBy('nomination_category.id');
      $buildercount = $nomcatmodel->select('nomination_category.id, nomination_category.nomination_category, COUNT(w.cat_id) as count')
         ->join('winner w', 'nomination_category.id = w.cat_id', 'left');

      if (($year) != "") {
         $buildercount->where('w.win_year', $year);
      }
      if (($award) != "") {
         $buildercount->where('w.cat_id', $award);
      }
      if (($field) != "") {
         $buildercount->where('w.awardarea_id', $field);
      }
      $buildercount->groupBy('nomination_category.id');
      $datacount = $buildercount->findAll();
      //print_r($datacount);exit;
      $token = csrf_hash();
      // $data['token'] = $token;
      $data = [
         'winnerResult' => $result,
         'datacount' => $datacount,
         'token' => $token
      ];

      // echo $winnermodel->db->getLastQuery();
      // echo $nomcatmodel->db->getLastQuery();
      // exit;

      echo json_encode($data);
   }

}
