<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Libraries\SiteSettings; // Import library
use App\Models\NominationsModel;
use App\Models\NominationdemoModel;
use App\Models\AwardModel;
use App\Models\RegisterModel;
use App\Models\StateModel;
use App\Models\DistrictModel;
use App\Models\ThalukModel;
use App\Models\AwardareaModel;
use App\Models\AwardareasubModel;
use App\Models\EsignModel;
use App\Models\NominationPeriodModel;
use App\Models\ReviewModel;

class Nomination extends BaseController
{
    public function __construct()
    {
        $this->request = \Config\Services::request();
        $this->nominationModel = new NominationsModel();
    }
    public function index()
    {
        //
        $session = session();
        $data = $this->getCommon();
        if (isset($_SESSION['UserLog'])) {
            $userid = $_SESSION['UserLog'];
            $statemodel = new StateModel(); //create state model
            $data['state'] = $statemodel->where('status', 1)
                ->orderBy('state', "ASC")->findall();
            $districtmodel = new DistrictModel(); //create district model
            $data['district'] = $districtmodel->where('status', 1)
                ->orderBy('district', "ASC")->findall();
            $talukmodel = new ThalukModel(); //create thaluk model
            $data['taluk'] = $talukmodel->where('status', 1)
                ->orderBy('taluk', "ASC")->findall();
            $awardareamodel = new AwardareaModel(); //create Awardarea model
            $data['workArea'] = $awardareamodel->where('status', 1)
                ->orderBy('awardID', "ASC")->findall();
            $awardareasubmodel = new AwardareasubModel(); //create Awardareasub model
            $data['workAreaSub'] = $awardareasubmodel->where('status', 1)
                ->orderBy('awardAreaSubID', "ASC")->findall();
            $data['nomCnt'] = $this->nominationModel->where('userID', $userid)
                ->where("year", date("Y"))
                ->countAllResults();
            echo view('nomination', $data);
        } else {
            return redirect()->to(base_url() . 'login');
        }
    }
    public function myAccount()
    {
        $session = session();
        if ((isset($_SESSION['isUserLoggedIn']) == 1) && (isset($_SESSION['UserLog']) != '')) {
            $data = $this->getCommon();
            $id = $_SESSION['UserLog'];

            $data['user'] = $this->nominationModel
                ->select("nominations.*,S.state,D.district,T.taluk,A.title,AB.awardAreaSubTitle,R.fName,
                        R.mName,R.lName,R.organisationName,R.regType")
                ->join('state AS S', 'S.stateID = nominations.state', 'LEFT')
                ->join('district AS D', 'D.districtID = nominations.district', 'LEFT')
                ->join('thaluk AS T', 'T.talukID = nominations.taluk', 'LEFT')
                ->join('awardarea AS A', 'A.awardID = nominations.workArea', 'LEFT')
                ->join('awardareasub AS AB', 'AB.awardAreaSubID = nominations.workAreaSub', 'LEFT')
                ->join('registration AS R', 'R.id = nominations.userID', 'LEFT')
                ->where("userID = '$id'")
                ->orderBy('nominations.nominationID', "DESC")->findall();
            //echo $this->nominationModel->db->getLastQuery();exit;
            $data['userCount'] = $this->nominationModel->where('userID', $id)
                //->where("year",date("Y"))
                ->countAllResults();

            $awardModel = new AwardModel(); //create award model
            $data['userAward'] = $awardModel->where('userID', $id)
                ->where("year", date("Y"))
                ->first();

            $nominationdemoModel = new NominationdemoModel();
            $data['status'] = $nominationdemoModel->select("step,nomDemoID")
                ->where("userID", $id)
                ->where("step <>", 5)
                ->where("YEAR(date)", date("Y"))
                ->orderBy('nomDemoID', "DESC")->first();
            // echo $nominationdemoModel->db->getLastQuery();exit;
            $nominationModel = new NominationsModel();
            $data['nomdata'] = $nominationModel
                ->select("nominationID")
                ->where("userID ", $id)
                ->orderBy('nominationID', "DESC")->countAllResults();
            //get the time period of nomination
            $data['nomperiod'] = $this->getNominationPeriod();
            echo view('myaccount', $data);

        } else {

            return redirect()->to(base_url() . 'login');
        }
    }
    //calculate age from dob
    public function calculateAge()
    {
        $session = session();
        if (isset($_SESSION['UserLog'])) {
            $dateOfBirth = $this->request->getVar('dob');
            $today = date("Y-m-d");
            $diff = date_diff(date_create($dateOfBirth), date_create($today));
            echo $diff->format('%y');
        } else {
            return redirect()->redirect(base_url() . 'login');
        }
    }
    //end calculate age

    //save step 1 data
    public function save()
    {
        $session = session();
        if (isset($_SESSION['UserLog'])) {
            $userid = $_SESSION['UserLog'];
            if (isset($_SESSION['nomDoc'])) {
                $nomDoc = $_SESSION['nomDoc'];
            } else {
                $nomDoc = "";
            }
            $rules = [
                'name' => 'required|alpha_space',
                'address' => 'required',
                'email' => 'permit_empty|valid_email',
            ];
            $messages = [
                "name" => [
                    "required" => "പേര് ഉറപ്പായും നൽകുക.",
                    "alpha_space" => "പേര് ഫീൽഡിൽ അക്ഷരങ്ങൾ മാത്രം നൽകുക.",
                ],
                "address" => [
                    "required" => "മേൽവിലാസം ഉറപ്പായും നൽകുക.",
                ],
                "email" => [
                    "valid_email" => "സാധുവായ ഇമെയിൽ വിലാസം നൽകുക.",
                ],
            ];

            if ($this->validate($rules, $messages)) {
                $dob = $this->request->getVar('dob');
                $today = date("Y-m-d");
                $diff = date_diff(date_create($dob), date_create($today));
                $cal_age = $diff->format('%y');
                if (($dob) && ($cal_age <= 0 || $dob > $today || strlen($cal_age) > 3)) {
                    $message = "ഒരു സാധുവായ ജനന തീയതി നൽകുക.";
                    $data['success'] = 0;
                    $data['error'] = $message;
                    return $this->response->setJSON($data);
                    exit;
                } else {
                    if ($this->request->getVar('dob') == '') {
                        $dob = '';
                    } else {
                        $dob = $this->request->getVar('dob');
                    }
                    if ($this->request->getVar('age') == '') {
                        $age = 0;
                    } else {
                        $age = sanitize_filename($this->request->getVar('age'));
                    }
                    if ($this->request->getVar('sex') == '') {
                        $sex = 0;
                    } else {
                        $sex = sanitize_filename($this->request->getVar('sex'));
                    }
                    if ($this->request->getVar('district') == '') {
                        $district = 0;
                    } else {
                        $district = sanitize_filename($this->request->getVar('district'));
                    }
                    if ($this->request->getVar('taluk') == '') {
                        $taluk = 0;
                    } else {
                        $taluk = sanitize_filename($this->request->getVar('taluk'));
                    }

                    $nominationdemoModel = new NominationdemoModel();
                    $nominationdemoModel->save([
                        'userID' => $userid,
                        'name' => sanitize_filename($this->request->getVar('name')),
                        'dob' => $dob,
                        'age' => $age,
                        'sex' => $sex,
                        'state' => sanitize_filename($this->request->getVar('state')),
                        'district' => $district,
                        'taluk' => $taluk,
                        'pob' => sanitize_filename($this->request->getVar('pob')),
                        'address' => sanitize_filename($this->request->getVar('address')),
                        'email' => sanitize_filename($this->request->getVar('email')),
                        'country' => sanitize_filename($this->request->getVar('country')),
                        'job' => sanitize_filename($this->request->getVar('job')),
                        'photo' => $nomDoc,
                        'date' => date("Y-m-d"),
                    ]);

                    $lastID = $nominationdemoModel->InsertID();
                    if (isset($_SESSION['NomLog'])) {
                        $session->remove('NomLog');
                    }
                    $ses_data = ['NomLog' => $lastID];
                    $session->set($ses_data);

                    $data['success'] = 1;
                    return $this->response->setJSON($data);
                    exit;
                }
            } else {
                $data1['validation'] = $this->validator;
                $data['success'] = 0;
                $data['error'] = $data1['validation']->listErrors();
                return $this->response->setJSON($data);
                exit;
            }

        } else {
            return redirect()->to(base_url() . 'login');
        }

    }
    //end step1 data


    //upload image   
    public function upload_file()
    {
        $session = session();
        $filedoc = $this->request->getFile('file');
        $name = $filedoc->getRandomName();

        $input = $this->validate([
            'file' => [
                'uploaded[file]',
                'mime_in[file,image/jpg,image/jpeg,image/png]',
                'max_size[file,2048]',
                'ext_in[file,jpg,jpeg,png]',
            ]
        ]);
        if (!$input) {
            echo "<span class='red'>Upload png,jpg,jpeg Files.</span>";
        } else {
            $upload_path = FCPATH . 'public/images/nomination';
            $ext = $filedoc->getClientExtension();
            $nomDoc = $filedoc->getName();
            $nomType = $filedoc->getClientMimeType();

            $newName = 'Nom-Pho-' . time() . '.' . $ext;
            $res = $filedoc->move($upload_path, $newName);
            if ($res == 1) {
                if (isset($_SESSION['nomDoc'])) {
                    $session->remove('nomDoc');
                }
                $_SESSION['nomDoc'] = $newName;
                $disPath = act_url() . 'public/images/nomination/' . $newName;
                echo '<img src="' . $disPath . '" height="75" width="100" class="img-thumbnail" />';
                echo "Uploaded..";
            } else {
                echo "Invalid File.Upload png,jpg,jpeg Files.";
            }

        }
        exit;
    }

    //start step2    
    public function step2()
    {
        $session = session();
        $data = $this->getCommon();

        if (isset($_SESSION['UserLog'])) {
            $userid = $_SESSION['UserLog'];
            $statemodel = new StateModel(); //create state model
            $data['state'] = $statemodel->where('status', 1)
                ->orderBy('state', "ASC")->findall();
            $districtmodel = new DistrictModel(); //create district model
            $data['district'] = $districtmodel->where('status', 1)
                ->orderBy('district', "ASC")->findall();
            $talukmodel = new ThalukModel(); //create thaluk model
            $data['taluk'] = $talukmodel->where('status', 1)
                ->orderBy('taluk', "ASC")->findall();
            $awardareamodel = new AwardareaModel(); //create Awardarea model
            $data['workArea'] = $awardareamodel->where('status', 1)
                ->orderBy('awardID', "ASC")->findall();
            $awardareasubmodel = new AwardareasubModel(); //create Awardareasub model
            $data['workAreaSub'] = $awardareasubmodel->where('status', 1)
                ->orderBy('awardAreaSubID', "ASC")->findall();

            $data['nomCnt'] = $this->nominationModel->where('userID', $userid)
                ->where("year", date("Y"))
                ->countAllResults();

            $awardModelchk = new AwardModel(); //create award model
            $data['chkuserAward'] = $awardModelchk->where('userID', $userid)
                                    ->where("year", date("Y"))->countAllResults();
               // print_r( $data['chkuserAward']);exit;
                if( $data['chkuserAward'] ==0)
                {
                    $awardModelsave = new AwardModel();
                    $awardModelsave->save([
                    'userID' => $userid,
                    'year' => date("Y"),
                    ]);
                 }
            $awardModel = new AwardModel(); //create award model
            $data['userAward'] = $awardModel->where('userID', $userid)
                ->where("year", date("Y"))
                ->findall();
               // echo $awardModel->db->getLastQuery();
            //print_r( $data['userAward']);exit;
            echo view('nomination2', $data);
        } else {
            return redirect()->to(base_url() . 'login');
        }
    }

    public function save2()
    {
        $session = session();
        helper(['form']);
        if (isset($_SESSION['UserLog'])) {
            $userid = $_SESSION['UserLog'];
            if (isset($_SESSION['nomDocument'])) {
                $nomDoc = $_SESSION['nomDocument'];
            } else {
                $nomDoc = "";
            }
            if (isset($_SESSION['NomLog'])) {
                $nomdemoid = $_SESSION['NomLog'];
            } else {
                $nomdemoid = "";
            }

            $rules = [
                'workArea' => 'required',
                'workAreaSub' => 'required',
                'award' => 'required',
                'pyn' => 'required',
                'note' => 'required',
                'donation' => 'required',
                'achivement' => 'required',
            ];
            $messages = [
                "workArea" => [
                    "required" => "പ്രവർത്തനമേഖല ഉറപ്പായും നൽകുക.",
                ],
                "workAreaSub" => [
                    "required" => "ഉപ മേഖല ഉറപ്പായും നൽകുക.",
                ],
                "award" => [
                    "required" => "ഏത് കേരള പുരസ്കാരത്തിനാണ് ശിപാർശ ഉറപ്പായും നൽകുക.",
                ],
                "pyn" => [
                    "required" => "പത്മ പുരസ്‌കാരം ലഭിച്ചിട്ടുണ്ടോ ഫീൽഡ് ഉറപ്പായും നൽകുക.",
                ],
                "note" => [
                    "required" => "നാമനിദ്ദേശം ചെയ്യുന്ന വ്യക്തിയെക്കുറിച്ചുള്ള ചെറുവിവരണം ഉറപ്പായും നൽകുക.",
                ],
                "donation" => [
                    "required" => "നാമനിദ്ദേശം ചെയ്യപ്പെടുന്ന  വ്യക്തി സമൂഹത്തിന് നൽകിയിട്ടുള്ള കാര്യമായ സംഭാവനകൾ ഉറപ്പായും നൽകുക.",
                ],
                "achivement" => [
                    "required" => "നാമനിദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തിയുടെ നേട്ടങ്ങൾ ഉറപ്പായും നൽകുക.",
                ],
            ];

            if ($this->validate($rules, $messages)) {
                if ($this->request->getVar('pyn') == 1) {
                    $rules1 = [
                        'pAward' => 'required',
                        'pYear' => 'required',
                    ];
                    $messages1 = [
                        "pAward" => [
                            "required" => "ഏത് പുരസ്‌കാരമാണ് ലഭിച്ചത് ഉറപ്പായും നൽകുക.",
                        ],
                        "pYear" => [
                            "required" => "ഏത് വർഷമാണ് ലഭിച്ചത് ഉറപ്പായും നൽകുക.",
                        ],
                    ];

                    if (!$this->validate($rules1, $messages1)) {
                        $data1['validation'] = $this->validator;
                        $data['success'] = 0;
                        $data['error'] = $data1['validation']->listErrors();
                        return $this->response->setJSON($data);
                        exit;
                    }
                }

                if ($this->request->getVar('workArea') == '') {
                    $workArea = 0;
                } else {
                    $workArea = sanitize_filename($this->request->getVar('workArea'));
                }
                if ($this->request->getVar('workAreaSub') == '') {
                    $workAreaSub = 0;
                } else {
                    $workAreaSub = sanitize_filename($this->request->getVar('workAreaSub'));
                }
                if ($this->request->getVar('award') == '') {
                    $award = 0;
                } else {
                    $award = sanitize_filename($this->request->getVar('award'));
                }
                if ($this->request->getVar('pyn') == '') {
                    $pyn = 0;
                } else {
                    $pyn = sanitize_filename($this->request->getVar('pyn'));
                }
                if ($this->request->getVar('pAward') == '') {
                    $pAward = "";
                } else {
                    $pAward = sanitize_filename($this->request->getVar('pAward'));
                }
                if ($this->request->getVar('pYear') == '') {
                    $pYear = 0;
                } else {
                    $pYear = sanitize_filename($this->request->getVar('pYear'));
                }
                $nominationdemoModel = new NominationdemoModel();
                $data = array(
                    'workArea' => $workArea,
                    'workAreaSub' => $workAreaSub,
                    'award' => $award,
                    'pyn' => $pyn,
                    'pAward' => $pAward,
                    'pYear' => $pYear,
                    'note' => sanitize_filename($this->request->getVar('note')),
                    'donation' => sanitize_filename($this->request->getVar('donation')),
                    'achivement' => sanitize_filename($this->request->getVar('achivement')),
                    'document' => $nomDoc,
                    'step' => 2
                );
                $nominationdemoModel->update($nomdemoid, $data);
                // echo $nominationdemoModel->db->getLastQuery();exit;
                $data['success'] = 1;
                return $this->response->setJSON($data);
                exit;

            } else {
                $data1['validation'] = $this->validator;
                $data['success'] = 0;
                $data['error'] = $data1['validation']->listErrors();
                return $this->response->setJSON($data);
                exit;
            }
        } else {

            return redirect()->to(base_url() . 'login');
        }
    }
    //edit step2
    public function saveEdit2()
    {
        $session = session();
        helper(['form']);
        if (isset($_SESSION['UserLog'])) {
            $userid = $_SESSION['UserLog'];
            if (isset($_SESSION['nomDocument'])) {
                $nomDoc = $_SESSION['nomDocument'];
            } else {
                $nomDoc = "";
            }
            if (isset($_SESSION['NomLog'])) {
                $nomdemoid = $_SESSION['NomLog'];
            } else {
                $nomdemoid = "";
            }

            $rules = [
                'workArea' => 'required',
                'workAreaSub' => 'required',
                'award' => 'required',
                'pyn' => 'required',
                'note' => 'required',
                'donation' => 'required',
                'achivement' => 'required',
            ];
            $messages = [
                "workArea" => [
                    "required" => "പ്രവർത്തനമേഖല ഉറപ്പായും നൽകുക.",
                ],
                "workAreaSub" => [
                    "required" => "ഉപ മേഖല ഉറപ്പായും നൽകുക.",
                ],
                "award" => [
                    "required" => "ഏത് കേരള പുരസ്കാരത്തിനാണ് ശിപാർശ ഉറപ്പായും നൽകുക.",
                ],
                "pyn" => [
                    "required" => "പത്മ പുരസ്‌കാരം ലഭിച്ചിട്ടുണ്ടോ ഉറപ്പായും നൽകുക.",
                ],
                "note" => [
                    "required" => "നാമനിദ്ദേശം ചെയ്യുന്ന വ്യക്തിയെക്കുറിച്ചുള്ള ചെറുവിവരണം ഉറപ്പായും നൽകുക.",
                ],
                "donation" => [
                    "required" => "നാമനിദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തി സമൂഹത്തിന് നൽകിയിട്ടുള്ള കാര്യമായ സംഭാവനകൾ ഉറപ്പായും നൽകുക.",
                ],
                "achivement" => [
                    "required" => "നാമനിദ്ദേശം ചെയ്യപ്പെടുന്ന വ്യക്തിയുടെ നേട്ടങ്ങൾ ഉറപ്പായും നൽകുക.",
                ],
            ];

            if ($this->validate($rules, $messages)) {
                if ($this->request->getVar('pyn') == 1) {
                    $rules1 = [
                        'pAward' => 'required',
                        'pYear' => 'required',
                    ];
                    $messages1 = [
                        "pAward" => [
                            "required" => "ഏത് പുരസ്‌കാരമാണ് ലഭിച്ചത് ഉറപ്പായും നൽകുക.",
                        ],
                        "pYear" => [
                            "required" => "ഏത് വർഷമാണ് ലഭിച്ചത് ഉറപ്പായും നൽകുക.",
                        ],
                    ];

                    if (!$this->validate($rules1, $messages1)) {
                        $data1['validation'] = $this->validator;
                        $data['success'] = 0;
                        $data['error'] = $data1['validation']->listErrors();
                        return $this->response->setJSON($data);
                        exit;
                    }
                }
                if ($this->request->getVar('workArea') == '') {
                    $workArea = 0;
                } else {
                    $workArea = sanitize_filename($this->request->getVar('workArea'));
                }
                if ($this->request->getVar('workAreaSub') == '') {
                    $workAreaSub = 0;
                } else {
                    $workAreaSub = sanitize_filename($this->request->getVar('workAreaSub'));
                }
                if ($this->request->getVar('award') == '') {
                    $award = 0;
                } else {
                    $award = sanitize_filename($this->request->getVar('award'));
                }
                if ($this->request->getVar('pyn') == '') {
                    $pyn = 0;
                } else {
                    $pyn = sanitize_filename($this->request->getVar('pyn'));
                }
                
                if ($this->request->getVar('pAward') == '') {
                    $pAward = "";
                } else {
                    $pAward = sanitize_filename($this->request->getVar('pAward'));
                }
                if ($this->request->getVar('pYear') == '') {
                    $pYear = 0;
                } else {
                    $pYear = sanitize_filename($this->request->getVar('pYear'));
                }
                $nominationdemoModel = new NominationdemoModel();
                $data = array(
                    'workArea' => $workArea,
                    'workAreaSub' => $workAreaSub,
                    'award' => $award,
                    'pyn' => $pyn,
                    'pAward' =>$pAward,
                    'pYear' => $pYear,
                    'note' => sanitize_filename($this->request->getVar('note')),
                    'donation' => sanitize_filename($this->request->getVar('donation')),
                    'achivement' => sanitize_filename($this->request->getVar('achivement')),
                    'document' => $nomDoc
                );
                $nominationdemoModel->update($nomdemoid, $data);
                // echo $nominationdemoModel->db->getLastQuery();exit;
                $data['success'] = 1;
                return $this->response->setJSON($data);
                exit;

            } else {
                $data1['validation'] = $this->validator;
                $data['success'] = 0;
                $data['error'] = $data1['validation']->listErrors();
                return $this->response->setJSON($data);
                exit;
            }
        } else {

            return redirect()->to(base_url() . 'login');
        }
    }
    //upload document
    public function upload_doc()
    {
        $session = session();
        $filedoc = $this->request->getFile('doc');
        $input = $this->validate([
            'doc' => [
                'uploaded[doc]',
                'mime_in[doc,application/pdf]',
                'max_size[doc,20480]',
                'ext_in[doc,pdf]',
            ]
        ]);
        if (!$input) {
            echo "<span class='red'>Upload Only Pdf Files.</span>";
        } else {
            $upload_path = FCPATH . 'public/images/nomination/documents';
            $ext = $filedoc->getClientExtension();
            $nomDoc = $filedoc->getName();
            $nomType = $filedoc->getClientMimeType();

            $newName = 'Nom-Doc-' . time() . '.' . $ext;
            $res = $filedoc->move($upload_path, $newName);
            if ($res == 1) {
                if (isset($_SESSION['nomDocument'])) {
                    $session->remove('nomDocument');
                }
                $_SESSION['nomDocument'] = $newName;
                echo "Uploaded..";
            } else {
                echo "Invalid File.Upload Pdf Files.";
            }
        }
    }

    //start step3
    public function step3()
    {

        $session = session();
        $data = $this->getCommon();
        if (isset($_SESSION['UserLog'])) {
            $userid = $_SESSION['UserLog'];
            echo view('nomination3', $data);
        } else {
            return redirect()->to(base_url() . 'login');
        }

    }

    //save step3
    public function save3()
    {
        $session = session();
        helper(['form']);
        $nomdemoid = "";
        if (isset($_SESSION['UserLog'])) {
            $userid = $_SESSION['UserLog'];
            if (isset($_SESSION['NomLog'])) {
                $nomdemoid = $_SESSION['NomLog'];
            }

            $rules = [
                'wDonation' => 'required',
                'sDonation' => 'required',
            ];
            $messages = [
                "wDonation" => [
                    "required" => "പ്രവർത്തനമേഖലയിലെ  സംഭാവന ഉറപ്പായും നൽകുക.",
                ],
                "sDonation" => [
                    "required" => "സമൂഹത്തിനുള്ള  സംഭാവന ഉറപ്പായും നൽകുക.",
                ]
            ];

            if ($this->validate($rules, $messages)) {
                $nominationdemoModel = new NominationdemoModel();
                $data = array(
                    'wDonation' => sanitize_filename($this->request->getVar('wDonation')),
                    'sDonation' => sanitize_filename($this->request->getVar('sDonation')),
                    'step' => 3
                );

                $nominationdemoModel->update($nomdemoid, $data);
                //echo $nominationdemoModel->db->getLastQuery();exit;
                $data['success'] = 1;
                return $this->response->setJSON($data);
                exit;
            } else {
                $data1['validation'] = $this->validator;
                $data['success'] = 0;
                $data['error'] = $data1['validation']->listErrors();
                return $this->response->setJSON($data);
                exit;
                /*  $data = $this->getCommon();
                 $userid = $_SESSION['UserLog']; 
                 echo view('nomination4',$data);  */
            }

        } else {
            return redirect()->to(base_url() . 'login');
        }
    }
    //end step3

    function step4()
    {
        $session = session();
        $data = $this->getCommon();
        if (isset($_SESSION['UserLog'])) {
            $userid = $_SESSION['UserLog'];
            if (isset($_SESSION['NomLog'])) {
                $nomdemoid = $_SESSION['NomLog'];

                $nominationdemoModel = new NominationdemoModel();
                $data['nomCnt'] = $nominationdemoModel->where('userID', $userid)
                    // ->where("year",date("Y"))
                    ->countAllResults();

                $data['nomDetails'] = $nominationdemoModel->select("nominationdemo.*,S.state,D.district,T.taluk,A.title,AB.awardAreaSubTitle,R.fName,
                            R.mName,R.lName,R.organisationName,R.regType")
                    ->join('state AS S', 'S.stateID = nominationdemo.state', 'LEFT')
                    ->join('district AS D', 'D.districtID = nominationdemo.district', 'LEFT')
                    ->join('thaluk AS T', 'T.talukID = nominationdemo.taluk', 'LEFT')
                    ->join('awardarea AS A', 'A.awardID = nominationdemo.workArea', 'LEFT')
                    ->join('awardareasub AS AB', 'AB.awardAreaSubID = nominationdemo.workAreaSub', 'LEFT')
                    ->join('registration AS R', 'R.id = nominationdemo.userID', 'LEFT')
                    ->where("nomDemoID = '$nomdemoid'")->first();
            }
            $registerModel = new RegisterModel();
            $data['userDetails'] = $registerModel->where('id', $userid)->first();
            //$data['userDetails'] = $Nomination_model->get_user($this->data['nomDetails']->userID);
            $awardModel = new AwardModel(); //create award model
            $data['userAward'] = $awardModel->where('userID', $userid)
                ->where("year", date("Y"))
                ->first();

            echo view('nomination4', $data);
        } else {
            return redirect()->to(base_url() . 'login');
        }

    }

    //step1 edit
    public function step1Edit()
    {
        $session = session();
        $data = $this->getCommon();
        if (isset($_SESSION['UserLog'])) {
            $userid = $_SESSION['UserLog'];
            if (isset($_SESSION['NomLog'])) {
                $nomdemoid = $_SESSION['NomLog'];
            }
            $statemodel = new StateModel(); //create state model
            $data['state'] = $statemodel->where('status', 1)
                ->orderBy('state', "ASC")->findall();
            $districtmodel = new DistrictModel(); //create district model
            $data['district'] = $districtmodel->where('status', 1)
                ->orderBy('district', "ASC")->findall();
            $talukmodel = new ThalukModel(); //create thaluk model
            $data['taluk'] = $talukmodel->where('status', 1)
                ->orderBy('taluk', "ASC")->findall();
            $awardareamodel = new AwardareaModel(); //create Awardarea model
            $data['workArea'] = $awardareamodel->where('status', 1)
                ->orderBy('awardID', "ASC")->findall();
            $awardareasubmodel = new AwardareasubModel(); //create Awardareasub model
            $data['workAreaSub'] = $awardareasubmodel->where('status', 1)
                ->orderBy('awardAreaSubID', "ASC")->findall();
            $data['nomCnt'] = $this->nominationModel->where('userID', $userid)
                ->where("year", date("Y"))
                ->countAllResults();

            $nominationdemoModel = new NominationdemoModel();
            $data['nomEdit'] = $nominationdemoModel->where('nomDemoID', $nomdemoid)->first();
            echo view('nominationStep1Edit', $data);
        } else {
            return redirect()->to(base_url() . 'login');
        }
    }
    //edit step1
    public function saveEdit()
    {

        helper(['form']);
        $session = session();
        $data = $this->getCommon();
        if (isset($_SESSION['UserLog'])) {
            $userid = $_SESSION['UserLog'];
            if (isset($_SESSION['NomLog'])) {
                $nomdemoid = $_SESSION['NomLog'];
            }
            if (isset($_SESSION['nomDoc'])) {
                $nomDoc = $_SESSION['nomDoc'];
            } else {
                $nomDoc = "";
            }

            $rules = [
                'name' => 'required|alpha_space',
                'address' => 'required',
                'email' => 'permit_empty|valid_email',
            ];
            $messages = [
                "name" => [
                    "required" => "പേര് ഉറപ്പായും നൽകുക",
                    "alpha_space" => "പേര് ഫീൽഡിൽ അക്ഷരങ്ങൾ മാത്രം നൽകുക.",
                ],
                "address" => [
                    "required" => "മേൽവിലാസം ഉറപ്പായും നൽകുക",
                ],
                "email" => [
                    "valid_email" => "സാധുവായ ഇമെയിൽ വിലാസം നൽകുക",
                ],
            ];

            if ($this->validate($rules, $messages)) {
                $dob = $this->request->getVar('dob');
                $today = date("Y-m-d");
                $diff = date_diff(date_create($dob), date_create($today));
                $cal_age = $diff->format('%y');
                if (($dob) && ($cal_age <= 0 || $dob > $today || strlen($cal_age) > 3)) {
                    $message = "ഒരു സാധുവായ ജനനത്തീയതി നൽകുക.";
                    $data['success'] = 0;
                    $data['error'] = $message;
                    return $this->response->setJSON($data);
                    exit;
                } else {
                    if ($this->request->getVar('dob') == '') {
                        $dob = '';
                    } else {
                        $dob = $this->request->getVar('dob');
                    }
                    if ($this->request->getVar('age') == '') {
                        $age = 0;
                    } else {

                        $age = sanitize_filename($this->request->getVar('age'));
                    }
                    if ($this->request->getVar('sex') == '') {
                        $sex = 0;
                    } else {
                        $sex = sanitize_filename($this->request->getVar('sex'));
                    }
                    if ($this->request->getVar('district') == '') {
                        $district = 0;
                    } else {
                        $district = sanitize_filename($this->request->getVar('district'));
                    }
                    if ($this->request->getVar('taluk') == '') {
                        $taluk = 0;
                    } else {
                        $taluk = sanitize_filename($this->request->getVar('taluk'));
                    }

                    $data = array(
                        'name' => sanitize_filename($this->request->getVar('name')),
                        'dob' => $dob,
                        'age' => $age,
                        'sex' => $sex,
                        'state' => sanitize_filename($this->request->getVar('state')),
                        'district' => $district,
                        'taluk' => $taluk,
                        'pob' => sanitize_filename($this->request->getVar('pob')),
                        'address' => sanitize_filename($this->request->getVar('address')),
                        'email' => sanitize_filename($this->request->getVar('email')),
                        'country' => sanitize_filename($this->request->getVar('country')),
                        'job' => sanitize_filename($this->request->getVar('job')),
                        'photo' => $nomDoc,
                        'date' => date("Y-m-d")
                    );

                    $nominationdemoModel = new NominationdemoModel();
                    $nominationdemoModel->update($nomdemoid, $data);
                    $data['success'] = 1;
                    return $this->response->setJSON($data);
                    exit;
                }

            } else {
                $data1['validation'] = $this->validator;
                $data['success'] = 0;
                $data['error'] = $data1['validation']->listErrors();
                //$data['error'].="Enter Valid Date of Birth";
                return $this->response->setJSON($data);
                exit;
            }
        } else {
            return redirect()->to(base_url() . 'login');
        }

    }
    //end step1 edit
//start step2
    public function step2Edit()
    {
        helper(['form']);
        $session = session();
        $data = $this->getCommon();
        if (isset($_SESSION['UserLog'])) {
            $userid = $_SESSION['UserLog'];
            if (isset($_SESSION['NomLog'])) {
                $nomdemoid = $_SESSION['NomLog'];
            }
            if (isset($_SESSION['nomDoc'])) {
                $nomDoc = $_SESSION['nomDoc'];
            } else {
                $nomDoc = "";
            }

            $statemodel = new StateModel(); //create state model
            $data['state'] = $statemodel->where('status', 1)
                ->orderBy('state', "ASC")->findall();
            $districtmodel = new DistrictModel(); //create district model
            $data['district'] = $districtmodel->where('status', 1)
                ->orderBy('district', "ASC")->findall();
            $talukmodel = new ThalukModel(); //create thaluk model
            $data['taluk'] = $talukmodel->where('status', 1)
                ->orderBy('taluk', "ASC")->findall();
            $awardareamodel = new AwardareaModel(); //create Awardarea model
            $data['workArea'] = $awardareamodel->where('status', 1)
                ->orderBy('awardID', "ASC")->findall();
            $awardareasubmodel = new AwardareasubModel(); //create Awardareasub model
            $data['workAreaSub'] = $awardareasubmodel->where('status', 1)
                ->orderBy('awardAreaSubID', "ASC")->findall();

            $data['nomCnt'] = $this->nominationModel->where('userID', $userid)
                ->where("year", date("Y"))
                ->countAllResults();
            $awardModel = new AwardModel(); //create award model
            $data['userAward'] = $awardModel->where('userID', $userid)
                ->where("year", date("Y"))
                ->findall();

            $nominationdemoModel = new NominationdemoModel();
            $data['nomEdit'] = $nominationdemoModel->where('nomDemoID', $nomdemoid)
                ->first();
            echo view('nomination2Edit', $data);
        } else {
            return redirect()->to(base_url() . 'login');
        }
    }
    //end step2
//start step3 edit
    public function step3Edit()
    {
        helper(['form']);
        $session = session();
        $data = $this->getCommon();
        if (isset($_SESSION['UserLog'])) {
            $userid = $_SESSION['UserLog'];
            if (isset($_SESSION['NomLog'])) {
                $nomdemoid = $_SESSION['NomLog'];
            }
            if (isset($_SESSION['nomDoc'])) {
                $nomDoc = $_SESSION['nomDoc'];
            } else {
                $nomDoc = "";
            }

            $data['nomCnt'] = $this->nominationModel->where('userID', $userid)
                ->where("year", date("Y"))
                ->countAllResults();
            $nominationdemoModel = new NominationdemoModel();
            $data['nomEdit'] = $nominationdemoModel->where('nomDemoID', $nomdemoid)
                ->first();
            echo view('nomination3Edit', $data);
        } else {
            return redirect()->to(base_url() . 'login');
        }
    }
    //end step3
//start esign check 
    function esigned_check($id)
    {

        $session = session();
        return redirect()->to(base_url() . 'nomination/esigned/' . $id);

    }
    public function esigned($id)
    {
        $session = session();
        if (isset($_SESSION['UserLog'])) {
            $userid = $_SESSION['UserLog'];
            $_SESSION['esignID'] = base64_decode(sanitize_filename($id));

            return redirect()->to(base_url() . 'nomination/esignSuccess#esign');
        } else {
            return redirect()->to(base_url() . 'login');
        }
    }
    function esignSuccess()
    {

        $session = session();
        if (isset($_SESSION['UserLog'])) {
            $userid = $_SESSION['UserLog'];
            if (isset($_SESSION['NomLog'])) {
                $nomdemoid = $_SESSION['NomLog'];
            }
            if (isset($_SESSION['esignID'])) {
                $esignID = $_SESSION['esignID'];
            }

            $data['nomCnt'] = $this->nominationModel->where('userID', $userid)
                ->where("year", date("Y"))
                ->countAllResults();

            $nominationdemoModel = new NominationdemoModel();
            $nomdemoid = sanitize_filename($nomdemoid);
            $data['nomDetails'] = $nominationdemoModel->select("nominationdemo.*,S.state,D.district,T.taluk,A.title,AB.awardAreaSubTitle,R.fName,
                                R.mName,R.lName,R.organisationName,R.regType")
                ->join('state AS S', 'S.stateID = nominationdemo.state', 'LEFT')
                ->join('district AS D', 'D.districtID = nominationdemo.district', 'LEFT')
                ->join('thaluk AS T', 'T.talukID = nominationdemo.taluk', 'LEFT')
                ->join('awardarea AS A', 'A.awardID = nominationdemo.workArea', 'LEFT')
                ->join('awardareasub AS AB', 'AB.awardAreaSubID = nominationdemo.workAreaSub', 'LEFT')
                ->join('registration AS R', 'R.id = nominationdemo.userID', 'LEFT')
                ->where("nomDemoID = '$nomdemoid'")->first();

            $registerModel = new RegisterModel();
            $data['userDetails'] = $registerModel->where('id', $userid);

            $esignmodel = new EsignModel();
            $esign = $esignmodel->where('id', $esignID)->first();

            $data['aadharName'] = $esign['name'];
            echo view('nominationesign', $data);
        } else {
            return redirect()->to(base_url() . 'login');
        }
    }
    function save4()
    {
        $session = session();
        $nomin = array();
        if (isset($_SESSION['UserLog'])) {
            $userid = $_SESSION['UserLog'];
            if (isset($_SESSION['NomLog'])) {
                $nomdemoid = $_SESSION['NomLog'];
            }
            if (isset($_SESSION['esignID'])) {
                $esignID = $_SESSION['esignID'];
            }

            $data['nomCnt_count'] = $this->nominationModel
                ->where("year", date("Y"))
                ->countAllResults();

            $nominationdemoModel = new NominationdemoModel();
            $data['nomini'] = $nominationdemoModel->where('nomDemoID', $nomdemoid)
                ->first();
            $cur_year = date("Y");
            if ($data['nomCnt_count'] == 0) {
                $new_count =  "1";
                if ($data['nomini']['award'] == 1) {
                    $reg = "KJH-10" . $cur_year . "1";
                } elseif ($data['nomini']['award'] == 2) {
                    $reg = "KPR-10" . $cur_year . "1";
                } elseif ($data['nomini']['award'] == 3) {
                    $reg = "KSR-10" . $cur_year . "1"; 
                }
            } else {
                $last_count = $this->nominationModel
                    ->where("year", date("Y"))
                    ->orderby("nominationID", "Desc")
                    ->first();
                    $new_count = $last_count['countID'] + 1;
                    if ($data['nomini']['award'] == 1) {
                        $reg = "KJH-10" . $cur_year . $new_count;
                    } elseif ($data['nomini']['award'] == 2) {
                        $reg = "KPR-10" . $cur_year . $new_count;
                    } elseif ($data['nomini']['award'] == 3) {
                        $reg =  "KSR-10" . $cur_year . $new_count;
                    }
            }
            //final submit to nomination table 
            $this->nominationModel->save([
                'nominationRegID' => $reg,
                'userID' => $userid,
                'name' => $data['nomini']['name'],
                'dob' => $data['nomini']['dob'],
                'age' => $data['nomini']['age'],
                'sex' => $data['nomini']['sex'],
                'state' => $data['nomini']['state'],
                'district' => $data['nomini']['district'],
                'taluk' => $data['nomini']['taluk'],
                'pob' => $data['nomini']['pob'],
                'address' => $data['nomini']['address'],
                'email' => $data['nomini']['email'],
                'country' => $data['nomini']['country'],
                'job' => $data['nomini']['job'],
                'photo' => $data['nomini']['photo'],
                'workArea' => $data['nomini']['workArea'],
                'workAreaSub' => $data['nomini']['workAreaSub'],
                'award' => $data['nomini']['award'],
                'pyn' => $data['nomini']['pyn'],
                'pAward' => $data['nomini']['pAward'],
                'pYear' => $data['nomini']['pYear'],
                'note' => $data['nomini']['note'],
                'donation' => $data['nomini']['donation'],
                'achivement' => $data['nomini']['achivement'],
                'document' => $data['nomini']['document'],
                'wDonation' => $data['nomini']['wDonation'],
                'sDonation' => $data['nomini']['sDonation'],
                'signature' => $data['nomini']['signature'],
                'countID' => $new_count,
                'date' => date("Y-m-d"),
                'year' => date("Y"),
            ]);
            $lastID = $this->nominationModel->InsertID();

            if (isset($_SESSION['RefLog'])) {
                $session->remove('RefLog');
            }
            $ses_data = ['RefLog' => $lastID];
            $session->set($ses_data);
            //update award table
            if ($data['nomini']['award'] == 1) {
                $res = array(
                    'keralajhothi' => 1,
                );
            } elseif ($data['nomini']['award'] == 2) {
                $res = array(
                    'keralaprabha' => 1,
                );
            } elseif ($data['nomini']['award'] == 3) {
                $res = array(
                    'keralashri' => 1,
                );
            }

            $awardModel = new AwardModel(); //create award model	
            $awardModel->where('userID', $userid)
                ->where("year", date("Y"))
                ->set($res)->update();

            $esignmodel = new EsignModel();
            //update esign table 
            $data = array(
                'nominationID' => $lastID,
            );
            $esignmodel->where('id', $esignID)
                ->set($data)->update();

            //update nomination demo table 
            $nominationdemoModel = new NominationdemoModel();
            $data = array(
                'step' => 5,
            );
            $nominationdemoModel->where('nomDemoID', $nomdemoid)
                ->set($data)->update();

            return redirect()->to(base_url() . 'nomination5');
        } else {
            return redirect()->to(base_url() . 'login');
        }

    }
    //final submit    
    public function step5()
    {
        $session = session();
        if (isset($_SESSION['UserLog'])) {
            $data = $this->getCommon();
            $userid = $_SESSION['UserLog'];
            if (isset($_SESSION['RefLog'])) {
                $RefLog = $_SESSION['RefLog'];
                $data['ref'] = $this->nominationModel->where('nominationID', $RefLog)->first();
                $data['nomCnt'] = $this->nominationModel
                    ->where('userID', $userid)
                    ->where("year", date("Y"))
                    ->countAllResults();
                //send sms on successfull registration
                $registerModel = new RegisterModel();
                $data['userDetails'] = $registerModel->where('id', $userid)->first();
                if (isset($data['ref'])) {
                    $refid = $data['ref']['nominationRegID'];
                }
                if (isset($data['userDetails'])) {
                    $uname = " " . $data['userDetails']['fName'];
                    $mobile = $data['userDetails']['mobileNumber'];
                }
                $message = "KLMGOV-Dear" . $uname . ", Your Nomination has been successfully completed. Ref.No:" . $refid;
                $str = 'username=eofficeksitm&password=eofcalerts&sender=KLMGov&numbers=' . $mobile . '&message=' . $message;
                $ch = curl_init();
                $url = 'http://api.esms.kerala.gov.in/fastclient/SMSclient.php?' . $str;

                curl_setopt($ch, CURLOPT_URL, $url) or die("error1");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //return as a variable
                $result = curl_exec($ch);
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);
                //end sms

                echo view('nomination5', $data);
            }
        } else {
            return redirect()->to(base_url() . 'login');
        }
    }
    //nomination details
    public function details($id)
    {
        $session = session();
        if (isset($_SESSION['UserLog'])) {
            $data = $this->getCommon();
            $userid = $_SESSION['UserLog'];
            $detailid = base64_decode(sanitize_filename($id));
            $data['userDetails'] = $this->nominationModel
                ->select("nominations.*,S.state,D.district,T.taluk,A.title,AB.awardAreaSubTitle,R.fName,
                            R.mName,R.lName,R.organisationName,R.regType")
                ->join('state AS S', 'S.stateID = nominations.state', 'LEFT')
                ->join('district AS D', 'D.districtID = nominations.district', 'LEFT')
                ->join('thaluk AS T', 'T.talukID = nominations.taluk', 'LEFT')
                ->join('awardarea AS A', 'A.awardID = nominations.workArea', 'LEFT')
                ->join('awardareasub AS AB', 'AB.awardAreaSubID = nominations.workAreaSub', 'LEFT')
                ->join('registration AS R', 'R.id = nominations.userID', 'LEFT')
                ->where("nominationID = '$detailid'")->first();

            $esignmodel = new EsignModel();
            $data['esign'] = $esignmodel->where('nominationID', $detailid)->first();

            echo view('myAccountDetails', $data);
        } else {
            return redirect()->to(base_url() . 'login');
        }
    }

    public function esignCancel()
    {
        $session = session();
        $data = $this->getCommon();
        echo view('esignCancel', $data);
    }

    //get nomination period
    public function getNominationPeriod()
    {
        $checkDate = date("Y-m-d");
        $checkYear = date("Y");
        //$d = date('Y-m-d', strtotime($dt));
        $nominationperiodModel = new NominationPeriodModel();
        $nomperiod = $nominationperiodModel->where('status', 1)
            ->where('start_date <=', $checkDate)
            ->where('end_date >=', $checkDate)
            ->where('year =', $checkYear)->countAllResults();
        //echo $nominationperiodModel->db->getLastQuery();
        return $nomperiod;
    }
    public function downloadReceipt($id)
    {
        $userid = 0;
        $session = session();
        $data = $this->getCommon();
        if (isset($_SESSION['UserLog'])) {
            $userid = $_SESSION['UserLog'];
            $nominationRegID = base64_decode(sanitize_filename($id));
            $data['ref'] = $this->nominationModel->
                select("nominations.*,A.title,AB.awardAreaSubTitle,R.fName,
                            R.mName,R.lName,R.organisationName,R.regType,E.name as esignname")
                ->join('awardarea AS A', 'A.awardID = nominations.workArea', 'LEFT')
                ->join('awardareasub AS AB', 'AB.awardAreaSubID = nominations.workAreaSub', 'LEFT')
                ->join('registration AS R', 'R.id = nominations.userID', 'LEFT')
                ->join('esign AS E', 'E.nominationid = nominations.nominationID', 'LEFT')
                ->where('nominationRegID', $nominationRegID)
                ->first();

            echo view('generateReceipt', $data);
        } else {
            return redirect()->to(base_url() . 'login');
        }

    }
    function review()
    {
        $nomid = $_POST['nomid'];
        $reviewmodel = new ReviewModel(); //create Nomination model
        $reviewdata = $reviewmodel->where('nominationID', $nomid)->first();
        echo $reviewdata['review'];
        exit;
    }

    //start site settings
    public function getCommon()
    {
        $sitesettngs = new SiteSettings(); // create an instance of Library
        $res = $sitesettngs->get_site_settings(); // calling method
        $data = array();
        foreach ($res as $row) {
            $data1[$row->config_key] = $row->config_value;
        }
        $data['Titletag'] = $data1['title_tags'];
        $data['metadescription'] = $data1['meta_description'];
        $data['metakeywords'] = $data1['meta_tags'];
        $data['site_name'] = $data1['site_name'];
        $data['pagetitle'] = 'Nomination';
        $data['copyright_year'] = $data1['copyright_year'];
        return $data;
    }
} //end class
