<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Libraries\SiteSettings; // Import library
use App\Models\AboutModel;
use App\Models\GalleryCategoryModel;

class About extends BaseController
{
    public function index()
    {
        $data	=	array();  
        $sitesettngs = new SiteSettings(); // create an instance of Library
        $res =$sitesettngs->get_site_settings(); // calling method
        foreach($res as $row){
            $data1[$row->config_key] = $row->config_value;
         }
         $data['Titletag'] = $data1['title_tags'];
         $data['site_name'] = $data1['site_name'];
         $data['copyright_year'] = $data1['copyright_year'];
         $data['pagetitle'] = 'പുരസ്കാരങ്ങളെക്കുറിച്ച്';

        $aboutModel = new AboutModel();//create about model
        $data['aboutus'] = $aboutModel->where('id', 1)->First();
        
        $galCategoryModel = new GalleryCategoryModel();//create gallery category model
        $data['gal_menu'] = $galCategoryModel->where('status', 1)->findAll();
       
        echo view('about',$data);
    }
    
   
}
