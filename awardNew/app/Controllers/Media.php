<?php

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Libraries\SiteSettings; // Import library
use App\Models\MediaModel;
use App\Models\GalleryCategoryModel;

class Media extends BaseController
{
    public function index()
    {
       	$sitesettngs = new SiteSettings(); // create an instance of Library
        $res =$sitesettngs->get_site_settings(); // calling method
		
        foreach($res as $row){
            $data1[$row->config_key] = $row->config_value;
         }

        $data	=	array(); 
		$data['Titletag'] = $data1['title_tags'];
		$data['metadescription'] = $data1['meta_description'];
		$data['metakeywords'] = $data1['meta_tags'];
		$data['pagetitle'] = 'ദൃശ്യമാധ്യമങ്ങൾ';
        $data['site_name'] = $data1['site_name'];
        $data['copyright_year'] = $data1['copyright_year'];

		$mediaModel = new MediaModel();//create media model
        $data['media'] = $mediaModel->where('status', 1)->FindAll();
		
        $galCategoryModel = new GalleryCategoryModel();//create gallery category model
        $data['gal_menu'] = $galCategoryModel->where('status', 1)->findAll();
       
		 //print_r($data);//exit;
		echo view('media',$data);
    }
}
