<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Libraries\SiteSettings; // Import library
use App\Models\LoginModel;
use App\Models\NominationdemoModel;
use App\Models\NominationsModel;
use App\Models\NominationPeriodModel;

class Login extends BaseController
{
public function index()
{
    helper (['form']); 
    $session = session();
    $sitesettngs = new SiteSettings(); // create an instance of Library
    $res =$sitesettngs->get_site_settings(); // calling method
    $data	=	array();
    foreach($res as $row){
        $data1[$row->config_key] = $row->config_value;
        }
        $data['Titletag'] = $data1['title_tags'];
        $data['metadescription'] = $data1['meta_description'];
        $data['metakeywords'] = $data1['meta_tags'];
        $data['site_name'] = $data1['site_name'];
        $data['siteKey'] = $data1['siteKey'];
        $data['copyright_year'] = $data1['copyright_year']; 
     //get the time period of nomination
    $data['nomperiod'] =  $this->getNominationPeriod();  
   
    if ((isset($_SESSION['UserLog']))&& ($_SESSION['UserLog']!=''))
    {
        $session->destroy();
    }   
    
    echo view('login',$data);
}

//check login
public  function loginCheck()
{
    helper(['form']);
    helper(['security']);
    $encrypter = \Config\Services::encrypter();
    $session = session();
    $sitesettngs = new SiteSettings(); // create an instance of Library
    $res =$sitesettngs->get_site_settings(); // calling method
    $data	=	array();
    foreach($res as $row){
        $data1[$row->config_key] = $row->config_value;
        }
    $data['secretKey'] =  $data1['secretKey'];
    $data['Titletag'] = $data1['title_tags'];
    $data['metadescription'] = $data1['meta_description'];
    $data['metakeywords'] = $data1['meta_tags'];
    $data['site_name'] = $data1['site_name'];
    $data['siteKey'] = $data1['siteKey'];
    $data['copyright_year'] = $data1['copyright_year']; 
    $username="";
    
    $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
   /*  if($captcha_response != '')
    { 
        $keySecret = $data['secretKey'];
        $check = array(
            'secret'		=>	$keySecret,
            'response'		=>	$this->request->getVar('g-recaptcha-response')
        );  
        $startProcess = curl_init();
        curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($startProcess, CURLOPT_POST, true);
        curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
        curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
        $receiveData = curl_exec($startProcess);
        $finalResponse = json_decode($receiveData, true);*/
        
        /* if($finalResponse['success'])
        {  */
            $rules = [
                'username'      => 'required',
                'password'      => 'required',
            ];
            $messages = [
                "username" => [
                    "required" => "User Name is required",
                ],
                "password" => [
                    "required" => "Password is required",
                ],
            ];  
                
            //print_r($this->validate($rules, $messages));exit;       
            if($this->validate($rules, $messages)){
                $lgnModel = new LoginModel();
                $username = sanitize_filename($this->request->getVar('username'));
                $password = $this->request->getVar('password');
                $data = $lgnModel->where('username', $username)->first();
                if($data){
                    $pass = $data['password'];
                    $authenticatePassword = password_verify($password, $pass);
                    if($authenticatePassword){
                        if($data['regType']==1){
                            $username=$data['fName'];
                        }else{
                            $username=$data['organisationName'];
                        }
                        $ses_data = [
                            'UserLog' => $data['id'],
                            'UserName' => $username,
                            'regType' => $data['regType'],
                            'displayName' => $username,
                            'last_activity' => time(),
                            'isUserLoggedIn' => TRUE
                        ];
                        $session->set($ses_data);
                        $id=$data['id'];
                     
                $nominationdemoModel = new NominationdemoModel();
                $stepstatus = $nominationdemoModel->select("step,nomDemoID")
                                        ->where("userID",$id)
                                        ->where("step <>",5)
                                        ->orderBy('nomDemoID',"DESC")->first();

                //check if user added nominations and not completed                       
                if (isset($_SESSION['RefLog'])) { $RefLog = $_SESSION['RefLog'];  }                      
                $nominationModel = new NominationsModel();
                $nomdata = $nominationModel
                                    ->select("nominationID")
                                    ->where("userID ",$id)
                                    ->orderBy('nominationID',"DESC")->countAllResults();   
                                    //echo $nominationModel->db->getLastQuery();exit;                     
               // print_r($nomdata); exit;
                if (isset($_SESSION['NomLog'])) { $session->remove('NomLog'); }
                //$_SESSION['NomLog']= $stepstatus['nomDemoID'];
                
                if ($nomdata==3)
                {
                    
                    return redirect()->to('nomination/myAccount');
                }else{
                    
                    if (isset($stepstatus) && $stepstatus['nomDemoID']!='')
                    {
                        $_SESSION['NomLog']=$stepstatus['nomDemoID'];
                        if($stepstatus['step']==1)
                        {
                            return redirect()->to('nomination2');
                        }
                        else if($stepstatus['step']==2)
                        {
                            return redirect()->to('nomination3');
                        }
                        else if($stepstatus['step']==3)
                        {
                            return redirect()->to('nomination4');
                        }
                    }
                    else{
                        return redirect()->to('nomination');
                        }

                }
                    }
                    else{
                        
                        $session->setFlashdata('msg', 'Password is incorrect.');
                        return redirect()->to('login');
                    }
            }else{ 
                $session->setFlashdata('msg', 'User Name not exist.');
                return redirect()->to('login');
            }
            }else{   
                $session->setFlashdata('msg', 'User Name Required.');
                // echo view('login',$data);
                return redirect()->back()->withInput();
                
            }  
      /*  }else{
            $session->setFlashdata('msg', 'Login Failed..');
            return redirect()->to('login');  
        }
    }
     else{
        $session->setFlashdata('msg', 'Please check CAPTCHA');
        return redirect()->to('login');
    }  */
    
}//end function
 //get nomination period
 public function getNominationPeriod()
 {
     $checkDate   = date("Y-m-d");
     $checkYear   = date("Y");
     //$d = date('Y-m-d', strtotime($dt));
     $nominationperiodModel = new NominationPeriodModel();
     $nomperiod =  $nominationperiodModel->where('status', 1)
                                                 ->where('start_date <=', $checkDate)
                                                 ->where('end_date >=', $checkDate)
                                                 ->where('year =', $checkYear)->countAllResults();
     //echo $nominationperiodModel->db->getLastQuery();
     return $nomperiod;  
 }

function logout()
{
    $session = session();
    if ((isset($_SESSION['UserLog']))&& ($_SESSION['UserLog']!=''))
    {
        $session->remove('UserLog');
        $session->remove('UserName');
        $session->remove('regType');
        $session->remove('displayName');
        $session->remove('isUserLoggedIn');
        $session->destroy();
        return redirect()->to(base_url().'login');
    }   
}
}//end class
