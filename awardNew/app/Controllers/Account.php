<?php

namespace App\Controllers;
use App\Controllers\BaseController;
use App\Libraries\SiteSettings; // Import library
use App\Models\RegisterModel;
use App\Models\AdminModel;

class Account extends BaseController
{
    public function index()
    {
        //
        helper(['form']);
    }
    public function changePassword()
    {
        $session = session();
        if ((isset($_SESSION['isUserLoggedIn'])==1) && (isset($_SESSION['UserLog'])!=''))
        {
            $data = $this->getCommon();
            $id = $_SESSION['UserLog'];
            echo view('changepassword',$data);
        }
        else{
            return redirect()->to(base_url().'login');
        }	  
    }
    public function save()
    {
        helper(['form']);
        $session = session();
        $encrypter = \Config\Services::encrypter();
        $email = \Config\Services::email();
        
        if ((isset($_SESSION['isUserLoggedIn'])==1) && (isset($_SESSION['UserLog'])!=''))
        {
            $data = $this->getCommon();
            $id = $_SESSION['UserLog'];

            $rules = [
                'oldpassword'      => 'required',
                'newpassword'      => 'required|min_length[8]|max_length[15]',
                'confirmpassword'  => 'required|min_length[8]|max_length[15]',];
            
            $messages = [
                "oldpassword" => ["required" => "Type Old Password",],
                "newpassword" => ["required" => "Type New Password",],
                "confirmpassword" => ["required" => "Confirm New Password",],
            ]; 

            if($this->validate($rules, $messages))
            {
                $oldpassword = $this->request->getVar('oldpassword');
                $newpassword = $this->request->getVar('newpassword');
                $confirmpassword = $this->request->getVar('confirmpassword');

                $regmodel = new RegisterModel();//create Register model
                $usrdata = $regmodel->where('id', $id)->first();
                if($usrdata['refCode']!=$oldpassword)
                {
                    $session->setFlashdata('msg', 'Old Password is incorrect.');
                    echo view('changepassword',$data);
                }
                elseif( $newpassword !=$confirmpassword)
                {
                    $session->setFlashdata('msg', 'Confirm Password should match with New Password.');
                    echo view('changepassword',$data);
                }
                else{
                    $pass = password_hash($newpassword, PASSWORD_DEFAULT);
                    $regmodel = new RegisterModel();
                    $data = array(
                        'password' => $pass, 
                        'refCode' => $this->request->getVar('newpassword'));
                    $regmodel->update($id,$data);

                    //send mail
                    $adminmodel = new AdminModel();   
                    $data = $adminmodel->where('status', 1)->first();
                    $frommail = $data['email'];
                    $fromname = $data['first_name'];
				 	$to = $usrdata['email'];
                   	$subject ="Password Reset";
					$message = "Your password has been changed <br>";
                    $message.="Please use the new password to login ";   
                    $content ="
					<html>
                        <head>
                        <title></title>
                        </head>
                        <body style='background-color:#EEFFD5'>
                        <table cellpadding='0' cellspacing='0' border='0' width='673'>
                                <tr>
                                <td width='673' bgcolor='#EEFFD5'>
                                    <p></p><br>
                                    <p>Dear User,</p><br>
                                    <p style='font-size:15px; margin:0px 10px 0px 15px; text-align:justify;'>$message</p><br>
                                    </td>
                                </tr>
                                <tr>
                                <td width='673' bgcolor='#EEFFD5'>
                                    <p></p>
                                    <p>Regards,</p>
                                    <p> $fromname</p><br>
                                     </td>
                                </tr>
                            
                        </table>
                        </body>
                        </html>";
                    


                    $email->setFrom($frommail, $fromname);
                    $email->setTo($to);
                    $email->setSubject($subject);
                    $email->setMessage($content);
                    //$email->send();
                    $session->setFlashdata('msg', 'Password Changed.<br>Login with New Password.');
                    return redirect()->to(base_url().'login');
                }

            }
            else{
                $data['validation'] = $this->validator;
                echo view('changepassword',$data);
            }    
        }
        else{
            return redirect()->to(base_url().'login');
        }

    }
    //start site settings
    public function getCommon()
    {
        $sitesettngs = new SiteSettings(); // create an instance of Library
        $res =$sitesettngs->get_site_settings(); // calling method
        $data	=	array();
        foreach($res as $row){
            $data1[$row->config_key] = $row->config_value;
        }
        $data['Titletag'] = $data1['title_tags'];
        $data['metadescription'] = $data1['meta_description'];
        $data['metakeywords'] = $data1['meta_tags'];
        $data['site_name'] = $data1['site_name'];
        $data['pagetitle'] = 'Nomination';
        $data['copyright_year'] = $data1['copyright_year']; 
        return $data;  
    }
   
   
}
