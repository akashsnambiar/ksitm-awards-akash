<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\SettingsModel;
use App\Models\HomeContentModel;
use App\Models\NominationsModel;

class HomeContentController extends BaseController
{
    public function index()
    {
        helper(['form']);
        $session = session();
        $settingsModel = new SettingsModel();
        $setting = $settingsModel->findAll();
        $this->data    =    array();
        foreach ($setting as $row) {
            $this->data[$row['config_key']] = $row['config_value'];
        }
        $secretKey = $this->data['secretKey'];
        $siteKey = $this->data['siteKey'];
        $admin_email = $this->data['admin_email'];
        $site_name = $this->data['site_name'];
        $title_tags = $this->data['title_tags'];
        $copyright_year = $this->data['copyright_year'];
        $meta_tags = $this->data['meta_tags'];
        $meta_description = $this->data['meta_description'];
        $nominationModel = new NominationsModel();
        $nominationNew = $nominationModel->where('status', 0)
            ->countAllResults();
        $nominationNewKeralajyothi = $nominationModel->where('status', 0)
            ->where('award', 1)
            ->countAllResults();
        $nominationNewKeralaprabha = $nominationModel->where('status', 0)
            ->where('award', 2)
            ->countAllResults();
        $nominationNewKeralashri = $nominationModel->where('status', 0)
            ->where('award', 3)
            ->countAllResults();
        $nominationAccepted = $nominationModel->where('status', 1)
            ->countAllResults();
        $nominationAcceptedKeralajyothi = $nominationModel->where('status', 1)
            ->where('award', 1)
            ->countAllResults();
        $nominationAcceptedKeralaprabha = $nominationModel->where('status', 1)
            ->where('award', 2)
            ->countAllResults();
        $nominationAcceptedKeralashri = $nominationModel->where('status', 1)
            ->where('award', 3)
            ->countAllResults();
        $nominationRejected = $nominationModel->where('status', 2)
            ->countAllResults();
        $nominationRejectedKeralajyothi = $nominationModel->where('status', 2)
            ->where('award', 1)
            ->countAllResults();
        $nominationRejectedKeralaprabha = $nominationModel->where('status', 2)
            ->where('award', 2)
            ->countAllResults();
        $nominationRejectedKeralashri = $nominationModel->where('status', 2)
            ->where('award', 3)
            ->countAllResults();





        $homecntmodel = new HomeContentModel();
        $homedata['homecontent'] = $homecntmodel->findAll();
        echo view('admin/home/homecnt', [
            "siteName" => $site_name,
            "siteKey" => $siteKey,
            "homecontent" => $homedata['homecontent']
        ]);

        echo view('admin/includes/footer', [
            "copyrightYear" => $copyright_year,
            "nominationNewKeralajyothi" => $nominationNewKeralajyothi,
            "nominationNewKeralaprabha" => $nominationNewKeralaprabha,
            "nominationNewKeralashri" => $nominationNewKeralashri,
            "nominationAcceptedKeralajyothi" => $nominationAcceptedKeralajyothi,
            "nominationAcceptedKeralaprabha" => $nominationAcceptedKeralaprabha,
            "nominationAcceptedKeralashri" => $nominationAcceptedKeralashri,
            "nominationRejectedKeralajyothi" => $nominationRejectedKeralajyothi,
            "nominationRejectedKeralaprabha" => $nominationRejectedKeralaprabha,
            "nominationRejectedKeralashri" => $nominationRejectedKeralashri,
        ]);
    }
    public function homecontentsubmit()
    {

        helper(['form']);
        $session = session();
        $settingsModel = new SettingsModel();
        $setting = $settingsModel->findAll();
        $this->data    =    array();
        foreach ($setting as $row) {
            $this->data[$row['config_key']] = $row['config_value'];
        }
        $secretKey = $this->data['secretKey'];
        $siteKey = $this->data['siteKey'];
        $admin_email = $this->data['admin_email'];
        $site_name = $this->data['site_name'];
        $title_tags = $this->data['title_tags'];
        $copyright_year = $this->data['copyright_year'];
        $meta_tags = $this->data['meta_tags'];
        $meta_description = $this->data['meta_description'];
        $nominationModel = new NominationsModel();
        $nominationNew = $nominationModel->where('status', 0)
            ->countAllResults();
        $nominationNewKeralajyothi = $nominationModel->where('status', 0)
            ->where('award', 1)
            ->countAllResults();
        $nominationNewKeralaprabha = $nominationModel->where('status', 0)
            ->where('award', 2)
            ->countAllResults();
        $nominationNewKeralashri = $nominationModel->where('status', 0)
            ->where('award', 3)
            ->countAllResults();
        $nominationAccepted = $nominationModel->where('status', 1)
            ->countAllResults();
        $nominationAcceptedKeralajyothi = $nominationModel->where('status', 1)
            ->where('award', 1)
            ->countAllResults();
        $nominationAcceptedKeralaprabha = $nominationModel->where('status', 1)
            ->where('award', 2)
            ->countAllResults();
        $nominationAcceptedKeralashri = $nominationModel->where('status', 1)
            ->where('award', 3)
            ->countAllResults();
        $nominationRejected = $nominationModel->where('status', 2)
            ->countAllResults();
        $nominationRejectedKeralajyothi = $nominationModel->where('status', 2)
            ->where('award', 1)
            ->countAllResults();
        $nominationRejectedKeralaprabha = $nominationModel->where('status', 2)
            ->where('award', 2)
            ->countAllResults();
        $nominationRejectedKeralashri = $nominationModel->where('status', 2)
            ->where('award', 3)
            ->countAllResults();


        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
        if ($captcha_response != '') {
            $keySecret = $secretKey;

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );

            $startProcess = curl_init();
            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            $receiveData = curl_exec($startProcess);
            $finalResponse = json_decode($receiveData, true);

            if ($finalResponse['success']) {
                $rules = [
                    'title' => 'required',
                    'title_e' => 'required',
                    'text' => 'required',
                    'text_e' => 'required'
                ];

                if ($this->validate($rules)) {
                    // $homecntmodel = array();
                    $homecntmodel = new HomeContentModel();
                    $id = 1;
                    $data = [
                        'title' => $this->request->getVar('home_title'),
                        'title_e'  => $this->request->getVar('home_title_e'),
                        'text'  => $this->request->getVar('text'),
                        'text_e'  => $this->request->getVar('text_e'),
                    ];
                    $homecntmodel->update($id, $data);
                    //print_r($homecntmodel);exit;
                    //$homedata['homecontent'] = $homecntmodel->findAll();
                    return redirect()->to('admin/home/homecnt');
                   


                    echo view('admin/includes/footer', [
                        "copyrightYear" => $copyright_year,
                        "nominationNewKeralajyothi" => $nominationNewKeralajyothi,
                        "nominationNewKeralaprabha" => $nominationNewKeralaprabha,
                        "nominationNewKeralashri" => $nominationNewKeralashri,
                        "nominationAcceptedKeralajyothi" => $nominationAcceptedKeralajyothi,
                        "nominationAcceptedKeralaprabha" => $nominationAcceptedKeralaprabha,
                        "nominationAcceptedKeralashri" => $nominationAcceptedKeralashri,
                        "nominationRejectedKeralajyothi" => $nominationRejectedKeralajyothi,
                        "nominationRejectedKeralaprabha" => $nominationRejectedKeralaprabha,
                        "nominationRejectedKeralashri" => $nominationRejectedKeralashri,
                    ]);
                } else {
                    $data['validation'] = $this->validator;
                    $homecntmodel = new HomeContentModel();
                    $homedata['homecontent'] = $homecntmodel->findAll();
                    echo view('admin/home/homecnt', [
                        "siteName" => $site_name,
                        "siteKey" => $siteKey,
                        "homecontent" => $homedata['homecontent'],
                        "validation" => $data
                    ]);
                   
                }
            }
        }
    }
}
