<?php

namespace App\Controllers\HigherEnd;

use App\Controllers\BaseController;
use App\Models\SettingsModel;
use App\Models\BannerModel;
use App\Models\WinnerModel;
use App\Models\NominationCategoryModel;
use App\Models\AwardareaModel;

class WinnerController extends BaseController
{
    public function common()
    {
        helper(['form']);
        $session = session();
        $settingsModel = new SettingsModel();
        $setting = $settingsModel->findAll();
        $this->data1   =    array();
        foreach ($setting as $row) {
            $this->data1[$row['config_key']] = $row['config_value'];
        }
        $data['secretKey'] = $this->data1['secretKey'];
        $data['siteKey'] = $this->data1['siteKey'];
        $data['admin_email'] = $this->data1['admin_email'];
        $data['site_name'] = $this->data1['site_name'];
        $data['title_tags'] = $this->data1['title_tags'];
        $data['copyright_year'] = $this->data1['copyright_year'];
        $data['meta_tags'] = $this->data1['meta_tags'];
        $data['meta_description'] = $this->data1['meta_description'];

        return $data;
    }
    public function index()
    {
        $data = $this->common();
        $bannermodel = new BannerModel();
        // $data['banner'] = $bannermodel->findAll();
        $nomcatmodel = new NominationCategoryModel();
        $data['nomcat'] = $nomcatmodel->findAll();
        $awardmodel = new AwardareaModel();
        $data['award'] = $awardmodel->findAll();
        $data['banner'] = $bannermodel
            ->select('banner.*,n.id as nid, n.nomination_category as nomination_category')
            ->join('nomination_category AS n', 'banner.category_id = n.id')->findAll();
        $winnermodel = new WinnerModel();
        $data['winner'] = $winnermodel
            ->select('winner.*,b.id as bid,b.banner_title_en as banner_title_en,b.banner_title_mal as banner_title_mal,b.year as year,n.id as nid, n.nomination_category as nomination_category, a.awardID, a.title, a.title_e')
            ->join('banner AS b', 'winner.banner_title_id = b.id')
            ->join('awardarea AS a', 'winner.awardarea_id = a.awardID')
            ->join('nomination_category AS n', 'b.category_id = n.id')->findAll();
        //print_r($data);exit;
        echo view('higherEnd/winner', $data);
    }
    public function bannercontent() 
    {
        if (isset($_POST['titleid'])) {
            $titleid = $_POST['titleid'];
            $bannermodel = new BannerModel();
            $data['banner'] = $bannermodel
                ->select('banner.*,n.id as nid, n.nomination_category as nomination_category')
                ->join('nomination_category AS n', 'banner.category_id = n.id')->where('banner.id', $titleid)->first();
                $token = csrf_hash();
                $data['token'] = $token;
            //echo json_encode($data);
            return $this->response->setJSON($data);
        }
    }
    public function addwinner()
    {

        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
        // if ($captcha_response != '') {
        if (isset($_POST['Submit'])) {
            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );

            $startProcess = curl_init();
            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            /* $receiveData = curl_exec($startProcess);
            $finalResponse = json_decode($receiveData, true); */
            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {

                $created_time = $this->request->getVar('year');
                $rules = [
                    'name' => 'required',
                    'name_mal' => 'required',

                ];
                $messages = [
                    "name" => [
                        "required" => "Winner Name in English is required",
                    ],
                    "name_mal" => [
                        "required" => "Winner Name in Malayalam is required",
                    ],

                ];

                if ($this->validate($rules, $messages)) {


                    $winnermodel = new WinnerModel();

                    $img = $this->request->getFile('file');
                    $input = $this->validate([
                        'file' => [
                            'uploaded[file]',
                            'mime_in[file,image/jpg,image/jpeg,image/png]',
                            'max_size[file,2048]',
                        ]
                    ]);
                    if (!$input) {
                        $data['error'] = "Invalid Image, Upload image leass than 2MB in jpeg, jpg or png format";
                        $bannermodel = new BannerModel();
                        $data['banner'] = $bannermodel
                            ->select('banner.*,n.id as nid, n.nomination_category as nomination_category')
                            ->join('nomination_category AS n', 'banner.category_id = n.id')->findAll();
                        $winnermodel = new WinnerModel();
                        $data['winner'] = $winnermodel
                            ->select('winner.*,b.id as bid,b.banner_title_en as banner_title_en,b.banner_title_mal as banner_title_mal,b.year as year,n.id as nid, n.nomination_category as nomination_category')
                            ->join('banner AS b', 'winner.banner_title_id = b.id')
                            ->join('nomination_category AS n', 'b.category_id = n.id')->findAll();
                        echo view('higherEnd/winner', $data);
                    } else {
                        $upload_path = FCPATH . 'public/images/banner';
                        //print_r($upload_path);exit;
                        $image = $img->getName();

                        $res = $img->move($upload_path, $image);

                        $data = [
                            'banner_title_id'   => sanitize_filename($this->request->getVar('title_cat_mal')),
                            'winner_name'       => sanitize_filename($this->request->getVar('name')),
                            'winner_name_mal'   => sanitize_filename($this->request->getVar('name_mal')),
                            'awardarea_id'      => sanitize_filename($this->request->getVar('awardarea')),
                            'description'       => sanitize_filename($this->request->getVar('description')),
                            'description_mal'   => sanitize_filename($this->request->getVar('description_mal')),
                            'win_year'          => sanitize_filename($this->request->getVar('win_year')),
                            'created_time'      => $created_time,
                            'winner_photo'      => sanitize_filename($image),

                        ];
                        // print_r($data);
                        // exit;
                        $winnermodel->save($data);
                        return redirect()->to('higherEnd/winners');
                    }
                } else {
                    $data = $this->common();
                    $bannermodel = new BannerModel();
                    $data['banner'] = $bannermodel->findAll();
                    $awardmodel = new AwardareaModel();
                    $data['award'] = $awardmodel->findAll();
                    $data['validation'] = $this->validator;
                    echo view('higherEnd/winner', $data);
                }
            }
        } else {
            $data = $this->common();
            $awardmodel = new AwardareaModel();
            $data['award'] = $awardmodel->findAll();
            $bannermodel = new BannerModel();
            $data['banner'] = $bannermodel
                ->select('banner.*,n.id as nid, n.nomination_category as nomination_category')
                ->join('nomination_category AS n', 'banner.category_id = n.id')->findAll();
            $winnermodel = new WinnerModel();
            $data['winner'] = $winnermodel
                ->select('winner.*,b.id as bid,b.banner_title_en as banner_title_en,b.banner_title_mal as banner_title_mal,b.year as year,n.id as nid, n.nomination_category as nomination_category, a.awardID, a.title, a.title_e')
                ->join('banner AS b', 'winner.banner_title_id = b.id')
                ->join('awardarea AS a', 'winner.awardarea_id = a.awardID')
                ->join('nomination_category AS n', 'b.category_id = n.id')->findAll();
            // print_r($data['winner']);
            $data['validation'] = $this->validator;
            echo view('higherEnd/winner', $data);
        }
    }
    public function winneredit($eid)
    {
        // print_r('hii');exit;
        $id = decode_url($eid);
        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
        // if ($captcha_response != '') {
        if (isset($_POST['Submit'])) {
            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );

            $startProcess = curl_init();
            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
           /*  $receiveData = curl_exec($startProcess);
            $finalResponse = json_decode($receiveData, true); */
            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {

                $rules = [
                    'name' => 'required',
                    'name_mal' => 'required',

                ];
                $messages = [
                    "name" => [
                        "required" => "Winner Name in English is required",
                    ],
                    "name_mal" => [
                        "required" => "Winner Name in Malayalam is required",
                    ],

                ];

                if ($this->validate($rules, $messages)) {


                    $winnermodel = new WinnerModel();

                    $img = $this->request->getFile('file');
                    $imgname = $img->getname();
                    // print_r($imgname);exit;
                    if ($imgname) {
                        $input = $this->validate([
                            'file' => [
                                'uploaded[file]',
                                'mime_in[file,image/jpg,image/jpeg,image/png]',
                                'max_size[file,2048]',
                            ]
                        ]);
                        if (!$input) {
                            $data['error'] = "Invalid Image, Upload image leass than 2MB in jpeg, jpg or png format";
                            $bannermodel = new BannerModel();
                            $data['banner'] = $bannermodel
                                ->select('banner.*,n.id as nid, n.nomination_category as nomination_category')
                                ->join('nomination_category AS n', 'banner.category_id = n.id')->findAll();
                            $winnermodel = new WinnerModel();
                            $data['winner'] = $winnermodel
                                ->select('winner.*,b.id as bid,b.banner_title_en as banner_title_en,b.banner_title_mal as banner_title_mal,b.year as year,n.id as nid, n.nomination_category as nomination_category')
                                ->join('banner AS b', 'winner.banner_title_id = b.id')
                                ->join('nomination_category AS n', 'b.category_id = n.id')->findAll();
                            echo view('higherEnd/winneredit', $data);
                        } else {
                            $upload_path = FCPATH . 'public/images/banner';
                            // print_r($upload_path);exit;
                            $image = $img->getName();

                            $res = $img->move($upload_path, $image);

                            $data = [
                                'banner_title_id'   => sanitize_filename($this->request->getVar('banner_title_mal')),
                                'winner_name'       => sanitize_filename($this->request->getVar('name')),
                                'winner_name_mal'   => sanitize_filename($this->request->getVar('name_mal')),
                                'awardarea_id'      => sanitize_filename($this->request->getVar('awardarea')),
                                'description'       => sanitize_filename($this->request->getVar('description')),
                                'description_mal'   => sanitize_filename($this->request->getVar('description_mal')),
                                'win_year'          => sanitize_filename($this->request->getVar('win_year')),
                                'winner_photo'      => sanitize_filename($image),

                            ];

                            $winnermodel->update($id, $data);
                            return redirect()->to('higherEnd/winners');
                        }
                    } else {
                        $data = [
                            'banner_title_id'   => sanitize_filename($this->request->getVar('banner_title_mal')),
                            'winner_name'       => sanitize_filename($this->request->getVar('name')),
                            'winner_name_mal'   => sanitize_filename($this->request->getVar('name_mal')),
                            'awardarea_id'      => sanitize_filename($this->request->getVar('awardarea')),
                            'description'       => sanitize_filename($this->request->getVar('description')),
                            'description_mal'   => sanitize_filename($this->request->getVar('description_mal')),
                            'win_year'          => sanitize_filename($this->request->getVar('win_year')),
                        ];

                        // print_r($data); echo "--------"; print_r($id);
                        // exit;
                        $winnermodel->update($id, $data);

                        return redirect()->to('higherEnd/winners');
                    }
                } else {
                    $data = $this->common();
                    $nomcatmodel = new NominationCategoryModel();
                    $data['nomcat'] = $nomcatmodel->findAll();
                    $awardmodel = new AwardareaModel();
                    $data['award'] = $awardmodel->findAll();
                    $bannermodel = new BannerModel();
                    $data['banner'] = $bannermodel
                        ->select('banner.id as bid,banner.id as bid,banner.banner_title_mal as banner_title_mal,banner.banner_title_en as banner_title_en,banner.banner_title_mal as banner_title_mal,banner.year as year,n.id as nid, n.nomination_category as nomination_category')
                        ->join('nomination_category AS n', 'banner.category_id = n.id')->findAll();
                    $data['validation'] = $this->validator;
                    $winnermodel = new WinnerModel();
                    $data['winner'] = $winnermodel
                        ->select('winner.*,b.id as bid,b.banner_title_en as banner_title_en,b.banner_title_mal as banner_title_mal,b.year as year,n.id as nid, n.nomination_category as nomination_category, a.awardID, a.title, a.title_e')
                        ->join('banner AS b', 'winner.banner_title_id = b.id')
                        ->join('awardarea AS a', 'winner.awardarea_id = a.awardID')
                        ->join('nomination_category AS n', 'b.category_id = n.id')->where('winner.id', $id)->first();
                    //print_r($data['winner']);exit;
                    echo view('higherEnd/winneredit', $data);
                }
            }
        } else {
            // print_r($id);exit;
            $data = $this->common();
            $nomcatmodel = new NominationCategoryModel();
            $data['nomcat'] = $nomcatmodel->findAll();
            $awardmodel = new AwardareaModel();
            $data['award'] = $awardmodel->findAll();
            $bannermodel = new BannerModel();
            $data['banner'] = $bannermodel
                ->select('banner.id as bid,banner.banner_title_mal as banner_title_mal,banner.banner_title_en as banner_title_en,banner.banner_title_mal as banner_title_mal,banner.year as year,n.id as nid, n.nomination_category as nomination_category')
                ->join('nomination_category AS n', 'banner.category_id = n.id')->findAll();
            $winnermodel = new WinnerModel();
            $data['winner'] = $winnermodel
                ->select('winner.*,b.id as bid,b.banner_title_en as banner_title_en,b.banner_title_mal as banner_title_mal,b.year as year,n.id as nid, n.nomination_category as nomination_category, a.awardID, a.title, a.title_e')
                ->join('banner AS b', 'winner.banner_title_id = b.id')
                ->join('awardarea AS a', 'winner.awardarea_id = a.awardID')
                ->join('nomination_category AS n', 'b.category_id = n.id')->where('winner.id', $id)->first();
            //print_r($data['winner']);exit;
            echo view('higherEnd/winneredit', $data);
        }
    }
    public function windelete($id)
    {

        $winnermodel = new WinnerModel();
        $data['winner'] = $winnermodel
            ->where('id', $id)
            ->delete();

        return redirect()->to('higherEnd/winners');
    }
}
