<?php

namespace App\Controllers\HigherEnd;

use App\Controllers\BaseController;
use App\Models\SettingsModel;
use App\Models\AboutModel;
use App\Models\NominationsModel;
use PhpParser\Node\Expr\Print_;

class SiteManagementController extends BaseController
{
    public function common()
    {
        helper(['form']);
        $session = session();
        $settingsModel = new SettingsModel();
        $setting = $settingsModel->findAll();
        $this->data1   =    array();
        foreach ($setting as $row) {
            $this->data1[$row['config_key']] = $row['config_value'];
        }
        $data['secretKey'] = $this->data1['secretKey'];
        $data['siteKey'] = $this->data1['siteKey'];
        $data['admin_email'] = $this->data1['admin_email'];
        $data['site_name'] = $this->data1['site_name'];
        $data['title_tags'] = $this->data1['title_tags'];
        $data['copyright_year'] = $this->data1['copyright_year'];
        $data['meta_tags'] = $this->data1['meta_tags'];
        $data['meta_description'] = $this->data1['meta_description'];

        return $data;
    }
    public function index()
    {

        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
        
      //  if ($captcha_response != '') {
        if(isset($_POST['Submit'])){
            
            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );
            // print_r($check);exit;
            $startProcess = curl_init();

            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            // $receiveData = curl_exec($startProcess);
            // $finalResponse = json_decode($receiveData, true);
            //print_r($finalResponse);exit;
            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {

                $aboutusmodel = new AboutModel();
                $id = 2;
                $data = [
                    'title'     => sanitize_filename($this->request->getVar('home_title')),
                    'title_e'   => sanitize_filename($this->request->getVar('home_title_e')),
                    'text'      => $this->request->getVar('text'),
                    'text_e'    => $this->request->getVar('text_e'),
                ];
                $aboutusmodel->update($id, $data);
                //print_r($aboutusmodel);exit;
                return redirect()->to('higherEnd/site/keralashri');
            }
            
        } else {

            $data = $this->common();
            $aboutusmodel = new AboutModel();
            $data['aboutcontent'] = $aboutusmodel->where('id', 2)->findAll();
            //print_r($data);exit;
            echo view('higherEnd/siteManagement/keralasree', $data);
        }
    }
    public function keralajyothi()
    {
        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));

        // if ($captcha_response != '') {
            if(isset($_POST['Submit'])){

            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );
            // print_r($check);exit;
            $startProcess = curl_init();

            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            // $receiveData = curl_exec($startProcess);
            // $finalResponse = json_decode($receiveData, true);
            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {

                $aboutusmodel = new AboutModel();
                $id = 3;
                $data = [
                    'title'     => sanitize_filename($this->request->getVar('home_title')),
                    'title_e'   => sanitize_filename($this->request->getVar('home_title_e')),
                    'text'      => $this->request->getVar('text'),
                    'text_e'    => $this->request->getVar('text_e'),
                ];
                $aboutusmodel->update($id, $data);
                //print_r($aboutusmodel);exit;
                return redirect()->to('higherEnd/site/keralaJyothi');
            }
        } else {

            $data = $this->common();
            $aboutusmodel = new AboutModel();
            $data['aboutcontent'] = $aboutusmodel->where('id', 3)->findAll();
            //print_r($data);exit;
            echo view('higherEnd/siteManagement/keralajyothi', $data);
        }
    }
    public function keralaprabha()
    {
        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));

        // if ($captcha_response != '') {
            if(isset($_POST['Submit'])){

            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );
            // print_r($check);exit;
            $startProcess = curl_init();

            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            // $receiveData = curl_exec($startProcess);
            // $finalResponse = json_decode($receiveData, true);
            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {

                $aboutusmodel = new AboutModel();
                $id = 4;
                $data = [
                    'title'     => sanitize_filename($this->request->getVar('home_title')),
                    'title_e'   => sanitize_filename($this->request->getVar('home_title_e')),
                    'text'      => $this->request->getVar('text'),
                    'text_e'    => $this->request->getVar('text_e'),
                ];
                $aboutusmodel->update($id, $data);
                //print_r($aboutusmodel);exit;
                return redirect()->to('higherEnd/site/keralaPrabha');
            }
        } else {

            $data = $this->common();
            $aboutusmodel = new AboutModel();
            $data['aboutcontent'] = $aboutusmodel->where('id', 4)->findAll();
            //print_r($data);exit;
            echo view('higherEnd/siteManagement/keralaprabha', $data);
        }
    }
}