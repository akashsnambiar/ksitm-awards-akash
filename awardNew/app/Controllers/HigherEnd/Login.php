<?php

namespace App\Controllers\HigherEnd;

use App\Controllers\BaseController;
use App\Models\AdminModel;
use App\Models\SettingsModel;

class Login extends BaseController
{
    public function index()
    {
        
        helper(['form']);
        $session = session();
        
        $settingsModel = new SettingsModel();
        $setting = $settingsModel->findAll();
        $data1   =    array();
        foreach ($setting as $row) {
            $data1[$row['config_key']] = $row['config_value'];
        }
        $data['secretKey'] = $data1['secretKey'];
        $data['siteKey'] = $data1['siteKey'];
        $data['admin_email'] = $data1['admin_email'];
        $data['site_name'] = $data1['site_name'];
        $data['title_tags'] = $data1['title_tags'];
        $data['copyright_year'] = $data1['copyright_year'];
        $data['meta_tags'] = $data1['meta_tags'];
        $data['meta_description'] = $data1['meta_description'];
        if ((isset($_SESSION['AdminID'])) && ($_SESSION['AdminID'] != '')) {
            $session->destroy();
        }
        echo view('higherEnd/includes/header',$data);
        echo view('higherEnd/login',$data);
        echo view('higherEnd/includes/footerLogin',$data);

    }

    public function loginAuth()
    {
        $session = session();
        
        if ((isset($_SESSION['AdminID'])) && ($_SESSION['AdminID'] != '')) {
            $session->destroy();
        }
        $settingsModel = new SettingsModel();
        $setting = $settingsModel->findAll();
        $data1   =    array();
        foreach ($setting as $row) {
            $data1[$row['config_key']] = $row['config_value'];
        }
        $data['secretKey'] = $data1['secretKey'];
        $data['siteKey'] = $data1['siteKey'];
        $data['admin_email'] = $data1['admin_email'];
        $data['site_name'] = $data1['site_name'];
        $data['title_tags'] = $data1['title_tags'];
        $data['copyright_year'] = $data1['copyright_year'];
        $data['meta_tags'] = $data1['meta_tags'];
        $data['meta_description'] = $data1['meta_description'];
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));

        // if ($captcha_response != '') {
        $keySecret = $data['secretKey'];

        $check = array(
            'secret'        =>    $keySecret,
            'response'        =>    $this->request->getVar('g-recaptcha-response')
        );

        $startProcess = curl_init();

        curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");

        curl_setopt($startProcess, CURLOPT_POST, true);

        curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));

        curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);

        // $receiveData = curl_exec($startProcess);
        // print_r($receiveData);exit;
        //$finalResponse = json_decode($receiveData, true);
        // print_r($finalResponse);exit;
        $finalResponse['success'] = 1;
        if ($finalResponse['success']) {
            $rules = [
                'username'      => 'required',
                'password'      => 'required',
            ];
            $messages = [
                "username" => [
                    "required" => "User Name is required",
                ],
                "password" => [
                    "required" => "Password is required",
                ],
            ];
            if ($this->validate($rules, $messages)) {
                // print_r('hii');
                // exit;
                $adminModel = new AdminModel();
                $username = $this->request->getVar('username');
                $password = $this->request->getVar('password');

                $data = $adminModel->where('username', $username)->first();

                if ($data) {

                    $pass = $data['password'];
                    //print_r($pass); echo '<br>'; print_r($password); echo '<br>';
                    $authenticatePassword = password_verify($password, $pass);
                    //print_r($authenticatePassword);exit;
                    if ($authenticatePassword) {
                        $ses_data = [
                            'AdminID' => $data['id'],
                            'AdminName' => $data['first_name'] . ' ' . $data['last_name'],
                            'AdminDesignation' => $data['designation'],
                            'AdminRole' => $data['level'],
                            'adminlast_activity' => time(),
                            'isAdminLoggedIn' => TRUE
                        ];
                        $session->set($ses_data);

                        return redirect()->to('/higherEnd/dashboard');
                    } else {
                        $session->setFlashdata('msg', 'Password is incorrect.');
                        return redirect()->to('/higherEnd');
                    }
                } else {
                    $session->setFlashdata('msg', 'User Name not exist.');
                    return redirect()->to('/higherEnd');
                }
            } else {
                echo view('higherEnd/includes/header', $data);
                echo view('higherEnd/login', $data);
                echo view('higherEnd/includes/footerLogin', $data);
            }
            //} 
            // else {
            //     $session->setFlashdata('msg', 'Login Failed..');
            //     return redirect()->to('/higherEnd');
            // }
        } else {
            $session->setFlashdata('msg', 'Please check CAPTCHA');
            return redirect()->to('/higherEnd');
        }
    }
    public function logout()
    {
        $session = session();
        if ((isset($_SESSION['AdminID'])) && ($_SESSION['AdminID'] != '')) {
            $session->remove('AdminID');
            $session->remove('AdminName');
            $session->remove('AdminDesignation');
            $session->remove('AdminRole');
            $session->remove('isAdminLoggedIn');
            $session->destroy();
            return redirect()->to('higherEnd');
        }
    }
}
