<?php

namespace App\Controllers\HigherEnd;

use App\Controllers\BaseController;
use App\Models\SettingsModel;
use App\Models\MediaModel;

class MediaController extends BaseController
{
    public function common()
    {
        helper(['form']);
        $session = session();
        $settingsModel = new SettingsModel();
        $setting = $settingsModel->findAll();
        $this->data1   =    array();
        foreach ($setting as $row) {
            $this->data1[$row['config_key']] = $row['config_value'];
        }
        $data['secretKey'] = $this->data1['secretKey'];
        $data['siteKey'] = $this->data1['siteKey'];
        $data['admin_email'] = $this->data1['admin_email'];
        $data['site_name'] = $this->data1['site_name'];
        $data['title_tags'] = $this->data1['title_tags'];
        $data['copyright_year'] = $this->data1['copyright_year'];
        $data['meta_tags'] = $this->data1['meta_tags'];
        $data['meta_description'] = $this->data1['meta_description'];

        return $data;
    }
    public function index()
    {

        $data = $this->common();
        $mediamodel = new MediaModel();
        $data['media'] = $mediamodel->findAll();
        //print_r($data);exit;
        echo view('higherEnd/media/list', $data);
    }
    public function mediaadd()
    {
        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
        // if ($captcha_response != '') {
            if (isset($_POST['Submit'])) {
            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );

            $startProcess = curl_init();
            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            // $receiveData = curl_exec($startProcess);
            // $finalResponse = json_decode($receiveData, true);

            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {

                $rules = [
                    'title' => 'required',
                    'title_e' => 'required',


                ];
                $messages = [
                    "title" => [
                        "required" => "Malayalam Title is required",
                    ],
                    "title_e" => [
                        "required" => "English Title is required",
                    ],

                ];

                if ($this->validate($rules, $messages)) {
                    $mediamodel = new MediaModel();
                    $data = [
                        'title'     => sanitize_filename($this->request->getVar('title')),
                        'title_e'   => sanitize_filename($this->request->getVar('title_e')),
                        'embed'     => $this->request->getVar('embed'),
                    ];
                    $mediamodel->save($data);

                    return redirect()->to('higherEnd/media');
                }
            }
        } else {
            $data = $this->common();
            $mediamodel = new MediaModel();
            $data['media'] = $mediamodel->findAll();
            $data['validation'] = $this->validator;
            echo view('higherEnd/media/add', $data);
        }
    }
    public function mediastatus($id, $status)
    {
        $mediamodel = new MediaModel();
        $data['user']  = $mediamodel
            ->set('status', $status)
            ->where('id', $id)
            ->update();

        return redirect()->to('higherEnd/media');
    }
    public function mediaedit($eid)
    {
        $id = decode_url($eid);
        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
        // if ($captcha_response != '') {
            if (isset($_POST['Submit'])) {
            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );

            $startProcess = curl_init();
            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            // $receiveData = curl_exec($startProcess);
            // $finalResponse = json_decode($receiveData, true);

            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {

                $rules = [
                    'title' => 'required',
                    'title_e' => 'required',
                ];
                $messages = [
                    "title" => [
                        "required" => "Malayalam Title is required",
                    ],
                    "title_e" => [
                        "required" => "English Title is required",
                    ],
                ];

                if ($this->validate($rules, $messages)) {

                    $mediamodel = new MediaModel();
                    $data = [
                        'title'     => sanitize_filename($this->request->getVar('title')),
                        'title_e'   => sanitize_filename($this->request->getVar('title_e')),
                        'embed'     => $this->request->getVar('embed'),

                    ];
                    $mediamodel->update($id, $data);
                    return redirect()->to('higherEnd/media');
                } else {
                    $data = $this->common();
                    $mediamodel = new MediaModel();
                    $data['media'] = $mediamodel->where('id', $id)->first();
                    $data['validation'] = $this->validator;
                    echo view('higherEnd/media/edit', $data);
                }
            }
        } else {
            $data = $this->common();
            $mediamodel = new MediaModel();
            $data['media'] = $mediamodel->where('id', $id)->first();
            $data['validation'] = $this->validator;
            echo view('higherEnd/media/edit', $data);
        }
    }
    public function mediadelete($id)
    {
        $mediamodel = new MediaModel();
        $data['media']  = $mediamodel
            ->where('id', $id)
            ->delete();

        return redirect()->to('higherEnd/media');
    }
}