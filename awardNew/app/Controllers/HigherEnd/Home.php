<?php

namespace App\Controllers\Admin;
use App\Controllers\BaseController;
use App\Models\SettingsModel;
class Home extends BaseController
{
    public function index()
    {  
        helper(['form']);
        $settingsModel = new SettingsModel();
        $setting = $settingsModel->findAll();
        $this->data	=	array();
        foreach($setting as $row){
            $this->data[$row['config_key']] = $row['config_value'];
        }
        $secretKey = $this->data['secretKey'];
        $siteKey = $this->data['siteKey']; 
        $admin_email = $this->data['admin_email'];
        $site_name = $this->data['site_name'];
        $title_tags = $this->data['title_tags'];
        $copyright_year = $this->data['copyright_year'];
        $meta_tags = $this->data['meta_tags'];
        $meta_description = $this->data['meta_description'];
        $settingsModel = new SettingsModel();
        $setting = $settingsModel->findAll();
        echo view('admin/includes/header',["siteName" => $site_name,
                                           "siteTag" => $title_tags,
                                           "metaTag" => $meta_tags,
                                           "metaDesc" => $meta_description
                                          ]);
        echo view('admin/login', ["siteKey" => $siteKey,
                                  "adminMail" => $admin_email
                                 ]);
        echo view('admin/includes/footerLogin', ["copyrightYear" => $copyright_year]);
    }
    
}
