<?php

namespace App\Controllers\HigherEnd;

use App\Controllers\BaseController;
use App\Models\SettingsModel;
use App\Models\NotificationModel;

class NotificationsController extends BaseController
{
    public function common()
    {
        helper(['form']);
        $session = session();
        $settingsModel = new SettingsModel();
        $setting = $settingsModel->findAll();
        $this->data1   =    array();
        foreach ($setting as $row) {
            $this->data1[$row['config_key']] = $row['config_value'];
        }
        $data['secretKey'] = $this->data1['secretKey'];
        $data['siteKey'] = $this->data1['siteKey'];
        $data['admin_email'] = $this->data1['admin_email'];
        $data['site_name'] = $this->data1['site_name'];
        $data['title_tags'] = $this->data1['title_tags'];
        $data['copyright_year'] = $this->data1['copyright_year'];
        $data['meta_tags'] = $this->data1['meta_tags'];
        $data['meta_description'] = $this->data1['meta_description'];

        return $data;
    }
    public function index()
    {

        $data = $this->common();
        $notmodel = new NotificationModel();
        $data['notification'] = $notmodel->findAll();
        //print_r($data);exit;
        echo view('higherEnd/notification/list', $data);
    }
    public function notifiadd()
    {
        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
        // if ($captcha_response != '') {
        if (isset($_POST['Submit'])) {
            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );

            $startProcess = curl_init();
            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            // $receiveData = curl_exec($startProcess);
            // $finalResponse = json_decode($receiveData, true);

            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {

                $rules = [
                    'title' => 'required',
                    'title_e' => 'required',
                    'not_type' => 'required'

                ];
                $messages = [
                    "title" => [
                        "required" => "Malayalam Title is required",
                    ],
                    "title_e" => [
                        "required" => "English Title is required",
                    ],
                    "not_type" => [
                        "required" => "Type of Notification is required",
                    ],
                ];

                if ($this->validate($rules, $messages)) {


                    $notifimodel = new NotificationModel();

                    $img = $this->request->getFile('file');

                    $input = $this->validate([
                        'file' => [
                            'uploaded[file]',
                            'mime_in[file,application/pdf]',
                            'max_size[file,10000]', //->10MB
                        ]
                    ]);

                    if (!$input) {
                        $data['error'] = "Invalid Document, Upload pdf less than 10MB";
                        $notifimodel = new NotificationModel();
                        $data['notification'] = $notifimodel->findAll();
                        echo view('higherEnd/notification/add', $data);
                    } else {
                        $upload_path = FCPATH . 'public/notifications';
                        $image = $img->getName();

                        $res = $img->move($upload_path, $image);

                        $data = [
                            'not_title'     => sanitize_filename($this->request->getVar('title')),
                            'not_title_e'   => sanitize_filename($this->request->getVar('title_e')),
                            'not_type'      => sanitize_filename($this->request->getVar('not_type')),
                            'year'          => sanitize_filename($this->request->getVar('year')),
                            'not_pdf'       => sanitize_filename($image),
                        ];
                        
                        $notifimodel->save($data);

                        return redirect()->to('higherEnd/notification');
                    }
                } else {

                    $data = $this->common();
                    $notifimodel = new NotificationModel();
                    $data['notification'] = $notifimodel->findAll();
                    $data['validation'] = $this->validator;
                    echo view('higherEnd/notification/add', $data);
                }
            }
        } else {
            $data = $this->common();
            $notifimodel = new NotificationModel();
            $data['notification'] = $notifimodel->findAll();
            $data['validation'] = $this->validator;
            echo view('higherEnd/notification/add', $data);
        }
    }
    public function notifistatus($id, $status)
    {
        $notifimodel = new NotificationModel();
        $data['user']  = $notifimodel
            ->set('status', $status)
            ->where('id', $id)
            ->update();

        return redirect()->to('higherEnd/notification');
    }
    public function notifiedit($eid)
    {
        $id = decode_url($eid);
        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
        // if ($captcha_response != '') {
        if (isset($_POST['Submit'])) {
            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );

            $startProcess = curl_init();
            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            // $receiveData = curl_exec($startProcess);
            // $finalResponse = json_decode($receiveData, true);

            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {

                $rules = [
                    'title' => 'required',
                    'title_e' => 'required',
                    'not_type' => 'required'

                ];
                $messages = [
                    "title" => [
                        "required" => "Malayalam Title is required",
                    ],
                    "title_e" => [
                        "required" => "English Title is required",
                    ],
                    "not_type" => [
                        "required" => "Type of Notification is required",
                    ],
                ];

                if ($this->validate($rules, $messages)) {

                    $notifimodel = new NotificationModel();
                    $img = $this->request->getFile('file');
                    $imgname = $img->getname();

                    if ($imgname) {
                        $input = $this->validate([
                            'file' => [
                                'uploaded[file]',
                                'mime_in[file,application/pdf]',
                                'max_size[file,10000]', //->10MB
                            ]
                        ]);
                        if (!$input) {
                            $data['error'] = "Invalid Document, Upload pdf less than 10MB";
                            $notifimodel = new NotificationModel();
                            $data['notification'] = $notifimodel->where('id', $id)->first();
                            echo view('higherEnd/notification/edit', $data);
                        } else {
                            $upload_path = FCPATH . 'public/notifications';
                            $image = $img->getName();

                            $res = $img->move($upload_path, $image);

                            $data = [
                                'not_title'     => sanitize_filename($this->request->getVar('title')),
                                'not_title_e'   => sanitize_filename($this->request->getVar('title_e')),
                                'not_type'      => sanitize_filename($this->request->getVar('not_type')),
                                'year'          => sanitize_filename($this->request->getVar('year')),
                                'not_pdf'       => sanitize_filename($image),
                            ];
                            $notifimodel->update($id, $data);
                            return redirect()->to('higherEnd/notification');
                        }
                    } else {
                        $data = [
                            'not_title'     => sanitize_filename($this->request->getVar('title')),
                            'not_title_e'   => sanitize_filename($this->request->getVar('title_e')),
                            'not_type'      => sanitize_filename($this->request->getVar('not_type')),
                            'year'          => sanitize_filename($this->request->getVar('year')),
                        ];
                        $notifimodel->update($id, $data);
                        return redirect()->to('higherEnd/notification');
                    }
                } else {
                    $data = $this->common();
                    $notifimodel = new NotificationModel();
                    $data['notification'] = $notifimodel->where('id', $id)->first();
                    $data['validation'] = $this->validator;
                    echo view('higherEnd/notification/edit', $data);
                }
            }
        } else {
            $data = $this->common();
            $notifimodel = new NotificationModel();
            $data['notification'] = $notifimodel->where('id', $id)->first();
            $data['validation'] = $this->validator;
            echo view('higherEnd/notification/edit', $data);
        }
    }
    public function notifidelete($id)
    {
        $notifimodel = new NotificationModel();
        $data['notification']  = $notifimodel
            ->where('id', $id)
            ->delete();

        return redirect()->to('higherEnd/notification');
    }
}