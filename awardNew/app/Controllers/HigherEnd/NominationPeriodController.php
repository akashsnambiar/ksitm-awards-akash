<?php

namespace App\Controllers\HigherEnd;

use App\Controllers\BaseController;
use App\Models\SettingsModel;
use App\Models\NominationPeriodModel;

class NominationPeriodController extends BaseController
{
    public function common()
    {
        helper(['form']);
        $session = session();
        $settingsModel = new SettingsModel();
        $setting = $settingsModel->findAll();
        $data1   =    array();
        foreach ($setting as $row) {
            $data1[$row['config_key']] = $row['config_value'];
        }
        $data['secretKey'] = $data1['secretKey'];
        $data['siteKey'] = $data1['siteKey'];
        $data['admin_email'] = $data1['admin_email'];
        $data['site_name'] = $data1['site_name'];
        $data['title_tags'] = $data1['title_tags'];
        $data['copyright_year'] = $data1['copyright_year'];
        $data['meta_tags'] = $data1['meta_tags'];
        $data['meta_description'] = $data1['meta_description'];

        return $data;
    }
    public function index()
    {
        helper(['form']);
        $session = session();
        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));

        //  if ($captcha_response != '') {
        if (isset($_POST['Submit'])) {

            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );
            // print_r($check);exit;
            $startProcess = curl_init();

            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            // $receiveData = curl_exec($startProcess);
            // $finalResponse = json_decode($receiveData, true);
            //print_r($finalResponse);exit; 
            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {
                
                $current_year = $this->request->getVar('year');
                $startyear = explode('-', $this->request->getVar('start_date'));
                $year = $startyear[0];
                // print_r($year);exit;
                $nominationModel = new NominationPeriodModel();
                $dataCount = $nominationModel->where('year', $year)->countAllResults();
                //print_r($dataCount);exit;

                if ($dataCount > 0) {

                    $data = $this->common();
                    $nominationModel = new NominationPeriodModel();
                    $data['nomperiod'] = $nominationModel->findAll();
                    //print_r($data);exit;
                    $data['message'] = 'Nomination period already exist for this year, please update';
                    echo view('higherEnd/nominationPeriod', $data);
                } else {
                    $data = [
                        'user_name'     => $_SESSION['AdminName'],
                        'start_date'    => $this->request->getVar('start_date'),
                        'end_date'      => $this->request->getVar('end_date'),
                        'news_feed_mal' => sanitize_filename($this->request->getVar('news_mal')),
                        'news_feed_eng' => sanitize_filename($this->request->getVar('news_eng')),
                        'ip'            => $this->request->getIPAddress(),
                        'year'          => $year,
                        'created_time'  => $current_year,
                    ];
                    $nominationModel->save($data);
                    //echo $nominationModel->db->getLastQuery(); exit;    // to check model last query

                    return redirect()->to('higherEnd/nominationperiod');
                }
            }
        } else {

            $data = $this->common();
            $nominationModel = new NominationPeriodModel();
            $data['nomperiod'] = $nominationModel->findAll();
            //print_r($data);exit;
            echo view('higherEnd/nominationPeriod', $data);
        }
    }
    public function nominationstatus($id, $status)
    {

        $nominationModel = new NominationPeriodModel();
        $data['nom']  = $nominationModel
            ->set('status', $status)
            ->where('id', $id)
            ->update();
        // echo $nominationModel->db->getLastQuery();exit;    // to check model last query

        return redirect()->to('higherEnd/nominationperiod');
    }
    // public function delete($id)
    // {
    //     $nominationModel = new NominationPeriodModel();

    //     $data['nomination']  = $nominationModel
    //         ->where('id', $id)
    //         ->delete();

    //     return redirect()->to('higherEnd/nominationperiod');
    // }
    public function nomPeriodedit($eid)
    {
        $id = decode_url($eid);
        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));

        //  if ($captcha_response != '') {
        if (isset($_POST['Submit'])) {

            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );
            // print_r($check);exit;
            $startProcess = curl_init();

            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            // $receiveData = curl_exec($startProcess);
            // $finalResponse = json_decode($receiveData, true);
            //print_r($finalResponse);exit;
            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {
               
                $nominationModel = new NominationPeriodModel();
                $data = [
                    'start_date' => $this->request->getVar('start_date'),
                    'end_date'  => $this->request->getVar('end_date'),
                    'news_feed_mal' => sanitize_filename($this->request->getVar('news_mal')),
                    'news_feed_eng' => sanitize_filename($this->request->getVar('news_eng')),
                    'ip' => $this->request->getIPAddress(),
                    
                ];
                // print_r($eid);exit;
                $nominationModel->update($id, $data);
                return redirect()->to('higherEnd/nominationperiod');
                
            }
        } else {

            $data = $this->common();
            $nominationModel = new NominationPeriodModel();
            $data['nomperiod'] = $nominationModel->where('id', $id)->first();
            //print_r($data);exit;
            echo view('higherEnd/nomPeriodedit', $data);
        }
    }
}