<?php

namespace App\Controllers\HigherEnd;

use App\Controllers\BaseController;
use App\Models\SettingsModel;
use App\Models\WhocanapplyModel;
use App\Models\HomeModel;

class WhoCanApplyController extends BaseController
{
    public function common()
    {
        helper(['form']);
        $session = session();
        $settingsModel = new SettingsModel();
        $setting = $settingsModel->findAll();
        $this->data1   =    array();
        foreach ($setting as $row) {
            $this->data1[$row['config_key']] = $row['config_value'];
        }
        $data['secretKey'] = $this->data1['secretKey'];
        $data['siteKey'] = $this->data1['siteKey'];
        $data['admin_email'] = $this->data1['admin_email'];
        $data['site_name'] = $this->data1['site_name'];
        $data['title_tags'] = $this->data1['title_tags'];
        $data['copyright_year'] = $this->data1['copyright_year'];
        $data['meta_tags'] = $this->data1['meta_tags'];
        $data['meta_description'] = $this->data1['meta_description'];

        return $data;
    }
    public function index()
    {
        $data = $this->common();
        $whocanapplymodel = new WhocanapplyModel();
        $data['whocanapply'] = $whocanapplymodel->findAll();
        $homemodel = new HomeModel();
        $data['homecnt'] = $homemodel->first();
        //print_r($data);exit;
        echo view('higherEnd/siteManagement/whocanapply', $data);
    }
    public function whocanapplyadd()
    {
        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
        // if ($captcha_response != '') {
        if (isset($_POST['Submit'])) {
            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );

            $startProcess = curl_init();
            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            // $receiveData = curl_exec($startProcess);
            // $finalResponse = json_decode($receiveData, true);

            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {

                $whocanapplymodel = new WhocanapplyModel();
                $data = [
                    'title'     => sanitize_filename($this->request->getVar('title')),
                    'title_e'   => sanitize_filename($this->request->getVar('title_e')),

                ];
                $whocanapplymodel->save($data);

                return redirect()->to('higherEnd/whocanapply');
            } else {

                $data = $this->common();
                $whocanapplymodel = new WhocanapplyModel();
                $data['whocanapply'] = $whocanapplymodel->findAll();
                $data['validation'] = $this->validator;
                echo view('higherEnd/siteManagement/whocanapply', $data);
            }
        }
    }
    public function whoapplyadd()
    {
        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
        // if ($captcha_response != '') {
        if (isset($_POST['Submit'])) {
            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );

            $startProcess = curl_init();
            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            // $receiveData = curl_exec($startProcess);
            // $finalResponse = json_decode($receiveData, true);

            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {

                $homemodel = new HomeModel();
                $id = 1;
                $data = [
                    'title'    => sanitize_filename($this->request->getVar('title')),
                    'title_e'  => sanitize_filename($this->request->getVar('title_e')),

                ];
                $homemodel->update($id, $data);

                return redirect()->to('higherEnd/whocanapply');
            } else {

                $data = $this->common();
                $homemodel = new HomeModel();
                $data['homecnt'] = $homemodel->first();
                echo view('higherEnd/siteManagement/whocanapply', $data);
            }
        }
    }
    public function whocanedit($eid)
    {
        $id = decode_url($eid);
        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
        // if ($captcha_response != '') {
        if (isset($_POST['Submit'])) {
            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );

            $startProcess = curl_init();
            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            // $receiveData = curl_exec($startProcess);
            // $finalResponse = json_decode($receiveData, true);

            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {

                $whocanapplymodel = new WhocanapplyModel();
                $data = [
                    'title'     => sanitize_filename($this->request->getVar('title')),
                    'title_e'   => sanitize_filename($this->request->getVar('title_e')),

                ];
                $whocanapplymodel->update($id, $data);;

                return redirect()->to('higherEnd/whocanapply');
            } else {
                $data = $this->common();
                $whocanapplymodel = new WhocanapplyModel();
                $data['whocanapply'] = $whocanapplymodel->where('id', $id)->first();
                if ($data['whocanapply']) {
                    $data['validation'] = $this->validator;
                    echo view('higherEnd/siteManagement/whocanedit', $data);
                } else {
                    echo view('higherEnd/index.html');
                }
            }
        } else {
            $data = $this->common();
            $whocanapplymodel = new WhocanapplyModel();
            $data['whocanapply'] = $whocanapplymodel->where('id', $id)->first();
            if ($data['whocanapply']) {
                $data['validation'] = $this->validator;
                echo view('higherEnd/siteManagement/whocanedit', $data);
            } else {
                echo view('higherEnd/index.html');
            }
        }
    }
    public function whocandelete($id)
    {
        $whocanapplymodel = new WhocanapplyModel();
        $data['whocanapply']  = $whocanapplymodel
            ->where('id', $id)
            ->delete();

        return redirect()->to('higherEnd/whocanapply');
    }
}