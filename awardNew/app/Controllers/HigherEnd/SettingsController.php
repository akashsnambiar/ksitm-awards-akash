<?php

namespace App\Controllers\HigherEnd;

use App\Controllers\BaseController;
use App\Models\SettingsModel;

class SettingsController extends BaseController
{
    public function common()
    {
        helper(['form']);
        $session = session();
        $settingsModel = new SettingsModel();
        $setting = $settingsModel->findAll();
        $this->data1   =    array();
        foreach ($setting as $row) {
            $this->data1[$row['config_key']] = $row['config_value'];
        }
        $data['secretKey'] = $this->data1['secretKey'];
        $data['siteKey'] = $this->data1['siteKey'];
        $data['admin_email'] = $this->data1['admin_email'];
        $data['site_name'] = $this->data1['site_name'];
        $data['title_tags'] = $this->data1['title_tags'];
        $data['copyright_year'] = $this->data1['copyright_year'];
        $data['meta_tags'] = $this->data1['meta_tags'];
        $data['meta_description'] = $this->data1['meta_description'];

        return $data;
    }
    public function index()
    {
        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));

        //  if ($captcha_response != '') {
        if (isset($_POST['Submit'])) {

            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );
            // print_r($check);exit;
            $startProcess = curl_init();

            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            $receiveData = curl_exec($startProcess);

            $finalResponse = json_decode($receiveData, true);
            //print_r($finalResponse);exit;
            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {
                $rules = [
                    'admin_email' => 'required|valid_email',
                    'site_name' => 'required',
                    'copyright_year' => 'required|numeric',
                    'title_tags' => 'required',
                    'meta_tags' => 'required',
                    'meta_description' => 'required',
                ];
                $messages = [
                    "admin_email" => [
                        "required" => "Email Required",
                        'valid_email' => 'Please check the Email field. It does not appear to be valid.',
                    ],
                    "site_name" => [
                        "required" => "Site Name Required",
                    ],
                    "copyright_year" => [
                        "required" => "Copy Right Required",
                    ],
                    "title_tags" => [
                        "required" => "Title Tag Required",
                    ],
                    "meta_tags" => [
                        "required" => "Meta Tag Required",
                    ],
                    "meta_description" => [
                        "required" => "Meta Description Required",
                    ],
                ];

                if ($this->validate($rules, $messages)) {

                    $settingsModel = new SettingsModel();
                    $data['admin_email']        = sanitize_filename($this->request->getVar('admin_email'));
                    $data['site_name']          = sanitize_filename($this->request->getVar('site_name'));
                    $data['title_tags']         = sanitize_filename($this->request->getVar('title_tags'));
                    $data['copyright_year']     = sanitize_filename($this->request->getVar('copyright_year'));
                    $data['meta_tags']          = sanitize_filename($this->request->getVar('meta_tags'));
                    $data['meta_description']   = sanitize_filename($this->request->getVar('meta_description'));
                    //print_r($data);exit;
                    $settingsModel->set('config_value', $data['admin_email'])->where('config_key', 'admin_email')->update();
                    $settingsModel->set('config_value', $data['site_name'])->where('config_key', 'site_name')->update();
                    $settingsModel->set('config_value', $data['title_tags'])->where('config_key', 'title_tags')->update();
                    $settingsModel->set('config_value', $data['copyright_year'])->where('config_key', 'copyright_year')->update();
                    $settingsModel->set('config_value', $data['meta_tags'])->where('config_key', 'meta_tags')->update();
                    $settingsModel->set('config_value', $data['meta_description'])->where('config_key', 'meta_description')->update();

                    return redirect()->to('higherEnd/settings');
                } else {
                    $data = $this->common();
                    $settingsModel = new SettingsModel();
                    $data['settings'] = $settingsModel->findAll();
                    //print_r($data['settings']);exit;
                    $data['validation'] = $this->validator;
                    //print_r($data['validation']->listErrors());exit;
                    echo view('higherEnd/settings', $data);
                }
            }
        } else {

            $data = $this->common();
            $settingsModel = new SettingsModel();
            $data['aboutcontent'] = $settingsModel->findAll();
            //print_r($data);exit;
            echo view('higherEnd/settings', $data);
        }
    }
}
