<?php

namespace App\Controllers\HigherEnd;

use App\Controllers\BaseController;
use App\Models\SettingsModel;
use App\Models\WhodecidesModel;
use App\Models\HomeModel;

class WhoDecidesController extends BaseController
{
    public function common()
    {
        helper(['form']);
        $session = session();
        $settingsModel = new SettingsModel();
        $setting = $settingsModel->findAll();
        $this->data1   =    array();
        foreach ($setting as $row) {
            $this->data1[$row['config_key']] = $row['config_value'];
        }
        $data['secretKey'] = $this->data1['secretKey'];
        $data['siteKey'] = $this->data1['siteKey'];
        $data['admin_email'] = $this->data1['admin_email'];
        $data['site_name'] = $this->data1['site_name'];
        $data['title_tags'] = $this->data1['title_tags'];
        $data['copyright_year'] = $this->data1['copyright_year'];
        $data['meta_tags'] = $this->data1['meta_tags'];
        $data['meta_description'] = $this->data1['meta_description'];

        return $data;
    }
    public function index()
    {
        $data = $this->common();
        $homemodel = new HomeModel();
        $data['homecnt'] = $homemodel->first();
        $whodecidesmodel = new WhodecidesModel();
        $data['whodecides'] = $whodecidesmodel->findAll();
        //print_r($data);exit;
        echo view('higherEnd/siteManagement/whodecides', $data);
    }
    public function whodecidesadd()
    {
        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
        // if ($captcha_response != '') {
        if (isset($_POST['Submit'])) {
            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );

            $startProcess = curl_init();
            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            // $receiveData = curl_exec($startProcess);
            // $finalResponse = json_decode($receiveData, true);

            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {

                $homemodel = new HomeModel();
                $id = 1;
                $data = [
                    'title4'    => sanitize_filename($this->request->getVar('title')),
                    'title4_e'  => sanitize_filename($this->request->getVar('text')),
                    'text4'     => $this->request->getVar('title4_e'),
                    'text4_e'   => $this->request->getVar('text4_e'),

                ];
                $homemodel->update($id, $data);

                return redirect()->to('higherEnd/whodecides');
            } else {

                $data = $this->common();
                $homemodel = new HomeModel();
                $data['homecnt'] = $homemodel->first();
                echo view('higherEnd/siteManagement/whodecides', $data);
            }
        }
    }
    public function whocandecidesadd()
    {
        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
        // if ($captcha_response != '') {
        if (isset($_POST['Submit'])) {
            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );

            $startProcess = curl_init();
            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            // $receiveData = curl_exec($startProcess);
            // $finalResponse = json_decode($receiveData, true);

            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {

                $rules = [
                    'title' => 'required',
                    'title_e' => 'required',


                ];
                $messages = [
                    "title" => [
                        "required" => "Malayalam Title is required",
                    ],
                    "title_e" => [
                        "required" => "English Title is required",
                    ],

                ];

                if ($this->validate($rules, $messages)) {


                    $whodecidesmodel = new WhodecidesModel();

                    $img = $this->request->getFile('file');
                    //print_r($img);exit;

                    $input = $this->validate([
                        'file' => [
                            'uploaded[file]',
                            'mime_in[file,application/pdf]',
                            'max_size[file,10000]', //->10MB
                        ]
                    ]);
                    if (!$input) {
                        $whodecidesmodel = new WhodecidesModel();
                        $data['notification'] = $whodecidesmodel->findAll();
                        $data['error'] = "Invalid Document, Upload pdf less than 10mb";
                        echo view('higherEnd/siteManagement/whodecidesadd', $data);
                    } else {
                        $upload_path = FCPATH . 'public/whodecides';
                        $image = $img->getName();

                        $res = $img->move($upload_path, $image);

                        $data = [
                            'whodecides'    => sanitize_filename($this->request->getVar('title')),
                            'whodecides_e'  => sanitize_filename($this->request->getVar('title_e')),
                            'document'      => $image,
                        ];
                        $whodecidesmodel->save($data);

                        return redirect()->to('higherEnd/whodecides');
                    }
                } else {

                    $data = $this->common();
                    $whodecidesmodel = new WhodecidesModel();
                    $data['notification'] = $whodecidesmodel->findAll();
                    $data['validation'] = $this->validator;
                    echo view('higherEnd/siteManagement/whodecidesadd', $data);
                }
            }
        } else {
            $data = $this->common();
            $whodecidesmodel = new WhodecidesModel();
            $data['notification'] = $whodecidesmodel->findAll();
            $data['validation'] = $this->validator;
            echo view('higherEnd/siteManagement/whodecidesadd', $data);
        }
    }
    public function whodecidesstatus($id, $status)
    {
        $whodecidesmodel = new WhodecidesModel();
        $data['user']  = $whodecidesmodel
            ->set('status', $status)
            ->where('id', $id)
            ->update();

        return redirect()->to('higherEnd/whodecides');
    }
    public function whoedit($eid)
    {
        $id = decode_url($eid);
        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
        // if ($captcha_response != '') {
        if (isset($_POST['Submit'])) {
            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );

            $startProcess = curl_init();
            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            // $receiveData = curl_exec($startProcess);
            // $finalResponse = json_decode($receiveData, true);

            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {

                $rules = [
                    'title' => 'required',
                    'title_e' => 'required',
                ];
                $messages = [
                    "title" => [
                        "required" => "Malayalam Title is required",
                    ],
                    "title_e" => [
                        "required" => "English Title is required",
                    ],
                ];

                if ($this->validate($rules, $messages)) {

                    $whodecidesmodel = new WhodecidesModel();
                    $img = $this->request->getFile('file');
                    $imgname = $img->getname();

                    if ($imgname) {
                        $input = $this->validate([
                            'file' => [
                                'uploaded[file]',
                                'mime_in[file,application/pdf]',
                                'max_size[file,10000]', //->10MB
                            ]
                        ]);
                        if (!$input) {
                            $whodecidesmodel = new WhodecidesModel();
                            $data['whodecides'] = $whodecidesmodel->where('id', $id)->first();
                            $data['error'] = "Invalid Document, Upload pdf less than 10mb";
                            echo view('higherEnd/siteManagement/whodecideedit', $data);
                        } else {
                            $upload_path = FCPATH . 'public/whodecides';
                            $image = $img->getName();

                            $res = $img->move($upload_path, $image);

                            $data = [
                                'whodecides'    => sanitize_filename($this->request->getVar('title')),
                                'whodecides_e'  => sanitize_filename($this->request->getVar('title_e')),
                                'document'      => sanitize_filename($image),

                            ];
                            $whodecidesmodel->update($id, $data);
                            //print_r($whodecidesmodel);
                            // exit;
                            return redirect()->to('higherEnd/whodecides');
                        }
                    } else {
                        $data = [
                            'whodecides'    => sanitize_filename($this->request->getVar('title')),
                            'whodecides_e'  => sanitize_filename($this->request->getVar('title_e')),
                        ];
                        $whodecidesmodel->update($id, $data);
                        //print_r($whodecidesmodel);
                        // exit;
                        return redirect()->to('higherEnd/whodecides');
                    }
                } else {
                    $data = $this->common();
                    $whodecidesmodel = new WhodecidesModel();
                    $data['whodecides'] = $whodecidesmodel->where('id', $id)->first();
                    $data['validation'] = $this->validator;
                    echo view('higherEnd/siteManagement/whodecideedit', $data);
                }
            }
        } else {
            $data = $this->common();
            $whodecidesmodel = new WhodecidesModel();
            $data['whodecides'] = $whodecidesmodel->where('id', $id)->first();
            $data['validation'] = $this->validator;
            echo view('higherEnd/siteManagement/whodecideedit', $data);
        }
    }
    public function whodelete($id)
    {
        $whodecidesmodel = new WhodecidesModel();
        $data['whodecides']  = $whodecidesmodel
            ->where('id', $id)
            ->delete();

        return redirect()->to('higherEnd/whodecides');
    }
}