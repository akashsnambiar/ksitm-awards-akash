<?php

namespace App\Controllers\HigherEnd;

use App\Controllers\BaseController;
use App\Models\SettingsModel;
use App\Models\BannerModel;
use App\Models\NominationCategoryModel;

class BannerController extends BaseController
{
    public function common()
    {
        helper(['form']);
        $session = session();
        $settingsModel = new SettingsModel();
        $setting = $settingsModel->findAll();
        $this->data1   =    array();
        foreach ($setting as $row) {
            $this->data1[$row['config_key']] = $row['config_value'];
        }
        $data['secretKey'] = $this->data1['secretKey'];
        $data['siteKey'] = $this->data1['siteKey'];
        $data['admin_email'] = $this->data1['admin_email'];
        $data['site_name'] = $this->data1['site_name'];
        $data['title_tags'] = $this->data1['title_tags'];
        $data['copyright_year'] = $this->data1['copyright_year'];
        $data['meta_tags'] = $this->data1['meta_tags'];
        $data['meta_description'] = $this->data1['meta_description'];

        return $data;
    }
    public function index()
    {
        $data = $this->common();
        $bannermodel = new BannerModel();
        // $data['banner'] = $bannermodel->findAll();
        $nomcatmodel = new NominationCategoryModel();
        $data['nomcat'] = $nomcatmodel->findAll();
        $data['banner'] = $bannermodel
            ->select('banner.*,n.id as nid, n.nomination_category as nomination_category')
            ->join('nomination_category AS n', 'banner.category_id = n.id')->findAll();
        //print_r($data);exit;
        echo view('higherEnd/banner', $data);
    }

    public function addbanner()
    {

        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
        // if ($captcha_response != '') {
        if (isset($_POST['Submit'])) {
            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );

            $startProcess = curl_init();
            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
           /*  $receiveData = curl_exec($startProcess);
            $finalResponse = json_decode($receiveData, true); */ 
            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {

                $created_time = $this->request->getVar('year');
                $rules = [
                    'title' => 'required',
                    'title_e' => 'required',
                    'title_cat' => 'required',
                    'banyear' => 'required',
                ];
                $messages = [
                    "title" => [
                        "required" => "Malayalam Title is required",
                    ],
                    "title_e" => [
                        "required" => "English Title is required",
                    ],
                    "title_cat" => [
                        "required" => "Banner Category is required",
                    ],
                    "banyear" => [
                        "required" => "Banner Year is required",
                    ],
                ];

                if ($this->validate($rules, $messages)) {

                    $year = $this->request->getVar('banyear');
                    $banCat = $this->request->getVar('title_cat');
                    $bannermodel = new BannerModel();
                    $dataCount = $bannermodel->where('year', $year)->where('category_id', $banCat)->countAllResults();
                    //echo $bannermodel->db->getLastQuery();
                    if ($dataCount > 0) {
                        $nomcatmodel = new NominationCategoryModel();
                        $data['nomcat'] = $nomcatmodel->findAll();
                        $bannermodel = new BannerModel();
                        $data['banner'] = $bannermodel
                            ->select('banner.*,n.id as nid, n.nomination_category as nomination_category')
                            ->join('nomination_category AS n', 'banner.category_id = n.id')->findAll();

                        $data['message'] = 'Banner Title already exist for same category in ' . $year . ', please update exiting entry';
                        // print_r($data);
                        // exit;
                        echo view('higherEnd/banner', $data);
                    } else {
                        $img = $this->request->getFile('file');
                        $input = $this->validate([
                            'file' => [
                                'uploaded[file]',
                                'mime_in[file,image/jpg,image/jpeg,image/png]',
                                'max_size[file,2048]',
                            ]
                        ]);
                        if (!$input) {
                            $data['error'] = "Invalid Image, Upload image leass than 2MB in jpeg, jpg or png format";
                            $bannermodel = new BannerModel();
                            $data['banner'] = $bannermodel
                                ->select('banner.*,n.id as nid, n.nomination_category as nomination_category')
                                ->join('nomination_category AS n', 'banner.category_id = n.id')->findAll();
                            $data['imgcat'] = $bannermodel->findAll();
                            echo view('higherEnd/banner', $data);
                        } else {
                            $upload_path = FCPATH . 'images/banner';
                            // print_r($upload_path);exit;
                            $image = $img->getName();

                            $res = $img->move($upload_path, $image);

                            $data = [
                                'banner_title_en'  => sanitize_filename($this->request->getVar('title_e')),
                                'banner_title_mal' => sanitize_filename($this->request->getVar('title')),
                                'category_id'      => sanitize_filename($this->request->getVar('title_cat')),
                                'year'             => sanitize_filename($this->request->getVar('banyear')),
                                'created_time'     => $created_time,
                                'category_image'   => sanitize_filename($image),


                            ];
                            // print_r($data);
                            // exit;
                            $bannermodel->save($data);
                            return redirect()->to('higherEnd/banner');
                        }
                    }
                } else {
                    $data = $this->common();
                    $nomcatmodel = new NominationCategoryModel();
                    $data['nomcat'] = $nomcatmodel->findAll();
                    $bannermodel = new BannerModel();
                    $data['banner'] = $bannermodel
                        ->select('banner.*,n.id as nid, n.nomination_category as nomination_category')
                        ->join('nomination_category AS n', 'banner.category_id = n.id')->findAll();
                    $data['validation'] = $this->validator;
                    echo view('higherEnd/banner', $data);
                }
            }
        } else {
            $data = $this->common();
            $nomcatmodel = new NominationCategoryModel();
            $data['nomcat'] = $nomcatmodel->findAll();
            $bannermodel = new BannerModel();
            $data['banner'] = $bannermodel
                ->select('banner.*,n.id as nid, n.nomination_category as nomination_category')
                ->join('nomination_category AS n', 'banner.category_id = n.id')->findAll();
            $data['validation'] = $this->validator;
            echo view('higherEnd/banner', $data);
        }
    }

    public function banneredit($eid)
    {
        // print_r('hii');exit;
        $id = decode_url($eid);
        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
        // if ($captcha_response != '') {
        if (isset($_POST['Submit'])) {
            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );

            $startProcess = curl_init();
            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
           /*  $receiveData = curl_exec($startProcess);
            $finalResponse = json_decode($receiveData, true); */
            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {

                $rules = [
                    'title' => 'required',
                    'title_e' => 'required',
                    // 'title_cat' => 'required',
                    'banyear' => 'required',
                ];
                $messages = [
                    "title" => [
                        "required" => "Malayalam Title is required",
                    ],
                    "title_e" => [
                        "required" => "English Title is required",
                    ],
                    // "title_cat" => [
                    //     "required" => "Banner Category is required",
                    // ],
                    "banyear" => [
                        "required" => "Banner Year is required",
                    ],
                ];

                if ($this->validate($rules, $messages)) {


                    $bannermodel = new BannerModel();

                    $img = $this->request->getFile('file');
                    $imgname = $img->getname();
                    // print_r($imgname);exit;
                    if ($imgname) {
                        $input = $this->validate([
                            'file' => [
                                'uploaded[file]',
                                'mime_in[file,image/jpg,image/jpeg,image/png]',
                                'max_size[file,2048]',
                            ]
                        ]);
                        if (!$input) {
                            $data['error'] = "Invalid Image, Upload image leass than 2MB in jpeg, jpg or png format";
                            $bannermodel = new BannerModel();
                            $data['imgcat'] = $bannermodel->where('id', $id)->first();
                            echo view('higherEnd/banneredit', $data);
                        } else {
                            $upload_path = FCPATH . 'images/banner';
                            // print_r($upload_path);exit;
                            $image = $img->getName();

                            $res = $img->move($upload_path, $image);

                            $data = [
                                'banner_title_en'   => sanitize_filename($this->request->getVar('title_e')),
                                'banner_title_mal'  => sanitize_filename($this->request->getVar('title')),
                                'category_id'       => sanitize_filename($this->request->getVar('title_cat')),
                                'year'              => sanitize_filename($this->request->getVar('banyear')),
                                'category_image'    => sanitize_filename($image),

                            ];

                            $bannermodel->update($id, $data);
                            return redirect()->to('higherEnd/banner');
                        }
                    } else {
                        $data = [
                            'banner_title_en'   => sanitize_filename($this->request->getVar('title_e')),
                            'banner_title_mal'  => sanitize_filename($this->request->getVar('title')),
                            'category_id'       => sanitize_filename($this->request->getVar('title_cat')),
                            'year'              => sanitize_filename($this->request->getVar('banyear')),

                        ];

                        // print_r($data); echo "--------"; print_r($id);
                        // exit;
                        $bannermodel->update($id, $data);

                        return redirect()->to('higherEnd/banner');
                    }
                } else {
                    $data = $this->common();
                    $nomcatmodel = new NominationCategoryModel();
                    $data['nomcat'] = $nomcatmodel->findAll();
                    $bannermodel = new BannerModel();
                    $data['banner'] = $bannermodel->where('id', $id)->first();
                    $data['validation'] = $this->validator;
                    echo view('higherEnd/banneredit', $data);
                }
            }
        } else {
            $data = $this->common();
            $nomcatmodel = new NominationCategoryModel();
            $data['nomcat'] = $nomcatmodel->findAll();
            $bannermodel = new BannerModel();
            $data['banner'] = $bannermodel
                ->select('banner.*,n.id as nid, n.nomination_category as nomination_category')
                ->join('nomination_category AS n', 'banner.category_id = n.id')->where('banner.id', $id)->first();
            // $data['banner'] = $bannermodel->where('id', $id)->first();
            $data['validation'] = $this->validator;
            echo view('higherEnd/banneredit', $data);
        }
    }
    public function status($eid, $status)
    {
        $id = decode_url($eid);
        $bannermodel = new BannerModel();
        $data['banner'] = $bannermodel
            ->set('status', $status)
            ->where('id', $id)
            ->update();

        return redirect()->to('higherEnd/banner');
    }
}
