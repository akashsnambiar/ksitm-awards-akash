<?php

namespace App\Controllers\HigherEnd;

use App\Controllers\BaseController;
use App\Models\SettingsModel;
use App\Models\RegisterModel;


class IndividualController extends BaseController
{
    public function common()
    {
        helper(['form']);
        helper('url');
        $session = session();
        $settingsModel = new SettingsModel();
        $setting = $settingsModel->findAll();
        $data1   =    array();
        foreach ($setting as $row) {
            $data1[$row['config_key']] = $row['config_value'];
        }
        $data['secretKey'] = $data1['secretKey'];
        $data['siteKey'] = $data1['siteKey'];
        $data['admin_email'] = $data1['admin_email'];
        $data['site_name'] = $data1['site_name'];
        return $data;
    }
    public function index()
    {
        $data = $this->common();

        $registermodel = new RegisterModel();
        $data['regyear'] = $registermodel->select('distinct(year(date)) as regdate')->where('registration.regType', 1)->groupBy('date')->findAll();
        $data['yr'] = "";
        $data['registercontent']  = $registermodel
            ->select("registration.id as id,registration.fName,registration.mName,registration.lName,registration.mobileCode,registration.mobileNumber,registration.date,registration.status, YEAR(registration.date) as year, N.NomID, N.nominator, N.nominator_e")
            ->join('nominator AS N', 'N.NomID = registration.nominaterType', 'LEFT')
            ->where('registration.regType', 1)
            ->where('YEAR(registration.date) in (select max(YEAR(date)) from registration)')
            ->orderBy('registration.id', "ASC")->findall();
        //echo $registermodel->db->getLastQuery();exit;
        echo view('higherEnd/registration/individual/list', $data);
    }

    public function yearsubmit()
    {
        $years = $this->request->getVar('year');
        if(!ctype_xdigit($years)){
            return view("index.html");
        }
        $year = decode_url($years);
    
        if ($year == '') {
            $registermodel = new RegisterModel();
            $data['regyear'] = $registermodel->select('DISTINCT YEAR(date) AS regdate')->where('regType', 1)->groupBy('date')->findAll();
            $data['yr'] = "All";
    
            $data['registercontent'] = $registermodel
                ->select("registration.id, registration.fName, registration.mName, registration.lName, registration.mobileCode, registration.mobileNumber, registration.date, YEAR(registration.date) as year, registration.status, N.NomID, N.nominator, N.nominator_e")
                ->join('nominator AS N', 'N.NomID = registration.nominaterType', 'LEFT')
                ->where('registration.regType', 1)
                ->orderBy('registration.id', 'ASC')
                ->findAll();
    
            return view('higherEnd/registration/individual/list', $data);
        } else {
            $registermodel = new RegisterModel();
            $data['regyear'] = $registermodel->select('DISTINCT YEAR(date) AS regdate')->where('regType', 1)->groupBy('date')->findAll();
            $data['yr'] = "";
    
            $data['registercontent'] = $registermodel
                ->select("registration.id, registration.fName, registration.mName, registration.lName, registration.mobileCode, registration.mobileNumber, registration.date, YEAR(registration.date) as year, registration.status, N.NomID, N.nominator, N.nominator_e")
                ->join('nominator AS N', 'N.NomID = registration.nominaterType', 'LEFT')
                ->where('registration.regType', 1)
                ->where('YEAR(registration.date)', $year)
                ->orderBy('registration.id', 'ASC')
                ->findAll();
    
            return view('higherEnd/registration/individual/list', $data);
        }
    }
    




    public function status($id, $state)
    {
        $registermodel = new RegisterModel();

        // $registerdata = $registermodel->findAll();
        if ($state == 1) {
            $status = 0;

            $data['user']  = $registermodel
                ->set('status', $status)
                ->where('id', $id)
                ->update();
        } else {
            $status = 1;

            $data['user']  = $registermodel
                ->set('status', $status)
                ->where('id', $id)
                ->update();
        }

        return redirect()->to('higherEnd/individual');

        //echo $registermodel->db->getLastQuery();    // to check model last query

    }
    public function view($eid)
    {
        $id = decode_url($eid);
        $data = $this->common();

        $registermodel = new RegisterModel();
        $data['registercontent'] = $registermodel
            ->select("registration.*, N.NomID, N.nominator, I.identityDocument")
            ->join('nominator AS N', 'N.NomID = registration.nominaterType', 'LEFT')
            ->join('identity_document AS I', 'I.identityID = registration.inputDoc', 'LEFT')
            ->where('id', $id)->first();

        echo view('higherEnd/registration/individual/view', $data);
    }
}
