<?php

namespace App\Controllers\HigherEnd;

use App\Controllers\BaseController;
use App\Models\SettingsModel;
use App\Models\GalleryCategoryModel;
use App\Models\GalleryModel;

class GalleryController extends BaseController
{
    public function common()
    {
        helper(['form']);
        $session = session();
        $settingsModel = new SettingsModel();
        $setting = $settingsModel->findAll();
        $this->data1   =    array();
        foreach ($setting as $row) {
            $this->data1[$row['config_key']] = $row['config_value'];
        }
        $data['secretKey'] = $this->data1['secretKey'];
        $data['siteKey'] = $this->data1['siteKey'];
        $data['admin_email'] = $this->data1['admin_email'];
        $data['site_name'] = $this->data1['site_name'];
        $data['title_tags'] = $this->data1['title_tags'];
        $data['copyright_year'] = $this->data1['copyright_year'];
        $data['meta_tags'] = $this->data1['meta_tags'];
        $data['meta_description'] = $this->data1['meta_description'];

        return $data;
    }
    public function index()
    {
        $data = $this->common();
        $gallerymodel = new GalleryCategoryModel();
        $data['imgcat'] = $gallerymodel->findAll();
        //print_r($data);exit;
        echo view('higherEnd/gallery/list', $data);
    }
    public function add()
    {
        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
        // if ($captcha_response != '') {
        if (isset($_POST['Submit'])) {
            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );

            $startProcess = curl_init();
            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            //$receiveData = curl_exec($startProcess);
            //$finalResponse = json_decode($receiveData, true);

            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {

                $rules = [
                    'home_title' => 'required',
                    'home_title_e' => 'required',

                ];
                $messages = [
                    "home_title" => [
                        "required" => "Malayalam Title is required",
                    ],
                    "home_title_e" => [
                        "required" => "English Title is required",
                    ],

                ];

                if ($this->validate($rules, $messages)) {


                    $gallerymodel = new GalleryCategoryModel();
                    // $id = 1;
                    $data = [
                        'title'     => sanitize_filename($this->request->getVar('home_title')),
                        'title_e'   => sanitize_filename($this->request->getVar('home_title_e')),
                        'year'      => sanitize_filename($this->request->getVar('year')),

                    ];
                    $gallerymodel->save($data);

                    return redirect()->to('higherEnd/galleryCategory');
                } else {
                    $data = $this->common();
                    $data['validation'] = $this->validator;


                    echo view('higherEnd/gallery/add', $data);
                }
            }
        } else {
            $data = $this->common();
            $gallerymodel = new GalleryCategoryModel();
            $data['validation'] = $this->validator;
            echo view('higherEnd/gallery/add', $data);
        }
    }
    public function edit($eid)
    {
        $id = decode_url($eid);
        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
        // if ($captcha_response != '') {
        if (isset($_POST['Submit'])) {
            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );

            $startProcess = curl_init();
            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            //$receiveData = curl_exec($startProcess);
            //$finalResponse = json_decode($receiveData, true);

            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {

                $rules = [
                    'home_title' => 'required',
                    'home_title_e' => 'required',

                ];
                $messages = [
                    "home_title" => [
                        "required" => "Malayalam Title is required",
                    ],
                    "home_title_e" => [
                        "required" => "English Title is required",
                    ],

                ];

                if ($this->validate($rules, $messages)) {


                    $gallerymodel = new GalleryCategoryModel();
                    // $id = 1;
                    $data = [
                        'title'     => sanitize_filename($this->request->getVar('home_title')),
                        'title_e'   => sanitize_filename($this->request->getVar('home_title_e')),
                        'year'      => sanitize_filename($this->request->getVar('year')),
                    ];
                    $gallerymodel->update($id, $data);
                    //print_r($gallerymodel);exit;
                    return redirect()->to('higherEnd/galleryCategory');
                } else {
                    $data = $this->common();
                    $data['validation'] = $this->validator;
                    echo view('higherEnd/gallery/listedit', $data);
                }
            }
        } else {
            $data = $this->common();
            $gallerymodel = new GalleryCategoryModel();
            $data['gallery'] = $gallerymodel->where('id', $id)->first();
            //print_r($data);exit;
            echo view('higherEnd/gallery/listedit', $data);
        }
    }
    public function status($id, $status)
    {
        $gallerymodel = new GalleryCategoryModel();
        $data['user']  = $gallerymodel
            ->set('status', $status)
            ->where('id', $id)
            ->update();

        return redirect()->to('higherEnd/galleryCategory');
    }
    public function delete($eid)
    {
        $id = decode_url($eid);
        $gallerymodel = new GalleryCategoryModel();
        $data['user']  = $gallerymodel
            ->where('id', $id)
            ->delete();

        return redirect()->to('higherEnd/galleryCategory');
    }
    public function gallery()
    {
        $data = $this->common();
        $gallerymodel = new GalleryModel();
        $data['imgcat'] = $gallerymodel->orderBy('id','DESC')->findAll();
        //print_r($data);exit;
        echo view('higherEnd/gallery/imagelist', $data);
    }

    public function addimage()
    {

        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
        // if ($captcha_response != '') {
        if (isset($_POST['Submit'])) {
            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );

            $startProcess = curl_init();
            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            //$receiveData = curl_exec($startProcess);
            //$finalResponse = json_decode($receiveData, true);
            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {

                $rules = [
                    'title' => 'required',
                    'title_e' => 'required',
                    'field' => 'required',
                    'field_e' => 'required',

                ];
                $messages = [
                    "title" => [
                        "required" => "Malayalam Title is required",
                    ],
                    "title_e" => [
                        "required" => "English Title is required",
                    ],
                    "field" => [
                        "required" => "Malayalam Field Name is required",
                    ],
                    "field_e" => [
                        "required" => "English Field Name is required",
                    ],

                ];

                if ($this->validate($rules, $messages)) {


                    $gallerymodel = new GalleryModel();

                    $img = $this->request->getFile('file');
                    //print_r($img);exit;
                    $input = $this->validate([
                        'file' => [
                            'uploaded[file]',
                            'mime_in[file,image/jpg,image/jpeg,image/png]',
                            'max_size[file,2048]',
                        ]
                    ]);
                    if (!$input) {
                        $data['error'] = "Invalid Image, Upload image leass than 2MB in jpeg, jpg or png format";
                        $gallerymodel = new GalleryCategoryModel();
                        $data['imgcat'] = $gallerymodel->findAll();
                        echo view('higherEnd/gallery/addimage', $data);
                    } else {
                        $upload_path = FCPATH . 'public/images/gallery';
                        $image = $img->getName();

                        $res = $img->move($upload_path, $image);

                        $data = [
                            'cat_id'    => sanitize_filename($this->request->getVar('title_cat')),
                            'title'     => sanitize_filename($this->request->getVar('title')),
                            'title_e'   => sanitize_filename($this->request->getVar('title_e')),
                            'field'     => sanitize_filename($this->request->getVar('field')),
                            'field_e'   => sanitize_filename($this->request->getVar('field_e')),
                            'gal_img'   => sanitize_filename($image),


                        ];
                        $gallerymodel->save($data);
                        // print_r($gallerymodel);
                        // exit;
                        return redirect()->to('higherEnd/galleryList');
                    }
                } else {
                    $data = $this->common();
                    $gallerymodel = new GalleryCategoryModel();
                    $data['imgcat'] = $gallerymodel->orderBy('id','DESC')->findAll();
                    $data['validation'] = $this->validator;
                    echo view('higherEnd/gallery/addimage', $data);
                }
            }
        } else {
            $data = $this->common();
            $gallerymodel = new GalleryCategoryModel();
            $data['imgcat'] = $gallerymodel->orderBy('id','DESC')->findAll();
            $data['validation'] = $this->validator;
            echo view('higherEnd/gallery/addimage', $data);
        }
    }
    public function imagestatus($id, $status)
    {
        $gallerymodel = new GalleryModel();
        $data['user']  = $gallerymodel
            ->set('status', $status)
            ->where('id', $id)
            ->update();

        return redirect()->to('higherEnd/galleryList');
    }
    public function galleryimgedit($eid)
    {
        $id = decode_url($eid);
        $data = $this->common();
        $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
        // if ($captcha_response != '') {
        if (isset($_POST['Submit'])) {
            $keySecret = $data['secretKey'];

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );

            $startProcess = curl_init();
            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            //$receiveData = curl_exec($startProcess);
            //$finalResponse = json_decode($receiveData, true);

            $finalResponse['success'] = 1;
            if ($finalResponse['success']) {

                $rules = [
                    'title' => 'required',
                    'title_e' => 'required',
                    'field' => 'required',
                    'field_e' => 'required',

                ];
                $messages = [
                    "title" => [
                        "required" => "Malayalam Title is required",
                    ],
                    "title_e" => [
                        "required" => "English Title is required",
                    ],
                    "field" => [
                        "required" => "Malayalam Field Name is required",
                    ],
                    "field_e" => [
                        "required" => "English Field Name is required",
                    ],



                ];

                if ($this->validate($rules, $messages)) {


                    $gallerymodel = new GalleryModel();

                    $img = $this->request->getFile('file');
                    $imgname = $img->getname();

                    if ($imgname) {
                        $input = $this->validate([
                            'file' => [
                                'uploaded[file]',
                                'mime_in[file,image/jpg,image/jpeg,image/png]',
                                'max_size[file,2048]',
                            ]
                        ]);
                        if (!$input) {
                            $gallerymodel = new GalleryCategoryModel();
                            $data['imgcat'] = $gallerymodel->where('id', $id)->first();
                            $data['error'] = "Invalid Image, Upload image leass than 2MB in jpeg, jpg or png format";
                            echo view('higherEnd/notification/add', $data);
                        } else {
                            $upload_path = FCPATH . 'public/images/gallery';
                            $image = $img->getName();

                            $res = $img->move($upload_path, $image);

                            $data = [
                                'cat_id'    => sanitize_filename($this->request->getVar('title_cat')),
                                'title'     => sanitize_filename($this->request->getVar('title')),
                                'title_e'   => sanitize_filename($this->request->getVar('title_e')),
                                'field'     => sanitize_filename($this->request->getVar('field')),
                                'field_e'   => sanitize_filename($this->request->getVar('field_e')),
                                'gal_img'   => sanitize_filename($image),


                            ];
                            $gallerymodel->update($id, $data);
                            //print_r($gallerymodel);
                            // exit;
                            return redirect()->to('higherEnd/galleryList');
                        }
                    } else {
                        $data = [
                            'cat_id'    => sanitize_filename($this->request->getVar('title_cat')),
                            'title'     => sanitize_filename($this->request->getVar('title')),
                            'title_e'   => sanitize_filename($this->request->getVar('title_e')),
                            'field'     => sanitize_filename($this->request->getVar('field')),
                            'field_e'   => sanitize_filename($this->request->getVar('field_e')),

                        ];
                        $gallerymodel->update($id, $data);
                        // print_r($gallerymodel);
                        // exit;
                        return redirect()->to('higherEnd/galleryList');
                    }
                } else {
                    $data = $this->common();
                    $gallerymodel = new GalleryCategoryModel();
                    $data['imgcat'] = $gallerymodel->where('id', $id)->first();
                    $data['validation'] = $this->validator;
                    echo view('higherEnd/gallery/imageedit', $data);
                }
            }
        } else {
            $data = $this->common();
            $galcatmodel = new GalleryCategoryModel();
            $data['imgcat'] = $galcatmodel
                ->select('gallery_category.title_e,gallery_category.status,gallery_category.id as gid,gallery_category.title as gtitle')
                ->findall();


            $gallerymodell = new GalleryModel();
            $data['gallery'] = $gallerymodell->where('id', $id)->first();
            // $data['imgcat'] = $gallerymodel
            // ->select('gallery.*,g.*,gallery.title as maltitle,gallery.title_e as engtitle,g.id as gid')
            // ->join('gallery_category AS g', 'g.id = gallery.cat_id')
            // ->where('gallery.id', $id)->first();
            //print_r($data);exit;
            echo view('higherEnd/gallery/imageedit', $data);
        }
    }
    public function imagedelete($eid)
    {
        $id = decode_url($eid);
        $gallerymodell = new GalleryModel();
        $data['gallery']  = $gallerymodell
            ->where('id', $id)
            ->delete();

            return redirect()->to('higherEnd/galleryList');
    }
}
