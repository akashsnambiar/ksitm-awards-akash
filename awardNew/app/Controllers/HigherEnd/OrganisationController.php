<?php

namespace App\Controllers\HigherEnd;

use App\Controllers\BaseController;
use App\Models\SettingsModel;
use App\Models\RegisterModel;

class OrganisationController extends BaseController
{
    public function common()
    {
        helper(['form']);
        helper('url');
        $session = session();
        $settingsModel = new SettingsModel();
        $setting = $settingsModel->findAll();
        $data1   =    array();
        foreach ($setting as $row) {
            $data1[$row['config_key']] = $row['config_value'];
        }
        $data['secretKey'] = $data1['secretKey'];
        $data['siteKey'] = $data1['siteKey'];
        $data['admin_email'] = $data1['admin_email'];
        $data['site_name'] = $data1['site_name'];
        $data['copyright_year'] = $data1['copyright_year'];
        return $data;
    }
    public function index()
    {
        $data = $this->common();
        $registermodel = new RegisterModel();
        $data['regyear'] = $registermodel->select('distinct(year(date)) as regdate')->where('registration.regType', 2)->groupBy('date')->findAll();
        $data['yr'] = "";
        $data['organisation']  = $registermodel
            ->select("registration.id,registration.fName,registration.mName,registration.lName,registration.mobileCode,registration.mobileNumber,registration.date, YEAR(registration.date) as year,registration.status,registration.organisationName,registration.concernedPerson,O.orgID,O.organisation,O.organisation_e")
            ->join('organisation AS O', 'O.orgID = registration.organisationType', 'LEFT')
            ->where('registration.regType', 2)
            ->where('YEAR(registration.date) in (select max(YEAR(date)) from registration)')
            ->orderBy('registration.id', "ASC")->findall();

        echo view('higherEnd/registration/organisation/list', $data);
    }
    public function yearsubmit()
    {
        $data = $this->common();
        $year = $this->request->getVar('year');
        $registermodel = new RegisterModel();
        $data['regyear'] = $registermodel->select('distinct(year(date)) as regdate')->where('registration.regType', 2)->groupBy('date')->findAll();
        $data['yr'] = "";
        $data['reg']  = $registermodel
            ->select("registration.id,registration.fName,registration.mName,registration.lName,registration.mobileCode,registration.mobileNumber,registration.date, YEAR(registration.date) as year,registration.status,registration.organisationName,registration.concernedPerson,O.orgID,O.organisation,O.organisation_e")
            ->join('organisation AS O', 'O.orgID = registration.organisationType', 'LEFT')
            ->where('registration.regType', 2);
        if (($year) != "") {
            $data['reg']->where('YEAR(registration.date)', $year);
        }
        if (($year) == "") {
            $data['yr'] = "All";
        }
        $data['reg']->orderBy('registration.id', "ASC");
        $data['organisation'] = $data['reg']->findall();
        //echo $registermodel->db->getLastQuery();exit;   // to check model last query

        echo view('higherEnd/registration/organisation/list', $data);
        //echo json_encode($data);
    }
    public function status($id, $state)
    {
        $registermodel = new RegisterModel();

        // $registerdata = $registermodel->findAll();
        if ($state == 1) {
            $status = 0;

            $data['user']  = $registermodel
                ->set('status', $status)
                ->where('id', $id)
                ->update();
        } else {
            $status = 1;

            $data['user']  = $registermodel
                ->set('status', $status)
                ->where('id', $id)
                ->update();
        }

        return redirect()->to('higherEnd/organisation');

        //echo $registermodel->db->getLastQuery();    // to check model last query

    }
    public function view($eid)
    {
        $id = decode_url($eid);
        $data = $this->common();

        $registermodel = new RegisterModel();
        $data['registercontent'] = $registermodel
            ->select("registration.*, O.*,OS.*,L.*,LB.*,B.*,P.*,M.*,S.*, I.identityDocument")
            ->join('organisation AS O', 'O.orgID = registration.organisationType', 'LEFT')
            ->join('organisation_sub AS OS', 'OS.orgSubID = registration.organisationDept', 'LEFT')
            ->join('localbody_type AS L', 'L.localID = registration.localbody', 'LEFT')
            ->join('localbodydistrict AS LB', 'LB.id = registration.localbodyDistrict', 'LEFT')
            ->join('blockpanchayath AS B', 'B.id = registration.blockpanchayath', 'LEFT')
            ->join('panchayath AS P', 'P.id = registration.gramapanchayath', 'LEFT')
            ->join('muncipality AS M', 'M.id = registration.muncipality', 'LEFT')
            ->join('state AS S', 'S.stateID = registration.state', 'LEFT')
            ->join('identity_document AS I', 'I.identityID = registration.inputDoc', 'LEFT')
            ->where('registration.id', $id)->first();

        echo view('higherEnd/registration/organisation/view', $data);
    }
}
