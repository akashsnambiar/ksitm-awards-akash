<?php

namespace App\Controllers\HigherEnd;

use App\Controllers\BaseController;
use App\Models\SettingsModel;
use App\Models\NominationsModel;

class Dashboard extends BaseController
{
    // protected $helpers = ['session'];
    // protected $filters = ['adminAuth'];
    
    public function index()
    {
        helper(['form']);
        $session = session();
        $settingsModel = new SettingsModel();
        $setting = $settingsModel->findAll();
        $this->data    =    array();
        foreach ($setting as $row) {
            $this->data[$row['config_key']] = $row['config_value'];
        }
        $secretKey = $this->data['secretKey'];
        $siteKey = $this->data['siteKey'];
        $admin_email = $this->data['admin_email'];
        $site_name = $this->data['site_name'];
        $title_tags = $this->data['title_tags'];
        $copyright_year = $this->data['copyright_year'];
        $meta_tags = $this->data['meta_tags'];
        $meta_description = $this->data['meta_description'];
        $nominationModel = new NominationsModel();
        $nominationNew = $nominationModel->where('status', 0)
            ->countAllResults();
        $nominationNewKeralajyothi = $nominationModel->where('status', 0)
            ->where('award', 1)
            ->countAllResults();
        $nominationNewKeralaprabha = $nominationModel->where('status', 0)
            ->where('award', 2)
            ->countAllResults();
        $nominationNewKeralashri = $nominationModel->where('status', 0)
            ->where('award', 3)
            ->countAllResults();
        $nominationAccepted = $nominationModel->where('status', 1)
            ->countAllResults();
        $nominationAcceptedKeralajyothi = $nominationModel->where('status', 1)
            ->where('award', 1)
            ->countAllResults();
        $nominationAcceptedKeralaprabha = $nominationModel->where('status', 1)
            ->where('award', 2)
            ->countAllResults();
        $nominationAcceptedKeralashri = $nominationModel->where('status', 1)
            ->where('award', 3)
            ->countAllResults();
        $nominationRejected = $nominationModel->where('status', 2)
            ->countAllResults();
        $nominationRejectedKeralajyothi = $nominationModel->where('status', 2)
            ->where('award', 1)
            ->countAllResults();
        $nominationRejectedKeralaprabha = $nominationModel->where('status', 2)
            ->where('award', 2)
            ->countAllResults();
        $nominationRejectedKeralashri = $nominationModel->where('status', 2)
            ->where('award', 3)
            ->countAllResults();
        echo view('higherEnd/includes/header', [
            "siteName" => $site_name,
            "siteTag" => $title_tags,
            "metaTag" => $meta_tags,
            "metaDesc" => $meta_description
        ]);
        
        echo view('higherEnd/includes/headerTop');
        echo view('higherEnd/includes/leftMenu');
        echo view('higherEnd/dashboard', [
            "nominationNew" => $nominationNew,
            "nominationAccepted" => $nominationAccepted,
            "nominationRejected" => $nominationRejected,
        ]);
        echo view('higherEnd/includes/footer', [
            "copyrightYear" => $copyright_year,
            "nominationNewKeralajyothi" => $nominationNewKeralajyothi,
            "nominationNewKeralaprabha" => $nominationNewKeralaprabha,
            "nominationNewKeralashri" => $nominationNewKeralashri,
            "nominationAcceptedKeralajyothi" => $nominationAcceptedKeralajyothi,
            "nominationAcceptedKeralaprabha" => $nominationAcceptedKeralaprabha,
            "nominationAcceptedKeralashri" => $nominationAcceptedKeralashri,
            "nominationRejectedKeralajyothi" => $nominationRejectedKeralajyothi,
            "nominationRejectedKeralaprabha" => $nominationRejectedKeralaprabha,
            "nominationRejectedKeralashri" => $nominationRejectedKeralashri,
        ]);
    }
}
