<?php

namespace App\Controllers\HigherEnd;

use App\Controllers\BaseController;
use App\Models\SettingsModel;
use App\Models\NominationsModel;
use App\Models\ReviewModel;

class KeralajyothiController extends BaseController
{
    public function common()
    {
        helper(['form']);
        helper('url');
        helper('security');
        $session = session();
        $settingsModel = new SettingsModel();
        $setting = $settingsModel->findAll();
        $data1   =    array();
        foreach ($setting as $row) {
            $data1[$row['config_key']] = $row['config_value'];
        }
        $data['secretKey'] = $data1['secretKey'];
        $data['siteKey'] = $data1['siteKey'];
        $data['admin_email'] = $data1['admin_email'];
        $data['site_name'] = $data1['site_name'];
        $data['copyright_year'] = $data1['copyright_year'];
        $data['yr'] = "";
        return $data;
    }
    public function index($printid)
    {

        helper('url');
        session();
        $data = $this->common();
        // print_r($data['print']);exit;
        $nomination = new NominationsModel();
        $data['year']  = $nomination->select('distinct(year)')->where('nominations.award', 1)->groupBy('year')->findAll();
        $data['maxyear']  = $nomination->select('max(year) as year')->where('nominations.award', 1)->first();
        $data['year_sel'] = $data['maxyear']['year'];

        $data['pagetitle'] = 'keralajyothi';
        $data['pagename'] = 'New Nomination';
        $_SESSION['pid'] = $printid;
        $data['print'] = $_SESSION['pid'];
        $data['reviewStatus'] = 1;
        $data['user']  = $nomination
            ->select('nominations.*,s.state,d.district,t.taluk,a.title,aws.awardAreaSubTitle,r.fName,r.mName,r.lName,r.organisationName,r.regType,r.nominaterType,n.*,nominations.review as reviews,nominations.status as statuss,e.name as ename')
            ->join('state AS s', 's.stateID = nominations.state', 'LEFT')
            ->join('district AS d', 'd.districtID = nominations.district', 'LEFT')
            ->join('thaluk AS t', 't.talukID = nominations.taluk', 'LEFT')
            ->join('awardarea AS a', 'a.awardID = nominations.workArea', 'LEFT')
            ->join('awardareasub AS aws', 'aws.awardAreaSubID = nominations.workAreaSub', 'LEFT')
            ->join('registration AS r', 'r.id = nominations.userID', 'LEFT')
            ->join('nominator AS n', 'n.NomId = r.nominaterType', 'LEFT')
            ->join('esign AS e', 'e.nominationID = nominations.nominationID', 'LEFT')
            ->where('nominations.status', 0)
            ->where('nominations.award', 1)
            ->where('year in (select max(year) from nominations)')
            ->orderBy("nominations.nominationID", "asc")
            ->findAll();
        //echo $nomination->db->getLastQuery();exit;

        echo view('higherEnd/newNomination/keralaJyothi/list', $data);
    }
    public function yearsubmit($printid)
    {
        //print_r($printid);exit;

        if($printid != ""){
        $data = $this->common();
        $year = $this->request->getVar('year');
        $data['year_sel'] = $year;
        $nomination = new NominationsModel();
        $data['year']  = $nomination->select('distinct(year)')->where('nominations.award', 1)->groupBy('year')->findAll();

        $data['pagetitle'] = 'keralajyothi';
        $data['pagename'] = 'New Nomination';
        $_SESSION['pid'] = $printid;
        $data['print'] = $_SESSION['pid'];
        $data['reviewStatus'] = 1;
        $data['users']  = $nomination
            ->select('nominations.*,s.state,d.district,t.taluk,a.title,aws.awardAreaSubTitle,r.fName,r.mName,r.lName,r.organisationName,r.regType,r.nominaterType,n.*,nominations.review as reviews,nominations.status as statuss,e.name as ename')
            ->join('state AS s', 's.stateID = nominations.state', 'LEFT')
            ->join('district AS d', 'd.districtID = nominations.district', 'LEFT')
            ->join('thaluk AS t', 't.talukID = nominations.taluk', 'LEFT')
            ->join('awardarea AS a', 'a.awardID = nominations.workArea', 'LEFT')
            ->join('awardareasub AS aws', 'aws.awardAreaSubID = nominations.workAreaSub', 'LEFT')
            ->join('registration AS r', 'r.id = nominations.userID', 'LEFT')
            ->join('nominator AS n', 'n.NomId = r.nominaterType', 'LEFT')
            ->join('esign AS e', 'e.nominationID = nominations.nominationID', 'LEFT')
            ->where('nominations.award', 1);

        if ($printid == 'kjn') {
            $data['users']->where('nominations.status', 0);
        }
        if ($printid == 'kja') {
            $data['users']->where('nominations.status', 1);
        }
        if ($printid == 'kjr') {
            $data['users']->where('nominations.status', 2);
        }
        if (($year) != "") {
            $data['users']->where('year', $year);
        }
        if (($year) == "") {
            $data['yr'] = "All";
        }
        $data['users']->orderBy('nominations.nominationID', 'asc');
        $data['user'] = $data['users']->findAll();
        // echo $nomination->db->getLastQuery();exit;
        echo view('higherEnd/newNomination/keralaJyothi/list', $data);
    }else{
        print_r('hiii');exit;
    }
    }
    public function keralajyothistatus($id, $status)
    {
        $nomination = new NominationsModel();
        $data['user']  = $nomination
            ->set('status', $status)
            ->where('nominationID', $id)
            ->update();

        return redirect()->to('higherEnd/keralaj/kj');

        //echo $registermodel->db->getLastQuery();    // to check model last query

    }

    public function view($eid)
    {
        //$id = base64_decode($eid);
        helper('url');
      
        $data = $this->common();
        //$_SESSION['pid'] = $printid;
        $id = decode_url($eid);
        //print_r($id); exit;
        $data['print'] = $_SESSION['pid'];
        $nomination = new NominationsModel();
        // $registerdata = $registermodel->findAll();

        $data['view']  = $nomination
            ->select('nominations.*,s.state,d.district,t.taluk,ad.title,aws.awardAreaSubTitle,e.name as ename')
            ->join('state AS s', 's.stateID = nominations.state', 'LEFT')
            ->join('district AS d', 'd.districtID = nominations.district', 'LEFT')
            ->join('thaluk AS t', 't.talukID = nominations.taluk', 'LEFT')
            ->join('awardarea AS ad', 'ad.awardID = nominations.workArea', 'LEFT')
            ->join('awardareasub AS aws', 'aws.awardAreaSubID = nominations.workAreaSub', 'LEFT')
            ->join('esign AS e', 'e.nominationID = nominations.nominationID', 'LEFT')
            ->where('nominations.nominationID', $id)->first();
        //print_r($data['view']);exit;
        if(isset($data['view']) == 1){
            echo view('higherEnd/newNomination/keralaJyothi/view', $data);
        }else{
            return redirect()->to('higherEnd/keralaj/kj');
         }
    }
    public function review($eid)
    {
        $id = decode_url($eid);
        $data = $this->common();
        //$_SESSION['pid'] = $printid;
        $data['print'] = $_SESSION['pid'];
        $data['nomid'] = $id;
        $reviewmodels = new ReviewModel();
        $data['reviewcount'] = $reviewmodels->where('nominationID', $id)->countAllResults();
        //print_r($review);exit;
        if ($data['reviewcount'] == 1) {
            $data['review'] = $reviewmodels
                ->select('reviews.*,a.first_name,a.last_name')
                ->join('admin AS a', 'a.id = reviews.adminID', 'LEFT')
                ->where('nominationID', $id)->findAll();
            //print_r($reviewdata['data']);exit;
            echo view('higherEnd/newNomination/keralaJyothi/review', $data);
        } else {
            $reviewmodels = new ReviewModel();
            $data['reviewcount'] = $reviewmodels->where('nominationID', $id)->first();
            echo view('higherEnd/newNomination/keralaJyothi/review', $data);
        }
    }
    public function reviewView($eid)
    {
        $id = decode_url($eid);
        $data = $this->common();
        helper('url');
        $data['nomid'] = $id;
        $reviewmodels = new ReviewModel();
        $data['reviewcount'] = $reviewmodels->where('nominationID', $id)->countAllResults();
        //print_r($review);exit;
        if ($data['reviewcount'] == 1) {
            $data['review'] = $reviewmodels
                ->select('reviews.*,a.first_name,a.last_name')
                ->join('admin AS a', 'a.id = reviews.adminID', 'LEFT')
                ->where('nominationID', $id)->findAll();
            //print_r($reviewdata['data']);exit;
            echo view('higherEnd/reviewView', $data);
        } else {

            echo view('higherEnd/reviewView', $data);
        }
    }

    public function reviewsave()
    {
        helper(['form']);
        $session = session();
        $id = $this->request->getVar('nomid');
        $reviewmodels = new ReviewModel();
        $reviewresult = $reviewmodels->where('nominationID', $id)->countAllResults();

        if ($reviewresult == 1) {
            $data1 = [
                'adminID' => $_SESSION['AdminID'],
                'nominationID' => $this->request->getVar('nomid'),
                'date'    => date("Y/m/d"),
                'review'  => sanitize_filename($this->request->getVar('text')),
            ];
            //print_r($data1);exit;
            $reviewmodels->set($data1)->where('nominationID', $id)->update();
            if ($reviewmodels) {
                $nominationmodel = new NominationsModel();
                $review = [
                    'review' => 1,
                    'status' => 2
                ];
                $nominationmodel->update($id, $review,);
                return redirect()->to('higherEnd/keralaj/kj');
            }
        } else {
            $data = [
                'adminID' => $_SESSION['AdminID'],
                'nominationID' => $this->request->getVar('nomid'),
                'date'    => date("Y/m/d"),
                'review'  => sanitize_filename($this->request->getVar('text')),
            ];

            $reviewmodels->where('nominationID', $id)->save($data);
            //print_r($reviewmodel);exit;
            $nominationmodel = new NominationsModel();
            $review = [
                'review' => 1,
                'status' => 2
            ];
            $nominationmodel->update($id, $review);
            //print_r($nominationmodel);exit;
            // return redirect()->to('higherEnd/keralajyothi/review/' . $id);
            return redirect()->to('higherEnd/keralaj/kj');
        }
    }
    public function reviewSubmit()
    {
        helper(['form']);
        helper('url');
        $session = session();
        $id = $this->request->getVar('nomid');
        $reviewmodels = new ReviewModel();
        $reviewresult = $reviewmodels->where('nominationID', $id)->countAllResults();

        if ($reviewresult == 1) {
            $data1 = [
                'adminID' => $_SESSION['AdminID'],
                'nominationID' => $this->request->getVar('nomid'),
                'date'    => date("Y/m/d"),
                'review'  => sanitize_filename($this->request->getVar('text')),
            ];
            //print_r($data1);exit;
            $reviewmodels->set($data1)->where('nominationID', $id)->update();
            //print_r($reviewmodel);exit;
            if ($reviewmodels) {
                $nominationmodel = new NominationsModel();
                $review = [
                    'review' => 1,

                ];
                $nominationmodel->update($id, $review,);
                return redirect()->to('higherEnd/keralajyothi/review/' . encode_url($id));
            }
        } else {
            $data = [
                'adminID' => $_SESSION['AdminID'],
                'nominationID' => $this->request->getVar('nomid'),
                'date'    => date("Y/m/d"),
                'review'  => sanitize_filename($this->request->getVar('text')),
            ];

            $reviewmodels->where('nominationID', $id)->save($data);
            //print_r($reviewmodel);exit;
            $nominationmodel = new NominationsModel();
            $review = [
                'review' => 1,

            ];
            $nominationmodel->update($id, $review);
            //print_r($nominationmodel);exit;
            // return redirect()->to('higherEnd/keralajyothi/review/' . $id);
            return redirect()->to('higherEnd/keralaj/kj');
        }
    }
    public function generatepdf()
    {

        helper('url');
        session();
        $data = $this->common();
        $year = $this->request->getVar('year');
        
        $nomination = new NominationsModel();
        if ($_SESSION['pid'] == 'kjn') {
            $data['keralajyothii']  = $nomination
                ->select('nominations.*,s.state,d.district,t.taluk,ad.title,aws.awardAreaSubTitle,e.name as ename,r.fName,r.mName,r.lName,r.organisationName,r.regType,r.nominaterType,n.*,e.name as ename')
                ->join('state AS s', 's.stateID = nominations.state', 'LEFT')
                ->join('district AS d', 'd.districtID = nominations.district', 'LEFT')
                ->join('thaluk AS t', 't.talukID = nominations.taluk', 'LEFT')
                ->join('awardarea AS ad', 'ad.awardID = nominations.workArea', 'LEFT')
                ->join('awardareasub AS aws', 'aws.awardAreaSubID = nominations.workAreaSub', 'LEFT')
                ->join('esign AS e', 'e.nominationID = nominations.nominationID', 'LEFT')
                ->join('registration AS r', 'r.id = nominations.userID', 'LEFT')
                ->join('nominator AS n', 'n.NomId = r.nominaterType', 'LEFT')
                ->where('nominations.award', 1)
                ->where('nominations.status', 0);
                if (($year) != "") {
                    $data['keralajyothii']->where('year', $year);
                }
                $data['keralajyothii']->orderBy('nominations.nominationID', 'asc');
                $data['keralajyothi'] = $data['keralajyothii']->findAll();
            //print_r($data['keralaprabha']);exit;
            echo view('higherEnd/newNomination/keralaJyothi/generatepdf', $data);
        }
        if ($_SESSION['pid'] == 'kja') {
            $data['keralajyothii']  = $nomination
                ->select('nominations.*,s.state,d.district,t.taluk,ad.title,aws.awardAreaSubTitle,e.name as ename,r.fName,r.mName,r.lName,r.organisationName,r.regType,r.nominaterType,n.*,e.name as ename')
                ->join('state AS s', 's.stateID = nominations.state', 'LEFT')
                ->join('district AS d', 'd.districtID = nominations.district', 'LEFT')
                ->join('thaluk AS t', 't.talukID = nominations.taluk', 'LEFT')
                ->join('awardarea AS ad', 'ad.awardID = nominations.workArea', 'LEFT')
                ->join('awardareasub AS aws', 'aws.awardAreaSubID = nominations.workAreaSub', 'LEFT')
                ->join('esign AS e', 'e.nominationID = nominations.nominationID', 'LEFT')
                ->join('registration AS r', 'r.id = nominations.userID', 'LEFT')
                ->join('nominator AS n', 'n.NomId = r.nominaterType', 'LEFT')
                ->where('nominations.award', 1)
                ->where('nominations.status', 1);
                if (($year) != "") {
                    $data['keralajyothii']->where('year', $year);
                }
                $data['keralajyothii']->orderBy('nominations.nominationID', 'asc');
                $data['keralajyothi'] = $data['keralajyothii']->findAll();
            //print_r($data['keralaprabha']);exit;
            echo view('higherEnd/newNomination/keralaJyothi/generatepdf', $data);
        }
        if ($_SESSION['pid'] == 'kjr') {
            $data['keralajyothii']  = $nomination
                ->select('nominations.*,s.state,d.district,t.taluk,ad.title,aws.awardAreaSubTitle,e.name as ename,r.fName,r.mName,r.lName,r.organisationName,r.regType,r.nominaterType,n.*,e.name as ename')
                ->join('state AS s', 's.stateID = nominations.state', 'LEFT')
                ->join('district AS d', 'd.districtID = nominations.district', 'LEFT')
                ->join('thaluk AS t', 't.talukID = nominations.taluk', 'LEFT')
                ->join('awardarea AS ad', 'ad.awardID = nominations.workArea', 'LEFT')
                ->join('awardareasub AS aws', 'aws.awardAreaSubID = nominations.workAreaSub', 'LEFT')
                ->join('esign AS e', 'e.nominationID = nominations.nominationID', 'LEFT')
                ->join('registration AS r', 'r.id = nominations.userID', 'LEFT')
                ->join('nominator AS n', 'n.NomId = r.nominaterType', 'LEFT')
                ->where('nominations.award', 1)
                ->where('nominations.status', 2);
                if (($year) != "") {
                    $data['keralajyothii']->where('year', $year);
                }
                $data['keralajyothii']->orderBy('nominations.nominationID', 'asc');
                $data['keralajyothi'] = $data['keralajyothii']->findAll();
            //print_r($data['keralaprabha']);exit;
            echo view('higherEnd/newNomination/keralaJyothi/generatepdf', $data);
        }
    }
    public function print($id)
    {
        helper('url');
        $data = $this->common();
        $nomination = new NominationsModel();
        // $registerdata = $registermodel->findAll();

        $data['view']  = $nomination
            ->select('nominations.*,s.state,d.district,t.taluk,ad.title,aws.awardAreaSubTitle,e.name as ename')
            ->join('state AS s', 's.stateID = nominations.state', 'LEFT')
            ->join('district AS d', 'd.districtID = nominations.district', 'LEFT')
            ->join('thaluk AS t', 't.talukID = nominations.taluk', 'LEFT')
            ->join('awardarea AS ad', 'ad.awardID = nominations.workArea', 'LEFT')
            ->join('awardareasub AS aws', 'aws.awardAreaSubID = nominations.workAreaSub', 'LEFT')
            ->join('esign AS e', 'e.nominationID = nominations.nominationID', 'LEFT')
            ->where('nominations.nominationID', $id)->first();
        //print_r($data['user']);exit;

        echo view('higherEnd/newNomination/keralaJyothi/print', $data);
    }
    public function getallaccepted()
    {
        helper('url');
        $data = $this->common();
        $data['reviewStatus'] = 1;
        $nomination = new NominationsModel();
        $data['year']  = $nomination->select('distinct(year)')->where('nominations.award', 1)->groupBy('year')->findAll();
        $data['maxyear']  = $nomination->select('max(year) as year')->where('nominations.award', 1)->first();
        $data['year_sel'] = $data['maxyear']['year'];
        
        $data['user']  = $nomination
            ->select('nominations.*,s.state,d.district,t.taluk,a.title,aws.awardAreaSubTitle,r.fName,r.mName,r.lName,r.organisationName,r.regType,r.nominaterType,n.*,nominations.review as reviews,nominations.status as statuss,e.name as ename')
            ->join('state AS s', 's.stateID = nominations.state', 'LEFT')
            ->join('district AS d', 'd.districtID = nominations.district', 'LEFT')
            ->join('thaluk AS t', 't.talukID = nominations.taluk', 'LEFT')
            ->join('awardarea AS a', 'a.awardID = nominations.workArea', 'LEFT')
            ->join('awardareasub AS aws', 'aws.awardAreaSubID = nominations.workAreaSub', 'LEFT')
            ->join('registration AS r', 'r.id = nominations.userID', 'LEFT')
            ->join('nominator AS n', 'n.NomId = r.nominaterType', 'LEFT')
            ->join('esign AS e', 'e.nominationID = nominations.nominationID', 'LEFT')
            ->where('nominations.status', 1)
            ->where('nominations.award', 1)
            ->where('year in (select max(year) from nominations)')
            ->orderBy("nominations.nominationID", "asc")->findall();
        return $data;
    }
    public function accepted($printid)
    {
        session();
        $data = $this->common();
        $data = $this->getallaccepted();
        $nomination = new NominationsModel();
        $data['year']  = $nomination->select('distinct(year)')->where('nominations.award', 1)->groupBy('year')->findAll();
        $data['maxyear']  = $nomination->select('max(year) as year')->where('nominations.award', 1)->first();
        $data['year_sel'] = $data['maxyear']['year'];

        $_SESSION['pid'] = $printid;
        $data['print'] = $_SESSION['pid'];
        $data['reviewStatus'] = 0;
        $data['pagename'] = 'Accepted Nomination';
        //print_r($data['user']);exit;

        echo view('higherEnd/newNomination/keralaJyothi/list', $data);
    }
    public function getallrejected()
    {
        helper('url');
        $data = $this->common();
        $data['reviewStatus'] = 1;
        $nomination = new NominationsModel();
        $data['year']  = $nomination->select('distinct(year)')->where('nominations.award', 1)->groupBy('year')->findAll();
        $data['maxyear']  = $nomination->select('max(year) as year')->where('nominations.award', 1)->first();
        $data['year_sel'] = $data['maxyear']['year'];

        $data['user']  = $nomination
            ->select('nominations.*,s.state,d.district,t.taluk,a.title,aws.awardAreaSubTitle,r.fName,r.mName,r.lName,r.organisationName,r.regType,r.nominaterType,n.*,nominations.review as reviews,nominations.status as statuss, e.name as ename')
            ->join('state AS s', 's.stateID = nominations.state', 'LEFT')
            ->join('district AS d', 'd.districtID = nominations.district', 'LEFT')
            ->join('thaluk AS t', 't.talukID = nominations.taluk', 'LEFT')
            ->join('awardarea AS a', 'a.awardID = nominations.workArea', 'LEFT')
            ->join('awardareasub AS aws', 'aws.awardAreaSubID = nominations.workAreaSub', 'LEFT')
            ->join('registration AS r', 'r.id = nominations.userID', 'LEFT')
            ->join('nominator AS n', 'n.NomId = r.nominaterType', 'LEFT')
            ->join('esign AS e', 'e.nominationID = nominations.nominationID', 'LEFT')
            ->where('nominations.status', 2)
            ->where('nominations.award', 1)
            ->where('year in (select max(year) from nominations)')
            ->orderBy("nominations.nominationID", "asc")->findall();
        return $data;
    }
    public function rejected($printid)
    {
        helper('url');
        $nomination = new NominationsModel();
        $data['year']  = $nomination->select('distinct(year)')->where('nominations.award', 1)->groupBy('year')->findAll();
        $data['maxyear']  = $nomination->select('max(year) as year')->where('nominations.award', 1)->first();
        $data['year_sel'] = $data['maxyear']['year'];

        $data = $this->common();
        $data = $this->getallrejected();
        $_SESSION['pid'] = $printid;
        $data['print'] = $_SESSION['pid'];
        $data['reviewStatus'] = 0;
        $data['pagename'] = 'Rejected Nomination';
        //print_r($data['user']);exit;

        echo view('higherEnd/newNomination/keralaJyothi/list', $data);
    }
}
