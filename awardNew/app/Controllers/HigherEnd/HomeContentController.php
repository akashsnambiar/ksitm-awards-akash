<?php

namespace App\Controllers\HigherEnd;

use App\Controllers\BaseController;
use App\Models\SettingsModel;
use App\Models\HomeModel;
use App\Models\NominationsModel;

class HomeContentController extends BaseController
{
    public function index()
    {
        helper(['form']);
        $session = session();
        $settingsModel = new SettingsModel();
        $setting = $settingsModel->findAll();
        $this->data    =    array();
        foreach ($setting as $row) {
            $this->data[$row['config_key']] = $row['config_value'];
        }
        $secretKey = $this->data['secretKey'];
        $siteKey = $this->data['siteKey'];
        $admin_email = $this->data['admin_email'];
        $site_name = $this->data['site_name'];
        $title_tags = $this->data['title_tags'];
        $copyright_year = $this->data['copyright_year'];
        $meta_tags = $this->data['meta_tags'];
        $meta_description = $this->data['meta_description'];
        $nominationModel = new NominationsModel();
        $nominationNew = $nominationModel->where('status', 0)
            ->countAllResults();
        $nominationNewKeralajyothi = $nominationModel->where('status', 0)
            ->where('award', 1)
            ->countAllResults();
        $nominationNewKeralaprabha = $nominationModel->where('status', 0)
            ->where('award', 2)
            ->countAllResults();
        $nominationNewKeralashri = $nominationModel->where('status', 0)
            ->where('award', 3)
            ->countAllResults();
        $nominationAccepted = $nominationModel->where('status', 1)
            ->countAllResults();
        $nominationAcceptedKeralajyothi = $nominationModel->where('status', 1)
            ->where('award', 1)
            ->countAllResults();
        $nominationAcceptedKeralaprabha = $nominationModel->where('status', 1)
            ->where('award', 2)
            ->countAllResults();
        $nominationAcceptedKeralashri = $nominationModel->where('status', 1)
            ->where('award', 3)
            ->countAllResults();
        $nominationRejected = $nominationModel->where('status', 2)
            ->countAllResults();
        $nominationRejectedKeralajyothi = $nominationModel->where('status', 2)
            ->where('award', 1)
            ->countAllResults();
        $nominationRejectedKeralaprabha = $nominationModel->where('status', 2)
            ->where('award', 2)
            ->countAllResults();
        $nominationRejectedKeralashri = $nominationModel->where('status', 2)
            ->where('award', 3)
            ->countAllResults();




       $captcha_response = trim($this->request->getVar('g-recaptcha-response'));
        // if ($captcha_response != '') {
            if(isset($_POST['Submit'])){
            $keySecret = $secretKey;

            $check = array(
                'secret'        =>    $keySecret,
                'response'      =>    $this->request->getVar('g-recaptcha-response')
            );

            $startProcess = curl_init();
            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            //$receiveData = curl_exec($startProcess);
            //$finalResponse = json_decode($receiveData, true);
            $finalResponse['success'] = 1;
           if ($finalResponse['success']) {
                
            $rules = [
                'home_title' => 'required',
                'home_title_e' => 'required',
                'text' => 'required',
                'text_e' => 'required',
            ];$messages = [
                "home_title" => [
                    "required" => "Malayalam Title is required",
                ],
                "home_title_e" => [
                    "required" => "English Title is required",
                ],
                "text" => [
                    "required" => "Malayalam text is required",
                ],
                "text_e" => [
                    "required" => "English text is required",
                ],
            ]; 
            
            if ($this->validate($rules, $messages)) {
                
                
                $homecntmodel = new HomeModel();
                $id = 1;
                $data = [
                    'title' => sanitize_filename($this->request->getVar('home_title')),
                    'title_e'  => sanitize_filename($this->request->getVar('home_title_e')),
                    'text'  => $this->request->getVar('text'),
                    'text_e'  => $this->request->getVar('text_e'),
                ];
                $homecntmodel->update($id, $data);
               
                return redirect()->to('higherEnd/homecontent');

                
            }else {
                //$data['validation'] = $this->validator;
                
                $homecntmodel = new HomeModel();
                $id = 1;
                $data = [
                    'title' => sanitize_filename($this->request->getVar('home_title')),
                    'title_e'  => sanitize_filename($this->request->getVar('home_title_e')),
                    'text'  => $this->request->getVar('text'),
                    'text_e'  => $this->request->getVar('text_e'),
                ];
                
                $homecntmodel->update($id, $data);
                
                echo view('higherEnd/siteManagement/homecnt', [
                    "siteName" => $site_name,
                    "siteKey" => $siteKey,
                    "homecontent" => $homecntmodel,
                    "validation" => $this->validator
                ]);
               
            }
        }
    }else{
        // $pagetitle = 'home';
        // echo view('higherEnd/includes/leftMenu', ["pagetitle" => $pagetitle]);
        $homecntmodel = new HomeModel();
        $homedata['homecontent'] = $homecntmodel->findAll();
        echo view('higherEnd/siteManagement/homecnt', [
            "siteName" => $site_name,
            "siteKey" => $siteKey,
            "copyright_year" => $copyright_year,
            "homecontent" => $homedata['homecontent']
        ]);
    }
        
        
    }
    
}