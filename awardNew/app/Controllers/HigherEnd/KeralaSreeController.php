<?php

namespace App\Controllers\HigherEnd;

use App\Controllers\BaseController;
use App\Models\SettingsModel;
use App\Models\NominationsModel;
use App\Models\ReviewModel;

class KeralaSreeController extends BaseController
{
    public function common()
    {
        helper(['form']);
        $session = session();
        $settingsModel = new SettingsModel();
        $setting = $settingsModel->findAll();
        $data1   =    array();
        foreach ($setting as $row) {
            $data1[$row['config_key']] = $row['config_value'];
        }
       $data['secretKey'] = $data1['secretKey'];
       $data['siteKey'] = $data1['siteKey'];
       $data['admin_email'] = $data1['admin_email'];
       $data['site_name']= $data1['site_name'];
       $data['copyright_year'] = $data1['copyright_year'];
       $data['yr'] = "";
       return $data;
    }
    public function index($printid)
    {
        session();
        $data = $this->common();
        $data['reviewStatus'] = 1;
        $_SESSION['pid'] = $printid;
        $data['print'] = $_SESSION['pid'];
        $data['pagename'] = 'New Nomination';
        $nomination = new NominationsModel();
        $data['year']  = $nomination->select('distinct(year)')->where('nominations.award', 3)->groupBy('year')->findAll();
        $data['maxyear']  = $nomination->select('max(year) as year')->where('nominations.award', 1)->first();
        $data['year_sel'] = $data['maxyear']['year'];
        
        $data['keralasree']  = $nomination
            ->select('nominations.*,s.state,d.district,t.taluk,a.title,aws.awardAreaSubTitle,r.fName,r.mName,r.lName,r.organisationName,r.regType,r.nominaterType,n.*,nominations.status as statuss,e.name as ename')
            ->join('state AS s', 's.stateID = nominations.state','LEFT')
            ->join('district AS d', 'd.districtID = nominations.district','LEFT')
            ->join('thaluk AS t', 't.talukID = nominations.taluk','LEFT')
            ->join('awardarea AS a', 'a.awardID = nominations.workArea','LEFT')
            ->join('awardareasub AS aws', 'aws.awardAreaSubID = nominations.workAreaSub','LEFT')
            ->join('registration AS r', 'r.id = nominations.userID','LEFT')
            ->join('nominator AS n', 'n.NomId = r.nominaterType','LEFT')
            ->join('esign AS e', 'e.nominationID = nominations.nominationID','LEFT')
            ->where('nominations.status', 0)
            ->where('nominations.award', 3)
            ->where('year in (select max(year) from nominations)')
            ->orderBy("nominations.nominationID", "asc")
            ->findAll();

        //print_r($data);exit;

        echo view('higherEnd/newNomination/keralaSree/list', $data);

        
    }
    public function yearsubmit($printid)
    {
        $data = $this->common();
        $year = $this->request->getVar('year');
        $data['year_sel'] = $year;
        $nomination = new NominationsModel();
        $data['year']  = $nomination->select('distinct(year)')->where('nominations.award', 3)->groupBy('year')->findAll();

        $data['pagetitle'] = 'keralasree';
        $data['pagename'] = 'New Nomination';
        $_SESSION['pid'] = $printid;
        $data['print'] = $_SESSION['pid'];
        $data['reviewStatus'] = 1;
        $data['users']  = $nomination
            ->select('nominations.*,s.state,d.district,t.taluk,a.title,aws.awardAreaSubTitle,r.fName,r.mName,r.lName,r.organisationName,r.regType,r.nominaterType,n.*,nominations.status as statuss,e.name as ename')
            ->join('state AS s', 's.stateID = nominations.state', 'LEFT')
            ->join('district AS d', 'd.districtID = nominations.district', 'LEFT')
            ->join('thaluk AS t', 't.talukID = nominations.taluk', 'LEFT')
            ->join('awardarea AS a', 'a.awardID = nominations.workArea', 'LEFT')
            ->join('awardareasub AS aws', 'aws.awardAreaSubID = nominations.workAreaSub', 'LEFT')
            ->join('registration AS r', 'r.id = nominations.userID', 'LEFT')
            ->join('nominator AS n', 'n.NomId = r.nominaterType', 'LEFT')
            ->join('esign AS e', 'e.nominationID = nominations.nominationID','LEFT')
            ->where('nominations.award', 3);
        
            if ($printid == 'ksn') {
                $data['users']->where('nominations.status', 0);
            }
            if ($printid == 'ksa') {
                $data['users']->where('nominations.status', 1);
            }
            if ($printid == 'ksr') {
                $data['users']->where('nominations.status', 2);
            }
            if (($year) != "") {
                $data['users']->where('year', $year);
            }
            if (($year) == "") {
                $data['yr'] = "All";
            }
            $data['users']->orderBy('nominations.nominationID', 'asc');
            $data['keralasree'] = $data['users']->findAll();
        // echo $nomination->db->getLastQuery();exit;
        echo view('higherEnd/newNomination/keralaSree/list', $data);
    }
    public function keralasreestatus($id, $status)
    {
        $nomination = new NominationsModel();
        $data['user']  = $nomination
        ->set('status', $status)
        ->where('nominationID', $id)
        ->update();
        
        return redirect()->to('higherEnd/keralashri/ksn');

        //echo $registermodel->db->getLastQuery();    // to check model last query

    }
    public function view($eid)
    {
        $id = decode_url($eid);
        $data = $this->common();
        $data['print'] = $_SESSION['pid'];
        $nomination = new NominationsModel();
        // $registerdata = $registermodel->findAll();

        $data['view']  = $nomination
            ->select('nominations.*,s.state,d.district,t.taluk,ad.title,aws.awardAreaSubTitle,e.name as ename')
            ->join('state AS s', 's.stateID = nominations.state','LEFT')
            ->join('district AS d', 'd.districtID = nominations.district','LEFT')
            ->join('thaluk AS t', 't.talukID = nominations.taluk','LEFT')
            ->join('awardarea AS ad', 'ad.awardID = nominations.workArea','LEFT')
            ->join('awardareasub AS aws', 'aws.awardAreaSubID = nominations.workAreaSub','LEFT')
            ->join('esign AS e', 'e.nominationID = nominations.nominationID','LEFT')
            ->where('nominations.nominationID', $id)->first();
        //print_r($data['user']);exit;

        echo view('higherEnd/newNomination/keralaSree/view', $data);
    }
    public function print($id){
        $data = $this->common();
        $nomination = new NominationsModel();
        // $registerdata = $registermodel->findAll();

        $data['view']  = $nomination
            ->select('nominations.*,s.state,d.district,t.taluk,ad.title,aws.awardAreaSubTitle,e.name as ename')
            ->join('state AS s', 's.stateID = nominations.state','LEFT')
            ->join('district AS d', 'd.districtID = nominations.district','LEFT')
            ->join('thaluk AS t', 't.talukID = nominations.taluk','LEFT')
            ->join('awardarea AS ad', 'ad.awardID = nominations.workArea','LEFT')
            ->join('awardareasub AS aws', 'aws.awardAreaSubID = nominations.workAreaSub','LEFT')
            ->join('esign AS e', 'e.nominationID = nominations.nominationID','LEFT')
            ->where('nominations.nominationID', $id)->first();
        //print_r($data['user']);exit;

        echo view('higherEnd/newNomination/keralaSree/print', $data);
    }
    public function generatepdf(){
        
        $data = $this->common();
        $year = $this->request->getVar('year');

        $nomination = new NominationsModel();
        if ($_SESSION['pid'] == 'ksn') {
        $data['keralashri']  = $nomination
            ->select('nominations.*,s.state,d.district,t.taluk,ad.title,aws.awardAreaSubTitle,e.name as ename,r.fName,r.mName,r.lName,r.organisationName,r.regType,r.nominaterType,n.*')
            ->join('state AS s', 's.stateID = nominations.state','LEFT')
            ->join('district AS d', 'd.districtID = nominations.district','LEFT')
            ->join('thaluk AS t', 't.talukID = nominations.taluk','LEFT')
            ->join('awardarea AS ad', 'ad.awardID = nominations.workArea','LEFT')
            ->join('awardareasub AS aws', 'aws.awardAreaSubID = nominations.workAreaSub','LEFT')
            ->join('esign AS e', 'e.nominationID = nominations.nominationID','LEFT')
            ->join('registration AS r', 'r.id = nominations.userID','LEFT')
            ->join('nominator AS n', 'n.NomId = r.nominaterType','LEFT')
            ->where('nominations.award', 3)
            ->where('nominations.status', 0);
            if (($year) != "") {
                $data['keralashri']->where('year', $year);
            }
            $data['keralashri']->orderBy('nominations.nominationID', 'asc');
            $data['keralasree'] = $data['keralashri']->findAll();

        echo view('higherEnd/newNomination/keralaSree/generatepdf', $data);
        }
        if ($_SESSION['pid'] == 'ksa') {
            $data['keralashri']  = $nomination
                ->select('nominations.*,s.state,d.district,t.taluk,ad.title,aws.awardAreaSubTitle,e.name as ename,r.fName,r.mName,r.lName,r.organisationName,r.regType,r.nominaterType,n.*')
                ->join('state AS s', 's.stateID = nominations.state','LEFT')
                ->join('district AS d', 'd.districtID = nominations.district','LEFT')
                ->join('thaluk AS t', 't.talukID = nominations.taluk','LEFT')
                ->join('awardarea AS ad', 'ad.awardID = nominations.workArea','LEFT')
                ->join('awardareasub AS aws', 'aws.awardAreaSubID = nominations.workAreaSub','LEFT')
                ->join('esign AS e', 'e.nominationID = nominations.nominationID','LEFT')
                ->join('registration AS r', 'r.id = nominations.userID','LEFT')
                ->join('nominator AS n', 'n.NomId = r.nominaterType','LEFT')
                ->where('nominations.award', 3)
                ->where('nominations.status', 1);
                if (($year) != "") {
                    $data['keralashri']->where('year', $year);
                }
                $data['keralashri']->orderBy('nominations.nominationID', 'asc');
                $data['keralasree'] = $data['keralashri']->findAll();
    
            echo view('higherEnd/newNomination/keralaSree/generatepdf', $data);
            }
            if ($_SESSION['pid'] == 'ksr') {
                $data['keralashri']  = $nomination
                    ->select('nominations.*,s.state,d.district,t.taluk,ad.title,aws.awardAreaSubTitle,e.name as ename,r.fName,r.mName,r.lName,r.organisationName,r.regType,r.nominaterType,n.*')
                    ->join('state AS s', 's.stateID = nominations.state','LEFT')
                    ->join('district AS d', 'd.districtID = nominations.district','LEFT')
                    ->join('thaluk AS t', 't.talukID = nominations.taluk','LEFT')
                    ->join('awardarea AS ad', 'ad.awardID = nominations.workArea','LEFT')
                    ->join('awardareasub AS aws', 'aws.awardAreaSubID = nominations.workAreaSub','LEFT')
                    ->join('esign AS e', 'e.nominationID = nominations.nominationID','LEFT')
                    ->join('registration AS r', 'r.id = nominations.userID','LEFT')
                    ->join('nominator AS n', 'n.NomId = r.nominaterType','LEFT')
                    ->where('nominations.award', 3)
                    ->where('nominations.status', 2);
                    if (($year) != "") {
                        $data['keralashri']->where('year', $year);
                    }
                    $data['keralashri']->orderBy('nominations.nominationID', 'asc');
                    $data['keralasree'] = $data['keralashri']->findAll();
        
                echo view('higherEnd/newNomination/keralaSree/generatepdf', $data);
                }
    }
    public function review($eid)
    { 
        $id = decode_url($eid);
        $data = $this->common();
        $data['print'] = $_SESSION['pid']; 
        $data['nomid']=$id; 
        $reviewmodels = new ReviewModel();
        $data['reviewcount'] = $reviewmodels->where('nominationID', $id)->countAllResults();
        //print_r($review);exit;
        if ($data['reviewcount']==1) {
            $data['review'] = $reviewmodels
            ->select('reviews.*,a.first_name,a.last_name')
            ->join('admin AS a', 'a.id = reviews.adminID', 'LEFT')
            ->where('nominationID', $id)->findAll();
            //print_r($reviewdata['data']);exit;
            echo view('higherEnd/newNomination/keralaSree/review',$data);
        } else {
        
            echo view('higherEnd/newNomination/keralaSree/review', $data);
        }
        
    }
    public function reviewsave()
    {

        helper(['form']);
        $session = session();
        $id = $this->request->getVar('nomid');
        $reviewmodels = new ReviewModel();
        $reviewresult = $reviewmodels->where('nominationID', $id)->countAllResults();
      
        if ($reviewresult == 1) {
            $data1 = [
                'adminID' => $_SESSION['AdminID'],
                'nominationID' => $this->request->getVar('nomid'),
                'date'    => date("Y/m/d"),
                'review'  => sanitize_filename($this->request->getVar('text')),
            ];
            //print_r($data1);exit;
            $reviewmodels->set($data1)->where('nominationID', $id)->update();
            //print_r($reviewmodel);exit;
            if($reviewmodels){
                $nominationmodel = new NominationsModel();
            $review = [
                'review' => 1,
                'status' => 2
            ];
            $nominationmodel->update($id, $review,);
            return redirect()->to('higherEnd/keralashri/ksn');
            }
        } else {
            $data = [
                'adminID' => $_SESSION['AdminID'],
                'nominationID' => $id,
                'date'    => date("Y/m/d"),
                'review'  => sanitize_filename($this->request->getVar('text')),
            ];

            $reviewmodels->where('nominationID', $id)->save($data);
            //print_r($reviewmodel);exit;
            $nominationmodel = new NominationsModel();
            $review = [
                'review' => 1,
                'status' => 2
            ];
            $nominationmodel->update($id, $review);
            //print_r($$nominationmode);exit;
            return redirect()->to('higherEnd/keralashri/ksn');
        }
    }
    public function reviewSubmit()
    {

        helper(['form']);
        $session = session();
        $id = $this->request->getVar('nomid');
        $reviewmodels = new ReviewModel();
        $reviewresult = $reviewmodels->where('nominationID', $id)->countAllResults();
      
        if ($reviewresult == 1) {
            $data1 = [
                'adminID' => $_SESSION['AdminID'],
                'nominationID' => $this->request->getVar('nomid'),
                'date'    => date("Y/m/d"),
                'review'  => sanitize_filename($this->request->getVar('text')),
            ];
            //print_r($data1);exit;
            $reviewmodels->set($data1)->where('nominationID', $id)->update();
            //print_r($reviewmodel);exit;
            if($reviewmodels){
                $nominationmodel = new NominationsModel();
            $review = [
                'review' => 1,
                
            ];
            $nominationmodel->update($id, $review,);
            return redirect()->to('higherEnd/keralasree/review/' . encode_url($id));
            }
        } else {
            $data = [
                'adminID' => $_SESSION['AdminID'],
                'nominationID' => $id,
                'date'    => date("Y/m/d"),
                'review'  => sanitize_filename($this->request->getVar('text')),
            ];

            $reviewmodels->where('nominationID', $id)->save($data);
            //print_r($reviewmodel);exit;
            $nominationmodel = new NominationsModel();
            $review = [
                'review' => 1
            ];
            $nominationmodel->update($id, $review);
            //print_r($$nominationmode);exit;
            return redirect()->to('higherEnd/keralashri/ksn');
        }
    }
    public function reviewView($eid){
        $id = decode_url($eid);
        $data = $this->common(); 
        $data['nomid']=$id; 
        $reviewmodels = new ReviewModel();
        $data['reviewcount'] = $reviewmodels->where('nominationID', $id)->countAllResults();
        //print_r($review);exit;
        if ($data['reviewcount']==1) {
            $data['review'] = $reviewmodels
            ->select('reviews.*,a.first_name,a.last_name')
            ->join('admin AS a', 'a.id = reviews.adminID', 'LEFT')
            ->where('nominationID', $id)->findAll();
            //print_r($reviewdata['data']);exit;
            echo view('higherEnd/reviewView',$data);
        } else {
        
            echo view('higherEnd/reviewView', $data);
        }
        
        
    }
    public function getallaccepted(){
    
        $data = $this->common();
        $data['reviewStatus'] = 1;
        $nomination = new NominationsModel();
        $data['year']  = $nomination->select('distinct(year)')->where('nominations.award', 3)->groupBy('year')->findAll();
        $data['maxyear']  = $nomination->select('max(year) as year')->where('nominations.award', 1)->first();
        $data['year_sel'] = $data['maxyear']['year'];
        
        $data['keralasree']  = $nomination
            ->select('nominations.*,s.state,d.district,t.taluk,a.title,aws.awardAreaSubTitle,r.fName,r.mName,r.lName,r.organisationName,r.regType,r.nominaterType,n.*,nominations.review as reviews,nominations.status as statuss,e.name as ename')
            ->join('state AS s', 's.stateID = nominations.state','LEFT')
            ->join('district AS d', 'd.districtID = nominations.district','LEFT')
            ->join('thaluk AS t', 't.talukID = nominations.taluk','LEFT')
            ->join('awardarea AS a', 'a.awardID = nominations.workArea','LEFT')
            ->join('awardareasub AS aws', 'aws.awardAreaSubID = nominations.workAreaSub','LEFT')
            ->join('registration AS r', 'r.id = nominations.userID','LEFT')
            ->join('nominator AS n', 'n.NomId = r.nominaterType','LEFT')
            ->join('esign AS e', 'e.nominationID = nominations.nominationID','LEFT')
            ->where('nominations.status', 1)
            ->where('nominations.award', 3)
            ->where('year in (select max(year) from nominations)')
            ->orderBy("nominations.nominationID", "asc")->findall();
            return $data;
    }
    public function accepted($printid){
        session();
        $data = $this->common();
        $data = $this->getallaccepted();
        $_SESSION['pid'] = $printid;
        $data['print'] = $_SESSION['pid'];
        $data['reviewStatus'] = 0;
        $data['pagename'] = 'Accepted Nomination';

        $nomination = new NominationsModel();
        $data['year']  = $nomination->select('distinct(year)')->where('nominations.award', 3)->groupBy('year')->findAll();
        $data['maxyear']  = $nomination->select('max(year) as year')->where('nominations.award', 1)->first();
        $data['year_sel'] = $data['maxyear']['year'];

        echo view('higherEnd/newNomination/keralaSree/list', $data);
    }
    public function getallrejected(){

        $data = $this->common();
        $data['reviewStatus'] = 1;

        $nomination = new NominationsModel();
        $data['year']  = $nomination->select('distinct(year)')->where('nominations.award', 3)->groupBy('year')->findAll();
        $data['maxyear']  = $nomination->select('max(year) as year')->where('nominations.award', 1)->first();
        $data['year_sel'] = $data['maxyear']['year'];
        
        $data['keralasree']  = $nomination
            ->select('nominations.*,s.state,d.district,t.taluk,a.title,aws.awardAreaSubTitle,r.fName,r.mName,r.lName,r.organisationName,r.regType,r.nominaterType,n.*,nominations.review as reviews,nominations.status as statuss,e.name as ename')
            ->join('state AS s', 's.stateID = nominations.state','LEFT')
            ->join('district AS d', 'd.districtID = nominations.district','LEFT')
            ->join('thaluk AS t', 't.talukID = nominations.taluk','LEFT')
            ->join('awardarea AS a', 'a.awardID = nominations.workArea','LEFT')
            ->join('awardareasub AS aws', 'aws.awardAreaSubID = nominations.workAreaSub','LEFT')
            ->join('registration AS r', 'r.id = nominations.userID','LEFT')
            ->join('nominator AS n', 'n.NomId = r.nominaterType','LEFT')
            ->join('esign AS e', 'e.nominationID = nominations.nominationID','LEFT')
            ->where('nominations.status', 2)
            ->where('nominations.award', 3)
            ->where('year in (select max(year) from nominations)')
            ->orderBy("nominations.nominationID", "asc")->findall();
            return $data;
    }
    public function rejected($printid){
        session();
        $nomination = new NominationsModel();
        
        $data = $this->common();
        $data = $this->getallrejected();
        $_SESSION['pid'] = $printid;
        $data['print'] = $_SESSION['pid'];
        $data['reviewStatus'] = 0;
        $data['pagename'] = 'Rejected Nomination';
        //print_r($data['user']);exit;

        echo view('higherEnd/newNomination/keralaSree/list', $data);
    }
}
