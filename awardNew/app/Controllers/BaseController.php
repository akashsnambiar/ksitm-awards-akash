<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\HTTP\CLIRequest;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 */
abstract class BaseController extends Controller
{
    /**
     * Instance of the main Request object.
     *
     * @var CLIRequest|IncomingRequest
     */
    protected $request;

    /**
     * An array of helpers to be loaded automatically upon
     * class instantiation. These helpers will be available
     * to all other controllers that extend BaseController.
     *
     * @var array
     */
    protected $helpers = [];

    /**
     * Be sure to declare properties for any property fetch you initialized.
     * The creation of dynamic property is deprecated in PHP 8.2.
     */
    // protected $session;

    /**
     * Constructor.
     */
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        // Preload any models, libraries, etc, here.

        // E.g.: $this->session = \Config\Services::session();

        //logout session inactivity checking
        
        $this->check_inactivity();


    }
     // Check for inactivity and update session timestamp
    protected function check_inactivity()
    {
        $session = \Config\Services::session(); 
        //front end user
        if ($session->has('UserLog'))
         {
            if ($session->has('last_activity'))
            {
                $lastActivityTime = $session->get('last_activity'); // Get the last activity timestamp from the session
                $timeout = 1800; // 30 minutes in seconds (30 * 60)
                if ((time() - $lastActivityTime) > $timeout)// Check if the last activity time is older than the timeout
                 { 
                    $session->destroy();// Destroy the session if the timeout is exceeded
                    return redirect()->to(base_url().'login');
                }
            }

        } 
        //admin user
        if ($session->has('AdminID'))
         {
            if ($session->has('adminlast_activity'))
            {
                $lastActivityTime = $session->get('adminlast_activity'); // Get the last activity timestamp from the session
                $timeout = 1800; // 30 minutes in seconds (30 * 60)
                if ((time() - $lastActivityTime) > $timeout)// Check if the last activity time is older than the timeout
                 { 
                    $session->destroy();// Destroy the session if the timeout is exceeded
                    return redirect()->to(base_url().'admin/login');
                }
            }

        } 
    }
    
}
