<?php

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Libraries\SiteSettings; // Import library
use App\Models\NotificationModel;
use App\Models\GalleryCategoryModel;

class Notification extends BaseController
{
    public function index()
    {
        $year="";
        $data = $this->getCommon();
		$data['pagetitle'] = 'Notification'; 
        $year = $this->request->getVar('year');

		$notificationModel = new NotificationModel();//create notification model
         //notification Years
         $data['notyear'] = $notificationModel
                            ->select('distinct (year)')
                            ->where('status', 1)
                            ->where('not_type', 'Y')
                            ->findAll();
        //general notifications
        $builder = $notificationModel->select('*');
        $builder ->where('status', 1)
        ->where('not_type', 'G')
        ->orderBy('id',"DESC");
        $data['notification'] = $builder->findAll();
		
       //echo $builder->getLastQuery();exit;
        
        //year wise notifications 
        $prevnotificationModel = new NotificationModel();
        $builder = $prevnotificationModel->select('*')
                ->where('status', 1)
                ->where('not_type', 'Y');
        if (($year) != "") {        
                $builder->where('year', $year);
        }
        else{
            $builder->where('year in (select max(year) from notification WHERE status = 1)');   
        }
        $builder->orderBy('id',"DESC"); 
        $data['prev_not'] = $builder->findAll(); 

        if (($year) == "") {
            $prevnotify_year = new NotificationModel();
            $builder = $prevnotify_year->select( 'year')
                    ->where('status', 1)
                    ->where('not_type', 'Y');
            $builder->where('year in (select max(year) from notification WHERE status = 1)');   
            $data['prev_not_yr'] = $builder->first(); 
            if (!empty($data['prev_not_yr']['year'])) {
                $year =  $data['prev_not_yr']['year'];
                }
        }
        //echo $builder->getLastQuery();
        $data['year_sel'] =$year;
		echo view('notification',$data);

    }
    function sub($eid,$yrid)
    {
        $year="";
        $id = base64_decode($eid);
        $year = base64_decode($yrid);
        
        $data = $this->getCommon();
        $data['pagetitle'] = 'വിജ്ഞാപനം';
        $notificationModel = new NotificationModel();//create notification model
        
        //notification Years
        $data['notyear'] = $notificationModel
                        ->select('distinct (year)')
                        ->where('status', 1)
                        ->where('not_type', 'Y')
                        ->findAll();
        //general notifications
        $builder = $notificationModel->select('*');
        $builder ->where('status', 1)
                 ->where('not_type', 'G')
                 ->orderBy('id',"DESC");
        $data['notification'] = $builder->findAll();
        //yearwise notifications
        $prevnotificationModel = new NotificationModel();//create prev notification model
        $builder = $prevnotificationModel->select('*');
        $builder ->where('status', 1)
                ->where('not_type', 'Y');
        if (($year) != "") {        
        $builder->where('year', $year);
        }
        else{
            $builder->where('year in (select max(year) from notification WHERE status = 1)');   
        }
        $builder->orderBy('id',"DESC"); 
        $data['prev_not'] = $builder->findAll(); 

        //notification details based on id        
        $data['not_dis'] = $notificationModel->where('id', $id)->first();

        $data['year_sel'] =$year;
        echo view('notification',$data);

		
	}
    function prevsub()
    {
        $year="";
        $year = $this->request->getVar('year');
        $data = $this->getCommon();
        $data['pagetitle'] = 'വിജ്ഞാപനം';
        //previous notification menu based on year
        if (($year) != "") {
            $prevnotificationModel = new NotificationModel();//create prev notification model
            $builder = $prevnotificationModel->select('*');
            $builder ->where('status', 1)
                    ->where('not_type', 'Y');
            $builder->where('year', $year);
            $builder->orderBy('id',"DESC"); 
            $data1 = $builder->findAll(); 
            echo json_encode($data1);     
        }
	}
     //start site settings
     public function getCommon()
     {
         $sitesettngs = new SiteSettings(); // create an instance of Library
         $res =$sitesettngs->get_site_settings(); // calling method
         $data	=	array();
         foreach($res as $row){
             $data1[$row->config_key] = $row->config_value;
         }
         $data['Titletag'] = $data1['title_tags_e'];
         $data['metadescription'] = $data1['meta_description_e'];
         $data['metakeywords'] = $data1['meta_tags_e'];
         $data['site_name'] = $data1['site_name_e'];
         $data['copyright_year'] = $data1['copyright_year']; 
         return $data;  
     }  
}
