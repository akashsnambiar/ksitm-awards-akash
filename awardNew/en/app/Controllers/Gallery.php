<?php

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Libraries\SiteSettings; // Import library
use App\Models\GalleryModel;
use App\Models\GalleryCategoryModel;

class Gallery extends BaseController
{
    public function getCommon()
    {
        $sitesettngs = new SiteSettings();
        $res = $sitesettngs->get_site_settings();
        $data   =   array();
        foreach ($res as $row) {
            $data1[$row->config_key] = $row->config_value;
        }
        $data['Titletag'] = $data1['title_tags_e'];
        $data['metadescription'] = $data1['meta_description_e'];
        $data['metakeywords'] = $data1['meta_tags_e'];
        $data['site_name'] = $data1['site_name_e'];
        $data['copyright_year'] = $data1['copyright_year'];
        return $data;
    }
    public function index()
    {
        $data = $this->getCommon();
        $data['pagetitle'] = 'ഗ്യാലറി';
        
        $year = $this->request->getVar('year');

        $galCategoryModel = new GalleryCategoryModel();
        $data['galcat'] = $galCategoryModel
            ->select('distinct(year)')
            ->where('status', 1)
            ->findAll();

            $data['currentyear'] = $galCategoryModel
            ->select ('max(year) as year')
            ->where('status', 1)
            ->first();
            $data['curr_year'] = $data['currentyear']['year'];

        $data['gallery'] = $galCategoryModel
            ->select('gallery_category.title as title,gallery_category.title_e as tile_e,gallery_category.year as year, gallery.title as gtitle, gallery.title_e as gtitle_e,gal_img,field,field_e')
            ->join('gallery AS gallery', 'gallery.cat_id = gallery_category.id','LEFT')
            ->where('year in (select max(year) from gallery_category)')
            ->where('gallery.status', 1)
            ->findAll();
           // echo $galCategoryModel->db->getLastQuery();exit; 
        echo view('gallery', $data);
    }
    public function galleryCategory()
    {
        $year = $this->request->getVar('year');

        $data = $this->getCommon();
        $galCategoryModel = new GalleryCategoryModel();
        $data['gallery'] = $galCategoryModel
            ->select('gallery_category.title as title,gallery_category.title_e as tile_e,gallery_category.year as year, gallery.title as gtitle, gallery.title_e as gtitle_e,gal_img,field,field_e')
            ->join('gallery AS gallery', 'gallery.cat_id = gallery_category.id', 'LEFT')
            ->where('year', $year)
            ->where('gallery.status', 1)
            ->findAll();
           // echo $galCategoryModel->db->getLastQuery();exit;         
        echo json_encode($data);
    }
    function sub($id)
    {

        $id = base64_decode($id);

        $sitesettngs = new SiteSettings(); // create an instance of Library
        $res = $sitesettngs->get_site_settings(); // calling method

        foreach ($res as $row) {
            $data1[$row->config_key] = $row->config_value;
        }

        $data    =    array();
        $data['Titletag'] = $data1['title_tags'];
        $data['metadescription'] = $data1['meta_description'];
        $data['metakeywords'] = $data1['meta_tags'];
        $data['site_name'] = $data1['site_name'];
        $data['copyright_year'] = $data1['copyright_year'];

        $galModel = new GalleryModel(); //create gallery  model
        $data['gallery'] = $galModel->where('cat_id', $id, 'status', 1)->findall();

        $galCategoryModel = new GalleryCategoryModel(); //create gallery category model
        $data['gal_menu'] = $galCategoryModel->where('status', 1)->findall();

        $galCatModel = new GalleryCategoryModel(); //create gallery category model
        $data['gal_cat_menu'] = $galCatModel->where('id', $id)->first();
        //$data['pagetitle'] =$data['gal_cat_menu']['title'];
        $data['pagetitle'] = 'Gallery';
        // print_r($data);exit;
        echo view('gallery', $data);
    }
}
