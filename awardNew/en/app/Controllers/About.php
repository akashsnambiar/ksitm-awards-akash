<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Libraries\SiteSettings; // Import library
use App\Models\AboutModel;
use App\Models\GalleryCategoryModel;

class About extends BaseController
{
    public function index()
    {
        $data	=	array();  
        $data = $this->getCommon();
        $data['pagetitle'] = 'About Us';

        $aboutModel = new AboutModel();//create about model
        $data['aboutus'] = $aboutModel->where('id', 1)->First();
        
        $galCategoryModel = new GalleryCategoryModel();//create gallery category model
        $data['gal_menu'] = $galCategoryModel->where('status', 1)->findAll();
       
        echo view('about',$data);
    }
    
    function keralajyothi(){
        $data = $this->getCommon();
        $data['pagetitle'] = 'Kerala Jyothi';
        
        $aboutModel = new AboutModel();//create about model
        $data['keralajyothi'] = $aboutModel->where('id', 3)->First();
        //print_r($data);exit;
        
        $galCategoryModel = new GalleryCategoryModel();//create gallery category model
        $data['gal_menu'] = $galCategoryModel->where('status', 1)->findAll();

        echo view('keralajyothi',$data);
       }
       
    function keralaprabha(){
        $data = $this->getCommon();
        $data['pagetitle'] = 'Kerala Prabha';

        $aboutModel = new AboutModel();//create about model
        $data['keralaprabha'] = $aboutModel->where('id', 4)->First();
        //print_r($data);exit;
        
        $galCategoryModel = new GalleryCategoryModel();//create gallery category model
        $data['gal_menu'] = $galCategoryModel->where('status', 1)->findAll();

        echo view('keralaprabha',$data);
       }
       
    function keralashri(){
        $data = $this->getCommon();
        $data['pagetitle'] ='Kerala Shri';

        $aboutModel = new AboutModel();//create about model
        $data['keralashri'] = $aboutModel->where('id', 2)->First();
        //print_r($data);exit;
        
        $galCategoryModel = new GalleryCategoryModel();//create gallery category model
        $data['gal_menu'] = $galCategoryModel->where('status', 1)->findAll();

        echo view('keralashri',$data);
       }

     //start site settings
    public function getCommon()
    {
        $sitesettngs = new SiteSettings(); // create an instance of Library
        $res =$sitesettngs->get_site_settings(); // calling method
        $data	=	array();
        foreach($res as $row){
            $data1[$row->config_key] = $row->config_value;
        }
        $data['Titletag'] = $data1['title_tags_e'];
        $data['metadescription'] = $data1['meta_description_e'];
        $data['metakeywords'] = $data1['meta_tags_e'];
        $data['site_name'] = $data1['site_name_e'];
        $data['copyright_year'] = $data1['copyright_year']; 
        return $data;  
    }  
}
