<?php

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Libraries\SiteSettings; // Import library
use App\Models\MediaModel;
use App\Models\GalleryCategoryModel;

class Media extends BaseController
{
    public function index()
    {
        $data = $this->getCommon();
		$data['pagetitle'] = 'Media';

		$mediaModel = new MediaModel();//create media model
        $data['media'] = $mediaModel->where('status', 1)->findAll();
		
        $galCategoryModel = new GalleryCategoryModel();//create gallery category model
        $data['gal_menu'] = $galCategoryModel->where('status', 1)->findAll();
       
		 //print_r($data);//exit;
		echo view('media',$data);
    }

    //start site settings
    public function getCommon()
    {
        $sitesettngs = new SiteSettings(); // create an instance of Library
        $res =$sitesettngs->get_site_settings(); // calling method
        $data	=	array();
        foreach($res as $row){
            $data1[$row->config_key] = $row->config_value;
        }
        $data['Titletag'] = $data1['title_tags_e'];
        $data['metadescription'] = $data1['meta_description_e'];
        $data['metakeywords'] = $data1['meta_tags_e'];
        $data['site_name'] = $data1['site_name_e'];
        $data['copyright_year'] = $data1['copyright_year']; 
        return $data;  
    }    
}
