<?php

namespace App\Libraries;
//use App\Models\Home;
//$db = \Config\Database::connect();
class SiteSettings
{

    protected $db;
    public function __construct()
    {
       $this->db = db_connect();
    } 
    // This function gets site settings
    public function get_site_settings()
    {
        $result_settings	=  $this->db->query('SELECT * FROM settings');
		$results	=$result_settings->getResult();
        return $results;
    }
}