<?php echo view('includes/header'); ?>
<?php echo view('includes/menu'); ?>

<div class="clearfix"> </div>
<section class="about-us">
    <div class="container">
        <div class="ntn_cls">
        </div>
        <div class="row slection-nav">
            <div class="col-md-3 col-sm-3 col-xs-12 text-center">
            <p> 
            <select class="btn btn-default dropdown-toggle notif-dropdown" id="year_lbl">
                <option value="">Select Year</option>
                <?php
                if (isset($notyear)){ 
                foreach ($notyear as $rows) { ?>
                    <option value="<?php echo $rows['year']; ?>" <?php if ($year_sel==$rows['year'])  { echo 'selected'; }?> ><?php echo $rows['year'] ?></option>
                <?php } }?>
            </select>
            </p>
            <p> Year wise Notifications </p>
            <div id="notDiv">
            <?php if (isset($prev_not)) { ?>
                <?php  foreach ($prev_not as $row) { ?>
                    <p class="dashNotification">
                        <a class="notificationLink" id="ctl00_ContentPlaceHolder2_LinkButton1" href="<?php echo base_url(); ?>notification/sub/<?php echo base64_encode($row['id']); ?>/<?php echo base64_encode($row['year']); ?>"><?php echo $row['not_title_e']; ?></a>
                    </p>
            <?php }
            } else { ?>
                <p class="dashNotification" id="errormsg">No Notifications</p>
           <?php }?>
           </div>
            <p> General Notifications </p>
            
            <?php if (isset($notification)) { ?>
                <?php  foreach ($notification as $row) { ?>
                    <p class="dashNotification">
                        <a class="notificationLink" id="ctl00_ContentPlaceHolder2_LinkButton1" href="<?php echo base_url(); ?>notification/sub/<?php echo base64_encode($row['id']); ?>/<?php echo base64_encode($row['year']); ?>"><?php echo $row['not_title']; ?></a>
                    </p>
            <?php }
            } ?>
            </div>
           <!-- general notifications  -->
            <?php if (isset($not_dis)) { ?>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <div class="pdf-box">
                        <iframe src="<?php echo act_url(); ?>public/notifications/<?php echo $not_dis['not_pdf']; ?>" id="ctl00_ContentPlaceHolder2_frame" class="pdfbox"></iframe>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>

<?php echo view('includes/footer'); ?>
<script>
    $(".notif-dropdown").change(function() {
        var year = $('#year_lbl').val();
        url = "<?php echo base_url(); ?>notification/prevsub/" + year;
        $.ajax({
                type: "POST",
                url: url,
                data: { year: year },
                success: function(response) {
                    var data = JSON.parse(response);
                    if (data != "") {
                        var notcount = data.length;
                            $("#notDiv").html("");
                            data.forEach(function(value) {
                            var subid =btoa(value.id)+'/'+btoa(year);
                            var txt_lbl='<p class="dashNotification"><a class="notificationLink" id="ctl00_ContentPlaceHolder2_LinkButton1" href="<?php echo base_url(); ?>notification/sub/'+subid+'/">'+value.not_title+'</a></p>';
                            $("#notDiv").append(txt_lbl);
                            });
                        } else {
                            $("#errormsg").show();
                            var error = "No notifications";
                            $("#errormsg").html(error);
                        }
                    }
                
            });


    });
  </script>
<!-- /footer -->
</body>

</html>