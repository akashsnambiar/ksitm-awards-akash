<?php echo view('includes/nomination/header');?>
  <style>
  #nav {
    display: inline-block;
    padding-left: 0;
    margin: 20px 0;
    border-radius: 4px;
}
#nav a {
    position: relative;
    float: left;
    padding: 6px 12px;
    margin-left: -1px;
    line-height: 1.42857143;
    color: #337ab7;
    text-decoration: none;
    background-color: #fff;
    border: 1px solid #ddd;
	
}
#nav a:first-child {
    margin-left: 0;
    border-top-left-radius: 4px;
    border-bottom-left-radius: 4px;
}
#nav a:last-child {
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
}
#nav .active{
    z-index: 3;
    color: #fff;
    cursor: default;
    background-color: #337ab7;
    border-color: #337ab7;
}
.norecord{
font-size:14px;
color:#006600;
font-weight:bold;
}

.clsnom{
margin-left: 10px;
color: #FFFFFF;
text-decoration: none;
background-color: #339933;
margin-left:10px;
padding: 5px 20px;
float: right;
}
.clsnom:hover {
  color: #fff;
  background-color: #339933;
  border-color: #b02a37;
}
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 100%;
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

a.clkreview
{
  color:#0d6efd;
}
a:hover.clkreview  {
  color: #0d6efd;
}
  </style>

<script>  
$(document).ready (function () {  
    $('#data').after ('<div id="nav"></div>');  
    var rowsShown = 10;  
    var rowsTotal = $('#data tbody tr').length;  
    var numPages = rowsTotal/rowsShown;  
    for (i = 0;i < numPages;i++) {  
        var pageNum = i + 1;  
        $('#nav').append ('<a href="#" rel="'+i+'">'+pageNum+'</a> ');  
    }  
    $('#data tbody tr').hide();  
    $('#data tbody tr').slice (0, rowsShown).show();  
    $('#nav a:first').addClass('active');  
    $('#nav a').bind('click', function() {  
    $('#nav a').removeClass('active');  
   $(this).addClass('active');  
        var currPage = $(this).attr('rel');  
        var startItem = currPage * rowsShown;  
        var endItem = startItem + rowsShown;  
        $('#data tbody tr').css('opacity','0.0').hide().slice(startItem, endItem).  
        css('display','table-row').animate({opacity:1}, 300);  
    });  
}); 
function myReview(nomid)
{
  
  jQuery.ajax({
    url: "<?php echo base_url();?>nomination/review",
    data: 'nomid='+nomid,
    method: 'POST',
    success: function(data) {
    if (data)
    {
      $('#myModal').modal('show');
      $('.modal').css("z-index","9999"); 
      $("textarea[id='resultDiv']").html(data);  
    } 
    },
    error: function(data) {
      // Some error in ajax call
      alert("some Error");
    }
});

} 
</script>  
<!-- <script src="<?php //echo act_url();?>public/js/login/jquery-3.4.1.min.js"></script>  -->
<script>
  function myReview(nomid)
  {
    jQuery.ajax({
    url: "<?php echo base_url();?>nomination/review",
    data: 'nomid='+nomid,
    method: 'POST',
    success: function(data) {
    if (data)
    {
      $('#myModal').modal('show');
      $('.modal').css("z-index","9999"); 
      $("textarea[id='resultDiv']").html(data);  
    } 
    },
    error: function(data) {
      // Some error in ajax call
      alert("some Error");
    }
});

} 
</script> 
<link rel="stylesheet" href="<?php echo act_url();?>public/css/css/nomination.css" />
  <main class="d-flex align-items-center min-vh-100 py-3 py-md-0">
    <div class="container">
      <div class="card login-card">
        <div class="row no-gutters">
          <div class="col-md-12">
            <div class="card-body">
              <div class="brand-wrapper log-class-menu">
               <img onclick="redirect('<?php echo base_url();?>')" src="<?php echo act_url();?>public/images/login/logo.png" alt="logo" class="logo">
                <a style="margin-left:10px;" class="login-menu" href="<?php echo base_url();?>login/logout">Logout</a>
                <a style="margin-left:10px;" class="login-menu" href="<?php echo base_url();?>Account/changePassword">Change Password</a>
                <a class="login-menu" href="<?php echo base_url();?>nomination/myAccount"> Welcome, 
                  <?php if (isset($_SESSION['displayName']))
                  {
                    echo $_SESSION['displayName'];//print_r($userAward);
                  }?></a>
                  </div> <hr/>
                  <?php if (isset( $nomperiod) && $nomperiod==1) {?>
                  <?php $seturl='';
                  if(($userAward['keralashri']==1)&&($userAward['keralaprabha']==1)&&($userAward['keralajhothi']==1)){?><?php }else{?>
                        <?php
                              if (isset($status))
                              {
                                $session = session();
                                if (isset($_SESSION['NomLog'])) { $session->remove('NomLog'); }
                                $_SESSION['NomLog']= $status['nomDemoID'];
                               $nomdemoid = $_SESSION['NomLog'];
                                if ($status['step']==1)
                                {
                                  $seturl='nomination2';
                                }
                                elseif($status['step']==2)
                                {
                                  $seturl='nomination3';
                                }
                                elseif($status['step']==3)
                                {
                                  $seturl='nomination4';
                                }
                                else{
                                  $seturl='myAccount';
                                }
                              }
                             else
                              {
                                $seturl= 'nomination';
                              }
                        ?>
        <a class="clsnom" href="<?php echo base_url();?>nomination" >
         <i class="fa fa-plus"></i>&nbsp;&nbsp; New Nomination </a>
         <?php if (isset($status)){
                if (($status['step']==1)|| ($status['step']==2)|| ($status['step']==3)){?>
          <a href="<?php echo base_url().$seturl;?>" class="btn btn-danger pull-right">
         <i class="fa fa-plus"></i>&nbsp;&nbsp; Proceed Existing Nomination </a>
         <?php }}?>
         <br/><br/><br/><br/>
			   <?php } ?><?php }?>
			   <?php if($userCount!=0){?>
			   <table class="table table-striped" id="data" style="align=center" >
				  <thead>
					<tr style="font-size:14px;">
					  <th scope="col">No.</th>
					  <th scope="col">Reg. No.</th>
					  <th scope="col">Nominated Person</th>
					  <th scope="col">Field of Service</th>
					  <th scope="col">Photo</th>
					  <th scope="col">Year</th>
					  <th scope="col">Status</th>
					</tr>
				  </thead>
				  <tbody>
				  
				  <?php $i=1; //$upload_path = FCPATH.'public/images/nomination/';
          $path = str_replace('/en', '', FCPATH);
          $upload_path = $path . 'public/images/nomination/';?>
				  <?php foreach ($user as $row) { ?>
					<tr style="font-size:14px;">
					  <th scope="row"><?php echo $i;?></th>
					  <td><a href="<?php echo base_url();?>nomination/details/<?php echo base64_encode($row['nominationID']);?>"><?php echo $row['nominationRegID'];?></a></td>
					  <td><?php echo $row['name'];?></td>
					  <td><?php echo $row['title_e'];?></td>
					  <td>
            <?php 
             if(file_exists($upload_path.$row['photo']) && $row['photo']!='') 
             {?><img src="<?php echo act_url();?>public/images/nomination/<?php echo $row['photo'];?>"  width="75" height="75"/>
             <?php } 
             else {?>
             <img src="<?php echo act_url();?>public/images/Image_Available.jpg"  width="75" height="75"/>
             <?php }?>
            </td>
            <td><?php echo $row['year'];?></td>
            <td>
					  <?php if($row['status']==0){?>
					        <a class="btn btn-warning"><b>Pending</b></a></td>
					  <?php }elseif($row['status']==1){?>
					        <a class="btn btn-success"><b>Accepted</b></a></td>
					  <?php }elseif($row['status']==2){?>
					        <a class="btn btn-danger"><b>Rejected</b></a><br>
                <a class="btn clkreview" onclick="myReview('<?php echo  $row['nominationID']?>')">Click Here</a></td>
					  <?php } ?>
           
					</tr>
					<?php $i+=1; ?>
					 <?php } ?>
					 
				  </tbody>
				</table>
			   <?php }else{?>
					 
					 <p class="norecord">You have not submitted any nomination. To proceed press the New Nomination button above .</p><br/><br/>
					
					 <?php } ?>
			    <?php echo view('includes/nomination/footer');?>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </main>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Review</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body" >
              <div class="mb-3">
                <textarea readonly name="text" class="form-control" rows="2" cols="200" style="height:100px;" id="resultDiv"></textarea>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
    </div>
</div>
	
</body>
 <?php echo view('includes/nomination/scriptDetails');?>

</html>
