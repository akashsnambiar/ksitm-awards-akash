<?php echo view('includes/header');?>
<?php echo view('includes/inner_page');?>


<div class="clearfix"></div>
<!-- About us section -->
<section class="about-us">
    <div class="container">
        <div class="row">
            <div class="col-md-12 inner-top">
            <?php if(isset($keralashri)) {?>
                <h3><?php echo $keralashri['title_e']; ?></h3>
                <?php echo $keralashri['text_e']; ?>
                <?php }?>
                
            </div>
        </div>
    </div>
</section>
<?php echo view('includes/help');?>	

 <!-- footer -->
<?php echo view('includes/award');?>
<?php echo view('includes/footer');?>
 <!-- /footer -->
</body>
</html>
