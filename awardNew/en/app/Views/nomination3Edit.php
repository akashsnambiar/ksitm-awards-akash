<?php echo view('includes/nomination/header');?>
<link rel="stylesheet" href="<?php echo act_url();?>public/css/css/nomination.css" />
  <main class="d-flex align-items-center min-vh-100 py-3 py-md-0">
    <div class="container">
      <div class="card login-card">
        <div class="row no-gutters">
          
          <div class="col-md-12">
            <div class="card-body">
              <div class="brand-wrapper log-class-menu">
               <img onclick="redirect('<?php echo base_url();?>')" src="<?php echo act_url();?>public/images/login/logo.png" alt="logo" class="logo">
				<a style="margin-left:10px;" class="login-menu" href="<?php echo base_url();?>login/logout">Logout</a>
			<a class="login-menu" href="<?php echo base_url();?>nomination/myAccount"> Welcome, 
			<?php if (isset($_SESSION['UserLog'])){ echo $_SESSION['displayName'];}?>
			</a>
              </div> <hr/>
			   <h2 class="login-card-title title-head nomh2-head">Nomination</h2>
			   <?php /*?> <?php if($nomCnt==0){?><?php */?>
			  <div id="demoStyle">
			<ul id="progressbar" class="li_lbl">
							<li class="active" id="step1">
								<strong>Details of the Person Nominated</strong>
							</li>
							<li class="active" id="step2"><strong>Details of Nomination</strong></li>
							<li class="active" id="step3"><strong>Influence of the Activities of the Nominated Person</strong></li>
							<li id="step4"><strong>Summary </strong></li>
							<li id="step5"><strong>Finish</strong></li>
						</ul>
						<div class="progress">
							<div class="progress-bar"></div>
						</div> <br>
						<?php echo view('includes/nomination/step3Edit');?>
				</div>		
              
			    <?php echo view('includes/nomination/footer');?>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </main>


	
</body>
 <?php echo view('includes/nomination/script3');?>

</html>
