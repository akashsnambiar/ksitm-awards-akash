<?php echo view('includes/header'); ?>
<?php echo view('includes/menu'); ?>

<section>
    <div class="container">
        <div class="row mb-3 justify-content-center">
            <div class="col-10 text-center">
                <select class="btn btn-default btn-lg dropdown-toggle winner-dropdown" id="year">
                    <option value="">Year</option>
                    <?php  
                    foreach ($galcat as $rows) { ?>
                        <option value="<?php echo $rows['year']; ?>"<?php if ($curr_year==$rows['year'])  { echo 'selected'; }?>><?php echo $rows['year'] ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="row photos" id="ceremony"></div>
        </div>

        <div class="row" id="gallery-img">
            <!-- <div class="intro">
            <h2 class="text-center"><?php // print_r($galcategory); 
                                    ?></h2>
            </div> -->
            <?php foreach ($gallery as $row) { ?>
                <div class="col-sm-6 col-md-4 col-lg-4 item">
                    <div class="thumb-nail mb-3">
                        <a href="<?php echo act_url();
                                    ?>public/images/gallery/<?php echo $row['gal_img'];
                                                                ?>" data-lightbox="photos"><img class="imgfluid img_cls" src="<?php echo act_url();
                                                                                                                            ?>public/images/gallery/<?php echo $row['gal_img'];
                                                                                                                                                    ?>"></a>
                        <p class="cls_p1"><?php echo $row['title_e']; ?></p>
                        <p class="cls_p2"><?php echo $row['field_e']; ?></p>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<?php echo view('includes/footer'); ?>

<!-- /footer -->
</body>
<script>
    $(document).ready(function() {
        $(".winner-dropdown").change(function() {
            var year = $('#year').val();
            url = "<?php echo base_url(); ?>Gallery/galleryCategory";

            $.ajax({
                type: "POST",
                url: url,
                data: {
                    year: year
                },
                success: function(response) {

                    var data = JSON.parse(response);
                    var gallery = data.gallery;
                    console.log("data", data.gallery);

                    $("#ceremony").html("");
                    gallery.forEach(function(value) {
                        var ur = "<?php echo act_url(); ?>public/images/gallery/" + value.gal_img;
                        var dynamicImage = $("<img>").attr("src", ur).addClass("imgfluid img_cls");
                        var title = value.gtitle_e;
                        var field = value.field_e;
                        var div = $('<div>').addClass("col-sm-6 col-md-4 col-lg-4 item");
                        var div1 = $('<div>').addClass("thumb-nail mb-3");
                        var div2 = $('<div>').addClass("cls_p1");
                        var div3 = $('<div>').addClass("cls_p2");
                        $(div1).append(dynamicImage);
                        $(div2).append(title);
                        $(div3).append(field);
                        div.append(div1);
                        div1.append(div2);
                        div1.append(div3);
                        $("#ceremony").append(div);
                        $("#gallery-img").hide();
                    });

                },
                error: function(error) {
                    console.log("Error:", error);
                }
            });
        });

    });
</script>

</html>