<?php echo view('includes/nomination/header');?>
<link rel="stylesheet" href="<?php echo act_url();?>public/css/css/nomination.css" />
  <main class="d-flex align-items-center min-vh-100 py-3 py-md-0">
    <div class="container">
      <div class="card login-card">
        <div class="row no-gutters">
          <div class="col-md-12">
            <div class="card-body">
              <div class="brand-wrapper log-class-menu">
               <img onclick="redirect('<?php echo base_url();?>')" src="<?php echo act_url();?>public/images/login/logo.png" alt="logo" class="logo">
                  <a style="margin-left:10px;" class="login-menu" href="<?php echo base_url();?>login/logout">Logout</a>
                  <a class="login-menu" href="<?php echo base_url();?>nomination/myAccount"> Welcome, 
                  <?php if (isset($_SESSION['UserLog'])){ echo $_SESSION['displayName'];}?></a>
                  </div> <hr/>
			  <div id="demoStyle">
						 <br>
						<?php echo view('includes/nomination/details');?>
				</div>		
                <?php /*?><?php }else{ ?>
			        <?php echo $this->load->view('includes/nomination/nom-submit','',TRUE);?>
			    <?php } ?><?php */?>
			    <?php echo view('includes/nomination/footer');?>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </main>
   <?php echo view('includes/nomination/scriptDetails');?>
</body>
</html>
