<?php echo view('includes/header'); ?>
<?php echo view('includes/menu'); ?>
<!---------------------- SLIDER ------------------------->
<form name="login">
<?= csrf_field() ?>
</form>
<div class="owl-carousel owl-theme hero-slider">
    <div class="slide slide1">
        <div class="container mt-3">
            <figure class="text-center">
                <blockquote class="blockquote">
                    <?php foreach ($banner as $row) {
                        if ($row['category_id'] == 1) { ?>
                            <p class="display-6 text-white ms-lg-5"><?Php echo $row['banner_title_en'];
                                                                } ?></p>
                        <?php } ?>
                </blockquote>
            </figure>
            <div class="row mt-5">
                <div class="col-4 d-flex justify-content-center">
                    <div class="banner-img">
                        <?php foreach ($banner as $row) {
                            if ($row['category_id'] == 1) { ?>
                                <img src="<?php echo act_url(); ?>public/images/banner/<?php echo $row['category_image']; ?>" />
                        <?php }
                        } ?>
                    </div>
                </div>
                <div class="col-8 justify-content-center">
                    <div class="row justify-content-center">
                        <?php foreach ($winner as $row) {
                            if ($row['category_id'] == 1) { ?>
                                <div class="col-4">
                                    <div class="awards p-3">
                                        <img src="<?php echo act_url(); ?>public/images/banner/<?php echo $row['winner_photo']; ?>" class="awd-img" />
                                    </div>
                                </div>
                        <?php }
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="slide slide2">
        <div class="container">
            <figure class="text-center">
                <blockquote class="blockquote">
                    <?php foreach ($banner as $row) {
                        if ($row['category_id'] == 2) { ?>
                            <p class="display-6 text-white ms-lg-5"><?Php echo $row['banner_title_en'];
                                                                } ?></p>
                        <?php } ?>
                </blockquote>
            </figure>
            <div class="row mt-5">
                <div class="col-4 d-flex justify-content-center">
                    <div class="banner-img">
                        <?php foreach ($banner as $row) {
                            if ($row['category_id'] == 2) { ?>
                                <img src="<?php echo act_url(); ?>public/images/banner/<?php echo $row['category_image']; ?>" />
                        <?php }
                        } ?>
                    </div>
                </div>
                <div class="col-8 justify-content-center">
                    <div class="row">
                        <?php foreach ($winner as $row) {
                            if ($row['category_id'] == 2) { ?>
                                <div class="col-4">
                                    <div class="awards p-3">
                                        <img src="<?php echo act_url(); ?>public/images/banner/<?php echo $row['winner_photo']; ?>" class="awd-img" />
                                    </div>
                                </div>
                        <?php }
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="slide slide3">
        <div class="container">
            <figure class="text-center">
                <blockquote class="blockquote">
                    <?php foreach ($banner as $row) {
                        if ($row['category_id'] == 2) { ?>
                            <p class="display-6 text-white ms-lg-5"><?Php echo $row['banner_title_en'];
                                                                } ?></p>
                        <?php } ?>
                </blockquote>
            </figure>
            <div class="row mt-5">
                <div class="col-4 d-flex justify-content-center">
                    <div class="banner-img">
                        <?php foreach ($banner as $row) {
                            if ($row['category_id'] == 3) { ?>
                                <img src="<?php echo act_url(); ?>public/images/banner/<?php echo $row['category_image']; ?>" />
                        <?php }
                        } ?>
                    </div>
                </div>
                <div class="col-8 justify-content-center">
                    <div class="row">
                        <?php foreach ($winner as $row) {
                            if ($row['category_id'] == 3) { ?>
                                <div class="col-4">
                                    <div class="awards p-3">
                                        <img src="<?php echo act_url(); ?>public/images/banner/<?php echo $row['winner_photo']; ?>" class="awd-img" />

                                    </div>
                                </div>
                        <?php }
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!------------------------- /SLIDER ----------------------------->
<!------------------------- News Marquee ------------------------------>
<div class="container mt-5">
    <?php 
    //if (isset( $nomperiod) && $nomperiod==1) {
    foreach ($nomperiod as $row) { ?>
        <marquee class="news_marquee"><?php echo $row['news_feed_eng'] ?></marquee>
    <?php   } ?>
</div>
<!------------------------- /News Marquee ----------------------------->

<!------------------ About Kerala Puraskaram--------------------->
<section>
    <div class="container">
        <div>
            <h3 class="about-label"><?php echo $home['title_e']; ?></h3>
        </div>
        <p><?php echo $home['text_e']; ?></p>
    </div>
</section>
<!------------------/About Kerala Puraskaram --------------------->
<!-- MILESTONE -->
<section id="milestone">
    <div class="container">
        <div class="row text-center justify-content-center gy-4">
            <div class="col-lg-2 col-sm-6">
                <h1 class="display-4"><?php echo $registered; ?></h1>
                <p class="mb-0">Registrations</p>
            </div>
            <div class="col-lg-2 col-sm-6">
                <h1 class="display-4"><?php echo $nominations; ?></h1>
                <p class="mb-0">Nominations</p>
            </div>
            <div class="col-lg-2 col-sm-6">
                <h1 class="display-4"><?php echo $nominationAccepted; ?></h1>
                <p class="mb-0">Accepted Nominations</p>
            </div>
        </div>
    </div>
</section>
<!-- MILESTONE -->

<!-- About Categories-->
<section id="about">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5 py-5">
                <div class="row">

                    <div class="col-12">
                        <div class="info-box">
                            <img src="<?php echo act_url() ?>public/kp/img/keralajyothi.png" alt="icon">
                            <div class="ms-4">
                                
                                <?php foreach ($keralajyothi as $row) { ?>
                                    <h5><?php echo $row['title_e'] ?></h5>
                                    <p class="award-para"><?php echo $row['text_e'] ?></p>
                                <?php  } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-4">
                        <div class="info-box">
                            <img src="<?php echo act_url() ?>public/kp/img/keralaprabha.png" alt="icon">
                            <div class="ms-4">
                               
                                <?php foreach ($keralaprabha as $row) { ?>
                                    <h5><?php echo $row['title_e'] ?></h5>
                                    <p class="award-para"><?php echo $row['text_e'] ?></p>
                                <?php  } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-4">
                        <div class="info-box">
                            <img src="<?php echo act_url() ?>public/kp/img/keralashri.png" alt="icon">
                            <div class="ms-4">
                                
                                <?php foreach ($keralashri as $row) { ?>
                                    <h5><?php echo $row['title_e'] ?></h5>
                                    <p class="award-para"><?php echo $row['text_e'] ?></p>
                                <?php  } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 star-space">
                <img src="<?php echo act_url() ?>public/kp/img/awd4.png" alt="icon" class="star-img">
            </div>
        </div>
    </div>
</section>
<!--------------- Who can Nominate --------------->
<section class="who-nominates">
    <div class="container">
        <div class="text-center">
            <h3 class="text-white nominate-label"><?php echo $home['title2_e']; ?></h3>
        </div>
        <div class="row justify-content-center mt-5">

            <?php
            if (isset($whocanapply)) {
                foreach ($whocanapply as $row) { ?>
                    <div class="col-sm-4" style="font-size: 16px; margin-bottom:25px;">
                        <span class="mdi mdi-account px-1"></span>
                        <?php echo $row['title_e']; ?>
                    </div>
            <?php }
            } ?>
        </div>
    </div>
</section>
<!--------------- Who can Nominate --------------->
<!--------------- Consideration for the awards --------------->
<div class="consideration pt-5">
    <div class="container">
        <div class="text-center">
            <h3 class="consider-label"><?php echo $home['title3_e']; ?></h3>
        </div>
        <div class="row justify-content-center mt-5">
            <?php
            if (isset($contri)) {
                foreach ($contri as $row) { ?>
                    <div class="col-sm-4" style="font-size: 15px; margin-bottom:25px;"><span class="mdi mdi-trophy-award"></span> <?php echo $row['title_e']; ?></div>
            <?php }
            } ?>
        </div>
    </div>
</div>
<!--------------- /Consideration for the awards --------------->
<!--------------- Who Decides --------------->
<section class="who-decides">
    <div class="container">
        <div class="">
            <h3 class="consider-label"><?php echo $home['title4_e']; ?></h3>
        </div>
        <div class="row">
            <p><?php echo $home['text4_e']; ?></p>
            <div class="col-sm-12">
                <div class="row" style="margin:15px 0px;">
                    <?php
                    if (isset($whodecides)) {
                        foreach ($whodecides as $row) { ?>
                            <div class="whodecides" style="font-family:Verdana, Geneva, sans-serif;"><a href="<?php echo act_url() ?>public/whodecides/<?php echo $row['document']; ?>" target="_blank"><?php echo $row['whodecides']; ?></a></div>
                    <?php }
                    } ?>
                </div>
            </div>
        </div>
</section>
<!--------------- /Who Decides --------------->
<?php echo view('includes/footer'); ?>
