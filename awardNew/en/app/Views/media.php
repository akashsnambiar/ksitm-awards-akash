<?php echo view('includes/header'); ?>
<?php echo view('includes/menu'); ?>

<div class="clearfix"></div>
<!-- About us section -->
<section class="about-us">
    <div class="container">
        <div class="row">
            <div class="clearfix"></div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <?php if (isset($media)) {
                    foreach ($media as $row) { ?>

                        <h5><b><?php echo $row['title']; ?></b></h5>
                        <?php echo $row['embed'] ?>

                <?php }
                } ?>
            </div>
        </div>
    </div>
</section>
<?php echo view('includes/help'); ?>
<?php echo view('includes/award'); ?>
<?php echo view('includes/footer'); ?>
<!-- /footer -->
</body>

</html>