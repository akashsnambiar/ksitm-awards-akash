<?php echo view('includes/header'); ?>
<?php echo view('includes/menu'); ?>

<section>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-sm-12">
                <div id="result"></div>
                <div class="row mb-3">
                    <div class="winner-sort">
                        <div class="col d-grid gap-2 p-1">
                            <select class="btn btn-default dropdown-toggle winner-dropdown" id="awardID" placeholder="Awards">
                                <option value="">Award</option>
                                <?php
                                foreach ($nomcat as $rows) { ?>
                                    <option value="<?php echo $rows['id']; ?>"><?php echo $rows['nomination_category'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div id="result"></div>
                        <div class="col d-grid gap-2 p-1">
                            <select class="btn btn-default dropdown-toggle winner-dropdown" id="year">
                                <option value="">Year</option>
                                <?php
                                foreach ($winneryear as $rows) { ?>
                                    <option value="<?php echo $rows['year']; ?>"><?php echo $rows['year'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col d-grid gap-2 p-1">
                            <select class="btn btn-default dropdown-toggle winner-dropdown" id="field">
                                <option value="">Field</option>
                                <?php
                                foreach ($awardarea as $rows) { ?>
                                    <option value="<?php echo $rows['awardID']; ?>"><?php echo $rows['title'] ?></option>
                                <?php } ?>
                            </select>
                        </div>

                    </div>
                </div>
                <div class="card">
                    <div class="row">
                        <div class="d-flex">
                            <div class="col d-grid gap-2"><button class="btn btn-danger kj clickable-element" data-id="1">Kerala Jyothi</button></div>
                            <div class="col d-grid gap-2"><button class="btn btn-danger kp clickable-element" data-id="2">Kerala Prabha</button></div>
                            <div class="col d-grid gap-2"><button class="btn btn-danger ks clickable-element" data-id="3">Kerala Shri</button></div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="row">
                        <div class="d-flex">
                            <div class="col d-grid gap-2 justify-content-center award-count" id="wkj"><?php echo $winner_kj; ?></div>
                            <div class="col d-grid gap-2 justify-content-center award-count" id="wkp"><?php echo $winner_kp; ?></div>
                            <div class="col d-grid gap-2 justify-content-center award-count" id="wks"><?php echo $winner_ks; ?></div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="row" id="winnerImg">
                        <?php foreach ($winner as $row) {
                        ?>
                            <div class="col-4 col-lg-3 col-xs-6">
                                <div class="winnerimg" data-bs-toggle="tooltip" data-bs-placement="top" title="<?php echo $row['nomination_category'] ?>[<?php echo $row['year'] ?>]
<?php echo $row['winner_name'] ?>

<?php echo $row['title'] ?>">
                                    <img src="<?php echo act_url();  ?>public/images/banner/<?php echo $row['winner_photo'];  ?>" class="awd-img" />
                                </div>
                                <p class="winnername"><?php echo $row['winner_name'];  ?></p>
                            </div>

                        <?php
                        }
                        ?>
                    </div>
                    <div class="row">
                        <div class="row" id="awardDiv"></div>
                    </div>
                    <div id="errormsg" class="errormsg p-5"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo view('includes/footer'); ?>
<!-- /footer -->
</body>
<script>
    $(document).ready(function() {
        $(".winner-dropdown").change(function() {
            var award = $('#awardID').val();
            var awardID = award;
            var year = $('#year').val();
            var field = $('#field').val();

            if (award != "") {
                url = "<?php echo base_url(); ?>Winner/winnerSearch/" + award;
            } else if (year != "") {
                url = "<?php echo base_url(); ?>Winner/winnerSearch/" + year;
            } else if (field != "") {
                url = "<?php echo base_url(); ?>Winner/winnerSearch/" + field;
            }
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    award: awardID,
                    year: year,
                    field: field
                },
                success: function(response) {

                    var data = JSON.parse(response);
                    console.log("data", data);
                    var winnerResult = data.winnerResult;
                    var datacount = data.datacount;
                    console.log("datacount", data.datacount);

                    var wincount = data.datacount;

                    $("#awardDiv").html("");
                    $("#errormsg").html("");

                    if (winnerResult != "") {
                        $("#awardDiv").html("");
                        winnerResult.forEach(function(value) {
                            var ur = "<?php echo act_url(); ?>public/images/banner/" + value.winner_photo;
                            var dynamicImage = $("<img>").attr("src", ur).addClass("awd-img");
                            var winnername = value.winner_name;
                            var div = $('<div>').addClass("col-4 col-lg-3 col-xs-6");
                            var div1 = $('<div>').addClass("winnerimg");
                            var div2 = $('<p>').addClass("winnername");
                            $(div1).append(dynamicImage);
                            $(div2).append(winnername);
                            div.append(div1);
                            div.append(div2);
                            $("#awardDiv").append(div);
                            $("#winnerImg").hide();
                            $("#errormsg").hide();
                        });

                        $("#wkj").html(0);
                        $("#wkp").html(0);
                        $("#wks").html(0);
                        datacount.forEach(function(value) {

                            var winnercount = value.count

                            if (value.id == 1) {
                                $("#wkj").html(winnercount);
                            }
                            if (value.id == 2) {
                                $("#wkp").html(winnercount);
                            }
                            if (value.id == 3) {
                                $("#wks").html(winnercount);
                            }
                        });
                    } else {
                        $("#errormsg").show();
                        var error = "No awards for these selection";
                        $("#errormsg").html(error);
                        $("#wks").html("-");
                        $("#wkj").html("-");
                        $("#wkp").html("-");
                        console.log("No awards for these selection");
                    }
                },
                error: function(error) {
                    console.log("Error:", error);
                }
            });
        });

    });
    document.addEventListener("DOMContentLoaded", function() {
        // Get all elements with the class 'clickable-element'
        const clickableElements = document.querySelectorAll(".clickable-element");

        clickableElements.forEach(function(element) {
            element.addEventListener("click", function() {
                // Extract the ID from the 'data-id' attribute
                const award = element.getAttribute("data-id");

                var year = $('#year').val();
                var field = $('#field').val();

                if (award != "") {
                    awardID = award;
                    url = "<?php echo base_url(); ?>Winner/winnerSearch/" + award;
                } else if (year != "") {
                    awardID = award;
                    url = "<?php echo base_url(); ?>Winner/winnerSearch/" + year;
                } else if (field != "") {
                    awardID = award;
                    url = "<?php echo base_url(); ?>Winner/winnerSearch/" + field;
                }

                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        award: award,
                        year: year,
                        field: field
                    },
                    success: function(response) {

                        var data = JSON.parse(response);
                        console.log("data", data);
                        var winnerResult = data.winnerResult;
                        var datacount = data.datacount;
                        console.log("datacount", winnerResult.length);
                        var wincount = winnerResult.length;

                        $("#awardDiv").html("");
                        $("#errormsg").html("");

                        if (winnerResult != "") {
                            winnerResult.forEach(function(value) {
                                var ur = "<?php echo act_url(); ?>public/images/banner/" + value.winner_photo;
                                var dynamicImage = $("<img>").attr("src", ur).addClass("awd-img");
                                var winnername = value.winner_name;
                                var div = $('<div>').addClass("col-4 col-lg-3 col-xs-6");
                                var div1 = $('<div>').addClass("winnerimg");
                                var div2 = $('<p>').addClass("winnername");
                                $(div1).append(dynamicImage);
                                $(div2).append(winnername);
                                div.append(div1);
                                div.append(div2);
                                $("#awardDiv").append(div);
                                $("#winnerImg").hide();
                                $("#errormsg").hide();

                                var awardSelected = value.category_id;
                                updateDropdownValue(awardSelected);

                                // console.log("wcat", awardSelected);
                                $("#wkj").html(0);
                                $("#wkp").html(0);
                                $("#wks").html(0);
                                if (value.category_id == 1) {
                                    //console.log("kj",count)
                                    $("#wkj").html(wincount);
                                    $("#wkp").html(0);
                                    $("#wks").html(0);
                                } else if (value.category_id == 2) {
                                    $("#wkp").html(wincount);
                                    $("#wkj").html(0);
                                    $("#wks").html(0);
                                } else if (value.category_id == 3) {
                                    $("#wks").html(wincount);
                                    $("#wkj").html(0);
                                    $("#wkp").html(0);
                                }

                            });
                            datacount.forEach(function(value) {
                                console.log("nummm", value.numbers)
                            });
                        } else {
                            $("#errormsg").show();
                            var error = "No awards for these selection";
                            $("#errormsg").html(error);
                            $("#wks").html("-");
                            $("#wkj").html("-");
                            $("#wkp").html("-");
                            console.log("No awards for these selection");
                        }
                    },
                    error: function(error) {
                        console.log("Error:", error);
                    }
                });
            });
        });

    });

    function updateDropdownValue(responseData) {
        // Check if the responseData contains a valid value to update the dropdown
        if (responseData) {
            // Select the dropdown element by its ID
            const dropdown = document.getElementById("awardID");
            // Loop through the options and set the selected option based on the responseData
            for (let i = 0; i < dropdown.options.length; i++) {

                if (dropdown.options[i].value === responseData) {
                    dropdown.selectedIndex = i;
                    break; // Exit the loop once the correct option is found
                }
            }
        }
    }
</script>

</html>