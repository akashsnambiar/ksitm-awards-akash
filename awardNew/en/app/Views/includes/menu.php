<body data-bs-spy="scroll" data-bs-target=".navbar" data-bs-offset="70">
    <!-- TOP NAV -->
    <div class="top-nav" id="home">
        <div class="container">
        <div class="row justify-content-between">
                <div class="col-auto">
                    <span class="logo-name">Kerala Puraskarangal</span>
                    <!-- <p> <i class='bx bxs-envelope'></i> info@example.com</p>
                    <p> <i class='bx bxs-phone-call'></i> 0471-2518531</p> -->
                </div>
                <div class="col-auto social-icons">
                    <!-- <a href="#"><i class='bx bxl-facebook'></i></a>
                    <a href="#"><i class='bx bxl-twitter'></i></a>
                    <a href="#"><i class='bx bxl-instagram'></i></a>
                    <a href="#"><i class='bx bxl-pinterest'></i></a> -->
                    <p> <i class='bx bxs-envelope'></i> info@example.com</p>
                    <p> <i class='bx bxs-phone-call'></i> 0471-2518531</p>
                </div>
            </div>
        </div>
    </div>

    <!-- BOTTOM NAV -->
    <nav id="myheader" class="navbar navbar-expand-lg navbar-light bg-white sticky-top">
        <div class="container d-flex">
            <a class="navbar-brand" href="#"><img src="<?php echo act_url() ?>public/kp/img/logo.png" alt="logo" class="logo-img" /></span></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item">
                        <a <?php if ($pagetitle == 'Home') { ?>class="nav-link active" <?php } else { ?>class="nav-link" <?php } ?> href="<?php echo base_url(); ?>"><i class="fa fa-home" aria-hidden="true" style="font-size:23px;"></i></a>
                    </li>
                    <li class="nav-item">
                        <a <?php if ($pagetitle == 'About Us') { ?>class="nav-link active" <?php } else { ?>class="nav-link" <?php } ?> href="<?php echo base_url(); ?>about">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a <?php if ($pagetitle == 'Notification') { ?>class="nav-link active" <?php } else { ?>class="nav-link" <?php } ?> href="<?php echo base_url(); ?>notification">Notification</a>
                    </li>
                    <li class="nav-item">
                        <a <?php if ($pagetitle == 'Media') { ?>class="nav-link active" <?php } else { ?>class="nav-link" <?php } ?> href="<?php echo base_url(); ?>media">Media</a>
                    </li> 
                    <li class="nav-item">
                        <a <?php if ($pagetitle == 'winners') { ?>class="nav-link active" <?php } else { ?>class="nav-link" <?php } ?> href="<?php echo base_url(); ?>Winner">Winners </a>
                    </li>
                    <li class="nav-item">
                        <a <?php if ($pagetitle == 'Gallery') { ?>class="nav-link active" <?php } else { ?>class="nav-link" <?php } ?> href="<?php echo base_url(); ?>gallery">AwardCeremony </a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link" target="_blank" href="<?php echo base_url(); ?>login">Nomination </a>
                    </li>
                    <li class="nav-item dropdown">
                         <a class="btn btn-brand ms-lg-3 nav-link text-white" href="<?php echo act_url(); ?>"  role="button">
                             മലയാളം
                        </a>
                        
                    </li>
                </ul>
               
            </div>
        </div>
    </nav>

    <script>
    document.addEventListener("DOMContentLoaded", function () {
        const navLinks = document.querySelectorAll('.navbar .navbar-nav .nav-link.active');
        const sections = document.querySelectorAll('.navbar .navbar-nav .nav-link.active');

        function isElementInViewport(el) {
            const rect = el.getBoundingClientRect();
            return (
                rect.top >= 0 &&
                rect.left >= 0 &&
                rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                rect.right <= (window.innerWidth || document.documentElement.clientWidth)
            );
        }

        function setNavLinkActive() {
            sections.forEach((section, index) => {
                const link = navLinks[index];
                if (isElementInViewport(section)) {
                    link.classList.add("active");
                } else {
                    link.classList.remove("active");
                }
            });
        }

        setNavLinkActive();

        window.addEventListener("scroll", setNavLinkActive);
    });
</script>
