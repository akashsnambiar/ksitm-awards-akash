<section class="pa-any__query">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d-flex justify-content-center mb-5">

				<div class="col-md-4">
					<div class="image-container">
						<img src="<?php echo act_url(); ?>public/images/keralajyothi.png" onclick="redirect('<?php echo base_url(); ?>about/keralajyothi')" alt="Kerala Jyothi">
						<span>Kerala Jyothi</span>
						<div class="overlay"><span>Kerala Jyothi</span></div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="image-container">
					<img src="<?php echo act_url(); ?>public/images/keralaprabha.png" onclick="redirect('<?php echo base_url(); ?>about/keralaprabha')" alt="Kerala Prabha">
						<span>Kerala Prabha</span>
						<div class="overlay"><span>Kerala Prabha</span></div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="image-container">
					<img src="<?php echo act_url(); ?>public/images/keralashri.png" onclick="redirect('<?php echo base_url(); ?>about/keralashri')" alt="Kerala Shri">
						<span>Kerala Shri </span>
						<div class="overlay"><span>Kerala Shri </span></div>
					</div>
				</div>
			</div>
		</div>
</section>