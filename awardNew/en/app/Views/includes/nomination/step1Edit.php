<form id="demoForm" method="POST" action="<?php echo base_url();?>nomination/saveEdit" enctype="multipart/form-data" class="frmcls">
<?= csrf_field() ?>
	<h2 class="nomh2">Details of the person to be Nominated</h2><br/><br/>
	<div class="err_text" id="err"></div>
			<div class="row">
			<div class="col-md-12 nomdiv">
					<div class="col-md-6">
						<div class="form-group  fl">
							<label for="inputCity"  class="inputlbl">Name  <span class="mandatory">*</span></label>
							<input type="text" class="form-control ba b--black-20 pa2 mb2 db" id="name" name="name" placeholder="Name" value="<?php echo $nomEdit['name'];?>">
						</div>
				</div>
				<div class="col-md-6">
						<div class="form-group  fl">
							<label for="inputCity"  class="inputlbl">DOB  <span class="mandatory"></span></label>
							<input type="date" class="form-control ba b--black-20 pa2 mb2 db" id="dob" name="dob" placeholder="Dob" onBlur="calculateAge();" value="<?php echo $nomEdit['dob'];?>">
						</div>
				</div>
				<div class="col-md-6">
						<div class="form-group fl">
						<label for="inputCity"  class="inputlbl">Age <span class="mandatory"></span></label>
							<input type="number" class="form-control ba b--black-20 pa2 mb2 db" id="age" name="age" placeholder="Age" value="<?php echo $nomEdit['age'];?>">
						</div>
				</div>
				<div class="col-md-6">
						<div class="form-group fl">
						<label for="inputCity"  class="inputlbl">Sex <span class="mandatory"></span></label>
							<select id="sex" name="sex" class="form-control">
								<option value="">--Select--</option>
								<option <?php if($nomEdit['sex']==1){?>selected="selected"<?php } ?> value="1">Female </option>
								<option <?php if($nomEdit['sex']==2){?>selected="selected"<?php } ?> value="2">Male </option>
								<option <?php if($nomEdit['sex']==3){?>selected="selected"<?php } ?> value="3">Transgender </option>
							</select>
						</div>	
				</div>
				<div class="col-md-6">
						<div class="form-group fl">
						<label for="inputCity"  class="inputlbl">State <span class="mandatory"></span></label>
							<select id="state" name="state" class="form-control">
							<option value="">--Select--</option>
							<?php foreach ($state as $row) { ?>
						<option value="<?php echo $row['stateID'];?>" <?php if($nomEdit['state']==$row['stateID']){?>selected="selected"<?php } ?>><?php echo $row['state'];?></option>
					<?php } ?>
					</select>
						</div>
						
				</div>
				<div class="col-md-6">
						<div class="form-group fl">
						<label for="inputCity"  class="inputlbl">District<span class="mandatory"></span></label>
							<select id="district" name="district" class="form-control">
							<option value="">--Select--</option>
							<?php foreach ($district as $row) { ?>
						<option value="<?php echo $row['districtID'];?>" <?php if($nomEdit['district']==$row['districtID']){?>selected="selected"<?php } ?>data-chained="<?php echo $row['stateID'];?>"><?php echo $row['district'];?></option>
					<?php } ?>
					</select>
						</div>
						
				</div>
				<div class="col-md-6">
						<div class="form-group fl">
						<label for="inputCity"  class="inputlbl">Taluk  <span class="mandatory"></span></label>
						<select id="taluk" name="taluk" class="form-control">
							<option value="">--Select--</option>
							<?php foreach ($taluk as $row) { ?>
						<option value="<?php echo $row['talukID'];?>" <?php if($nomEdit['taluk']==$row['talukID']){?>selected="selected"<?php } ?> data-chained="<?php echo $row['districtID']?>"><?php echo $row['taluk'];?></option>
							<?php } ?>
							</select>
						</div>
						
				</div>
				<div class="col-md-6">
						<div class="form-group fl">
						<label for="inputCity"  class="inputlbl">Place of Birth <span class="mandatory"></span></label>
							<input type="text" class="form-control ba b--black-20 pa2 mb2 db" id="pob" name="pob" placeholder="Place of Birth" value="<?php echo $nomEdit['pob'];?>">
						</div>
						
				</div>
				<div class="col-md-6">
						<div class="form-group fl">
						<label for="inputCity"  class="inputlbl">Address<span class="mandatory">*</span></label>
							<input type="text" class="form-control ba b--black-20 pa2 mb2 db" id="address" name="address" placeholder="Address" value="<?php echo $nomEdit['address'];?>">
						</div>
						
				</div>
				<div class="col-md-6">
						<div class="form-group fl">
						<label for="inputCity"  class="inputlbl">E-Mail  <span class="mandatory"></span></label>
							<input type="text" class="form-control ba b--black-20 pa2 mb2 db" id="email" name="email" placeholder="E-Mail" value="<?php echo $nomEdit['email'];?>">
						</div>
						
				</div>
				<div class="col-md-6">
						<div class="form-group fl">
						<label for="inputCity"  class="inputlbl">Country <span class="mandatory"></span></label>
								<select id="country" name="country" class="form-control">
									<option value="Afganistan" <?php if($nomEdit['country']=="Afganistan"){?>selected="selected"<?php } ?>>Afghanistan</option>
									<option value="Albania" <?php if($nomEdit['country']=="Albania"){?>selected="selected"<?php } ?>>Albania</option>
									<option value="Algeria" <?php if($nomEdit['country']=="Algeria"){?>selected="selected"<?php } ?>>Algeria</option>
									<option value="American Samoa" <?php if($nomEdit['country']=="American Samoa"){?>selected="selected"<?php } ?>>American Samoa</option>
									<option value="Andorra" <?php if($nomEdit['country']=="Andorra"){?>selected="selected"<?php } ?>>Andorra</option>
									<option value="Angola" <?php if($nomEdit['country']=="Angola"){?>selected="selected"<?php } ?>>Angola</option>
									<option value="Anguilla" <?php if($nomEdit['country']=="Anguilla"){?>selected="selected"<?php } ?>>Anguilla</option>
									<option value="Antigua & Barbuda" <?php if($nomEdit['country']=="Antigua & Barbuda"){?>selected="selected"<?php } ?>>Antigua & Barbuda</option>
									<option value="Argentina" <?php if($nomEdit['country']=="Argentina"){?>selected="selected"<?php } ?>>Argentina</option>
									<option value="Armenia" <?php if($nomEdit['country']=="Armenia"){?>selected="selected"<?php } ?>>Armenia</option>
									<option value="Aruba" <?php if($nomEdit['country']=="Aruba"){?>selected="selected"<?php } ?>>Aruba</option>
									<option value="Australia" <?php if($nomEdit['country']=="Australia"){?>selected="selected"<?php } ?>>Australia</option>
									<option value="Austria" <?php if($nomEdit['country']=="Austria"){?>selected="selected"<?php } ?>>Austria</option>
									<option value="Azerbaijan" <?php if($nomEdit['country']=="Azerbaijan"){?>selected="selected"<?php } ?>>Azerbaijan</option>
									<option value="Bahamas" <?php if($nomEdit['country']=="Bahamas"){?>selected="selected"<?php } ?>>Bahamas</option>
									<option value="Bahrain" <?php if($nomEdit['country']=="Bahrain"){?>selected="selected"<?php } ?>>Bahrain</option>
									<option value="Bangladesh" <?php if($nomEdit['country']=="Bangladesh"){?>selected="selected"<?php } ?>>Bangladesh</option>
									<option value="Barbados" <?php if($nomEdit['country']=="Barbados"){?>selected="selected"<?php } ?>>Barbados</option>
									<option value="Belarus" <?php if($nomEdit['country']=="Belarus"){?>selected="selected"<?php } ?>>Belarus</option>
									<option value="Belgium" <?php if($nomEdit['country']=="Belgium"){?>selected="selected"<?php } ?>>Belgium</option>
									<option value="Belize" <?php if($nomEdit['country']=="Belize"){?>selected="selected"<?php } ?>>Belize</option>
									<option value="Benin" <?php if($nomEdit['country']=="Benin"){?>selected="selected"<?php } ?>>Benin</option>
									<option value="Bermuda" <?php if($nomEdit['country']=="Bermuda"){?>selected="selected"<?php } ?>>Bermuda</option>
									<option value="Bhutan" <?php if($nomEdit['country']=="Bhutan"){?>selected="selected"<?php } ?>>Bhutan</option>
									<option value="Bolivia" <?php if($nomEdit['country']=="Bolivia"){?>selected="selected"<?php } ?>>Bolivia</option>
									<option value="Bonaire" <?php if($nomEdit['country']=="Bonaire"){?>selected="selected"<?php } ?>>Bonaire</option>
									<option value="Bosnia & Herzegovina" <?php if($nomEdit['country']=="Bosnia & Herzegovina"){?>selected="selected"<?php } ?>>Bosnia & Herzegovina</option>
									<option value="Botswana" <?php if($nomEdit['country']=="Botswana"){?>selected="selected"<?php } ?>>Botswana</option>
									<option value="Brazil" <?php if($nomEdit['country']=="Brazil"){?>selected="selected"<?php } ?>>Brazil</option>
									<option value="British Indian Ocean Ter" <?php if($nomEdit['country']=="British Indian Ocean Ter"){?>selected="selected"<?php } ?>>British Indian Ocean Ter</option>
									<option value="Brunei" <?php if($nomEdit['country']=="Brunei"){?>selected="selected"<?php } ?>>Brunei</option>
									<option value="Bulgaria" <?php if($nomEdit['country']=="Bulgaria"){?>selected="selected"<?php } ?>>Bulgaria</option>
									<option value="Burkina Faso" <?php if($nomEdit['country']=="Burkina Faso"){?>selected="selected"<?php } ?>>Burkina Faso</option>
									<option value="Burundi" <?php if($nomEdit['country']=="Burundi"){?>selected="selected"<?php } ?>>Burundi</option>
									<option value="Cambodia" <?php if($nomEdit['country']=="Cambodia"){?>selected="selected"<?php } ?>>Cambodia</option>
									<option value="Cameroon" <?php if($nomEdit['country']=="Cameroon"){?>selected="selected"<?php } ?>>Cameroon</option>
									<option value="Canada" <?php if($nomEdit['country']=="Canada"){?>selected="selected"<?php } ?>>Canada</option>
									<option value="Canary Islands" <?php if($nomEdit['country']=="Canary Islands"){?>selected="selected"<?php } ?>>Canary Islands</option>
									<option value="Cape Verde" <?php if($nomEdit['country']=="Cape Verde"){?>selected="selected"<?php } ?>>Cape Verde</option>
									<option value="Cayman Islands" <?php if($nomEdit['country']=="Cayman Islands"){?>selected="selected"<?php } ?>>Cayman Islands</option>
									<option value="Central African Republic" <?php if($nomEdit['country']=="Central African Republic"){?>selected="selected"<?php } ?>>Central African Republic</option>
									<option value="Chad" <?php if($nomEdit['country']=="Chad"){?>selected="selected"<?php } ?>>Chad</option>
									<option value="Channel Islands" <?php if($nomEdit['country']=="Channel Islands"){?>selected="selected"<?php } ?>>Channel Islands</option>
									<option value="Chile" <?php if($nomEdit['country']=="Chile"){?>selected="selected"<?php } ?>>Chile</option>
									<option value="China" <?php if($nomEdit['country']=="China"){?>selected="selected"<?php } ?>>China</option>
									<option value="Christmas Island" <?php if($nomEdit['country']=="Christmas Island"){?>selected="selected"<?php } ?>>Christmas Island</option>
									<option value="Cocos Island" <?php if($nomEdit['country']=="Cocos Island"){?>selected="selected"<?php } ?>>Cocos Island</option>
									<option value="Colombia" <?php if($nomEdit['country']=="Colombia"){?>selected="selected"<?php } ?>>Colombia</option>
									<option value="Comoros" <?php if($nomEdit['country']=="Comoros"){?>selected="selected"<?php } ?>>Comoros</option>
									<option value="Congo" <?php if($nomEdit['country']=="Congo"){?>selected="selected"<?php } ?>>Congo</option>
									<option value="Cook Islands" <?php if($nomEdit['country']=="Cook Islands"){?>selected="selected"<?php } ?>>Cook Islands</option>
									<option value="Costa Rica" <?php if($nomEdit['country']=="Costa Rica"){?>selected="selected"<?php } ?>>Costa Rica</option>
									<option value="Cote DIvoire" <?php if($nomEdit['country']=="Cote DIvoire"){?>selected="selected"<?php } ?>>Cote DIvoire</option>
									<option value="Croatia" <?php if($nomEdit['country']=="Croatia"){?>selected="selected"<?php } ?>>Croatia</option>
									<option value="Cuba" <?php if($nomEdit['country']=="Cuba"){?>selected="selected"<?php } ?>>Cuba</option>
									<option value="Curaco" <?php if($nomEdit['country']=="Curaco"){?>selected="selected"<?php } ?>>Curacao</option>
									<option value="Cyprus" <?php if($nomEdit['country']=="Cyprus"){?>selected="selected"<?php } ?>>Cyprus</option>
									<option value="Czech Republic" <?php if($nomEdit['country']=="Czech Republic"){?>selected="selected"<?php } ?>>Czech Republic</option>
									<option value="Denmark" <?php if($nomEdit['country']=="Denmark"){?>selected="selected"<?php } ?>>Denmark</option>
									<option value="Djibouti" <?php if($nomEdit['country']=="Djibouti"){?>selected="selected"<?php } ?>>Djibouti</option>
									<option value="Dominica" <?php if($nomEdit['country']=="Dominica"){?>selected="selected"<?php } ?>>Dominica</option>
									<option value="Dominican Republic" <?php if($nomEdit['country']=="Dominican Republic"){?>selected="selected"<?php } ?>>Dominican Republic</option>
									<option value="East Timor" <?php if($nomEdit['country']=="East Timor"){?>selected="selected"<?php } ?>>East Timor</option>
									<option value="Ecuador" <?php if($nomEdit['country']=="Ecuador"){?>selected="selected"<?php } ?>>Ecuador</option>
									<option value="Egypt" <?php if($nomEdit['country']=="Egypt"){?>selected="selected"<?php } ?>>Egypt</option>
									<option value="El Salvador" <?php if($nomEdit['country']=="El Salvador"){?>selected="selected"<?php } ?>>El Salvador</option>
									<option value="Equatorial Guinea" <?php if($nomEdit['country']=="Equatorial Guinea"){?>selected="selected"<?php } ?>>Equatorial Guinea</option>
									<option value="Eritrea" <?php if($nomEdit['country']=="Eritrea"){?>selected="selected"<?php } ?>>Eritrea</option>
									<option value="Estonia" <?php if($nomEdit['country']=="Estonia"){?>selected="selected"<?php } ?>>Estonia</option>
									<option value="Ethiopia" <?php if($nomEdit['country']=="Ethiopia"){?>selected="selected"<?php } ?>>Ethiopia</option>
									<option value="Falkland Islands" <?php if($nomEdit['country']=="Falkland Islands"){?>selected="selected"<?php } ?>>Falkland Islands</option>
									<option value="Faroe Islands" <?php if($nomEdit['country']=="Faroe Islands"){?>selected="selected"<?php } ?>>Faroe Islands</option>
									<option value="Fiji" <?php if($nomEdit['country']=="Fiji"){?>selected="selected"<?php } ?>>Fiji</option>
									<option value="Finland" <?php if($nomEdit['country']=="Finland"){?>selected="selected"<?php } ?>>Finland</option>
									<option value="France" <?php if($nomEdit['country']=="France"){?>selected="selected"<?php } ?>>France</option>
									<option value="French Guiana" <?php if($nomEdit['country']=="French Guiana"){?>selected="selected"<?php } ?>>French Guiana</option>
									<option value="French Polynesia" <?php if($nomEdit['country']=="French Polynesia"){?>selected="selected"<?php } ?>>French Polynesia</option>
									<option value="French Southern Ter" <?php if($nomEdit['country']=="French Southern Ter"){?>selected="selected"<?php } ?>>French Southern Ter</option>
									<option value="Gabon" <?php if($nomEdit['country']=="Gabon"){?>selected="selected"<?php } ?>>Gabon</option>
									<option value="Gambia" <?php if($nomEdit['country']=="Gambia"){?>selected="selected"<?php } ?>>Gambia</option>
									<option value="Georgia" <?php if($nomEdit['country']=="Georgia"){?>selected="selected"<?php } ?>>Georgia</option>
									<option value="Germany" <?php if($nomEdit['country']=="Germany"){?>selected="selected"<?php } ?>>Germany</option>
									<option value="Ghana" <?php if($nomEdit['country']=="Ghana"){?>selected="selected"<?php } ?>>Ghana</option>
									<option value="Gibraltar" <?php if($nomEdit['country']=="Gibraltar"){?>selected="selected"<?php } ?>>Gibraltar</option>
									<option value="Great Britain" <?php if($nomEdit['country']=="Great Britain"){?>selected="selected"<?php } ?>>Great Britain</option>
									<option value="Greece" <?php if($nomEdit['country']=="Greece"){?>selected="selected"<?php } ?>>Greece</option>
									<option value="Greenland" <?php if($nomEdit['country']=="Greenland"){?>selected="selected"<?php } ?>>Greenland</option>
									<option value="Grenada" <?php if($nomEdit['country']=="Grenada"){?>selected="selected"<?php } ?>>Grenada</option>
									<option value="Guadeloupe" <?php if($nomEdit['country']=="Guadeloupe"){?>selected="selected"<?php } ?>>Guadeloupe</option>
									<option value="Guam" <?php if($nomEdit['country']=="Guam"){?>selected="selected"<?php } ?>>Guam</option>
									<option value="Guatemala" <?php if($nomEdit['country']=="Guatemala"){?>selected="selected"<?php } ?>>Guatemala</option>
									<option value="Guinea" <?php if($nomEdit['country']=="Guinea"){?>selected="selected"<?php } ?>>Guinea</option>
									<option value="Guyana" <?php if($nomEdit['country']=="Guyana"){?>selected="selected"<?php } ?>>Guyana</option>
									<option value="Haiti" <?php if($nomEdit['country']=="Haiti"){?>selected="selected"<?php } ?>>Haiti</option>
									<option value="Hawaii" <?php if($nomEdit['country']=="Hawaii"){?>selected="selected"<?php } ?>>Hawaii</option>
									<option value="Honduras" <?php if($nomEdit['country']=="Honduras"){?>selected="selected"<?php } ?>>Honduras</option>
									<option value="Hong Kong" <?php if($nomEdit['country']=="Hong Kong"){?>selected="selected"<?php } ?>>Hong Kong</option>
									<option value="Hungary" <?php if($nomEdit['country']=="Hungary"){?>selected="selected"<?php } ?>>Hungary</option>
									<option value="Iceland" <?php if($nomEdit['country']=="Iceland"){?>selected="selected"<?php } ?>>Iceland</option>
									<option value="Indonesia" <?php if($nomEdit['country']=="Indonesia"){?>selected="selected"<?php } ?>>Indonesia</option>
									<option value="India"  <?php if($nomEdit['country']=="India"){?>selected="selected"<?php } ?>>India</option>
									<option value="Iran" <?php if($nomEdit['country']=="Iran"){?>selected="selected"<?php } ?>>Iran</option>
									<option value="Iraq" <?php if($nomEdit['country']=="Iraq"){?>selected="selected"<?php } ?>>Iraq</option>
									<option value="Ireland" <?php if($nomEdit['country']=="Ireland"){?>selected="selected"<?php } ?>>Ireland</option>
									<option value="Isle of Man" <?php if($nomEdit['country']=="Isle of Man"){?>selected="selected"<?php } ?>>Isle of Man</option>
									<option value="Israel" <?php if($nomEdit['country']=="Israel"){?>selected="selected"<?php } ?>>Israel</option>
									<option value="Italy" <?php if($nomEdit['country']=="Italy"){?>selected="selected"<?php } ?>>Italy</option>
									<option value="Jamaica" <?php if($nomEdit['country']=="Jamaica"){?>selected="selected"<?php } ?>>Jamaica</option>
									<option value="Japan" <?php if($nomEdit['country']=="Japan"){?>selected="selected"<?php } ?>>Japan</option>
									<option value="Jordan" <?php if($nomEdit['country']=="Jordan"){?>selected="selected"<?php } ?>>Jordan</option>
									<option value="Kazakhstan" <?php if($nomEdit['country']=="Kazakhstan"){?>selected="selected"<?php } ?>>Kazakhstan</option>
									<option value="Kenya" <?php if($nomEdit['country']=="Kenya"){?>selected="selected"<?php } ?>>Kenya</option>
									<option value="Kiribati" <?php if($nomEdit['country']=="Kiribati"){?>selected="selected"<?php } ?>>Kiribati</option>
									<option value="Korea North" <?php if($nomEdit['country']=="Korea North"){?>selected="selected"<?php } ?>>Korea North</option>
									<option value="Korea South" <?php if($nomEdit['country']=="Korea South"){?>selected="selected"<?php } ?>>Korea South</option>
									<option value="Kuwait" <?php if($nomEdit['country']=="Kuwait"){?>selected="selected"<?php } ?>>Kuwait</option>
									<option value="Kyrgyzstan" <?php if($nomEdit['country']=="Kyrgyzstan"){?>selected="selected"<?php } ?>>Kyrgyzstan</option>
									<option value="Laos" <?php if($nomEdit['country']=="Laos"){?>selected="selected"<?php } ?>>Laos</option>
									<option value="Latvia" <?php if($nomEdit['country']=="Latvia"){?>selected="selected"<?php } ?>>Latvia</option>
									<option value="Lebanon" <?php if($nomEdit['country']=="Lebanon"){?>selected="selected"<?php } ?>>Lebanon</option>
									<option value="Lesotho" <?php if($nomEdit['country']=="Lesotho"){?>selected="selected"<?php } ?>>Lesotho</option>
									<option value="Liberia" <?php if($nomEdit['country']=="Liberia"){?>selected="selected"<?php } ?>>Liberia</option>
									<option value="Libya" <?php if($nomEdit['country']=="Libya"){?>selected="selected"<?php } ?>>Libya</option>
									<option value="Liechtenstein" <?php if($nomEdit['country']=="Liechtenstein"){?>selected="selected"<?php } ?>>Liechtenstein</option>
									<option value="Lithuania" <?php if($nomEdit['country']=="Lithuania"){?>selected="selected"<?php } ?>>Lithuania</option>
									<option value="Luxembourg" <?php if($nomEdit['country']=="Luxembourg"){?>selected="selected"<?php } ?>>Luxembourg</option>
									<option value="Macau" <?php if($nomEdit['country']=="Macau"){?>selected="selected"<?php } ?>>Macau</option>
									<option value="Macedonia" <?php if($nomEdit['country']=="Macedonia"){?>selected="selected"<?php } ?>>Macedonia</option>
									<option value="Madagascar" <?php if($nomEdit['country']=="Madagascar"){?>selected="selected"<?php } ?>>Madagascar</option>
									<option value="Malaysia" <?php if($nomEdit['country']=="Malaysia"){?>selected="selected"<?php } ?>>Malaysia</option>
									<option value="Malawi" <?php if($nomEdit['country']=="Malawi"){?>selected="selected"<?php } ?>>Malawi</option>
									<option value="Maldives" <?php if($nomEdit['country']=="Maldives"){?>selected="selected"<?php } ?>>Maldives</option>
									<option value="Mali" <?php if($nomEdit['country']=="Mali"){?>selected="selected"<?php } ?>>Mali</option>
									<option value="Malta" <?php if($nomEdit['country']=="Malta"){?>selected="selected"<?php } ?>>Malta</option>
									<option value="Marshall Islands" <?php if($nomEdit['country']=="Marshall Islands"){?>selected="selected"<?php } ?>>Marshall Islands</option>
									<option value="Martinique" <?php if($nomEdit['country']=="Martinique"){?>selected="selected"<?php } ?>>Martinique</option>
									<option value="Mauritania" <?php if($nomEdit['country']=="Mauritania"){?>selected="selected"<?php } ?>>Mauritania</option>
									<option value="Mauritius" <?php if($nomEdit['country']=="Mauritius"){?>selected="selected"<?php } ?>>Mauritius</option>
									<option value="Mayotte" <?php if($nomEdit['country']=="Mayotte"){?>selected="selected"<?php } ?>>Mayotte</option>
									<option value="Mexico" <?php if($nomEdit['country']=="Mexico"){?>selected="selected"<?php } ?>>Mexico</option>
									<option value="Midway Islands" <?php if($nomEdit['country']=="Midway Islands"){?>selected="selected"<?php } ?>>Midway Islands</option>
									<option value="Moldova" <?php if($nomEdit['country']=="Moldova"){?>selected="selected"<?php } ?>>Moldova</option>
									<option value="Monaco" <?php if($nomEdit['country']=="Monaco"){?>selected="selected"<?php } ?>>Monaco</option>
									<option value="Mongolia" <?php if($nomEdit['country']=="Mongolia"){?>selected="selected"<?php } ?>>Mongolia</option>
									<option value="Montserrat" <?php if($nomEdit['country']=="Montserrat"){?>selected="selected"<?php } ?>>Montserrat</option>
									<option value="Morocco" <?php if($nomEdit['country']=="Morocco"){?>selected="selected"<?php } ?>>Morocco</option>
									<option value="Mozambique" <?php if($nomEdit['country']=="Mozambique"){?>selected="selected"<?php } ?>>Mozambique</option>
									<option value="Myanmar" <?php if($nomEdit['country']=="Myanmar"){?>selected="selected"<?php } ?>>Myanmar</option>
									<option value="Nambia" <?php if($nomEdit['country']=="Nambia"){?>selected="selected"<?php } ?>>Nambia</option>
									<option value="Nauru" <?php if($nomEdit['country']=="Nauru"){?>selected="selected"<?php } ?>>Nauru</option>
									<option value="Nepal" <?php if($nomEdit['country']=="Nepal"){?>selected="selected"<?php } ?>>Nepal</option>
									<option value="Netherland Antilles" <?php if($nomEdit['country']=="Netherland Antilles"){?>selected="selected"<?php } ?>>Netherland Antilles</option>
									<option value="Netherlands" <?php if($nomEdit['country']=="Netherlands"){?>selected="selected"<?php } ?>>Netherlands (Holland, Europe)</option>
									<option value="Nevis" <?php if($nomEdit['country']=="Nevis"){?>selected="selected"<?php } ?>>Nevis</option>
									<option value="New Caledonia" <?php if($nomEdit['country']=="New Caledonia"){?>selected="selected"<?php } ?>>New Caledonia</option>
									<option value="New Zealand" <?php if($nomEdit['country']=="New Zealand"){?>selected="selected"<?php } ?>>New Zealand</option>
									<option value="Nicaragua" <?php if($nomEdit['country']=="Nicaragua"){?>selected="selected"<?php } ?>>Nicaragua</option>
									<option value="Niger" <?php if($nomEdit['country']=="Niger"){?>selected="selected"<?php } ?>>Niger</option>
									<option value="Nigeria" <?php if($nomEdit['country']=="Nigeria"){?>selected="selected"<?php } ?>>Nigeria</option>
									<option value="Niue" <?php if($nomEdit['country']=="Niue"){?>selected="selected"<?php } ?>>Niue</option>
									<option value="Norfolk Island" <?php if($nomEdit['country']=="Norfolk Island"){?>selected="selected"<?php } ?>>Norfolk Island</option>
									<option value="Norway" <?php if($nomEdit['country']=="Norway"){?>selected="selected"<?php } ?>>Norway</option>
									<option value="Oman" <?php if($nomEdit['country']=="Oman"){?>selected="selected"<?php } ?>>Oman</option>
									<option value="Pakistan" <?php if($nomEdit['country']=="Pakistan"){?>selected="selected"<?php } ?>>Pakistan</option>
									<option value="Palau Island" <?php if($nomEdit['country']=="Palau Island"){?>selected="selected"<?php } ?>>Palau Island</option>
									<option value="Palestine" <?php if($nomEdit['country']=="Palestine"){?>selected="selected"<?php } ?>>Palestine</option>
									<option value="Panama" <?php if($nomEdit['country']=="Panama"){?>selected="selected"<?php } ?>>Panama</option>
									<option value="Papua New Guinea" <?php if($nomEdit['country']=="Papua New Guinea"){?>selected="selected"<?php } ?>>Papua New Guinea</option>
									<option value="Paraguay" <?php if($nomEdit['country']=="Paraguay"){?>selected="selected"<?php } ?>>Paraguay</option>
									<option value="Peru" <?php if($nomEdit['country']=="Peru"){?>selected="selected"<?php } ?>>Peru</option>
									<option value="Phillipines" <?php if($nomEdit['country']=="Phillipines"){?>selected="selected"<?php } ?>>Philippines</option>
									<option value="Pitcairn Island" <?php if($nomEdit['country']=="Pitcairn Island"){?>selected="selected"<?php } ?>>Pitcairn Island</option>
									<option value="Poland" <?php if($nomEdit['country']=="Poland"){?>selected="selected"<?php } ?>>Poland</option>
									<option value="Portugal" <?php if($nomEdit['country']=="Portugal"){?>selected="selected"<?php } ?>>Portugal</option>
									<option value="Puerto Rico" <?php if($nomEdit['country']=="Puerto Rico"){?>selected="selected"<?php } ?>>Puerto Rico</option>
									<option value="Qatar" <?php if($nomEdit['country']=="Qatar"){?>selected="selected"<?php } ?>>Qatar</option>
									<option value="Republic of Montenegro" <?php if($nomEdit['country']=="Republic of Montenegro"){?>selected="selected"<?php } ?>>Republic of Montenegro</option>
									<option value="Republic of Serbia" <?php if($nomEdit['country']=="Republic of Serbia"){?>selected="selected"<?php } ?>>Republic of Serbia</option>
									<option value="Reunion" <?php if($nomEdit['country']=="Reunion"){?>selected="selected"<?php } ?>>Reunion</option>
									<option value="Romania" <?php if($nomEdit['country']=="Romania"){?>selected="selected"<?php } ?>>Romania</option>
									<option value="Russia" <?php if($nomEdit['country']=="Russia"){?>selected="selected"<?php } ?>>Russia</option>
									<option value="Rwanda" <?php if($nomEdit['country']=="Rwanda"){?>selected="selected"<?php } ?>>Rwanda</option>
									<option value="St Barthelemy" <?php if($nomEdit['country']=="St Barthelemy"){?>selected="selected"<?php } ?>>St Barthelemy</option>
									<option value="St Eustatius" <?php if($nomEdit['country']=="St Eustatius"){?>selected="selected"<?php } ?>>St Eustatius</option>
									<option value="St Helena" <?php if($nomEdit['country']=="St Helena"){?>selected="selected"<?php } ?>>St Helena</option>
									<option value="St Kitts-Nevis" <?php if($nomEdit['country']=="St Kitts-Nevis"){?>selected="selected"<?php } ?>>St Kitts-Nevis</option>
									<option value="St Lucia" <?php if($nomEdit['country']=="St Lucia"){?>selected="selected"<?php } ?>>St Lucia</option>
									<option value="St Maarten" <?php if($nomEdit['country']=="St Maarten"){?>selected="selected"<?php } ?>>St Maarten</option>
									<option value="St Pierre & Miquelon" <?php if($nomEdit['country']=="St Pierre & Miquelon"){?>selected="selected"<?php } ?>>St Pierre & Miquelon</option>
									<option value="St Vincent & Grenadines" <?php if($nomEdit['country']=="St Vincent & Grenadines"){?>selected="selected"<?php } ?>>St Vincent & Grenadines</option>
									<option value="Saipan" <?php if($nomEdit['country']=="Saipan"){?>selected="selected"<?php } ?>>Saipan</option>
									<option value="Samoa" <?php if($nomEdit['country']=="Samoa"){?>selected="selected"<?php } ?>>Samoa</option>
									<option value="Samoa American" <?php if($nomEdit['country']=="Samoa American"){?>selected="selected"<?php } ?>>Samoa American</option>
									<option value="San Marino" <?php if($nomEdit['country']=="San Marino"){?>selected="selected"<?php } ?>>San Marino</option>
									<option value="Sao Tome & Principe" <?php if($nomEdit['country']=="Sao Tome & Principe"){?>selected="selected"<?php } ?>>Sao Tome & Principe</option>
									<option value="Saudi Arabia" <?php if($nomEdit['country']=="Saudi Arabia"){?>selected="selected"<?php } ?>>Saudi Arabia</option>
									<option value="Senegal" <?php if($nomEdit['country']=="Senegal"){?>selected="selected"<?php } ?>>Senegal</option>
									<option value="Seychelles" <?php if($nomEdit['country']=="Seychelles"){?>selected="selected"<?php } ?>>Seychelles</option>
									<option value="Sierra Leone" <?php if($nomEdit['country']=="Sierra Leone"){?>selected="selected"<?php } ?>>Sierra Leone</option>
									<option value="Singapore" <?php if($nomEdit['country']=="Singapore"){?>selected="selected"<?php } ?>>Singapore</option>
									<option value="Slovakia" <?php if($nomEdit['country']=="Slovakia"){?>selected="selected"<?php } ?>>Slovakia</option>
									<option value="Slovenia" <?php if($nomEdit['country']=="Slovenia"){?>selected="selected"<?php } ?>>Slovenia</option>
									<option value="Solomon Islands" <?php if($nomEdit['country']=="Solomon Islands"){?>selected="selected"<?php } ?>>Solomon Islands</option>
									<option value="Somalia" <?php if($nomEdit['country']=="Somalia"){?>selected="selected"<?php } ?>>Somalia</option>
									<option value="South Africa" <?php if($nomEdit['country']=="South Africa"){?>selected="selected"<?php } ?>>South Africa</option>
									<option value="Spain" <?php if($nomEdit['country']=="Spain"){?>selected="selected"<?php } ?>>Spain</option>
									<option value="Sri Lanka" <?php if($nomEdit['country']=="Sri Lanka"){?>selected="selected"<?php } ?>>Sri Lanka</option>
									<option value="Sudan" <?php if($nomEdit['country']=="Sudan"){?>selected="selected"<?php } ?>>Sudan</option>
									<option value="Suriname" <?php if($nomEdit['country']=="Suriname"){?>selected="selected"<?php } ?>>Suriname</option>
									<option value="Swaziland" <?php if($nomEdit['country']=="Swaziland"){?>selected="selected"<?php } ?>>Swaziland</option>
									<option value="Sweden" <?php if($nomEdit['country']=="Sweden"){?>selected="selected"<?php } ?>>Sweden</option>
									<option value="Switzerland" <?php if($nomEdit['country']=="Switzerland"){?>selected="selected"<?php } ?>>Switzerland</option>
									<option value="Syria" <?php if($nomEdit['country']=="Syria"){?>selected="selected"<?php } ?>>Syria</option>
									<option value="Tahiti" <?php if($nomEdit['country']=="Tahiti"){?>selected="selected"<?php } ?>>Tahiti</option>
									<option value="Taiwan" <?php if($nomEdit['country']=="Taiwan"){?>selected="selected"<?php } ?>>Taiwan</option>
									<option value="Tajikistan" <?php if($nomEdit['country']=="Tajikistan"){?>selected="selected"<?php } ?>>Tajikistan</option>
									<option value="Tanzania" <?php if($nomEdit['country']=="Tanzania"){?>selected="selected"<?php } ?>>Tanzania</option>
									<option value="Thailand" <?php if($nomEdit['country']=="Thailand"){?>selected="selected"<?php } ?>>Thailand</option>
									<option value="Togo" <?php if($nomEdit['country']=="Togo"){?>selected="selected"<?php } ?>>Togo</option>
									<option value="Tokelau" <?php if($nomEdit['country']=="Tokelau"){?>selected="selected"<?php } ?>>Tokelau</option>
									<option value="Tonga" <?php if($nomEdit['country']=="Tonga"){?>selected="selected"<?php } ?>>Tonga</option>
									<option value="Trinidad & Tobago" <?php if($nomEdit['country']=="Trinidad & Tobago"){?>selected="selected"<?php } ?>>Trinidad & Tobago</option>
									<option value="Tunisia" <?php if($nomEdit['country']=="Tunisia"){?>selected="selected"<?php } ?>>Tunisia</option>
									<option value="Turkey" <?php if($nomEdit['country']=="Turkey"){?>selected="selected"<?php } ?>>Turkey</option>
									<option value="Turkmenistan" <?php if($nomEdit['country']=="Turkmenistan"){?>selected="selected"<?php } ?>>Turkmenistan</option>
									<option value="Turks & Caicos Is" <?php if($nomEdit['country']=="Turks & Caicos Is"){?>selected="selected"<?php } ?>>Turks & Caicos Is</option>
									<option value="Tuvalu" <?php if($nomEdit['country']=="Tuvalu"){?>selected="selected"<?php } ?>>Tuvalu</option>
									<option value="Uganda" <?php if($nomEdit['country']=="Uganda"){?>selected="selected"<?php } ?>>Uganda</option>
									<option value="United Kingdom" <?php if($nomEdit['country']=="United Kingdom"){?>selected="selected"<?php } ?>>United Kingdom</option>
									<option value="Ukraine" <?php if($nomEdit['country']=="Ukraine"){?>selected="selected"<?php } ?>>Ukraine</option>
									<option value="United Arab Erimates" <?php if($nomEdit['country']=="United Arab Erimates"){?>selected="selected"<?php } ?>>United Arab Emirates</option>
									<option value="United States of America" <?php if($nomEdit['country']=="United States of America"){?>selected="selected"<?php } ?>>United States of America</option>
									<option value="Uraguay" <?php if($nomEdit['country']=="Uraguay"){?>selected="selected"<?php } ?>>Uruguay</option>
									<option value="Uzbekistan" <?php if($nomEdit['country']=="Uzbekistan"){?>selected="selected"<?php } ?>>Uzbekistan</option>
									<option value="Vanuatu" <?php if($nomEdit['country']=="Vanuatu"){?>selected="selected"<?php } ?>>Vanuatu</option>
									<option value="Vatican City State" <?php if($nomEdit['country']=="Vatican City State"){?>selected="selected"<?php } ?>>Vatican City State</option>
									<option value="Venezuela" <?php if($nomEdit['country']=="Venezuela"){?>selected="selected"<?php } ?>>Venezuela</option>
									<option value="Vietnam" <?php if($nomEdit['country']=="Vietnam"){?>selected="selected"<?php } ?>>Vietnam</option>
									<option value="Virgin Islands (Brit)" <?php if($nomEdit['country']=="Virgin Islands (Brit)"){?>selected="selected"<?php } ?>>Virgin Islands (Brit)</option>
									<option value="Virgin Islands (USA)" <?php if($nomEdit['country']=="Virgin Islands (USA)"){?>selected="selected"<?php } ?>>Virgin Islands (USA)</option>
									<option value="Wake Island" <?php if($nomEdit['country']=="Wake Island"){?>selected="selected"<?php } ?>>Wake Island</option>
									<option value="Wallis & Futana Is" <?php if($nomEdit['country']=="Wallis & Futana Is"){?>selected="selected"<?php } ?>>Wallis & Futana Is</option>
									<option value="Yemen" <?php if($nomEdit['country']=="Yemen"){?>selected="selected"<?php } ?>>Yemen</option>
									<option value="Zaire" <?php if($nomEdit['country']=="Zaire"){?>selected="selected"<?php } ?>>Zaire</option>
									<option value="Zambia" <?php if($nomEdit['country']=="Zambia"){?>selected="selected"<?php } ?>>Zambia</option>
									<option value="Zimbabwe" <?php if($nomEdit['country']=="Zimbabwe"){?>selected="selected"<?php } ?>>Zimbabwe</option>
								</select>
						</div>
						
				</div>
				<div class="col-md-6">
						<div class="form-group fl">
						<label for="inputCity"  class="inputlbl">Job <span class="mandatory"></span></label>
							<input type="text" class="form-control ba b--black-20 pa2 mb2 db" id="job" name="job" placeholder="Job " value="<?php echo $nomEdit['job'];?>">
						</div>
						
				</div>
					<div class="col-md-6">
			<div class="form-group fl">
		<label for="inputZip"  class="inputlbl">Photo  <span class="mandatory"></span></label>
		<input type="file" class="form-control ba b--black-20 pa2 mb2 db file_lbl" id="file" name="file">
		
		<p class="nomination_plabel">Only PNG,JPG and JPEG files are allowed.</p>
		<div id="uploaded_image">
		<?php if($nomEdit['photo']!=''){?><img src="<?php echo act_url();?>public/images/nomination/<?php echo $nomEdit['photo'];?>" width="150" height="150"/><?php } ?></td>
			

		</div>
		</div>
</div>
<div class="col-md-6 cf mb2">
<div class="fl w-100">
<div class="fl w-25 pa2 bnm"></div>
<div class="fl w-100">
	<button  type="submit" class="btn login-btn mb-4 btn_nom" name="Submit" id="Submit" value="Send">Update</button>
</div>
</div>
</div>
</div>
</div>	
		
</form>	