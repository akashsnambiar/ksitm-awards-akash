<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="<?php echo act_url();?>public/images/favioc.png"> 
<title><?php echo (isset($Titletag) ? ''.$Titletag :'').(isset($pagetitle) ? ' :: '.$pagetitle :'').(isset($site_name) ? ' :: ' .$site_name :'');?></title>
<meta name="Description" content="<?php echo (isset($metadescription) ? $metadescription :''); ?>" />
<meta name="KeyWords" content="<?php echo (isset($metakeywords) ? $metakeywords :''); ?>" />
<meta name="author" content="">
  <link rel="stylesheet" href="<?php echo act_url();?>public/css/login/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo act_url();?>public/css/login/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo act_url();?>public/css/login/login.css">
  <link rel="stylesheet" href="<?php echo act_url(); ?>public/kp/css/nomfont.css">
  <link rel="stylesheet" href="<?php echo act_url(); ?>public/kp/css/fontawesome-free-6.4.2-web/css/fontawesome.min.css"  />
  <link rel="stylesheet" href="<?php echo act_url(); ?>public/kp/css/fontawesome-free-6.4.2-web/css/all.min.css"  />
  <link rel="stylesheet" href="<?php echo act_url();?>public/css/css/validation/tachyons.min.css" />
  <link rel="stylesheet" href="<?php echo act_url();?>public/css/css/validation/formValidation.min.css" />
  <script src="<?php echo act_url();?>public/js/js/jquery-3.7.0.min.js"></script>
</head>
<body>