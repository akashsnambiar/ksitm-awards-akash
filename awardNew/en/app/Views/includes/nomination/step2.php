<form id="demoForm" method="POST" action="<?php echo base_url(); ?>nomination/save2" enctype="multipart/form-data"
	class="frmcls">
	<?= csrf_field() ?>
	<h2 class="nomh2">Details of Nomination</h2><br /><br />
	<div class="err_text" id="err"></div>
	<?php if (isset($validation)): ?>
		<div class="alert alert-warning">
			<?= $validation->listErrors() ?>
		</div>
	<?php endif; ?>
	<div class="row">
		<div class="col-md-12 nomdiv">
			<div class="col-md-6">
				<div class="form-group fl">
					<label for="inputCity" class="inputlbl">Field of Activity <span class="mandatory">*</span></label>
					<select id="workArea" name="workArea" class="form-control">
						<option value="">--Select--</option>
						<?php foreach ($workArea as $row) { ?>
							<?php if ($row['awardID'] != 0) { ?>
								<option value="<?php echo $row['awardID']; ?>">
									<?php echo $row['title_e']; ?>
								</option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>

			</div>
			<div class="col-md-6">
				<div class="form-group fl">
					<label for="inputCity" class="inputlbl">Sub Field<span class="mandatory">*</span></label>
					<select id="workAreaSub" name="workAreaSub" class="form-control">
						<option value="">--Select--</option>
						<?php foreach ($workAreaSub as $row) { ?>
							<option value="<?php echo $row['awardAreaSubID']; ?>"
								data-chained="<?php echo $row['awardAreaID']; ?>">
								<?php echo $row['awardAreaSubTitle_e']; ?>
							</option>
						<?php } ?>
					</select>
				</div>

			</div>
			<div class="col-md-6">
				<div class="form-group fl">
					<label for="inputCity" class="inputlbl">Category of Kerala Awards Nominated for<span
							class="mandatory">*</span></label>
					<select id="award" name="award" class="form-control">
						<option value="">--Select--</option>
						<?php foreach ($userAward as $row) {
							?>
							<?php if ($row['keralajhothi'] == 0) { ?>
								<option value="1">Kerala Jyothi</option>
							<?php } ?>
							<?php if ($row['keralaprabha'] == 0) { ?>
								<option value="2">Kerala Prabha</option>
							<?php } ?>
							<?php if ($row['keralashri'] == 0) { ?>
								<option value="3">Kerala Shri</option>
							<?php }
						} ?>
					</select>
				</div>

			</div>
			<div class="col-md-6">
				<div class="form-group fl">
					<label for="inputCity" class="inputlbl">Is a Recipient of Padma Awards<span
							class="mandatory">*</span></label>
					<select id="pyn" name="pyn" class="form-control">
						<option value="">--Select--</option>
						<option value="1">Yes</option>
						<option value="2">No</option>
					</select>
				</div>

			</div>
			<div class="col-md-6">
				<div class="form-group fl">
					<label for="inputCity" class="inputlbl">Padma Awards Received<span class="mandatory"></span></label>
					<select id="pAward" name="pAward" class="form-control">
						<option value="">--Select--</option>
						<option value="പദ്മ വിഭൂഷൺ">Padma Vibhushan</option>
						<option value="പദ്മ ഭൂഷൺ">Padma Bhushan</option>
						<option value="പദ്മ ശ്രീ">Padma Shri</option>
					</select>
					<!-- <input type="text" class="form-control ba b--black-20 pa2 mb2 db" id="pAward" name="pAward" placeholder="ഏത് പുരസ്‌കാരമാണ് ലഭിച്ചത് ">-->
				</div>

			</div>
			<div class="col-md-6">
				<div class="form-group fl">
					<label for="inputCity" class="inputlbl">Year of Achievement<span class="mandatory"></span></label>
					<input type="number" class="form-control ba b--black-20 pa2 mb2 db" id="pYear" name="pYear"
						placeholder="Year of achievement">
				</div>

			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group fl">
				<label for="inputCity" class="inputlbl">Small Description of the Nominated person <span
						class="mandatory">*</span></label>
				<textarea class="form-control textAr" id="note" name="note"></textarea>
			</div>

		</div>
		<div class="col-md-12">
			<div class="form-group fl">
				<label for="inputCity" class="inputlbl">Contributions of the Nominated Person to the Society<span
						class="mandatory">*</span></label>
				<textarea class="form-control textAr" id="donation" name="donation"></textarea>
			</div>

		</div>
		<div class="col-md-12">
			<div class="form-group fl">
				<label for="inputCity" class="inputlbl">Achievements of the Nominated Person <span
						class="mandatory">*</span></label>
				<textarea class="form-control textAr" id="achivement" name="achivement"></textarea>
			</div>

		</div>
		<div class="col-md-6">
			<div class="form-group fl">
				<label for="inputZip" class="inputlbl">Document <span class="mandatory"></span></label>
				<input type="file" class="form-control ba b--black-20 pa2 mb2 db file_lbl" id="doc" name="doc">
					<p class="nomination_plabel">Only PDF files are allowed, with a maximum size of 15 MB.
				</p>
				<div id="uploaded_image_doc"></div>
			</div>
		</div>
		<div class="col-md-6 cf mb2">
			<div class="fl w-100">
				<div class="fl w-25 pa2 bnm"></div>
				<div class="fl w-100">
					<button  type="submit" class="btn login-btn mb-4 btn_nom" name="Submit" id="Submit" value="Send">next-step</button>
				</div>
			</div>
		</div>
	</div>
</form>