<div class="row">
	<div class="col-md-12 div_p">
	<table id="customers">
		<tr>
			<th>Details of the Person Nominated</th>
		</tr>
	</table><br/>
	<table id="customers">
			<tr>
			<td>Name :  <?php //print_r($nomDetails);exit;
			echo $nomDetails['name'];?></td>
			<td><?php if($nomDetails['photo']!=''){?><img src="<?php echo act_url();?>public/images/nomination/<?php echo $nomDetails['photo'];?>" width="150" height="150"/><?php } ?></td>
			</tr>
		<tr>
			<td>DOB  : <?php if($nomDetails['age']!=0){?><?php echo date("d-m-Y", strtotime($nomDetails['dob']));?><?php } ?></td>
			<td>Age : <?php if($nomDetails['age']!=0){ echo $nomDetails['age'];}else {echo "-";}?></td>
			</tr>
			<tr>
			<td>Sex : <?php if($nomDetails['sex']==1){?>Female<?php }else if($nomDetails['sex']==2){ ?>Male<?php }else if($nomDetails['sex']==3){ ?>Transgender<?php } ?></td>
			<td>State : <?php echo $nomDetails['state'];?></td>
			</tr>
			<tr>
			<td>District : <?php echo $nomDetails['district'];?></td>
			<td>Taluk : <?php echo $nomDetails['taluk'];?></td>
			</tr>
			<tr>
			<td>Place of Birth : <?php echo $nomDetails['pob'];?></td>
			<td>Address : <?php echo $nomDetails['address'];?></td>
			</tr>
			<tr>
			<td>E-Mail : <?php echo $nomDetails['email'];?></td>
			<td>Country : <?php echo $nomDetails['country'];?></td>
			</tr>
			<tr>
			<td>Job : <?php echo $nomDetails['job'];?></td>
			<td></td>
			</tr>
		
		</table><br/>	
		<table id="customers">
		<tr>
			<th>Details of Nomination </th>
		</tr>
	</table><br/>	
	<table id="customers">
		<tr>
			<td>Field of Activity  : <?php echo $nomDetails['title_e'];?></td>
			<td>Sub Field : <?php echo $nomDetails['awardAreaSubTitle_e'];?> </td>
			</tr>
			<tr>
			<td>Category of Kerala Awards Nominated for : <?php if($nomDetails['award']==1){?>Kerala Jyothi<?php }else if($nomDetails['award']==2){ ?>Kerala Prabha<?php }else if($nomDetails['award']==3){ ?> Kerala Shri <?php } ?></td>
			<td>Is a Recipient of Padma Award: <?php if($nomDetails['pyn']==1){?>Yes<?php }else{ ?>No<?php } ?></td>
			</tr>
			<tr>
			<td>Mention the Category of Padma Award : <?php echo $nomDetails['pAward']; ?></td>
			<td>Year of Achievement : <?php if($nomDetails['pYear']!=0) {echo $nomDetails['pYear'];}else{echo "-";}?></td>
			</tr>
			</table>
			<table id="customers">
			<tr>
			<td>Small Decription of the Nominated Person : <?php echo $nomDetails['note'];?></td>
			</tr>
			<tr>
			<td>Contributions of the Nominated Person to the Society : <?php echo $nomDetails['donation'];?></td>
			</tr>
			<tr>
			<td>Achievments of the Nominated Person : <?php echo $nomDetails['achivement'];?></td>
			
			</tr>
		<tr>
			<td>Document &nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;<?php if($nomDetails['document']!=''){?><a href="<?php echo act_url();?>public/images/nomination/documents/<?php echo $nomDetails['document'];?>" target="_blank">
				<img src="<?php echo act_url();?>public/images/pdficon.png"  width="50" height="50"/></a><?php } ?></td>
			
			</tr>
		</table><br/>
		<table id="customers">
		<tr>
			<th>Influence of the Activities of the Nominated Person </th>
		</tr>
	</table><br/>		
		<table id="customers">
			<tr>
			<td>Contributions in the Field of Activity : <?php echo $nomDetails['wDonation'];?></td>
			</tr>
			<tr>
			<td>Contributions to the Society : <?php echo $nomDetails['sDonation'];?></td>
			</tr>
			</table>
			<br/>
		
		<table id="customers_edit">
			<tr>
				<td colspan="2">
				<?php if(($nomDetails['award']==1 && $userAward['keralajhothi']==1))
				{ ?>
					<span style="float:left;text-align:left !important; font-size:14px;padding:12px;color:red;">
					A nomination has already been added for the "Keralajyothi" category of the Kerala Award. Please modify the category by selecting "Edit Nomination."</span>
				<?php
				}elseif(($nomDetails['award']==2 && $userAward['keralaprabha']==1))
				{ ?>
					<span style="float:left;text-align:left !important; font-size:14px;padding:12px;color:red;">
					A nomination has already been added for the "Keralaprabha" category of the Kerala Award. Please modify the category by selecting "Edit Nomination."</span>
				<?php
				}elseif(($nomDetails['award']==3 && $userAward['keralashri']==1))
				{?>
					<span style="float:left;text-align:left !important; font-size:14px;padding:12px;color:red;">
					A nomination has already been added for the "Keralashri" category of the Kerala Award. Please modify the category by selecting "Edit Nomination."</span>
				<?php
				}
				else 
				{
				?>
			<span style=" float:left;text-align:left !important; font-size:14px;padding:12px;">
			 <input style="margin-right:10px;" type="checkbox" class="ba b--black-20 pa2 mb2 db" id="agree" name="agree" onclick="checkAgree()">
			</span><span style=" float:left;text-align:left !important; font-size:14px; padding:10px;">Nominated person has not given any personal recommendation. I have no relation with him/ her in any form.</span><br/><br/><br/>
			</td><?php }?></tr><tr><td colspan="2" style="background-color:#fff">
		<a style="float:right; margin-right:20px;" class="btn login-btn mb-4" href="<?php echo base_url();?>nomination/step1Edit">Edit Nomination</a>
			</td></tr>
		
		</table><br/>															   


		
	</div>
</div>
