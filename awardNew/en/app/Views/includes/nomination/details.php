<style>
#customers {
font-family: Arial, Helvetica, sans-serif;
border-collapse: collapse;
width: 100%;
font-size:12px;
}

#customers td{
border: 1px solid #ddd;
padding: 12px 30px;
text-align: left;
} 
#customers th {
border: 1px solid #ddd;
padding: 8px;
text-align:center;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers td:hover {background-color: #ddd;}

#customers th {
padding-top: 12px;
padding-bottom: 12px;
background-color: #04AA6D;
color: white;
font-size: 14px !important;
}
</style>


	<div class="row">
	<div class="col-md-12" style="padding:0px;">
	
	
	<table id="customers">
		<tr>
			<th>Details of the Person to be Nominated</th>
		</tr>
	</table><br/>
	<table id="customers">
			<tr>
			<td><h3 style=" color:#006600; margin:0px;">Reg. No. : <?php echo $userDetails['nominationRegID'];?></h3></td>
			<td><?php if($userDetails['photo']!=''){?><img src="<?php echo act_url();?>public/images/nomination/<?php echo $userDetails['photo'];?>" width="150" height="150"/><?php } ?></td>
			</tr>
		<tr>
			<td> <b>Name : </b> <?php echo $userDetails['name'];?></td>
			<td><b>DOB  : </b><?php echo date("d-m-Y", strtotime($userDetails['dob']));?></td>
			
			</tr>
			<tr>
			<td><b>Age : </b><?php echo $userDetails['age'];?></td>
			<td><b>Sex : </b><?php if($userDetails['sex']==1){?>Female<?php }else if($userDetails['sex']==2){ ?>Male<?php }else if($userDetails['sex']==3){ ?>TransGender<?php } ?></td>
			
			</tr>
			<tr>
			<td><b>State : </b><?php echo $userDetails['state'];?></td>
			<td><b>District : </b><?php echo $userDetails['district'];?></td>
			
			</tr>
			<tr>
			<td><b>Taluk : </b><?php echo $userDetails['taluk'];?></td>
			<td><b>Place of Birth : </b><?php echo $userDetails['pob'];?></td>
			
			</tr>
			<tr>
			<td><b>Address : </b><?php echo $userDetails['address'];?></td>
			<td><b>E-Mail : </b><?php echo $userDetails['email'];?></td>
			
			</tr>
			<tr>
			<td><b>Country : </b><?php echo $userDetails['country'];?></td>
			<td><b>Job : </b><?php echo $userDetails['job'];?></td>
			
			</tr>
		
		</table><br/>	
		<table id="customers">
		<tr>
			<th>Details of Nomination </th>
		</tr>
	</table><br/>	
	<table id="customers">
		<tr>
			<td><b>Field of Activity  : </b><?php echo $userDetails['title_e'];?></td>
			<td><b>Sub Field : </b><?php echo $userDetails['awardAreaSubTitle_e'];?> </td>
			</tr>
			<tr>
			<td><b>Category of Kerala Awards Opted : </b><?php if($userDetails['award']==1){?>Kerala Jyothi<?php }else if($userDetails['award']==2){ ?>Kerala Prabha<?php }else if($userDetails['award']==3){ ?> Kerala Shri <?php } ?></td>
			<td><b>Is a Recipient of Padma Award: </b><?php if($userDetails['pyn']==1){?>Yes<?php }else{ ?>No<?php } ?></td>
			</tr>
			<tr>
			<td><b>Category of Padma Award : </b><?php echo $userDetails['pAward']; ?></td>
			<td><b>Year of Achievement of the Padma Award : </b><?php if($userDetails['pYear']!=0){echo $userDetails['pYear'];}else {echo "-";}?></td>
			</tr>
			</table>
			<table id="customers">
			<tr>
			<td><b>Small Description of the Nominated Person : </b><?php echo $userDetails['note'];?></td>
			</tr>
			<tr>
			<td><b>Contributions of the Nominated Person to the Society : </b><?php echo $userDetails['donation'];?></td>
			</tr>
			<tr>
			<td><b>Achievments of the Nominated Person : </b><?php echo $userDetails['achivement'];?></td>
			
			</tr>
		<tr>
			<td><b>Document &nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;</b>
			<?php if ($userDetails['document'] !==''){?>
			<a href="<?php echo act_url();?>public/images/nomination/documents/<?php echo $userDetails['document'];?>" target="_blank"><img src="<?php echo act_url();?>public/images/pdficon.png"  width="50" height="50"/></a>
			<?php }?>
			</td>
			
			</tr>
		</table><br/>
		<table id="customers">
		<tr>
			<th>Influence of the Activities of the Nominated Person </th>
		</tr>
	</table><br/>		
		<table id="customers">
			<tr>
			<td><b>Contributions in the Field of Activity : </b><?php echo $userDetails['wDonation'];?></td>
			</tr>
			<tr>
			<td><b>Contributions to the Society : </b><?php echo $userDetails['sDonation'];?></td>
			</tr>
			</table>
			<?php if (isset($esign)) { ?>
			<table id="customers">
			<tr>
			<td><b>Signature</b></td>
			<td><img src="<?php echo act_url();?>public/images/esign.jpg" width="50" height="50"/>&nbsp;&nbsp;Digitally Signed By : <?php echo $esign['name'];?></td>
			</tr>
		
		</table><?php } ?><br/>
		
																	


		
	</div>
</div>	

