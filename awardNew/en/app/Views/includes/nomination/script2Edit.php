<script src="<?php echo act_url();?>public/js/login/popper.min.js"></script>
<script src="<?php echo act_url();?>public/js/login/bootstrap.min.js"></script>
<script src="<?php echo act_url();?>public/js/login/es6-shim.min.js"></script>
<script src="<?php echo act_url();?>public/js/validation/FormValidation.min.js"></script>
<script src="<?php echo act_url();?>public/js/validation/plugins/Tachyons.min.js"></script>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        const demoForm = document.getElementById('demoForm');
        const fv = FormValidation.formValidation(demoForm, {
            fields: {
                workArea: {
                    validators: {
                        notEmpty: {
                            message: 'The Field is required.',
                        },
                    },
                },
                workAreaSub: {
                    validators: {
                        notEmpty: {
                            message: 'The Field is required.',
                        },
                    },
                },
                award: {
                    validators: {
                        notEmpty: {
                            message: 'The Field is required.',
                        },
                    },
                },
                
                note: {
                    validators: {
                        notEmpty: {
                            message: 'The Field is required.',
                        },
                    },
                },
                donation: {
                    validators: {
                        notEmpty: {
                            message: 'The Field is required.',
                        },
                    },
                },
                    achivement: {
                    validators: {
                        notEmpty: {
                            message: 'The Field is required.',
                        },
                    },
                },
                
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                tachyons: new FormValidation.plugins.Tachyons(),
                submitButton: new FormValidation.plugins.SubmitButton(),
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh',
                    onPlaced: function (e) {
                        e.iconElement.addEventListener('click', function () {
                            fv.resetField(e.field);
                        });
                    },
                }),
            },
        }).on('core.form.valid', function () {
                var form = $("#demoForm");
                var url = form.attr('action');
                dataString = $("#demoForm").serialize();
                //document.write(url);
                $.ajax({
                type: 'POST',
                url: url,
                data: dataString,
                dataType:"JSON",
                success: function(data) {
                if (data.success==1)
                {
                    $('#err').removeClass('alert');
                    $('#err').removeClass('alert-warning');
                    $("#err").html('');
                    window.location.href = "<?php echo base_url();?>nomination/step3Edit";
                }
                else{
                  
                    $("#err").html('');
                    $('#err').addClass('alert alert-warning');
                    $("#err").html(data.error);
                    $('html, body').animate({
                        scrollTop: 300
                    }, 400); // Adjust the duration as needed
                }
        },
        error: function(data) {
            
            // Some error in ajax call
            alert("some Error");
        }
            });
        });
    });
</script>
<script src="<?php echo act_url();?>public/js/chained/jquery.chained.js?v=1.0.0" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" charset="utf-8">
    $(function() {
         $("#workAreaSub").chained("#workArea");
    });

    $(document).ready(function() {
            function disableBack() {
        window.history.forward()
    }
    window.onload = disableBack();
    window.onpageshow = function(e) {
        if (e.persisted)
            disableBack();
    }
    });

$(document).ready(function(){
 $(document).on('change', '#doc', function(){
  var name = document.getElementById("doc").files[0].name;
  var form_data = new FormData();
  var ext = name.split('.').pop().toLowerCase();
// document.write(name);
  var oFReader = new FileReader();
  oFReader.readAsDataURL(document.getElementById("doc").files[0]);
  var f = document.getElementById("doc").files[0];
  var fsize = f.size||f.fileSize;
  
   form_data.append("doc", document.getElementById('doc').files[0]);
   $.ajax({
    url:"<?php echo base_url();?>nomination/upload_doc",
    method:"POST",
    data: form_data,
    contentType: false,
    cache: false,
    processData: false,
    beforeSend:function(){
     $('#uploaded_image_doc').html("<label class='text-success'>PDF Uploading...</label>");
    },   
    success:function(data)
    {
     $('#uploaded_image_doc').addClass('red');
     $('#uploaded_image_doc').html(data);
    }
   });
 
 });
});
//check padma puraskaram received or not

$(function () { 
    var value= $('select[name="pyn"]').val();
    if (value==1)
    {
        $("#pAward").removeAttr("disabled");
        $("#pYear").removeAttr("disabled");
        $("#pAward").focus();
    }
    else{
        $("#pAward").attr("disabled", "disabled");
            $("#pYear").attr("disabled", "disabled");
            $("#pAward").val('');
            $("#pYear").val('');
    }
    $("#pyn").change(function () {
        if ($(this).val() == 1) {
            $("#pAward").removeAttr("disabled");
            $("#pYear").removeAttr("disabled");
            $("#pAward").focus();
        } else {
            $("#pAward").attr("disabled", "disabled");
            $("#pYear").attr("disabled", "disabled");
            $("#pAward").val('');
            $("#pYear").val('');
        }
    });
    });
</script>