<form id="demoForm" method="POST" action="<?php echo base_url();?>nomination/saveEdit2" enctype="multipart/form-data" class="frmcls">
<?= csrf_field() ?>
<h2 class="nomh2">Details of Nomination  </h2><br/><br/>
<div class="err_text" id="err"></div>
		<div class="row">
		<div class="col-md-12" style="padding:0px;">
			<div class="col-md-6">
					<div class="form-group fl">
					<label for="inputCity" class="inputlbl">Field of Activity <span class="mandatory">*</span></label>
						<select id="workArea" name="workArea" class="form-control">
						<option value="">--Select--</option>
						<?php foreach ($workArea as $row) { ?>
					<option value="<?php echo $row['awardID'];?>" <?php if($nomEdit['workArea']==$row['awardID']){?>selected="selected"<?php } ?>><?php echo $row['title_e'];?></option>
				<?php } ?>
				</select>
			</div>
			</div>
			<div class="col-md-6">
					<div class="form-group fl">
					<label for="inputCity" class="inputlbl">Sub Field<span class="mandatory">*</span></label>
						<select id="workAreaSub" name="workAreaSub" class="form-control">
						<option value="">--Select--</option>
						<?php foreach ($workAreaSub as $row) { ?>
					<option value="<?php echo $row['awardAreaSubID'];?>" <?php if($nomEdit['workAreaSub']==$row['awardAreaSubID']){?>selected="selected"<?php } ?> data-chained="<?php echo $row['awardAreaID'];?>"><?php echo $row['awardAreaSubTitle_e'];?></option>
				<?php } ?>
				</select>
			</div>
			</div>
			<div class="col-md-6">
					<div class="form-group fl">
					<label for="inputCity" class="inputlbl">Category of Kerala Awards Nominated for<span class="mandatory">*</span></label>
					<select id="award" name="award" class="form-control">
					<option value="">--Select--</option>
					<?php foreach ($userAward as $row) { 
					?>
					<?php if($row['keralajhothi']==0){?>
					<option value="1" <?php if($nomEdit['award']==1){?>selected="selected"<?php } ?>>Kerala Jyothi</option>
					<?php } ?>
					<?php if($row['keralaprabha']==0){?>
					<option value="2"<?php if($nomEdit['award']==2){?>selected="selected"<?php } ?>>Kerala Prabha</option>
					<?php } ?>
					<?php if($row['keralashri']==0){?>	
					<option value="3"<?php if($nomEdit['award']==3){?>selected="selected"<?php } ?>>Kerala Shri</option>
					<?php } }?>
					</select>
					</div>
					</div>
					<div class="col-md-6">
							<div class="form-group fl">
							<label for="inputCity" class="inputlbl">Is a recipient of Padma Awards<span class="mandatory">*</span></label>
							<select id="pyn" name="pyn" class="form-control">
								<option value="">--Select--</option>
								<option value="1" <?php if($nomEdit['pyn']==1){?>selected="selected"<?php } ?>>Yes</option>
								<option value="2" <?php if($nomEdit['pyn']==2){?>selected="selected"<?php } ?>>No</option>
								</select>
							</div>
							
					</div>
					<div class="col-md-6">
							<div class="form-group fl">
							<label for="inputCity" class="inputlbl">Padma Awards Received<span class="mandatory"></span></label>
								<select id="pAward" name="pAward" class="form-control">
								<option value="">--Select--</option>
								<option value="പദ്മ വിഭൂഷൺ" <?php if($nomEdit['pAward']=='പദ്മ വിഭൂഷൺ'){?>selected="selected"<?php } ?>>Padma Vibhushan</option>
								<option value="പദ്മ ഭൂഷൺ" <?php if($nomEdit['pAward']=='പദ്മ ഭൂഷൺ'){?>selected="selected"<?php } ?>>Padma Bhushan</option>
								<option value="പദ്മ ശ്രീ" <?php if($nomEdit['pAward']=='പദ്മ ശ്രീ'){?>selected="selected"<?php } ?>>Padma Shri</option>
								</select>
								</div>
							
					</div>
					<div class="col-md-6">
							<div class="form-group fl">
							<label for="inputCity" class="inputlbl">Year of Achievement<span class="mandatory"></span></label>
								<input type="number" class="form-control ba b--black-20 pa2 mb2 db" id="pYear" name="pYear" placeholder="Year of achievement" value="<?php if ($nomEdit['pYear']==0){echo '';}else {echo $nomEdit['pYear'];}?>">
							</div>
							
					</div>
					</div>
					<div class="col-md-12">
							<div class="form-group fl">
							<label for="inputCity" class="inputlbl">Small decription of the Nominated person <span class="mandatory">*</span></label>
								<textarea class="form-control textAr" id="note" name="note"> <?php echo $nomEdit['note'];?></textarea>
							</div>
							
					</div>
					<div class="col-md-12">
							<div class="form-group fl">
					<label for="inputCity" class="inputlbl">Contributions of the nominated person to the society<span class="mandatory">*</span></label>
								<textarea class="form-control textAr" id="donation" name="donation"><?php echo $nomEdit['donation'];?></textarea>
							</div>
							
					</div>
					<div class="col-md-12">
							<div class="form-group fl">
							<label for="inputCity" class="inputlbl">Achievements of the nominated person<span class="mandatory">*</span></label>
								<textarea class="form-control textAr" id="achivement" name="achivement"><?php echo $nomEdit['achivement'];?></textarea>
							</div>
					</div>
					<div class="col-md-6">
				<div class="form-group fl">
			<label for="inputZip" class="inputlbl">Document <span class="mandatory"></span></label>
			<input type="file" class="form-control ba b--black-20 pa2 mb2 db" id="doc" name="doc" style="padding: 4px 26px; line-height: 2;">
			<p class="nomination_plabel">Only PDF files are allowed, with a maximum size of 15 MB.
				</p><div id="uploaded_image_doc"></div>
			</div>
	</div>
	<div class="col-md-6 cf mb2">
<div class="fl w-100">
<div class="fl w-25 pa2 bnm"></div>
<div class="fl w-100">
		<button type="submit" class="btn login-btn mb-4 btn_nom" name="Submit" id="Submit" value="Send">next-step</button>
</div>
</div>
</div>
	</div>
</form>