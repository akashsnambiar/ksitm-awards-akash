  <script src="<?php echo act_url();?>public/js/login/jquery-3.4.1.min.js"></script>
  <script src="<?php echo act_url();?>public/js/login/popper.min.js"></script>
  <script src="<?php echo act_url();?>public/js/login/bootstrap.min.js"></script>
   <script src="<?php echo act_url();?>public/js/login/es6-shim.min.js"></script>
        <script src="<?php echo act_url();?>public/js/validation/FormValidation.min.js"></script>
        <script src="<?php echo act_url();?>public/js/validation/plugins/Tachyons.min.js"></script>
		<script>
            document.addEventListener('DOMContentLoaded', function () {
			   // $('#Submit').prop('disabled', true);
                const demoForm = document.getElementById('demoForm');
                const fv = FormValidation.formValidation(demoForm, {
                    fields: {
                        userName: {
                            validators: {
                                notEmpty: {
                                    message: 'User Name is required.',
                                },
                            },
                        },
                        password: {
                            validators: {
                                notEmpty: {
                                    message: 'Password is required.',
                                },
                            },
                        },
                    },
                    plugins: {
                       trigger: new FormValidation.plugins.Trigger(),
                        tachyons: new FormValidation.plugins.Tachyons(),
                        submitButton: new FormValidation.plugins.SubmitButton(),
                        icon: new FormValidation.plugins.Icon({
                            valid: 'fa fa-check',
                            invalid: 'fa fa-times',
                            validating: 'fa fa-refresh',
                            onPlaced: function (e) {
                                e.iconElement.addEventListener('click', function () {
                                    fv.resetField(e.field);
                                });
                            },
                        }),
                    },
                }).on('core.form.valid', function () {
					   var form = $("#demoForm");
                       var url = form.attr('action');
					   dataString = $("#demoForm").serialize();
					    //document.write(url);
                        $.ajax({
                        type: 'POST',
                        url: url,
                        data: dataString,
						
						success: function(data) {
							//alert(data);
						   if(data=="success"){
						       window.location.href = "<?php echo base_url();?>nomination/myAccount";
						   }else{
						       $('#loginError').html("<div class='alert alert-danger' role='alert'>Login Failed.</div>");
						   }
                },
                error: function(data) {
                    
                    $('#loginError').html("<div class='alert alert-danger' role='alert'>Login Failed.</div>");
                }
                    });
                });
            });
        </script>         