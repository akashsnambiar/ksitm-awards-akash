<form id="demoForm" method="POST" action="<?php echo base_url();?>nomination/save3" enctype="multipart/form-data" class="frmcls">
<?= csrf_field() ?>	
<h2 class="nomh2">Influence of the Activities of the Nominated Person</h2><br/><br/>
<div class="err_text" id="err"></div>						
<?php if(isset($validation)):?>
	<div class="alert alert-warning">
	<?= $validation->listErrors() ?>
	</div>
	<?php endif;?>
		<div class="row">
			<div class="col-md-12">
					<div class="form-group fl">
			<label for="inputCity" class="inputlbl">Contributions in the Field of Activity<span class="mandatory">*</span></label>
						<textarea class="form-control textAr" id="wDonation" name="wDonation"></textarea>
					</div>
			</div>
			<div class="col-md-12">
					<div class="form-group fl">
					<label for="inputCity" class="inputlbl">Contributions to the Society <span class="mandatory">*</span></label>
						<textarea class="form-control textAr" id="sDonation" name="sDonation"></textarea>
					</div>
			</div>
			
			<div class="col-md-12 cf mb2">
				<div class="fl w-100">
					<div class="fl w-25 pa2 bnm"></div>
					<div class="fl w-100">
				<button type="submit" class="btn login-btn mb-4 btn_nom" name="Submit" id="Submit" value="Send">next-step</button>
					</div>
				</div>
			</div>
	</div>
</form>