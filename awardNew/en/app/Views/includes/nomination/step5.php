<div class="finish">
	<?php if (isset($ref['nominationRegID'])) { 
		$regId=  base64_encode($ref['nominationRegID']);
		?>
		<form autocomplete="off" id="demoForm" method="POST" action="<?php echo base_url(); ?>Nomination/downloadReceipt"
			style="max-width:100%;">
			<?= csrf_field() ?>
			<div id="makepdf"><br>
			<h2 class="text text-center">
				<strong>Your Nomination has been completed. Ref. No :<?php echo $ref['nominationRegID']; ?></strong>
			</h2>
			<p class="text text-center">
			<div class="row">
				<div class="col-md-6">
					<input type="hidden" id="nominationRegID" name="nominationRegID" style="margin-bottom:4px;" value="<?php echo $regId; ?>">
				</div>
				<div class="col-md-6">
					<button style="float:right;" type="button" class="btn btn-primary btn-pdf" name="buttoncre" id="button" value="Send">
					<i class="fa fa-download"></i>Download Receipt</button>
				</div>
			</div>
			</p>
			</div>
		</form>
	<?php } ?>
</div>
<script>
$(document).ready( function () {
    $("#button").on('click', function() {
        //window.open("<?php echo base_url(); ?>nomination/downloadReceipt", '_blank');
		var nominationRegID = $("#nominationRegID").val(); // Get the value of the input field
        var url = "<?php echo base_url(); ?>nomination/downloadReceipt/"+nominationRegID;
        window.open(url, '_blank');
    });
});
</script>


							
					