					<style>
#customers {
font-family: Arial, Helvetica, sans-serif;
border-collapse: collapse;
width: 100%;
font-size:12px;
}

#customers td{
border: 1px solid #ddd;
padding: 12px 30px;
text-align: left;
} 
#customers th {
border: 1px solid #ddd;
padding: 8px;
text-align:center;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers td:hover {background-color: #ddd;}

#customers th {
padding-top: 12px;
padding-bottom: 12px;
background-color: #04AA6D;
color: white;
}
</style>
					
						
		<div class="row">
		<div class="col-md-12" style="padding:0px;">
		
		
		<table id="customers">
			<tr>
				<th>Details of the person Nominated </th>
			</tr>
		</table><br/>
		<table id="customers">
				<tr>
				<td><b>Name : </b> <?php echo $nomDetails['name'];?></td>
				<td><?php if($nomDetails['photo']!=''){?><img src="<?php echo act_url();?>public/images/nomination/<?php echo $nomDetails['photo']?>" width="150" height="150"/><?php } ?></td>
				</tr>
			<tr>
				<td><b>DOB  : </b><?php if($nomDetails['age']!=0){?><?php  echo date("d M Y", strtotime($nomDetails['dob'])); ?><?php } ?></td>
				<td><b>Age : </b><?php echo $nomDetails['age'];?></td>
				</tr>
				<tr>
				<td><b>Sex : </b><?php if($nomDetails['sex']==1){?>Female<?php }else if($nomDetails['sex']==2){ ?>Male<?php }else if($nomDetails['sex']==3){ ?>Transgender<?php } ?></td>
				<td><b>State : </b><?php echo $nomDetails['state'];?></td>
				</tr>
				<tr>
				<td><b>District : </b><?php echo $nomDetails['district'];?></td>
				<td><b>Taluk : </b><?php echo $nomDetails['taluk'];?></td>
				</tr>
				<tr>
				<td><b>Place of Birth : </b><?php echo $nomDetails['pob'];?></td>
				<td><b>Address : </b><?php echo $nomDetails['address'];?></td>
				</tr>
				<tr>
				<td><b>E-Mail : </b><?php echo $nomDetails['email'];?></td>
				<td><b>Country : </b><?php echo $nomDetails['country'];?></td>
				</tr>
				<tr>
				<td><b>Job : </b><?php echo $nomDetails['job'];?></td>
				
				</tr>
			
			</table><br/>	
			<table id="customers">
			<tr>
				<th>Details of Nomination</th>
			</tr>
		</table><br/>	
		<table id="customers">
				
			<tr>
				<td><b>Field of Activity  : </b><?php echo $nomDetails['title_e'];?></td>
				<td><b>Sub Field : </b><?php echo $nomDetails['awardAreaSubTitle_e'];?> </td>
				</tr>
				<tr>
				<td><b>Category of Kerala Awards Opted : </b><?php if($nomDetails['award']==1){?>Kerala Jyothi<?php }else if($nomDetails['award']==2){ ?>Kerala Prabha<?php }else if($nomDetails['award']==3){ ?> Kerala Shri <?php } ?></td>
				<td><b>Is a Recipient of Padma Awards: </b><?php if($nomDetails['pyn']==1){?>Yes<?php }else{ ?>No<?php } ?></td>
				</tr>
				<tr>
				<td><b>Category of Padma Award : </b><?php echo $nomDetails['pAward']; ?></td>
				<td><b>Year of Achievement of the Padma Award : </b><?php echo $nomDetails['pYear'];?></td>
				</tr>
				</table>
				<table id="customers">
				<tr>
				<td><b>Small Description of the Nominated Person : </b><?php echo $nomDetails['note'];?></td>
				</tr>
				<tr>
				<td><b>Contributions of the Nominated Person to the Society  : </b><?php echo $nomDetails['donation'];?></td>
				</tr>
				<tr>
				<td><b>Achievments of the Nominated Person : </b><?php echo $nomDetails['achivement'];?></td>
				
				</tr>
			<tr>
				<td><b>Document &nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;</b><?php if($nomDetails['document']!=''){?><a href="<?php echo act_url();?>public/images/nomination/documents/<?php echo $nomDetails['document'];?>" target="_blank"><img src="<?php echo act_url();?>public/images/pdficon.png"  width="50" height="50"/></a><?php } ?></td>
				
				</tr>
			</table><br/>
			<table id="customers">
			<tr>
				<th>Influence of the Activities of the Nominated Person </th>
			</tr>
		</table><br/>		
			<table id="customers">
				<tr>
				<td><b>Contributions in the Field of Activity : </b><?php echo $nomDetails['wDonation'];?></td>
				</tr>
				<tr>
				<td><b>Contributions to the Society : </b><?php echo $nomDetails['sDonation'];?></td>
				</tr>
				</table>
				<br/>
			
			<table id="esign">
				<img style="float:left;" src="<?php echo act_url();?>public/images/esign.jpg"  width="75" height="75"/><span style="float:left;">&nbsp;&nbsp;Digitally Signed By : <?php echo $aadharName;?></span>
				<br/>
		</div>
	</div>
							
<script>
window.setTimeout(function() {
		window.location.href = '<?php echo base_url();?>nomination/save4';
		}, 9000);
</script> 