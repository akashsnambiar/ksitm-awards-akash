<script src="<?php echo act_url();?>public/js/login/popper.min.js"></script>
<script src="<?php echo act_url();?>public/js/login/bootstrap.min.js"></script>
<script src="<?php echo act_url();?>public/js/login/es6-shim.min.js"></script>
<script src="<?php echo act_url();?>public/js/validation/FormValidation.min.js"></script>
<script src="<?php echo act_url();?>public/js/validation/plugins/Tachyons.min.js"></script>
<script>
document.addEventListener('DOMContentLoaded', function () {
    const demoForm = document.getElementById('demoForm');
    const fv = FormValidation.formValidation(demoForm, {
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'The Field is required.',
                    },
                },
            },
              
                address: {
                    validators: {
                        notEmpty: {
                            message: 'The Field is required.',
                        },
                    },
                },
               
                    
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                tachyons: new FormValidation.plugins.Tachyons(),
                submitButton: new FormValidation.plugins.SubmitButton(),
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh',
                    onPlaced: function (e) {
                        e.iconElement.addEventListener('click', function () {
                            fv.resetField(e.field);
                        });
                    },
                }),
            },
        }).on('core.form.valid', function () {
                var form = $("#demoForm");
                var url = form.attr('action');
                dataString = $("#demoForm").serialize();
                $.ajax({
                type: 'POST',
                url: url,
                data: dataString,
                dataType:"JSON",
                success: function(data) {
                if (data.success==1)
                {
                    $('#err').removeClass('alert');
                    $('#err').removeClass('alert-warning');
                    $("#err").html('');
                    window.location.href = "<?php echo base_url();?>nomination/step2Edit";
                }
                else{
                  
                    $("#err").html('');
                    $('#err').addClass('alert alert-warning');
                    $("#err").html(data.error);
                    $('html, body').animate({
                        scrollTop: 300
                    }, 400); // Adjust the duration as needed
                }
        },
        error: function(data) {
            
            // Some error in ajax call
            alert("some Error");
        }
            });
        });
    });
</script>


    
<script src="<?php echo act_url();?>public/js/chained/jquery.chained.js?v=1.0.0" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" charset="utf-8">
    $(function() {
        $("#district").chained("#state");
         $("#taluk").chained("#district");
    });

$(document).ready(function(){
    $(document).on('change', '#file', function(){
            var name = document.getElementById("file").files[0].name;
            var form_data = new FormData();
            var ext = name.split('.').pop().toLowerCase();
            // document.write(name);
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("file").files[0]);
            var f = document.getElementById("file").files[0];
            var fsize = f.size||f.fileSize;

            form_data.append("file", document.getElementById('file').files[0]);
            $.ajax({
                url:"<?php echo base_url();?>nomination/upload_file",
                method:"POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
            beforeSend:function(){
                $('#uploaded_image').html("<label class='text-success'>Image Uploading...</label>");
            },   
            success:function(data)
            {
                $('#uploaded_image').html('');
                $('#uploaded_image').html(data);
            }
        });

        });
    });
</script>
<script>
$(document).ready(function() {
    function disableBack() {
        window.history.forward()
    }
    window.onload = disableBack();
    window.onpageshow = function(e) {
        if (e.persisted)
            disableBack();
    }
});
function calculateAge() {
    $("#loaderIcon").show();
    jQuery.ajax({
    url: "<?php echo base_url();?>nomination/calculateAge",
    data:'dob='+$("#dob").val(),
    type: "POST",
    success:function(data){
    $("#age").val(data);
    $("#loaderIcon").hide();

    },
    error:function (){}
    });
}
</script>