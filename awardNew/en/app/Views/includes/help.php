<!-- contact for any query -->
<section id="reviews">
    <div class="container pa-about-contact">
        <div class="row helpbox">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <h5 class="pa-help-text text-white"><span>Helpline</span> </h5>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="pa-help-contact">
                    <h3 class="text-white">0471 2518531, 8223, &nbsp;&nbsp;+91 471 2525444</h3>
                    <h6 class="text-white">(Mon to sat, 10 am - 5 pm)</h6>
                </div>
            </div>
        </div>
    </div>
</section>