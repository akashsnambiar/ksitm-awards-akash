<?php

namespace App\Models;

use CodeIgniter\Model;

class ThalukModel extends Model
{
    protected $table            = 'thaluk';
    protected $allowedFields    = ['talukID', 'stateID', 'districtID', 'taluk', 'status'];
}
