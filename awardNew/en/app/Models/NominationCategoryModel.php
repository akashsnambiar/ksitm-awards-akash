<?php

namespace App\Models;

use CodeIgniter\Model;

class NominationCategoryModel extends Model
{
    protected $table            = 'nomination_category';
    protected $allowedFields    = ['id','nomination_category'];

}
