<?php

namespace App\Models;

use CodeIgniter\Model;

class GalleryModel extends Model
{
    protected $table            = 'gallery';
    protected $allowedFields    = ['id','cat_id','gal_img','title','title_e','field','field_e','status'];

    
}
