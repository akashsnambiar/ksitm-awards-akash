<?php

namespace App\Models;

use CodeIgniter\Model;

class OrganisationModel extends Model
{
    protected $table            = 'organisation';
    protected $primaryKey = 'orgID';
    protected $allowedFields    = ['orgID','organisation','status'];

    
}