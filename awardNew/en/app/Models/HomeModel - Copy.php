<?php

namespace App\Models;

use CodeIgniter\Model;

class HomeModel extends Model
{
    
    protected $DBGroup          = 'default';
    protected $table            = 'homes';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    protected $db;
    public function __construct()
    {
       $this->db = db_connect();
    } 
    
	function get_gal_menu()
    {
        $result_settings = $this->db->query('select * from gallery_category where status=1 order by title ASC' );
		$results	=$result_settings->getResult();
       // print_r($results);exit;
        return $results;
    }
    function get_home()
    {
        $result = $this->db->query('select * from home_cnt where id=1' );
        $result_home = $result->getRow();
        //return $result_selected->row();
        return $result_home;
     }
    function get_whocanaaply()
     {
        $result = $this->db->query('select * from whocanapply' );
        $result_apply = $result->getResult();
        return $result_apply;
     }
     
	function get_contri()
    {
        $result = $this->db->query('select * from contribution' );
        $result_contr = $result->getResult();
        return $result_contr;
    } 
    
	function get_whodecides()
    {
        $result = $this->db->query('select * from whodecides' );
        $result_whodecides = $result->getResult();
        return $result_whodecides;
        }  
}
