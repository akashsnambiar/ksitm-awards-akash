<?php

namespace App\Models;

use CodeIgniter\Model;

class NotificationModel extends Model
{
   
    protected $table            = 'notification';
    protected $allowedFields    = ['id','not_title','not_title_e','not_pdf','status'];

    
}
