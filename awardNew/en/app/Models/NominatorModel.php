<?php

namespace App\Models;

use CodeIgniter\Model;

class NominatorModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'nominator';
    protected $primaryKey = 'NomID';
    protected $allowedFields    = ['NomID','nominator','status'];

    
}
