<?php

namespace App\Models;

use CodeIgniter\Model;

class NominationdemoModel extends Model
{
    
    protected $table            = 'nominationdemo';
    protected $primaryKey = 'nomDemoID';
    protected $allowedFields    = ['nomDemoID', 'userID', 'name', 'dob', 'age', 'sex', 'state', 'district', 'taluk', 'pob', 'address', 'email', 'country', 'job', 'photo', 'workArea', 'workAreaSub', 'award', 'pyn', 'pAward', 'pYear', 'note', 'donation', 'achivement', 'document', 'wDonation', 'sDonation', 'signature', 'date', 'step'


    ];
}
