<?php

namespace App\Models;

use CodeIgniter\Model;

class DistrictModel extends Model
{
    protected $table            = 'district';
    protected $primaryKey = 'districtID';
    protected $allowedFields    = ['districtID', 'stateID', 'district', 'district_m', 'status'];

    
}
