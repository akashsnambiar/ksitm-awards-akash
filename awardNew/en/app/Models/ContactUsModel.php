<?php

namespace App\Models;

use CodeIgniter\Model;

class ContactUsModel extends Model
{
    protected $table            = 'contact_us';
    protected $primaryKey       = 'id';
    protected $allowedFields    = ['id','text','text_e'];

}
