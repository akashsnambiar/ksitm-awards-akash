<?php

namespace App\Models;

use CodeIgniter\Model;

class Home extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'homes';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];



    protected $db;
    public function __construct()
    {
       $this->db = db_connect();
    } 
    
	function get_gal_menu(){
       
     
       /*  $this->db->order_by("title","ASC");
        $this->db->select('*');
        $this->db->from('gallery_category');
        $this->db->where("status",1);
        $result_settings = $this->db->get(); */

       // $result_settings = $db->table('gallery_category')->get();

       // $result_settings	=  $this->db->query('SELECT * FROM settings');
		$results	="vdv";//$result_settings->getResult();

        print_r($results);exit;
        return $results;
    }

}
