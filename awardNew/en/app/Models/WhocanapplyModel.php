<?php

namespace App\Models;

use CodeIgniter\Model;

class WhocanapplyModel extends Model
{
    protected $table            = 'whocanapply';
    protected $allowedFields    = ['id','title','title_e'];

}
