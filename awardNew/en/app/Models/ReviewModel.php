<?php

namespace App\Models;

use CodeIgniter\Model;

class ReviewModel extends Model
{
    protected $table            = 'reviews';
    protected $primaryKey       = 'reviewID';
    protected $allowedFields    = ['reviewID', 'nominationID', 'adminID', 'review', 'date', 'time'];
}
