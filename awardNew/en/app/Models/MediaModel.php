<?php

namespace App\Models;

use CodeIgniter\Model;

class MediaModel extends Model
{
    protected $table            = 'media';
    protected $allowedFields    = ['id','title','title_e','embed','status'];

   
}
