<?php

namespace App\Models;

use CodeIgniter\Model;

class WinnerModel extends Model
{
    protected $table            = 'winner';
    protected $allowedFields    = ['id','banner_title_id','winner_name','winner_name_mal','awardarea_id','description','description_mal','winner_photo','created_time','updated_time','status'];

}
