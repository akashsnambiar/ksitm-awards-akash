<?php 

use CodeIgniter\HTTP\CLIRequest;
use CodeIgniter\HTTP\URI;

use Config\App;
use Config\Services;

if (! function_exists('act_url')) {
    /**
     * act URL
     *
     * Returns the "actURL" from your config file
     *
     * @param App|null $altConfig Alternate configuration to use
     */
    function act_url(?App $altConfig = null): string
    {
         $config = $altConfig ?? config('App');
        return $config->actURL;
    }
}
