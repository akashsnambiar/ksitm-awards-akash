<?php
require "config.php";

//$file = file_get_contents(FILE_NAME); //binary value
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$imagick = new Imagick();
$imagick->setBackgroundColor(new ImagickPixel('transparent'));
$imagick->setResolution(288, 288);
//echo($project_path.'cert/'.FILE_NAME);
$fileone = realpath($project_path.'cert/'.FILE_NAME);

/*if (is_readable($fileone)) {
    echo 'file readable';
}*/
$imagick->readImage($project_path.'cert/'.FILE_NAME); //$imagick can read pdf and image too
//$imagick->readImage(file_get_contents('https://keralapuraskaram.kerala.gov.in/award/esign/cert/testfile.pdf'));
//echo "ok";
//get file name only w/o ext.

$num_pages = $imagick->getNumberImages();
// Convert PDF pages to images
for ($i = 0; $i < $num_pages; $i++) {
    $imagick->setIteratorIndex($i);
    $imagick->setImageFormat('jpeg');
    $imagick->stripImage();
    $imagick->writeImage($tmp_path . $file_name_wo_ext . '-' . $i . '.jpg');
}
$imagick->destroy();
// set certificate file
$info = array();
// set document signature
$pdf->my_set_sign('', '', '', '', 2, $info); //custom function TCPDF  library tcpdf.php

//take created images from folder to set esign & create new pdf
for ($i = 0; $i < $num_pages; $i++) {
    $pdf->AddPage();
    $pdf->Image($tmp_path . $file_name_wo_ext . '-' . $i . '.jpg');
}

//start to add bg image for the 'esigned by' cell on document--- on 03-08-2018
// get the current page break margin
$bMargin = $pdf->getBreakMargin();
// get current auto-page-break mode
$auto_page_break = $pdf->getAutoPageBreak();
// disable auto-page-break
$pdf->SetAutoPageBreak(false, 0);
// set bacground image

//$pdf->SetAlpha(0.5);

// restore auto-page-break status
$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
// set the starting point for the page content
$pdf->setPageMark();
//end to add bg image on cell
// define active area for signature appearance
$pdf->setSignatureAppearance(140, 255, 60, 17); //X,Y,Width,Height

$pdf->SetFont('times', '', 8);
$pdf->setCellPaddings(1, 2, 1, 1);
$pdf->MultiCell(40, 10, 'Digitally Signed by: '  . "\n" . 'Date: ' . date('d-m-Y') . "\n" . date('h:i a'), 0, '', 0, 1, 157, 255, true);

$doc_path = $tmp_path . 'unsigned-' . $file_name_wo_ext . '.pdf';
//Close and output PDF document

$file = $pdf->my_output($doc_path, 'F'); //F-Force download, S-Source buffer returns binary, reffer my_output function from tcpdf.php file
//print_pdf($file_name_wo_ext.'.pdf',$file); die;
$pdf_byte_range = $pdf->pdf_byte_range;
$pdf->_destroy();

$file_hash = hash_file('sha256',$doc_path);
//after pdf done using images, delete that temp images from folder.
for ($i = 0; $i < $num_pages; $i++) {
    unlink($tmp_path . $file_name_wo_ext . '-' . $i . '.jpg'); //remove images after PDF generated/converted from temp folder
}
$doc = new DOMDocument();

//randome number gerator rand(1,9)
$txn = rand(111111111111,999999999999). '----' . $pdf_byte_range; //$pdf_byte_range signiture space location

$ts = date('Y-m-d\TH:i:s');

$doc_info = FILE_NAME;

$xmlstr = '<Esign AuthMode="1" aspId="' . ASPID . '" ekycId="" ekycIdType="A" responseSigType="pkcs7" responseUrl="' . RESPONSE_URL . '" sc="y" ts="' . $ts . '" txn="' . $txn . '" ver="2.1"><Docs><InputHash docInfo="' . $txn . '" hashAlgorithm="SHA256" id="1">' . $file_hash . '</InputHash></Docs></Esign>';

$doc->loadXML($xmlstr); //parser

// Create a new Security object 
$objDSig = new RobRichards\XMLSecLibs\XMLSecurityDSig();

// Use the c14n exclusive canonicalization
$objDSig->setCanonicalMethod(RobRichards\XMLSecLibs\XMLSecurityDSig::C14N);
// Sign using SHA-256
$objDSig->addReference(
        $doc,
        RobRichards\XMLSecLibs\XMLSecurityDSig::SHA1,
        array('http://www.w3.org/2000/09/xmldsig#enveloped-signature'),
        array('force_uri' => true)
);

// Create a new (private) Security key
$objKey = new RobRichards\XMLSecLibs\XMLSecurityKey(RobRichards\XMLSecLibs\XMLSecurityKey::RSA_SHA256, array('type' => 'private'));

//If key has a passphrase, set it using
$objKey->passphrase = '';

// Load the private key
$objKey->loadKey(PRIVATEKEY, TRUE);

 //print_r($objDSig);echo "<br>";
// print_r($objDSig->sign($objKey));


// Sign the XML file
$objDSig->sign($objKey);
// Append the signature to the XML
/* try {
    // Call the sign method
    $objDSig->appendSignature($doc->documentElement);
} catch (Exception $e) {
    // Handle the exception
    echo 'Error signing: ' . $e->getMessage();
} */
$objDSig->appendSignature($doc->documentElement);

$signXML = $doc->saveXML();
$signXML = str_replace('<?xml version="1.0"?>', '', $signXML);
ob_end_clean();



?>
<form action="<?php echo ESIGN_URL; ?>" method="post" id="formid">
    <input type="hidden" id="eSignRequest" name="eSignRequest" value='<?php echo $signXML; ?>'/>
    <input type="hidden" id="aspTxnID" name="aspTxnID" value="<?php echo $txn; ?>"/>
    <input type="hidden" id="Content-Type" name="Content-Type" value="application/xml"/>
</form>
<script>

    document.getElementById("formid").submit();
</script>



