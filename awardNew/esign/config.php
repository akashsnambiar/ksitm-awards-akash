<?php
require "library/XMLSecurityDSig.php";
require "library/XMLSecurityKey.php";
require 'TCPDF/tcpdf.php';

const FILE_NAME = 'testfile.pdf';
$project_path = $_SERVER["DOCUMENT_ROOT"].'awardNew/esign/';
//$project_path ='https://keralapuraskaram.kerala.gov.in/awardNew/esign/';
$tmp_path = $project_path.'temp/';

const ASPID = 'KSIM-002';
//const RESPONSE_URL = 'https://keralapuraskaram.kerala.gov.in/esign/resp.php';
const RESPONSE_URL = 'http://10.5.93.232/awardNew/esign/resp.php';

const PRIVATEKEY = 'cert/KISM-private.key';
const ESIGN_URL = 'https://esignservice.cdac.in/esign2.1/2.1/form/signdoc';

function print_pdf($name, $file) {
    header('Content-Type: application/pdf');
     header('Cache-Control: private, must-revalidate, post-check=0, pre-check=0, max-age=1');
    header('Pragma: public');
    header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header('Content-Disposition: inline; filename="' . basename($name) . '"');
    header('Content-Length: ' . strlen($file));
}

$file_name_array = explode('.', FILE_NAME);
$ext = end($file_name_array);
unset($file_name_array[count($file_name_array) - 1]);
$file_name_wo_ext = implode('.', $file_name_array);




