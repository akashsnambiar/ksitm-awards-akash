<?php
require 'config.php';
$unsigned_file_path = $tmp_path . 'unsigned-' . $file_name_wo_ext . '.pdf';
$signed_file_path = $project_path . 'signed-' . $file_name_wo_ext . '.pdf';

$xmldata = (array) simplexml_load_string(filter_input(INPUT_POST, 'eSignResponse')) or die("Failed to load");
if ($xmldata["@attributes"]["errCode"] != 'NA') {
    $msg = $xmldata ["@attributes"]["errMsg"];
    if(empty($msg)){
        //$msg ='eSign Request Canceled.[#'.$xmldata["@attributes"]["errCode"].']';
		//header("Location: https://keralapuraskaram.kerala.gov.in/index.php/nomination/esignCancel");
    header("Location: http://10.5.93.232/awardNew/index.php/nomination/esignCancel");
    }
    print($msg);
    exit();
}

$unsigned_file = file_get_contents($unsigned_file_path);

$txn = $xmldata ["@attributes"]["txn"];
$txn_array = explode('----', $txn);
$pdf_byte_range = $txn_array[1];

$pkcs7 = (array) $xmldata['Signatures'];
$pkcs7_value = $pkcs7['DocSignature'];
$cer_value = $xmldata['UserX509Certificate'];


$beginpem = "-----BEGIN CERTIFICATE-----\n";
$endpem = "-----END CERTIFICATE-----\n";
$pemdata = $beginpem . trim($cer_value) . "\n" . $endpem;

$cert_data = openssl_x509_parse($pemdata);


$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$file = $pdf->my_output($signed_file_path, 'F', $unsigned_file, $cer_value, $pkcs7_value, true, $pdf_byte_range);
$pdf->_destroy();
unlink($unsigned_file_path);

//db connection
/* $servername = "localhost";
$username = "root";
$password = "VAJ%5C22uL";
$dbname = "awards"; */
//
$servername = "localhost";
$username = "root";
$password = 'User@123';
$dbname = "awards";


// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
$name = $cert_data['subject']['CN'];
$country = $cert_data['subject']['C'];
$state = $cert_data['subject']['ST'];
$personal = $cert_data['subject']['O'];
$postal = $cert_data['subject']['postalCode'];
$pseudonym = $cert_data['subject']['pseudonym'];
$title = $cert_data['subject']['title'];
$dnQualifier = $cert_data['subject']['dnQualifier'];
$hash = $cert_data['hash'];
$issuerConuntry = $cert_data['issuer']['C'];
$issuerState = $cert_data['issuer']['ST'];
$issuerlocal = $cert_data['issuer']['L'];
$issuerorg = $cert_data['issuer']['O'];
$issuerOU = $cert_data['issuer']['OU'];
$issuerCN = $cert_data['issuer']['CN'];
$version = $cert_data['version'];
$serialNumber = $cert_data['serialNumber'];
$serialNumberHex = $cert_data['serialNumberHex'];
$validFrom = $cert_data['validFrom'];
$validTo = $cert_data['validTo'];
$validFrom_time_t = $cert_data['validFrom_time_t'];
$validTo_time_t = $cert_data['validTo_time_t'];
$signatureTypeSN = $cert_data['signatureTypeSN'];
$signatureTypeLN = $cert_data['signatureTypeLN'];
$signatureTypeNID = $cert_data['signatureTypeNID'];
$basicConstraints = $cert_data['extensions']['basicConstraints'];
$subjectKeyIdentifier = $cert_data['extensions']['subjectKeyIdentifier'];
$authorityKeyIdentifier = $cert_data['extensions']['authorityKeyIdentifier'];
$keyUsage = $cert_data['extensions']['keyUsage'];
$crlDistributionPoints = $cert_data['extensions']['crlDistributionPoints'];
$certificatePoliciesval = str_replace(',', ' ', $cert_data['extensions']['certificatePolicies']);
$certificatePolicies = str_replace("'", ' ', $certificatePoliciesval);
$authorityInfoAccess = $cert_data['extensions']['authorityInfoAccess'];
$sql = "INSERT INTO esign (name, country, state, personal, postal, pseudonym, title, dnQualifier, hash, issuerConuntry, issuerState, issuerlocal, issuerorg, issuerOU, issuerCN, version, serialNumber, serialNumberHex, validFrom, validTo, validFrom_time_t, validTo_time_t, signatureTypeSN, signatureTypeLN, signatureTypeNID, basicConstraints, subjectKeyIdentifier, authorityKeyIdentifier, keyUsage, crlDistributionPoints, certificatePolicies, authorityInfoAccess)
VALUES ('$name', '$country', '$state', '$personal', '$postal', '$pseudonym', '$title', '$dnQualifier', '$hash', '$issuerConuntry', '$issuerState', '$issuerlocal', '$issuerorg', '$issuerOU', '$issuerCN', '$version', '$serialNumber', '$serialNumberHex', '$validFrom', '$validTo', '$validFrom_time_t', '$validTo_time_t', '$signatureTypeSN', '$signatureTypeLN', '$signatureTypeNID', '$basicConstraints', '$subjectKeyIdentifier', '$authorityKeyIdentifier', '$keyUsage', '$crlDistributionPoints', '$certificatePolicies', '$authorityInfoAccess');";

if ($conn->multi_query($sql) === TRUE) {
  $last_id = $conn->insert_id;
  $id = base64_encode($last_id);
 //header("Location: https://keralapuraskaram.kerala.gov.in/index.php/nomination/esigned_check/".$id);
 header("Location: http://10.5.93.232/awardNew/index.php/nomination/esigned_check/".$id);
} else {
  echo "Error: " . $sql . "<br>" . $conn->error;
}

$conn->close();


?>