async function encodeUrl(data, key = "samplekey") {
    // Check for null or undefined data
    if (data === null || data === undefined) {
        throw new Error("Input data is null or undefined");
    }
    // Convert key to Uint8Array
    const keyData = new TextEncoder().encode(key);

    // Adjust key length if necessary
    let adjustedKey;
    if (keyData.length < 32) {
        // Pad the key with zero bytes or repeat it until it reaches 32 bytes
        adjustedKey = new Uint8Array(32);
        adjustedKey.set(keyData);
    } else if (keyData.length > 32) {
        // Truncate the key if it's longer than 32 bytes
        adjustedKey = keyData.slice(0, 32);
    } else {
        adjustedKey = keyData;
    }

    // Derive a cryptographic key from the adjustedKey
    const cryptoKey = await window.crypto.subtle.importKey(
        "raw",
        adjustedKey,
        { name: "AES-CBC" },
        false,
        ["encrypt"]
    );

    // Generate a random IV (Initialization Vector)
    const iv = window.crypto.getRandomValues(new Uint8Array(16));

    // Encode the data using AES-256 in CBC mode
    const encodedData = await window.crypto.subtle.encrypt(
        {
            name: "AES-CBC",
            iv: iv
        },
        cryptoKey,
        new TextEncoder().encode(data)
    );

    // Combine IV and encrypted data
    const combined = new Uint8Array(iv.length + new Uint8Array(encodedData).length);
    combined.set(iv);
    combined.set(new Uint8Array(encodedData), iv.length);

    // Convert to hexadecimal string
    const hexString = Array.from(combined)
        .map(byte => ('00' + byte.toString(16)).slice(-2))
        .join('');

    return hexString;
}
